# DELOY KAKUSEIDA.COM

Instructions for deploying Kakuseida

## Prerequisites

- NGINX ( Run NodeJS and PHP )
- NODE v8.14.0
- NPM 6.4.1
- PM2 3.1.3
- PHP 5.6.39
- Composer 1.7.2
- GIT

## Installing

### APP ReactJS
Please make sure you have started nginx and nodejs "Hello World!". With each service we use a subdomain

* APP:DOMAIN
* SOCKET:socket.DOMAIN
* API:api.DOMAIN
* MEDIA:static.DOMAIN

Install https to run firebase services

```
git clone https://gitlab.com/DungBuiDeveloper/kakuseida.com.git
cd kakuseida.com
cd app
```
Rename file src/common-base.js to src/common.js

```
npm install
pm2 start index.js
```

### Socket RealTime

```
cd kakuseida.com/socket
npm install
pm2 start server.js
```


### API CODEIGNITER

Install this project with Composer

```
$ cd api
$ composer install
```
file sql
```
api\kakuseida_com.sql
```
Rename File
```
Rename file application/config/config.php.default to application/config/config.php
Rename file application/config/database.php.default to application/database/config.php
```

### STATIC CODEIGNITER

Install this project with Composer

```
$ cd api
$ cd static
$ composer install
```

Rename File

```
Rename file application/config/config.php.default to application/config/config.php
Rename file application/config/database.php.default to application/database/config.php
```
