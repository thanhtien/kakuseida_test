<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends MY_Controller {

    // public $menu = 'dashboard';
    // public $page_title = 'Admin Dashboard';

    public function __construct() {
        parent::__construct();
        $this->_is_admin();
        $this->menu = 'dashboard';
        $this->page_title = 'Admin Dashboard';
        $this->load->helper(array('form', 'html', 'file', 'path', 'secure'));
        $this->load->library('form_validation');
        $this->lang->load('category_lang','japanese');
        $this->load->model('admin/project_model');
        $this->load->model('admin/category_model');
        $this->load->model('admin/backing_model');
        $this->load->model('admin/backed_model');
        $this->load->model('admin/projectreturn_model');
        $this->load->model('admin/users_model');
        $this->lang->load('project_lang','japanese');
    }
    /**
     * [index Show Project Exprire But Return Amount for Onwer]
     * @return [type] [description]
     */
    public function index() {
        if ($this->uri->segment(4) === FALSE) {
            $page = 0;
        } else {
            $page = $this->uri->segment(4);
        }
        $date = new DateTime();
        $date = $date->format('Y-m-d H:i:s');
        $parts = explode('-', $date);


        // $datePlusFive = date('Y-m-d H:i:s',mktime(0, 0, 0, $parts[1], $parts[2] + 5, $parts[0]));
        // Get Project
        $data['data'] = $this->project_model->getProjectNearExpres($page);
        // Foreach project
        foreach ($data['data']['project'] as $key => $value) {
        // Get $backing_level(Get now_count)
            $sum = $this->backing_model->abc($value->id);
            if (isset($sum->project_id) && isset($sum->now_count)) {
                if($value->id == $sum->project_id){
                    $value->now_count = $sum->now_count;
                }
            } else {
                $value->now_count = 0;
            }
            $value->color = 'black';
            if($value->collection_end_date <= $date){
                $value->color = 'red';
            }
        }
        $data['page_title'] = $this->config->item('site_name') . ' | ' . 'Pages';
        $this->_renderAdminLayout('admin/dashboard',$data);
    }

    /**
     * [edit Edit Confirm return amount for onwer]
     * @param  [type] $id [description]
     * @return [type]     [description]
     */
    public function edit($id) {
        // Get Date Curent
        $date = new DateTime();
        $date = $date->format('Y-m-d H:i:s');
        // For Pagination( GET URL element 5 )
        if ($this->uri->segment(5) === FALSE) {
            $page = 0;
        } else {
            $page = $this->uri->segment(5);
            if($page >= 1){
                $this->session->set_flashdata('tab', '2');
            }
        }
        // Check ID
        if(!isset($id) && $id){
            $this->session->set_flashdata('error', '更新が保存されました。');
            redirect('/dashboard');
        }
        // Get Project Folow Id
        $data['project'] = $this->project_model->getProjectById($id);
        // Check Project
        if(!isset($data['project'])){
            // Show Error and redirect Project
            $this->session->set_flashdata('error', '【プロジェクト】はすでに存在します。');
            redirect('/dashboard/');
        }
        //Load ckeditor
        $this->carabiner->js('ckeditor/ckeditor.js');
        $this->carabiner->js('js/ckeditor-loader.js');
        //Get Category
        $data['category'] = $this->category_model->getAllCategories();
        // Get Project folow Id
        if($data['project']->collection_end_date <= $date){
            $data['project']->show_btn = 'true';
        }
        $data['user_name'] = $this->users_model->getUserId($data['project']->user_id);
        // Check id is exists

        //Get Return (backed_level)
        $data['project_return'] = $this->projectreturn_model->getProjectReturnByProjectId($id);
        $data['backedProject'] = $this->backed_model->getBackedProjectId($id,$page);
        $data['idProject'] = $id;
        $data['goal_amount'] = $data['project']->goal_amount;
        $data['total'] = 0;
        $backedall = $this->backed_model->getBackedAll($id);

        foreach ($backedall as $key => $value) {
            $data['total'] = $data['total'] + $value->invest_amount;
        }
        foreach ($data['backedProject']['backed'] as $key => $value) {
            $user = $this->users_model->getUserId($value->user_id);
            $value->username = $user->username;
        }
        //If click save
        if ($this->input->post('save')) {
            //Use Model Function update with id
            $this->project_model->refunds_owner($id);
            // After update redirect project edit with id
            $this->session->set_flashdata('msg', '更新が保存されました。');
            redirect('/dashboard');
        } else {
            // load layout
            $this->_renderAdminLayout('admin/uploaddashboard', $data);
      }
    }
}
