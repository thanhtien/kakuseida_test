<?php

defined('BASEPATH') OR exit('No direct script access allowed');
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Page
 *
 * @author TNM Group
 */
class FPR_Project extends MY_Controller {

    // public $menu = 'category';
    // public $page_title = 'Sản phẩm';

    public function __construct() {
        parent::__construct();
        // $this->_is_admin();
        $this->menu = 'project';
        $this->page_title = 'プロジェクトのカテゴリー';
        $this->load->helper(array('form', 'html'));
        $this->load->library('form_validation');
        $this->lang->load('category_lang','japanese');
        $this->load->model('admin/favourite_model');
        $this->load->model('admin/pickup_model');
        $this->load->model('admin/recommends_model');
        $this->load->model('admin/project_model');
        $this->load->model('admin/expired_model');
        $this->load->model('admin/fanclub_model');

        $this->lang->load('project_lang','japanese');
    }
    // favourite_projects
    public function Findex() {
        // click save
        if($this->input->post('save')){
            // get data
            $kaku = $this->input->post('pickup');
            // json data
            $newF = json_encode($kaku);
            // array for update
            $data = array(
                'project_id'=>$newF
            );
            // check id = 1
            $this->db->where('id', 1);
            // update
            $update = $this->db->update('favourite_projects', $data);
            // check update
            if($update){
                // message
                $this->session->set_flashdata('msg', 'プロジェクト選択ができました。');
                //redirect
                redirect('/FPR_Project/Findex/');
            }
        }
        // for siderbar show
        $this->menu = 'F_Project';
        // time current
        $date = new DateTime();
        // format
        $date = $date->format('Y-m-d H:i:s');
        // get all favourite_model
        $data['data'] = $this->favourite_model->getAllFavourite();
        // get all project
        $data['project'] = $this->project_model->getAllProjectActive();
        // show data
        $data['show'] = json_decode($data['data']->project_id);
        // load view
        $this->_renderAdminLayout('admin/FPR/Findex', $data);
    }

    // same F project
    public function Pindex() {
        if($this->input->post('save')){
            $kaku = $this->input->post('pickup');
            $newF = json_encode($kaku);
            $data = array(
                'project_id'=>$newF
            );
            $this->db->where('id', 1);
            $update = $this->db->update('pickup_projects', $data);
            if($update){
                $this->session->set_flashdata('msg', 'プロジェクト選択ができました。');
                //redirect
                redirect('/FPR_Project/Pindex/');
            }
        }
        $this->menu = 'P_Project';
        $date = new DateTime();
        $date = $date->format('Y-m-d H:i:s');
        $data['data'] = $this->pickup_model->getAllPickup();
        $data['project'] = $this->project_model->getAllProjectActive();
        $fanClub = $this->project_model->getFanClubs();
        foreach ($fanClub as $key => $value) {
            array_push($data['project'] ,$value);
        }
        $data['show'] = json_decode($data['data']->project_id);
        $this->_renderAdminLayout('admin/FPR/Pindex', $data);

    }
    // same F project
    public function Rindex() {

        if($this->input->post('save')){
            $kaku = $this->input->post('pickup');
            $newF = json_encode($kaku);
            $data = array(
                'project_id'=>$newF
            );
            $this->db->where('id', 1);
            $update = $this->db->update('recommends_projects', $data);
            if($update){
                $this->session->set_flashdata('msg', 'プロジェクト選択ができました。');
                //redirect
                redirect('/FPR_Project/Rindex/');
            }
        }

        $this->menu = 'R_Project';
        $date = new DateTime();
        $date = $date->format('Y-m-d H:i:s');
        $data['data'] = $this->recommends_model->getAllRecommends();
        $data['project'] = $this->project_model->getAllProjectActive();
        $data['show'] = json_decode($data['data']->project_id);
        $this->_renderAdminLayout('admin/FPR/Rindex', $data);

    }
    // same F project
    public function Eindex() {
        if($this->input->post('save')){
            $kaku = $this->input->post('expired');
            $newF = json_encode($kaku);
            $data = array(
                'project_id'=>$newF
            );
            $this->db->where('id', 1);
            $update = $this->db->update('expired_projects', $data);
            if($update){
                $this->session->set_flashdata('msg', 'プロジェクト選択ができました。');
                //redirect
                redirect('/FPR_Project/Eindex/');
            }
        }

        $this->menu = 'E_Project';
        $date = new DateTime();
        $date = $date->format('Y-m-d H:i:s');
        $data['data'] = $this->expired_model->getAllExpired();
        $data['project'] = $this->project_model->getProjectExpired();
        $data['show'] = json_decode($data['data']->project_id);
        $this->_renderAdminLayout('admin/FPR/Eindex', $data);

    }


    // same F project
    public function FanClub() {
        if($this->input->post('save')){
            $kaku = $this->input->post('fanclub');
            $newF = json_encode($kaku);
            $data = array(
                'project_id'=>$newF
            );
            $this->db->where('id', 1);
            $update = $this->db->update('fanclub_projects', $data);
            if($update){
                $this->session->set_flashdata('msg', 'プロジェクト選択ができました。');
                //redirect
                redirect('/FPR_Project/FanClub/');
            }
        }

        $this->menu = 'FC_Project';
        $date = new DateTime();
        $date = $date->format('Y-m-d H:i:s');
        $data['data'] = $this->fanclub_model->getAllFanClub();

        $data['project'] = $this->project_model->getFanClubs();
        $data['show'] = json_decode($data['data']->project_id);
        $this->_renderAdminLayout('admin/FPR/FClub', $data);

    }
}
