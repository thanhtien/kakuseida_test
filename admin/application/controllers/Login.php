<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->helper(array('form', 'html', 'file', 'path', 'secure'));
        //$this->load->library('form_validation');
        //$this->load->model('pages_model');
    }

    /**
     * [index Check Login(If Logged ok else not ok)]
     * @return [type] [description]
     */
    public function index() {
        if ($this->session->userdata('is_admin')) {
            redirect(site_url('/'));
        }
        $data = [];
        // Has Email
        if ($this->input->post('email')) {
            // Create Remember Login
            $remember = $this->input->post('remember');
            if($remember == 'on'){
                $remember = TRUE;
            }else{
                $remember = FALSE;
            }

            $email = $this->input->post('email');
            $pass = $this->input->post('password');
            $this->load->library('form_validation');
            // Form Validation
            $this->form_validation->set_rules('password', 'Password', 'trim|required');
            $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');
            // Data Validation true
            if ($this->form_validation->run() !== false) {
                // Use Login of ion_auth
                if ($this->ion_auth->login($email, $pass,$remember)) {
                    // Role of account != Admin redirect Login
                    if (!$this->ion_auth->is_admin()) {
                        $this->session->set_flashdata('error', 'この画面は管理者しか閲覧できません');
                        redirect('login');
                            // Create Session and Get data of User
                  } else {
                      $user = $this->ion_auth->user()->row();
                      $this->session->set_userdata('is_admin', true);
                      $this->session->set_userdata('user_info', array(
                          'email' => $user->email,
                          'id' => $user->id,
                          'name' => $user->username));
                      redirect("/");
                  }
            // Login Fallied
              } else {
                  $this->session->set_flashdata('error', 'ユーザー名かパスワードが正しくない');
                  redirect('login');
              }
              // Data  of form_validation error
          } else {
              $this->session->set_flashdata('error', 'メールアドレスを正しく入力してください');
          }
      }
      // Load view
      $this->load->view('admin/login',$data);
    }
    /**
     * [signout Logout]
     * @return [type] [description]
     */
    public function signout() {
    // Delete Session
      $this->session->unset_userdata("is_admin");
      $this->session->unset_userdata("user_info");
      redirect("/");
    }

}
