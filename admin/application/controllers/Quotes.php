<?php

class Quotes extends CI_Controller
{

    function __construct()
    {
        parent::__construct();

    }


    // this function will redirect to book service page
    function index()
    {
        $this->subscribe();
    }

    // this function to load service book page
    function subscribe()
    {
        $this->load->view('oneSignal/site_firebase');
    }


    /**
     * Create New Notification
     *
     * Creates adjacency list based on item (id or slug) and shows leafs related only to current item
     *
     * @param int $user_id Current user id
     * @param string $title Current title
     *
     * @return string $response
     */
    function send_message(){
        $message = $this->input->post("message");
        $user_id = '4444';
		$url = $this->input->post("url");
		$headings = $this->input->post("headings");
		$img = $this->input->post("img");


        $content = array(
            "en" => "$message"
        );
		$headings = array(
            "en" => "$headings"
        );

        // $fields = array(
        //   'app_id' => "06f65992-cc02-483e-a9df-68d5c03c1dfa",
        //   'included_segments' => array(
        //     'All'
        //   ),
        //   'url' => $url,
        //   'contents' => $content,
        //   'chrome_web_icon' => $img,
    	// 		'headings' => $headings
        // );
        // $fields = array(
        //     "message" => array(
        //         "token" => "eP1fRfMbvKk:APA91bEeqCQ6ekXwsAME_0Ck2vUhwMWAtGvAr7STI6tTHB8X0i2qMd27afbb07lkkWvhVs7jCwOsohB5qfeKGX58zSTBkzmQH1HrVyf8TLiSALEM9mWOnPrlxqo-50JfgxwETgQ1OEuN",
        //         "notification" => array(
        //             "title"=> "FCM Message",
        //             "body"=> "This is a message from FCM"
        //         ),
        //         "webpush" => array(
        //             "headers" => array(
        //                 "Urgency"=> "high"
        //             ),
        //         ),
        //         "notification" => array(
        //             "body"=> "This is a message from FCM to web",
        //             "requireInteraction"=> "true",
        //             "badge"=> "/badge-icon.png"
        //         ),
        //     ),
        // );
        $fields = array(
            "data"=>array(
                "score"=> "5x1",
                "time"=> "15:10"
            ),
            "to" => "fJmXq2k7tV4:APA91bHXOdpWXXYPh9sQ0ryymkf_rjKhrBMkWVYTEKwCXl7Se1gfqXFbPbEDSiZ4QEkco6OIaKpOK5JvjSzrcj8hi5H-ZujXio5eGt1oUO02CSYcJMArhHaEbz30cuLttGrAk166KufF"
        );



        $fields = json_encode($fields);
        // print("\nJSON sent:\n");
        // print($fields);

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "https://fcm.googleapis.com/fcm/send");
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json; charset=utf-8',
            'Authorization: key=AAAA2w7EEDU:APA91bGYNIlkQSzfCVnZgyTAyfrsfawgaWZ6NDScDelBbzAf1nXGzZyKQS9UfzxOqetbKfTbTRwSaiA1znHgAPRtb_gj7x0kZSdrOYq03c6htbb8kDRl86YLxfASrMb18-6RiTGOtOKM'));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_HEADER, FALSE);
        curl_setopt($ch, CURLOPT_POST, TRUE);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);

        $response = curl_exec($ch);
        curl_close($ch);
        var_dump($response);
        exit;
       return $response;
    }

}
/* End of file quotes.php */
/* Location: ./application/controllers/Services.php */
