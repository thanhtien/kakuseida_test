<?php

defined('BASEPATH') OR exit('No direct script access allowed');
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Page
 *
 * @author TNM Group
 */
class Setting extends MY_Controller {

    public $menu = 'setting';

    public function __construct() {
        parent::__construct();
        $this->_is_admin();
        $this->menu = 'setting';
        $this->lang->load('category_lang','japanese');
        $this->load->model('admin/setting_model');
    }

    public function index() {
        $data['settings'] = $this->setting_model->getSettings();
        $this->_renderAdminLayout('admin/setting/index', $data);
    }

    public function edit() {
        $data['setting'] = $this->setting_model->getSettings();
        if($this->input->post('save')){
            $public_key = $this->input->post('public_key');
            $secret_key = $this->input->post('secret_key');
            $data = array(
                'public_key' => $public_key,
                'secret_key' => $secret_key
            );
            $update = $this->setting_model->updateSetting($data);
            if($update){
                $this->session->set_flashdata('msg', '更新ができました。');
                redirect('setting');
            }else{
                $this->session->set_flashdata('error', 'エラーがありましたので、更新ができません。');
                redirect('setting/edit');
            }
        }else{
            $this->_renderAdminLayout('admin/setting/edit', $data);
        }
    }

    public function add() {
        if($this->input->post('save')){
            $month = date("m", strtotime($this->input->post('exp_month')));
            $year = date("Y", strtotime($this->input->post('exp_month')));
            $month = $this->input->post('exp_month');
            $year = $this->input->post('exp_year');
            $key = $this->setting_model->getSettings();
            $cvc = $this->input->post('cvc');
            require_once APPPATH."third_party/stripe/init.php";

            \Stripe\Stripe::setApiKey($key->secret_key);
            try {
                if(isset($cvc) && $cvc){
                    $stripeToken = \Stripe\Token::create(array(
                        "card" => [
                            "number" => $this->input->post('number'),
                            "exp_month" => $month,
                            "exp_year" => $year,
                            "cvc" => $this->input->post('cvc')
                            ]
                    ));
                    $this->session->set_flashdata('msg', 'テストができました。');
                    redirect('setting');
                }else{
                    $this->session->set_flashdata('error', 'CVCを正しく入力してください');
                    redirect('setting');
                }
            } catch(Stripe_CardError $e) {
                $error1 = $e->getMessage();
                $this->session->set_flashdata('error', $error1);
                redirect('setting');
            } catch (Stripe_InvalidRequestError $e) {
                // Invalid parameters were supplied to Stripe's API
                $error2 = $e->getMessage();
                $this->session->set_flashdata('error', $error2);
                redirect('setting');
            } catch (Stripe_AuthenticationError $e) {
                // Authentication with Stripe's API failed
                $error3 = $e->getMessage();
                $this->session->set_flashdata('error', $error3);
                redirect('setting');
            } catch (Stripe_ApiConnectionError $e) {
                // Network communication with Stripe failed
                $error4 = $e->getMessage();
                $this->session->set_flashdata('error', $error4);
                redirect('setting');
            } catch (Stripe_Error $e) {
                // Display a very generic error to the user, and maybe send
                // yourself an email
                $error5 = $e->getMessage();
                $this->session->set_flashdata('error', $error5);
                redirect('setting');
            } catch (Exception $e) {
                // Something else happened, completely unrelated to Stripe
                $errrr = json_decode($e->getHttpBody());
                if($errrr->error->type === 'invalid_request_error'){
                    $this->session->set_flashdata('error', 'シークレットキーを正しく入力してください');
                }else if($errrr->error->code === "invalid_expiry_month"){
                    $this->session->set_flashdata('error', '有効期限の年を正しく入力してください');
                }else if($errrr->error->code === "incorrect_number"){
                    $this->session->set_flashdata('error', 'カード番号は正しくありません。');
                }else if($errrr->error->code === "invalid_expiry_year"){
                    $this->session->set_flashdata('error', '有効期限の年を正しく入力してください');
                }else if($errrr->error->code === "invalid_cvc"){
                    $this->session->set_flashdata('error', 'CVCを正しく入力してください');
                }else if($errrr->error->code === "expired_card"){
                    $this->session->set_flashdata('error', '有効期限の年を正しく入力してください');
                }else if($errrr->error->code === "incorrect_cvc"){
                    $this->session->set_flashdata('error', 'CVCを正しく入力してください');
                }
                redirect('setting');
            }
        }else{
            $this->_renderAdminLayout('admin/setting/add');
        }

    }

}
