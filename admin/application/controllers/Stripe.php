<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Stripe extends CI_Controller {

	public function index()
	{
    $this->load->library('form_validation');
		$this->load->view('product_form');
	}

	public function check()
	{

		//check whether stripe token is not empty
		if(!empty($_POST['stripeToken']))
		{
			//get token, card and user info from the form
			$token  = $_POST['stripeToken'];
			$name = $_POST['name'];
			$email = $_POST['email'];
			$card_num = $_POST['card_num'];
			$card_cvc = $_POST['cvc'];
			$card_exp_month = $_POST['exp_month'];
			$card_exp_year = $_POST['exp_year'];

			//include Stripe PHP library
			require_once APPPATH."third_party/stripe/init.php";

			//set api key
			$stripe = array(
			  "secret_key"      => "sk_test_wnYXmHrrqVFs90anXZTKj6FZ",
			  "publishable_key" => "pk_test_PAvjGUMSeIqIAyS7xp0rYHfA"
			);

			\Stripe\Stripe::setApiKey($stripe['secret_key']);

			//add customer to stripe
			$customer = \Stripe\Customer::create(array(
				'email' => $email,
				'source'  => $token
			));
			//item information
			$itemName = "Stripe Donation";
			$itemNumber = "PS12czx3456";
			$itemPrice = 3000;
			$currency = "usd";
			$orderID = "SKA92712382139";

			//charge a credit or a debit card
			$charge = \Stripe\Charge::create(array(
				'customer' => $customer->id,
				'amount'   => $itemPrice,
				'currency' => $currency,
				'description' => $itemNumber,
				'metadata' => array(
					'item_id' => $itemNumber
				)
			));

			//retrieve charge details
			$chargeJson = $charge->jsonSerialize();

			//check whether the charge is successful
			if($chargeJson['amount_refunded'] == 0 && empty($chargeJson['failure_code']) && $chargeJson['paid'] == 1 && $chargeJson['captured'] == 1)
			{
				//order details
				$amount = $chargeJson['amount'];
				$balance_transaction = $chargeJson['balance_transaction'];
				$currency = $chargeJson['currency'];
				$status = $chargeJson['status'];
				$date = date("Y-m-d H:i:s");

        $project_id = '9';
        $backing_level_id = '9';
        $comment = 'aaaa';
        $invest_amount = 30000;
        $user_id = '1';

				//insert tansaction data into the database
				$dataDB = array(
          'project_id' => $project_id,
          'user_id' => $user_id,
          'backing_level_id' => $backing_level_id,
          'invest_amount' => $invest_amount,
          'stripe_charge_id' => $charge->id,
          'type'=>'Gredit',
          'comment' => $comment,
          'status' => $status,
          'created' => $date,
          'manual_flag' => '0'
				);

				if ($this->db->insert('backed_projects', $dataDB)) {
					if($this->db->insert_id() && $status == 'succeeded'){
						$data['insertID'] = $this->db->insert_id();
						$this->load->view('payment_success', $data);
						// redirect('Welcome/payment_success','refresh');
					}else{
						echo "Transaction has been failed";
					}
				}
				else
				{
					echo "not inserted. Transaction has been failed";
				}

			}
			else
			{
				echo "Invalid Token";
				$statusMsg = "";
			}
		}
	}

	public function payment_success()
	{
		$this->load->view('payment_success');
	}

	public function payment_error()
	{
		$this->load->view('payment_error');
	}

	public function help()
	{
		$this->load->view('help');
	}
}
