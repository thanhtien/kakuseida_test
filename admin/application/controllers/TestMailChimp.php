<?php

defined('BASEPATH') OR exit('No direct script access allowed');

// require FCPATH . 'vendor/autoload.php';
//
include(__DIR__.'/MailChimp.php');
use \DrewM\MailChimp\MailChimp;
use \DrewM\MailChimp\Batch;


class TestMailChimp extends MY_Controller {

    // public $menu = 'category';
    // public $page_title = 'Sản phẩm';

    // __construct
    public function __construct() {
        parent::__construct();

        $this->menu = 'email_template';
        $this->page_title = 'プロジェクトのカテゴリー';
        $this->load->helper(array('form', 'html'));
        $this->load->model('admin/subscription_model');
        $this->load->model('admin/project_model');
        $this->load->model('admin/users_model');
        $this->load->model('admin/backed_model');
        $this->load->model('admin/template_model');
        $this->lang->load('category_lang','japanese');
    }
    /**
     *  use curl call api get email template
     */
    function index(){
        $MailChimp = new MailChimp('a8053b351c8c491f23164c182ac7d757-us20');

        $template = $this->template_model->getAllEmailSubscription();

        foreach ($template as $key => $value) {

            $campaign_id = $value->campaign_id;
            $messageTemplate = $value->message_template;
            $list_id = $value->list_id;
            var_dump($value);

            $arrayEmail = explode(",",$value->email_template);

            $listEmail = array();


            foreach ($arrayEmail as $key => $email) {

                if(count($listEmail) > 40){
                    break;
                } else {
                    array_push($listEmail,$email);
                }

            }

            $newArrayEmail = array_diff($arrayEmail,$listEmail);
            $listEmailString = implode(",", $newArrayEmail);
            $dataTemplate = array(
                'email_template' => $listEmailString
            );

            $update = $this->template_model->update($value->id,$dataTemplate);

            foreach ($listEmail as $key => $email) {
                $result = $MailChimp->post("lists/$list_id/members", [
                    'email_address' => $email,
                    'status'        => 'subscribed',
                ]);

                if ($MailChimp->success()) {
                } else {
                    $emailError = $value->email_error.''.$email.',';

                    $dataError = array(
                        'email_error' => $emailError
                    );
                    $update = $this->template_model->update($value->id,$dataError);
                }
            }

            if($value->email_template === ''){
                $result = $MailChimp->put("campaigns/".$campaign_id."/content", [
                    'html' => $messageTemplate
                ]);

                $dataUpdateTemplate = array(
                    'status_template' => '1'
                );
                $update = $this->template_model->update($value->id,$dataUpdateTemplate);

                $reLoadDataTemplate = $this->template_model->getTemplateById($value->id);
                if(isset($reLoadDataTemplate) && $reLoadDataTemplate->status_template === '1') {
                        $send = $MailChimp->post("campaigns/".$campaign_id."/actions/send", [
                            "is_ready" => true
                        ]);
                        if($send) {
                            $dataUpdateTemplate = array(
                                'status' => '1'
                            );
                            $update = $this->template_model->update($value->id,$dataUpdateTemplate);
                        }
                }

            }

        }
    }


}
