<?php

defined('BASEPATH') OR exit('No direct script access allowed');
/*
 * To change this license header, choose License Headers in Users Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Page
 *
 * @author TNM Group
 */
class Users extends MY_Controller {

    // public $menu = 'category';
    // public $page_title = 'Sản phẩm';

    public function __construct() {
        parent::__construct();
        $this->_is_admin();
        $this->menu = 'users';
        // $this->page_title = 'プロジェクトのカテゴリー';
        $this->load->helper(array('form', 'html'));
        $this->load->library('form_validation');
        $this->load->helper('email');
        $this->lang->load('category_lang','japanese');
        $this->lang->load('user_lang','japanese');
        $this->load->model('admin/users_model');
        $this->load->model('admin/project_model');
        $this->load->model('admin/backed_model');
        $this->load->model('admin/blacklist_model');
        $this->load->model('admin/subscription_model');

    }

    public function index($sorting="id",$by="DESC") {

        if ($this->uri->segment(6) === FALSE) {
            $page = 0;
        } else {
            $page = $this->uri->segment(6);
        }
        $data['data'] = $this->users_model->getUsers($sorting,$by,$page);
        $data['total'] = $data['data']['total'];
        $data['sort'] =$sorting;
        $data['by'] = $by;
        //var_dump($data);die();
        $this->_renderAdminLayout('admin/users/index', $data);
    }

    public function searchUsers($sorting="id",$by="DESC") {
        if ($this->uri->segment(6) === FALSE) {
            $page = 0;
        } else {
            $page = $this->uri->segment(6);
        }
        $search = $this->input->get('srch-term');
        $data['search'] = $search;
        $data['data'] = $this->users_model->searchUsers($sorting,$by,$search,$page);
        $data['total'] = $data['data']['total'];
        $data['sort'] =$sorting;
        $data['by'] = $by;
        $this->_renderAdminLayout('admin/users/index', $data);
    }
    public function testmail(){
        $email = array('hodacquyenpx@gmail.com','hodacquyenpx123123@gmail.com');
        $subject = '【KAKUSEIDA】本人確認のお知らせ';
        $message = 'tếtataetasteast';
        SendMail($email,$subject,$message);
    }
    public function add() {
        $date = new DateTime();
        $date = $date->format('Y-m-d H:i:s');
        if ($this->input->post('save')) {
            $this->load->library('form_validation');
            $this->form_validation->set_rules(
              'username', 'ユーザー名',
              'trim|required|is_unique[users.username]',
              array(
                'trim'=>"ユーザー名を入力してください",
                "required"=>"ユーザー名を入力してください",
                "is_unique"=>"このユーザー名が既にあります"
              )
            );
            $this->form_validation->set_rules(
              'email', 'メールアドレス',
              'trim|required|valid_email|is_unique[users.email]',
              array(
                'trim'=>"メールアドレスを入力してください",
                'required'=>"メールアドレスを入力してください",
                'valid_email'=>"メールアドレスが正しくないので、確認してください",
                'is_unique'=>"このメールアドレスが既にあります",
              )
            );
            $this->form_validation->set_rules(
              'password', 'パスワード',
              'trim|required|min_length[6]|max_length[32]',
              array(
                'trim'=>'パスワードを入力してください',
                'required'=>'パスワードを入力してください',
                'min_length'=>'パスワードは6術～32術まで入力してください',
                'max_length'=>'パスワードは6術～32術まで入力してください',
              )
            );
            $this->form_validation->set_rules(
              're_pass', 'パスワード確認',
              'trim|required|matches[password]',
              array(
                'trim'=>'パスワード確認を入力してください',
                'required'=>'パスワード確認を入力してください',
                'matches'=>'パスワード確認は以上のパスワードと合っていません。確認してください。',
              )
            );

            if ($this->form_validation->run() !== false) {
                $identity = $this->input->post('username');
                $email = $this->input->post('email');
                $password = $this->input->post('password');
                $time = time();
                $image = $this->input->post('hiden_image');
                if($image == ''){
                  $image = ''.image_url().'images/2018/default/noimage-01.png';
                }
                $active = 1;
                $checkSubscription = $this->subscription_model->getSubId($email);
                if(isset($checkSubscription) && $checkSubscription->registered === '1'){
                    $this->session->set_flashdata('error', 'このメールアドレスが既にあります');
                    redirect('/users/add');
                }
                $additional_data = array(
                  'profileImageURL'=>$image,
                  'created'=>$date,
                  'active' => $active,
                );
                $new_user =  $this->ion_auth->register($identity, $password, $email, $additional_data);
                if($new_user){
                    if(isset($checkSubscription) && $checkSubscription->registered === '0'){
                        $dataUser = array(
                            'eventProjectSocket' => 'all_user,user_'.$new_user
                        );
                        $this->users_model->updateUser($new_user, $dataUser);
                        $dataSubscription = array(
                            'token' => '',
                            'registered' => '1',
                            'created' => $date
                        );
                        $updateSubscription = $this->subscription_model->update($checkSubscription->id,$dataSubscription);
                    }
                    if(!isset($checkSubscription)){
                        $dataSubscription = array(
                            'email' => $email,
                            'token' => '',
                            'registered' => '1',
                            'created' => $date
                        );
                        $createSubscription = $this->subscription_model->create($dataSubscription);
                    }
                    $subject = '【KAKUSEIDA】本人確認のお知らせ';
                    $message = 'クラウドファンディングKAKUSEIDAをご利用いただき、誠にありがとうございます。<br /><br />
                    アカウント情報は以下のようです。<br />
                    ユーザー名：'.$email.'   <br />
                    パスワード：'.$password.'   <br /><br />
                    <a href="'.app_url().'/signin">こちら</a>URLをクリックして、会員登録を続けてください。<br />';
                  if(SendMail($email,$subject,$message)){
                      $this->session->set_flashdata('msg', '新しいユーザーの作成ができました。');
                      redirect('/users/');
                  }else{
                    $this->session->set_flashdata('error', 'エラーがありましたので、ユーザーの作成ができませんでした。再度作成してください。');
                    redirect('/users/add');
                  }
                }else{
                  $this->session->set_flashdata('error', 'エラーがありましたので、ユーザーの作成ができませんでした。再度作成してください。');
                  redirect('/users/add');
                }
            } else {
                $this->session->set_flashdata('error', strip_tags(validation_errors()));
                redirect('/users/add');
            }
        } else {
            $this->_renderAdminLayout('admin/users/add', null);
        }
    }

    public function edit($id) {

        $date = new DateTime();
        //fomat date
        $date = $date->format('Y-m-d H:i:s');

        $data['users'] = $this->users_model->getUserId($id);

        if(isset($data['users']) && $data['users']){

            $data['address_user'] = $this->users_model->getAddressUserByUserId($id);

            $emailOnwer = $data['users']->email;
            $usernameOnwer = $data['users']->username;
            $data['pid'] = $this->input->post('pid'); // $id;

            if ($this->input->post('save')) {

                $date = new DateTime();
                //fomat date
                $date = $date->format('Y-m-d H:i:s');
                $image = $this->input->post('hiden_image');

                $dataUpdateUser = array (
                     'active' => $this->input->post('active'),
                     'profileImageURL' => $this->input->post('hiden_image'),
                     'modified' => $date,
                );

                if($this->input->post('active') === '0'){

                    $dataUpdateUser['blacklist'] = '1';

                }
                if($this->input->post('change_pass') === '1'){
                    $password = $this->input->post('password');
                    $dataUpdateUser['password'] = $password;
                }

                $edit = $this->ion_auth->update($id, $dataUpdateUser);
                if($edit){

                    $project = $this->project_model->getProjectByUserId($id);
                    $dataProject = array(
                        'active'=>'blk',
                        'opened'=>'yes',
                        'banner'=>'1',
                    );

                    $dataUnBanner = array(
                        'active'=>'yes',
                        'opened'=>'yes',
                        'banner'=>'0',
                    );

                    $banner = [];
                    $unbanner = [];
                    $listSendMail = [];

                    if($this->input->post('active') === '0' && isset($project)) {

                        $listEmail = [];

                        foreach ($project as $key => $value) {
                            if($value->active === 'yes'){
                                array_push($banner,$value);
                            }
                        }

                        foreach ($banner as $key => $value) {
                            $this->project_model->uploadUserNoActive($value->id,$dataProject);
                            $list = $this->backed_model->getBackedAll($value->id);
                            if(isset($list)){
                                foreach ($list as $key => $value) {
                                    array_push($listSendMail,$value);
                                }
                            }
                        }

                        foreach ($listSendMail as $key => $value) {

                            $user = $this->users_model->getUserId($value->user_id);

                            $project = $this->project_model->getProjectById($value->project_id);
                            $owner = $this->users_model->getUserId($project->user_id);
                            array_push($listEmail,$user->email);
                        }

                        $dataBlackList = array(
                            'email'         =>   $data['users']->email,
                            'last4'         =>   $data['users']->last4,
                            'hash_token'    =>   $data['users']->hash_token,
                            'status'        =>   1,
                            'created'       =>   $date
                        );

                        $checkBlackList =  $this->blacklist_model->checkBackList($data['users']->email);

                        if(isset($checkBlackList) && $checkBlackList) {

                            $update = $this->blacklist_model->editBlackList($data['users']->email,$dataBlackList);

                        }else {

                            $create = $this->blacklist_model->createBlackList($dataBlackList);

                        }
                        $this->session->set_flashdata('msg', 'ブロックできました。');
                    }else if($this->input->post('active') === '1' && isset($project)){

                        foreach ($project as $key => $value) {
                            if($value->active === 'blk' && $value->banner === '1'){
                                array_push($unbanner,$value);
                            }
                        }
                        foreach ($unbanner as $key => $value) {
                            $this->project_model->uploadUserNoActive($value->id,$dataUnBanner);
                        }
                        $dataUpdateBlackList = array(
                            'status'        =>   '0'
                        );

                        $checkBlackList =  $this->blacklist_model->checkBackList($data['users']->email);

                        if(isset($checkBlackList) && $checkBlackList) {

                            $update = $this->blacklist_model->editBlackList($data['users']->email,$dataUpdateBlackList);

                        }
                        $this->session->set_flashdata('msg', '公開できました。');
                    }
                    redirect('/users');
                }
            } else {
                $this->_renderAdminLayout('admin/users/edit', $data);
            }
        }else{
            $this->session->set_flashdata('error', 'ユーザー管理に失敗しました');
            redirect('/users/');
        }
    }

    public function blockUser(){
      $id = $this->input->post('id');
      $role = $this->ion_auth->get_users_groups($id)->row()->name;
      $data['users'] = $this->users_model->getUserId($id);
      $emailOnwer = $data['users']->email;
      $usernameOnwer = $data['users']->username;
      $username = $data['users']->username;
      $email = $data['users']->email;
      $created = $data['users']->created;
      $date = new DateTime();
      //fomat date
      $date = $date->format('Y-m-d H:i:s');
      $image = $this->input->post('hiden_image');

      $dataUpdate = array(
        'active' => $this->input->post('active'),
      );
      if($this->input->post('active') === '0'){
        $dataUpdate['blacklist'] = '1';
      }
      $edit = $this->ion_auth->update($id, $dataUpdate);
      if($edit){

        $dataBlackList = array(
            'email'         =>   $data['users']->email,
            'last4'         =>   $data['users']->last4,
            'hash_token'    =>   $data['users']->hash_token,
            'status'        =>   1,
            'created'       =>   $date
        );

        $checkBlackList =  $this->blacklist_model->checkBackList($data['users']->email);

        if(isset($checkBlackList) && $checkBlackList) {

            $update = $this->blacklist_model->editBlackList($data['users']->email,$dataBlackList);

        }else {

            $create = $this->blacklist_model->createBlackList($dataBlackList);

        }

        $project = $this->project_model->getProjectByUserId($id);
        $data = array(
          'active'=>'blk',
          'opened'=>'yes',
          'banner'=>'1',
        );
        $dataUnBanner = array(
          'active'=>'yes',
          'opened'=>'yes',
          'banner'=>'0',
        );
        $banner = [];
        $unbanner = [];
        $listSendMail = [];
        if($this->input->post('active') === '0' && isset($project)){
          foreach ($project as $key => $value) {
            if($value->active === 'yes'){
              array_push($banner,$value);
            }
          }
          foreach ($banner as $key => $value) {
            $this->project_model->uploadUserNoActive($value->id,$data);
            $list = $this->backed_model->getBackedAll($value->id);
            if(isset($list) && $list){
              foreach ($list as $key => $value) {
                array_push($listSendMail,$value);
              }
            }
          }
          $listEmail = [];
          if(isset($listSendMail) && $listSendMail){
            foreach ($listSendMail as $key => $value) {
              $user = $this->users_model->getUserId($value->user_id);
              array_push($listEmail,$user->email);
            }
              $project = $this->project_model->getProjectById($value->project_id);
              $owner = $this->users_model->getUserId($project->user_id);
          }
        }
        $myObj['user_id']  = $id;
        $myObj['username'] = $username;
        $myObj['email']    = $email;
        $myObj['role']     = $role;
        $myObj['created']     = $created;
        $myObj['active']   = $this->input->post('active');
        echo json_encode($myObj);
      }

    }

    public function _save() {
        $data = $this->input->post();
        $id = (int) $data["pid"];
        //If have page id parameter then update page, else add new page
        if ($id > 0) {
            $this->users_model->update();
            $this->session->set_flashdata('msg', 'プロジェクトのカテゴリーが更新できました。');
            redirect('/users/edit/' . $id);
        }
    }

    public function delete($id) {
        if ((int) $id > 0) {
            $this->users_model->delete($id);
        }
        $this->session->set_flashdata('msg', 'ユーザが削除されました。');
        redirect('/users/');
    }

    public function action() {
      $val = $this->input->post('val');
      // Get Action
      $action = $this->input->post('hidAction');
      // Get id
      $in = implode(',', $val);
      // Use where check project with id
      $this->db->where("id in ($in)");
      // Delete talbe project with id
      $this->db->delete('users');
      // Assign flashdata
      $this->session->set_flashdata('msg', 'ユーザが削除されました。');
      redirect('/users');
    }
}
