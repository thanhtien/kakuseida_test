<?php
  // thong tin category
  $lang['category'] = 'カテゴリー';
  $lang['category_name'] = 'カテゴリー名';
  $lang['seo_slug'] ='SEOスラッグ（アルファベットとハイフンだけを入力してください。）';
  $lang['order'] = '注文';

  //them sua xoa
  $lang['create'] = '新規作成';
  $lang['delete'] = '削除';
  $lang['are_you_sure'] = 'これを削除したいですか。';
  $lang['yes'] = 'はい';
  $lang['no'] = 'いいえ!';
  $lang['save'] = '保存';
  $lang['cancel'] = 'キャンセル';

  // crud
  $lang['create_category'] = 'カテゴリー・新規作成';
  $lang['edit_category'] = 'カテゴリー・編集';
    
  // Message
  $lang['create_category_success'] = 'カテゴリ作成ができました。';
  $lang['create_category_failure'] ='【カテゴリー名】はすでに存在します。';
  $lang['delete_category_success'] = 'カテゴリーが削除されました。';
  $lang['edit_category_success'] = 'カテゴリーが更新できました。';
  $lang['success'] = '成功 !';
  $lang['error'] = 'エラー！';

?>
