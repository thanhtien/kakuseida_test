<?php
  // thong tin category
  $lang['user'] = 'ユーザー管理';
  $lang['username'] = 'ユーザー名';
  $lang['role'] ='役割';
  $lang['active'] = '公開';
  $lang['create_user'] = 'ユーザー詳細画面';
  $lang['email'] = 'メールアドレス';

  $lang['active_user'] = '公開';
  $lang['unactive_user'] = '非公開';
  $lang['password'] = 'パスワード';
  $lang['re_password'] = 'パスワード確認';
  $lang['change_pass'] = 'パスワードを変更する';
  $lang['edit_user'] = 'ユーザーの情報を編集する';
?>
