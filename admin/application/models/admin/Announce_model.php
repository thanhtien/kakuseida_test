<?php

/*
 * To change this license header, choose License Headers in Category Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Categorys_model
 *
 * @author TNM Group
 */
class Announce_model extends CI_Model {
    private $table = 'announce';

    public function __construct() {
        parent::__construct();
    }
    // Get all category
    public function getAllCategories($order_by = 'id', $order_direction = 'ASC') {
        $this->db->order_by($order_by,$order_direction);
        $query = $this->db->get($this->table);
        return $query->result();
    }
    //get category with pagination
    public function getAnnounce($sorting,$by,$page = 0) {
        $total = $this->db->count_all_results($this->table);
        $limit = $this->config->item('admin_per_page');
        $start = ($page <= 1) ?  0 : ($page - 1)  * $limit;
        $this->db->limit($limit, $start);
        $this->db->order_by('id','DESC');
        $this->db->from($this->table .' c1');
        $query = $this->db->get();
        $announces = $query->result();
        // debug_sql();
        return ["total" => $total, "announce" => $announces];
    }

    function searchCategories($sorting,$by,$search,$page = 0){
      $this->db->select('id');
      $this->db->from($this->table);
      $this->db->like('name', $search);
      $total = $this->db->count_all_results();

      $limit = $this->config->item('admin_per_page');
      $start = ($page <= 1) ?  0 : ($page - 1)  * $limit;
      $this->db->select('*');
      $this->db->from($this->table);
      $this->db->like('name', $search);
      $this->db->limit($limit, $start);
      $data = $this->db->get();

      $categories = $data->result();

      return ["total" => $total, "categories" => $categories];
    }

    //Get category with id
    public function getCategoryById($id = 0) {
        if ((int) $id > 0) {
            $query = $this->db->get_where($this->table, array('id' => $id));
            return $query->row();
        } else {
            return NULL;
        }
    }

    // // get category by slug
    // public function getCategoryBySlug($slug = '') {
    //     if (!empty($slug)) {
    //         $query = $this->db->get_where($this->table, array('slug' => $slug));
    //         return $query->row();
    //     } else {
    //         return NULL;
    //     }
    // }
    // Add Category
    public function insert() {

        $datetime = new DateTime();
        // format date
        $datetime = $datetime->format('Y-m-d H:i:s');
        // change time current -> time UTC
        $asia_timestamp = strtotime($datetime);
        date_default_timezone_set('UTC');
        $date = date("Y-m-d H:i:s", $asia_timestamp);

        $title = $this->input->post('title');
        $message = $this->input->post('message');

        if($this->input->post('save')) {
            $status = '0';
        }else{
            $status = '1';
        }

        $data = array(
            'title' => $title,
            'message' => $message,
            'status' => $status,
            'created' => $date,
            'updated' => $date
        );
        $this->db->insert($this->table, $data);
        return $this->db->insert_id();
    }
    // Update Category
    public function update() {
        $id = $this->input->post('pid');
        $datetime = new DateTime();
        // format date
        $datetime = $datetime->format('Y-m-d H:i:s');
        // change time current -> time UTC
        $asia_timestamp = strtotime($datetime);
        date_default_timezone_set('UTC');
        $date = date("Y-m-d H:i:s", $asia_timestamp);

        $title = $this->input->post('title');
        $message = $this->input->post('message');
        $status = $this->input->post('status');

        if($status === '0') {

            if($this->input->post('save')) {
                $status = '0';
            }else{
                $status = '1';
            }

        } else {
            if($this->input->post('public')) {
                $status = '0';
            }
        }


        $data = array(
            'title' => $title,
            'message' => $message,
            'status' => $status,
            'updated' => $date
        );
        $this->db->update($this->table, $data, array('id' => $id));
        return $id;
    }
    // Delete Category with id
    public function delete($id) {
        $this->db->delete($this->table, array('id' => $id));
    }

    public function getProjectByCategoryId($id = 0) {
        //Check Id is exists
        if ((int) $id > 0) {
            //Use get where Check Id in database
            $query = $this->db->get_where('projects', array('category_id' => $id));
            //Return Data
            return $query->result();
        } else {
            // Return Null
            return NULL;
        }
    }

    public function getProjectByCategoryIds($id) {
        //Check Id is exists
        if ($id) {
            //Use get where Check Id in database
            $this->db->from('projects');
            $this->db->where("category_id in ($id)");
            //Return Data
            $query = $this->db->get();
            return $query->result();
        } else {
            // Return Null
            return NULL;
        }
    }
}
