<?php if(!defined('BASEPATH')) exit('No direct script allowed');

class Listdonatefanclub_model extends CI_Model{
	var $table = 'listuserdonate_fanclub';

    public function getAllDonateFanClub($id){
		$this->db->select('users.email');
        $this->db->from($this->table);
        $this->db->where('project_id',$id);
        $this->db->join('users','users.id = listuserdonate_fanclub.user_id');
		// $this->db->where()
        $query = $this->db->get();
        $query = $query->result();
        return $query;
    }

	public function getInfoDonateFanClub($id){
		$this->db->from($this->table);
		$this->db->where('project_id',$id);
		// $this->db->where()
		$query = $this->db->get();
		$query = $query->result();
		return $query;
	}
}
