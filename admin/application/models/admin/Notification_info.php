<?php

/*
 * To change this license header, choose License Headers in New Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of News_model
 *
 * @author TNM Group
 */
class Notification_info extends CI_Model {
    private $table = 'notification_info';

    public function __construct() {
        parent::__construct();
    }

    public function insert($data) {
        if(isset($data) && $data){
            $insert = $this->db->insert($this->table, $data);
            if($insert){
                return $this->db->insert_id();
            }else{
                return false;
            }
        }else{
            return false;
        }
    }

}
