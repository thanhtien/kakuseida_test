<?php

/*
 * To change this license header, choose License Headers in Page Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Pages_model
 *
 * @author TNM Group
 */
class Page_model extends CI_Model {

    private $table = 'pages';

    public function __construct() {
        parent::__construct();
    }

    public function getAllPages() {
        $this->db->order_by('id', 'DESC');
        $query = $this->db->get($this->table);
        return $query->result();
    }

    public function getPages($page = 0) {
        $total = $this->db->count_all_results($this->table);
        $limit = $this->config->item('admin_per_page');
        $start = ($page <= 1) ? 0 : ($page - 1) * $limit;
        $this->db->limit($limit, $start);
        $this->db->order_by('id', 'DESC');
        $this->db->from($this->table . ' p');
        $query = $this->db->get();
        $pages = $query->result();
        // debug_sql();
        return ["total" => $total, "pages" => $pages];
    }

    public function getPageById($id = 0) {
        if ((int) $id > 0) {
            $this->db->select('p.*');
            $this->db->where('p.id', $id);
            $this->db->from($this->table . ' p');
            $query = $this->db->get();
            $page = $query->row();
            if (!$page) {
                show_404();
            }
            return $page;
        } else {
            return NULL;
        }
    }

    public function getPageBySlug($slug) {
        $this->db->select('p.*');
        $this->db->where('p.slug', $slug);
        $this->db->from($this->table . ' p');
        $query = $this->db->get();
        $page = $query->row();
        if (!$page) {
            show_404();
        }
        return $page;
    }

    public function insert() {
        // custom_debug($_POST); die();
        $slug = (!empty($this->input->post('slug'))) ? $this->input->post('slug') : create_slug($this->input->post('title'));
        $title = $this->input->post('title');
        $content = $this->input->post('content');
        $now = date('Y-m-d H:i:s');

        $uniq_slug = createUniqueSlug($slug, 'pages', 'slug');

        $data = array(
            'title' => $title,
            'slug' => $uniq_slug,
            'content' => $content,
            'created_date' => $now
        );
        if ($this->input->post('is_active') == 'Yes') {
            $data['is_active'] = 'Yes';
        } else {
            $data['is_active'] = 'No';
        }
        if (!$this->db->insert($this->table, $data)) {
            return false;
        }
        $page_id = $this->db->insert_id();
        return $page_id;
    }

    public function update() {
        $id = $this->input->post('pid');
        $slug = (!empty($this->input->post('slug'))) ? $this->input->post('slug') : create_slug($this->input->post('title'));
        $title = $this->input->post('title');
        $content = $this->input->post('content');
        $now = date('Y-m-d H:i:s');

        $uniq_slug = createUniqueSlug($slug, 'pages', 'slug', $id);

        $data = array(
            'title' => $title,
            'slug' => $uniq_slug,
            'content' => $content,
            'updated_date' => $now
        );
        if ($this->input->post('is_active') == 'Yes') {
            $data['is_active'] = 'Yes';
        } else {
            $data['is_active'] = 'No';
        }
        return $this->db->update($this->table, $data, array('id' => $id));
    }

    public function delete($id) {
        $this->db->update($this->table, ['is_active' => 'No'], array('id' => $id));
    }

}
