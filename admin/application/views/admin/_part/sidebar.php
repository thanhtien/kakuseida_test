<!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
            <img src="<?php echo site_url('assets/adminlte/') ?>/dist/img/avatar5.png" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p>Admin</p>
          <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
        </div>
      </div>
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu">
        <li class="<?php echo active_class($menu, ['dashboard']) ?>">
          <a href="/">
            <i class="fa fa-dashboard"></i> <span>ダッシュボード</span>
          </a>

        </li>

        <li class="<?php echo active_class($menu, ['category']) ?>">
          <a href="/category">
            <i class="fa fa-industry"></i> <span>プロジェクトのカテゴリー</span>
          </a>
        </li>
        <li id="submenu" class="<?php echo active_class($menu, ['project','F_Project','R_Project','P_Project','E_Project','FC_Project']) ?>">
          <a href="/project">
            <i class="fa fa-th"></i> <span>プロジェクト</span>
            <span id="menu_sub">

            </span>
          </a>
          <ul class="treeview-menu">
            <li class="<?php echo active_class($menu, ['project']) ?>"><a href="/project"><i class="fa fa-circle-o"></i> 全てのプロジェクト一覧</a></li>
            <li class="<?php echo active_class($menu, ['F_Project']) ?>"><a href="/FPR_Project/Findex"><i class="fa fa-circle-o"></i> 人気のプロジェクト</a></li>
            <li class="<?php echo active_class($menu, ['R_Project']) ?>"><a href="/FPR_Project/Rindex"><i class="fa fa-circle-o"></i> オススメのプロジェクト</a></li>
            <li class="<?php echo active_class($menu, ['P_Project']) ?>"><a href="/FPR_Project/Pindex"><i class="fa fa-circle-o"></i> 注目のプロジェクト</a></li>
            <li class="<?php echo active_class($menu, ['E_Project']) ?>"><a href="/FPR_Project/Eindex"><i class="fa fa-circle-o"></i> 過去のプロジェクト </a></li>
            <li class="<?php echo active_class($menu, ['FC_Project']) ?>"><a href="/FPR_Project/FanClub"><i class="fa fa-circle-o"></i> 定期課金のプロジェクト </a></li>
          </ul>
        </li>

        <li class="<?php echo active_class($menu, ['news']) ?>">
          <a href="/news">
            <i class="fa fa-file"></i> <span>ニュース</span>
          </a>
        </li>
        <li class="<?php echo active_class($menu, ['users']) ?>">
          <a href="/users">
            <i class="fa fa-users"></i> <span>ユーザー管理</span>
          </a>
        </li>
        <li class="<?php echo active_class($menu, ['email_template']) ?>">
          <a href="/EmailTemplate">
            <i class="fa fa-envelope-square"></i> <span>メールテンプレート</span>
          </a>
        </li>
        <li class="<?php echo active_class($menu, ['announce']) ?>">
          <a href="/announce">
            <i class="fa fa-bullhorn"></i> <span>管理者からの通知一覧</span>
          </a>
        </li>
        <li class="<?php echo active_class($menu, ['setting']) ?>"><a href="/setting"><i class="fa fa-cog"></i> STRIPEカード設定</a></li>
        <li class="<?php echo active_class($menu, ['mailtemplate']) ?>"><a target="_blank" href="https://mail.kakuseida.com/"><i class="fa fa-envelope"></i>WEBメールシステム</a></li>
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>
