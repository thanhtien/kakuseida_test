<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            管理者からの通知一覧
        </h1>
        <ol class="breadcrumb">
            <li><a href="/"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active"><a href="<?php echo site_url('/announce')?>">管理者からの通知一覧</a></li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <?php if ($this->session->flashdata('msg')) { ?>
            <div class="alert alert-success" id="success-alert">
                <button type="button" class="close" data-dismiss="alert"><i class="fa fa-times"></i></button>
                <strong><?php echo $this->lang->line('success'); ?></strong>
                <?php echo $this->session->flashdata('msg'); ?>
            </div>
        <?php } ?>
        <?php if ($this->session->flashdata('error')) { ?>
          <div class="alert alert-danger" id="success-alert">
              <button type="button" class="close" data-dismiss="alert"><i class="fa fa-times"></i></button>
              <strong><?php echo $this->lang->line('error'); ?></strong>
              <?php echo $this->session->flashdata('error'); ?>
          </div>
        <?php } ?>
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <form id="frmMain" method="POST" action="/announce/action">
                        <div class="box-header">
                            <h3 class="box-title">&nbsp;</h3>

                            <div class="box-tools">
                                <div class="btn-group pull-right">
                                    <a class="btn btn-sm btn-primary " href="<?php echo site_url('/announce/add') ?>"><i class="fa fa-plus"></i> 新規作成</a>
                                    <a id="bulk-delete" class="btn btn-sm btn-danger " data-title="選択されたものを削除しますか。" data-btn-ok-label="<?php echo $this->lang->line('yes'); ?>"  data-btn-cancel-label="<?php echo $this->lang->line('no'); ?>"  data-toggle="confirmation" data-placement="left" data-singleton="true"><i class="fa fa-trash-o"></i> 削除</a>
                                    <input type="hidden" id="hidAction" name="hidAction" value="" />
                                </div>
                            </div>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body no-padding">
                            <table class="table table-bordered table-hover" id="tableAnnounce">
                                <tr>
                                    <th style="width: 20px"><input type="checkbox"  class="minimal checkth"  ></th>
                                    <th style="width:270px;"><a href="#">タイトル</a></th>
                                    <th style="width:700px;"><a href="#">本文</a></th>
                                    <th style="width:120px;"><a href="#">ステータス</a></th>
                                    <!-- <th><?php// echo $this->lang->line('order'); ?></th> -->
                                    <th style="width: 80px"></th>
                                </tr>
                                <?php if ($data['total'] > 0) { ?>
                                    <?php foreach ($data['announce'] as $key => $value) { ?>
                                        <tr class="trTableAnnounce" style="cursor: pointer;" id="<?php echo $value->id; ?>">
                                            <td><input type="checkbox"  class="minimal checkitem" name="val[]" value="<?php echo $value->id ?>" ></td>
                                            <td>
                                              <a href="/announce/edit/<?php echo $value->id; ?>" class="title_shorten" style="color:black" ><?php echo $value->title ?></a>
                                            </td>
                                            <td>
                                              <a href="/announce/edit/<?php echo $value->id; ?>" class="comment_return" style="color:black" ><?php echo strip_tags($value->message); ?></a>
                                            </td>
                                            <td>
                                              <a href="/announce/edit/<?php echo $value->id; ?>" style="color:black" >
                                                    <?php if($value->status === '1') {
                                                            echo "公開";
                                                        } else {
                                                            echo "非公開";
                                                        }
                                                    ?>
                                              </a>
                                            </td>
                                            <td style="text-align:center">
                                                <a href="/announce/edit/<?php echo $value->id; ?>" class="btn btn-xs btn-primary"><i class="fa fa-edit"></i></a>
                                                <a href="/announce/delete/<?php echo $value->id; ?>" class="btn btn-xs btn-danger" data-title="このカテゴリを削除しますか" data-btn-ok-label="<?php echo $this->lang->line('yes'); ?>"  data-btn-cancel-label="<?php echo $this->lang->line('no'); ?>" data-toggle="confirmation2" data-placement="left" data-singleton="true"><i class="fa fa-trash-o"></i></a>
                                            </td>
                                        </tr>
                                    <?php } ?>
                                <?php } else { ?>
                                        <tr>
                                            <td colspan="5" class="text-center"><?php echo $this->config->item('no_data')?></td>
                                        </tr>
                                <?php }?>
                            </table>

                        </div>
                        <div class="box-footer clearfix">
                          <?php
                            if(isset($search)){
                              echo custom_paginationGET('/announce/searchCategories/'.$sort.'/'.$by.'/', $search, $data['total'],10);
                            }else{
                              echo custom_pagination('/announce/index/'.$sort.'/'.$by.'/', $data['total'],10);
                            }
                          ?>
                        </div>
                    </form>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->

            </div>
        </div>
    </section>
</section>
<!-- /.content -->
</div>
<!-- /.content-wrapper -->
