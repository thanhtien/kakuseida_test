<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            <?php echo $this->lang->line('category'); ?>
        </h1>
        <ol class="breadcrumb">
            <li><a href="/"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active"><a href="<?php echo site_url('/category')?>"><?php echo $this->lang->line('category'); ?></a></li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <?php if ($this->session->flashdata('error')) { ?>
          <div class="alert alert-danger" id="success-alert">
              <button type="button" class="close" data-dismiss="alert"><i class="fa fa-times"></i></button>
              <strong><?php echo $this->lang->line('error'); ?></strong>
              <?php echo $this->session->flashdata('error'); ?>
          </div>
        <?php } ?>
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title"><?php echo $this->lang->line('create_category'); ?></h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form role="form" action="" method="POST" enctype="multipart/form-data">
                <div class="nav-tabs-custom">
                    <div class="tab-content">
                        <div class="tab-pane active" id="tab_1">
                            <div class="box-body">
                                <div class="form-group">
                                    <label for="name"><?php echo $this->lang->line('category_name'); ?></label>
                                    <input type="text" class="form-control" required="" id="name" name="name" placeholder="" maxlength="100">
                                </div>
                                <div class="form-group">
                                    <label for="name"><?php echo $this->lang->line('seo_slug'); ?></label>
                                    <input type="text" class="form-control" required="" id="slug" name="slug" placeholder="" maxlength="100">
                                </div>

                            </div>
                            <!-- /.box-body -->
                        </div>
                    </div>
                </div>


                <div class="box-footer">
                    <button type="submit" name="save" class="btn btn-primary" value="1"><?php echo $this->lang->line('save'); ?></button>
                    <a href="<?php echo site_url('/category/') ?>" class="btn btn-default"><?php echo $this->lang->line('cancel'); ?></a>
                    <input type="hidden" name="pid" value="0" />
                </div>
            </form>
        </div>

    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
