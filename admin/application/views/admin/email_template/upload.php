<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            HTMLファイルアップロード
        </h1>
        <ol class="breadcrumb">
            <li><a href="/"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active"><a href="<?php echo site_url('/EmailTemplate/')?>">HTMLファイルアップロード</a></li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">HTMLファイルアップロード</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form role="form" action="" id="uploadEmailTemplate"  method="POST" enctype="multipart/form-data">
                <div class="nav-tabs-custom">
                    <div class="tab-content">
                        <div class="form-group">
                            <label for="name">サブジェクト</label>
                            <input type="text" class="form-control"  name="title">
                        </div>
                        <div class="form-group">
                            <label for="name">アップロード</label>
                            <input type="file" class="" required=""  name="file" placeholder="">
                        </div>
                        <div class="form-group">
                            <label for="name">メールテンプレート名（ローマ字・数字だけで入力してください）</label>
                            <input type="text" class="form-control" id="name_mailtemplate" name="name" placeholder="" maxlength="100">
                            <label id="name_mailtemplate_error_js" class="error" style="display:none;">ローマ字・数字だけで入力してください</label>
                        </div>
                            <!-- /.box-body -->
                    </div>
                    <!-- /.tab-content -->
                </div>

                <div class="box-footer">
                    <!-- <p name="save" id="uploadfile" class="btn btn-primary" value="1"><?php echo $this->lang->line('save'); ?></p> -->
                    <button type="" name="save"  class="btn btn-primary" value="1"><?php echo $this->lang->line('save'); ?></button>
                    <a href="<?php echo site_url('/EmailTemplate/') ?>" class="btn btn-default"><?php echo $this->lang->line('cancel'); ?></a>
                </div>
            </form>
        </div>

    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
