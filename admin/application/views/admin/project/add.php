<!-- Content Wrapper. Contains page content -->
<style >
  .pd-banner .pannel .pannel-body .pannel-content .row .row-process-bar:after{
        background: #ededed;
  }
</style>
<div class="content-wrapper">
  <section class="content-header">
    <h1>
      プロジェクト
    </h1>
    <ol class="breadcrumb">
      <li><a href="/"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="active"><a href="<?php echo site_url('/project')?>">プロジェクト</a></li>
    </ol>
  </section>
  <section class="content">
      <?php if ($this->session->flashdata('msg')) { ?>
        <div class="alert alert-success" id="success-alert">
          <button type="button" class="close" data-dismiss="alert"><i class="fa fa-times"></i></button>
          <strong><?php echo $this->lang->line('success'); ?></strong>
          <?php echo $this->session->flashdata('msg'); ?>
        </div>
      <?php } ?>
      <?php if ($this->session->flashdata('error')) { ?>
        <div class="alert alert-danger" id="success-alert">
          <button type="button" class="close" data-dismiss="alert"><i class="fa fa-times"></i></button>
          <strong><?php echo $this->lang->line('error'); ?></strong>
          <?php echo $this->session->flashdata('error'); ?>
        </div>
      <?php } ?>
      <div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title">プロジェクト概要・詳細</h3>
        </div>
        <div class="modal fade " id="ModalPreView" role="dialog">
          <div class="modal-dialog modal-lg" style="width:1100px;">
            <!-- Modal content-->
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" style="color:red"><i class="fa fa-times-circle"></i></button>
                <h4 class="modal-title"></h4>
              </div>
              <div class="modal-body">
                <div class="preview">
                  <div class="pd-banner">
                      <div class="wraper">
                          <div class="pannel">
                              <p class="pannel-head"></p>
                              <div class="pannel-body">
                                  <div class="pannel-slider">
                                      <div class="cover_image_preview">
                                          <img width="100%" id="slider_img" src="<?php echo image_url(); ?>images/2018/default/noimage-01.png" alt="">
                                          <p class="click_image_preview" id="clickYoutube" href="#"></p>
                                          <iframe id="video" class="playvideo"  style=""width="420" height="315" src="" frameborder="0" allowfullscreen></iframe>
                                      </div>
                                  </div>
                                  <div class="pannel-content">
                                      <div class="row row-1">
                                          <p class="row-label">現在の支援総額</p>
                                          <p class="row-number">0円</p>
                                          <p class="row-process-bar">0%</p>
                                          <p class="row-note">目標金額は0円</p>
                                      </div>
                                      <div class="row row-2">
                                          <p class="row-label">支援者数</p>
                                          <p class="row-number">0人</p>
                                      </div>
                                      <div class="row row-3">
                                          <p class="row-label">募集終了まで残り</p>
                                          <p class="row-number">10日</p>
                                      </div>
                                      <p class="pannel-btn"><a style="cursor: pointer;">プロジェクトを支援する</a></p>
                                  </div>
                              </div>
                          </div>
                      </div>
                  </div>
                  <!-- BANNER -->
                  <div id="content">
                      <div class="wraper">
                          <div class="tab-area">
                              <ul style="margin-left: -40px;">
                                  <li id="tab_pr2" class="active">プロジェクト詳細</li>
                                  <li id="tab_pr1" >プロジェクト概要</li>
                              </ul>
                          </div>
                          <!-- .tab-area -->

                          <div class="pd-area  " id="tabPre-1">
                          </div>
                          <!-- #tab-1 -->
                          <div class="pd-area active" id="tabPre-2">
                              <div class="pd-box pd-box-1">
                                  <h4 class="pd-box-title">ファンディングの企画者について</h4>
                                  <div class="pd-box-content">
                                      <div class="pd-box-content-panel">
                                          <p class="pd-box-content-panel-avatar"><img id="img_user" width="100%" src="<?php echo image_url(); ?>images/2018/default/default-profile_01.png" alt=""></p>
                                          <div class="pd-box-content-panel-text-wraper">
                                              <p class="pd-box-text pd-box-text-name">氏名 ：氏名</p>
                                              <p class="pd-box-text pd-box-text-address">現在地  ：現在地 　</p>
                                              <p class="pd-box-text pd-box-text-job">職業 ：　職業</p>
                                          </div>
                                      </div>
                                      <div class="pd-box-content-intro">
                                          <p class="pd-box-content-intro-title">はじめにご挨拶：</p>
                                          <p class="pd-box-text">PreView</p>
                                          <p class="pd-box-btn"><a style="cursor: pointer;">プロジェクト概要 ＞</a></p>
                                      </div>
                                  </div>
                              </div>
                          </div>
                          <!-- #tab-2 -->
                      </div>
                  </div>
                  <!-- CONTENT -->
                </div>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">閉じる</button>
              </div>
            </div>

          </div>
        </div>


          <form role="form" action=""  id="demoForm" method="POST" enctype="multipart/form-data">
            <div class="nav-tabs-custom">
              <div class="tab-content">
                <div class="tab-pane active" id="tab_1">
                  <div class="box-body">
                      <div class="form-group">
                        <div class="form-check col-sm-2">
                          <input class="form-check-input" type="radio" name="project_type" id="Project_Normal" value="0" checked>
                          <label class="form-check-label" for="Project_Normal">
                            普通のプロジェクト
                          </label>
                        </div>
                        <div class="form-check col-sm-2">
                          <input class="form-check-input" type="radio" name="project_type" id="Project_FanClub" value="1">
                          <label class="form-check-label" for="Project_FanClub">
                            ファンクラブ
                          </label>
                        </div>
                      </div>

                    <div class="form-group col-sm-12">
                      <label for="name">プロジェクト名（最大150文字）</label>
                      <input type="text" class="form-control" id="project_name" name="project_name" maxlength="150" >
                    </div>
                    <div class="form-group col-sm-12">
                      <label for="name">ユーザー名</label>
                      <select class="form-control" id="itemName" name="itemName"></select>
                    </div>
                    <div class="form-group col-sm-12" id="form_img">
                      <label for="name">画像（サムネイル表示）</label>
                      <p>※画像のサイズは600px＊400pxで最高です。容量 2MBまで 。</p>
                      <input type="file" name="file" id="profile-img">
                      <input type="hidden" name="hiden_image" id="hiden_image" >
                      <p id="announce" style="color:red" ></p>
                      <p style="text-align: center;"><img id="profile-img-tag"/ width="600px" alt=""></p>
                    </div>
                    <div class="form-group col-sm-12">
                      <label for="name">はじめにご挨拶 （最大1000文字）</label>
                      <textarea class="form-control " name="summary" id="summary" maxlength="1000"></textarea>
                    </div>

                    <div class="form-group">
                      <p class="col-sm-12">※「動画のURL入力」か「画像アップロード」を選択して、該当のURLか、画像をアップロードしてください。</p>
                      <div class="form-check col-sm-2">
                        <input class="form-check-input" type="radio" name="Radios" id="Video" value="1" checked>
                        <label class="form-check-label" for="Video">
                          動画のURL入力
                        </label>
                      </div>
                      <div class="form-check col-sm-2">
                        <input class="form-check-input" type="radio" name="Radios" id="Image" value="0">
                        <label class="form-check-label" for="Image">
                          画像アップロード
                        </label>
                      </div>
                      <div class="col-sm-12">
                        <div id="video">
                          <div class="col-sm-12">
                            <label for="name">動画URL</label>
                            <input type="text" class="form-control youtubevideo" id="video" name="video" placeholder="" >
                          </div>
                        </div>
                        <div id="image_thumnail">
                          <p class="col-sm-12">※2～３枚の画像をアップロードしたら、スライダーで表示されるようになります。</p>
                          <p class="col-sm-12">※画像のサイズは600px＊400pxで最高です。容量 2MBまで 。</p>
                          <div class="form-group col-sm-4" id="form_img">
                            <label for="name">画像 1</label>
                            <input type="file" name="file1" id="profile_thumnail1">
                            <input type="hidden" name="hiden_image1" id="hiden_image1" >
                            <p id="announce1" style="color:red" ></p>
                            <p><img src=""  width="300px" id="profile_thumnail1-tag" alt=""/></p>
                          </div>
                          <div class="form-group col-sm-4" id="form_img">
                            <label for="name">画像 2</label>
                            <input type="file" name="file2" id="profile_thumnail2">
                            <input type="hidden" name="hiden_image2" id="hiden_image2" >
                            <p id="announce2" style="color:red" ></p>
                            <p><img src=""  width="300px" id="profile_thumnail2-tag" alt=""/></p>
                          </div>
                          <div class="form-group col-sm-4" id="form_img">
                            <label for="name">画像 3</label>
                            <input type="file" name="file3" id="profile_thumnail3">
                            <input type="hidden" name="hiden_image3" id="hiden_image3" >
                            <p id="announce3" style="color:red" ></p>
                            <p><img src=""  width="300px" id="profile_thumnail3-tag" alt=""/></p>
                          </div>
                        </div>
                      </div>
                      <div></div>
                    </div>
                    <div id="info_project_normal">
                        <div class="form-group">
                          <div class="col-sm-6">
                            <br />
                            <label for="name">プロジェクトのカテゴリ</label>
                            <select class="form-control form-control-sm" id="category" name="category">
                              <?php foreach ($category as $key => $value) { ?>
                                  <option value="<?php echo $value->id?>"><?php echo $value->name; ?></option>
                                <?php } ?>
                            </select>
                          </div>
                          <div class="col-sm-6">
                            <br />
                            <label for="name">募集終了日</label>
                            <div class="input-group date" data-date-format="datepicker-mm-yyyy">
                              <input  type="text" autocomplete="off" id="date" class="form-control" placeholder="yyyy-mm-dd" name="date">
                              <div class="input-group-addon" >
                                <span class="glyphicon glyphicon-th"></span>
                              </div>
                            </div>
                          </div>
                          <div></div>
                        </div>
                        <div class="form-group">
                          <div class="form-check-status col-sm-3">
                            <input class="form-check-input" type="radio" id="all_in" name="status_all_in" value="1" checked>
                            <label class="form-check-label" for="all_in">
                                All-in
                            </label>
                          </div>
                          <div class="form-check-status col-sm-3">
                            <input class="form-check-input" type="radio" id="allornothing" name="status_all_in" value="0">
                            <label class="form-check-label" for="allornothing">
                                All-or-nothing
                            </label>
                          </div>
                        </div>
                        <div class="form-group col-sm-12">
                          <br />
                            <label for="name">目標金額</label>
                            <input type="text" autocomplete="off" class="form-control money" required="" id="goal_amount" name="goal_amount" onkeypress='validate(event)'>
                        </div>
                    </div>
                    <div id="info_project_fanclub">
                        <div class="form-group">
                          <div class="col-sm-6">
                            <br />
                            <label for="name">プロジェクトのカテゴリ</label>
                            <select class="form-control form-control-sm" id="category" name="category">
                              <?php foreach ($category as $key => $value) { ?>
                                  <option value="<?php echo $value->id?>"><?php echo $value->name; ?></option>
                                <?php } ?>
                            </select>
                          </div>
                          <div class="col-sm-6">
                            <br />
                            <label for="name">
                              定期課金間隔
                            </label>
                            <select class="form-control" id="number_month" name="number_month" >
                                <?php for ($i=1; $i <=12 ; $i++) { ?>
                                    <option value="<?php echo $i; ?>"><?php echo $i; ?>カ月</option>
                                <?php } ?>
                            </select>
                          </div>
                          <div></div>
                        </div>
                    </div>
                    <div class="form-group col-sm-12">
                        <label for="name">プロジェクト概要</label>
                        <textarea class="form-control ckeditor" name="description" id="description"></textarea>
                    </div>
                  </div>
                    <!-- /.box-body -->
                </div>
              </div>
            </div>
            <div class="box-footer">
              <button type="submit" name="save" class="btn btn-primary" value="1" id="upload"><?php echo $this->lang->line('save'); ?></button>
              <button type="submit" name="public" class="btn btn-danger" value="1" id="upload">公開</button>
              <p id="setPreview" class="btn btn-danger" data-toggle="modal" data-target="#ModalPreView" >プレビュー</p>
              <a href="<?php echo site_url('/project/') ?>" class="btn btn-default"><?php echo $this->lang->line('cancel'); ?></a>
              <input type="hidden" name="pid" value="0" />
              <input type="hidden" name="settingKey" value="<?php echo $settingKey; ?>" />
            </div>
          </form>
        </div>
      </div>
  </section>
</div>
