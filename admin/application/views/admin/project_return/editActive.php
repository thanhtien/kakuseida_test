<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            プロジェクト内容更新、リターン追加依頼
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active"><a href="<?php echo site_url('project/edit/'.$project->id)?>">プロジェクト内容更新、リターン追加依頼</a></li>
        </ol>
    </section>
    <!-- Main content -->
    <section class="content">
        <div class="box box-primary">



        <div class="modal fade " id="ModalMailRefuse" role="dialog">
            <div class="modal-dialog" >
                <!-- Modal content-->
                <div class="modal-content">
                    <form id="SendMailRefuse" action="/projectreturn/refuse?project_id=<?php echo $project->id; ?>&status=<?php echo $status; ?>" method="post">
                        <div class="modal-header">
                            却下の場合へのメール送信
                        </div>
                        <div class="modal-body">

                            <div class="preview">
                                <!-- BANNER -->
                                <div id="content">
                                    <div class="form-group">
                                        <p>【<?php echo $project->project_name; ?>】の更新申請が却下されました。</p>
                                    </div>
                                    <div class="form-group">
                                        <p>却下理由</p>
                                        <textarea name="message" class="form-control" style="resize:none;" rows="8" cols="80"></textarea>
                                    </div>
                                </div>
                                <!-- CONTENT -->
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="submit" id="btnrefuse" class="btn btn-primary" form="SendMailRefuse" value="Submit">送信</button>
                            <button type="button" class="btn btn-default" data-dismiss="modal">キャンセル</button>
                        </div>
                    </form>

                </div>

            </div>
        </div>
        <form id="frmMain" method="POST" action="/project/actionActive/<?php echo $project->id; ?>?status=<?php echo $status ?>">

        <div class="box-body no-padding">
            <div id="content">
                <div class="wraper">
                    <div class="pd-area active" id="tabPre-2">
                        <div class="pd-box pd-box-2">
                            <h4 class="pd-box-title fix_space">依頼名</h4>
                            <p class="message_project"><?php echo $title; ?></p>
                            <h4 class="pd-box-title fix_space">内容（理由などについて）</h4>
                            <!-- <pre class="message_project premessage" ><?php echo $message; ?></pre> -->
                            <textarea disabled name="name" id="previewArea" class="premessage" ><?php echo $message; ?></textarea>
                            <?php if($project_type === '0') { ?>
                                <h4 class="pd-box-title fix_space">目標金額</h4>
                                <p class="message_project" ><?php echo number_format($goal_amount); ?> 円</p>
                            <?php } ?>
                            <?php if(isset($listReturnActive) && $listReturnActive){ ?>
                                <h4 class="pd-box-title fix_space">追加のリターン</h4>
                                <div class="return-list">
                                    <ul>
                                        <?php    foreach ($listReturnActive as $key => $value) { ?>
                                            <li>
                                                <p class="return-list-img"><a href="#"><img style="width:100%;" src="<?php echo $value->thumnail; ?>" alt=""></a></p>
                                                <div class="return-list-content cover_relative" >
                                                    <p class="return-list-content-number"><?php echo number_format($value->invest_amount); ?>円</p>
                                                    <p class="return-list-content-number">name:<?php echo ($value->name); ?></p>
                                                    <p class="return-list-text"><?php echo $value->return_amount; ?></p>
                                                    <div class="return-list-status" style="    width: 600px;">
                                                        <p class="return-list-s return-list-s-1">支援者数：　<span><?php echo $value->now_count; ?></span></p>
                                                        <?php if(isset($value->schedule) && $value->schedule){ ?>
                                                            <p class="return-list-s return-list-s-2">お届け予定：<span><?php echo $value->schedule; ?></span></p>
                                                        <?php } ?>
                                                    </div>
                                                    <?php if(isset($value->max_count) && $value->max_count){ ?>
                                                        <p class="max_count">残り件数:<span style="margin-left: 10px;"><?php echo $value->max_count; ?></span>件</p>
                                                    <?php } ?>
                                                    <p class="return-list-btn"><a style="cursor:pointer;">このリターンを選択する</a></p>
                                                </div>
                                            </li>
                                        <?php } ?>
                                    </ul>
                                </div>
                            <?php } ?>
                        </div>
                    </div>
                    <!-- #tab-2 -->
                </div>
            </div>
        </div>
        <div class="box-footer">
            <button type="submit" <?php if($current_status != '0'){ echo "disabled"; }?> name="save" class="btn btn-primary" value="1">承認</button>
            <p id="setPreview"   class="btn btn-danger" data-toggle="modal" <?php if($current_status === '0'){?> data-target="#ModalMailRefuse" <?php }else { echo "disabled";} ?> > 却下</p>
            <a <?php $this->session->set_flashdata('tab', '2'); ?> href="<?php echo site_url('project/edit/'.$project->id.'?tab=2')?>" class="btn btn-default">戻る</a>
            <input type="hidden" name="ActiveProjectReturn" value="<?php echo $project->id ?>" />
        </div>
        </form>
     </div>
    </section>
</div>
