<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
             STRIPEカード設定
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active"> STRIPEカード設定</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">STRIPE設定へのテスト</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form role="form" action="" method="POST" enctype="multipart/form-data">
                <div class="box-body">

                    <div class="form-group">
                        <label for="name_vn">カード番号</label>
                        <input type="text" class="form-control" required="" autocomplete="false" id="number" name="number" placeholder="">
                    </div>
                    <div class="form-group " style="overflow:hidden">
                        <label for="name_vn" style="display:block">有効期限</label>
                        <div class=" col-sm-2" style="padding:0">
                            <input type="text" class="form-control col-sm-4" required="" id="exp_year" name="exp_year" placeholder="年">
                        </div>
                        <div class=" col-sm-2" style="padding:0">
                            <input type="text" class="form-control col-sm-4" required="" id="exp_month" name="exp_month" placeholder="月">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="title_vn" style="display:block">CVC(セキュリティーコード）</label>
                        <input type="text" class="form-control" id="cvc" name="cvc" placeholder="">
                    </div>
                </div>
                    <!-- /.box-body -->
                <div class="box-footer">
                    <button type="submit" name="save" class="btn btn-primary" value="1"><?php echo $this->lang->line('save'); ?></button>
                    <a href="<?php echo site_url('/setting') ?>" class="btn btn-default"><?php echo $this->lang->line('cancel'); ?></a>
                    <input type="hidden" name="pid" value="0" />
                </div>
            </form>
        </div>

    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
