<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class API extends BD_Controller {
  function __construct()
  {
    // Construct the parent class
    parent::__construct();
    $this->load->model('Project');
    $this->load->model('News');
    $this->load->database();
    $this->load->library(array('ion_auth', 'form_validation'));
    $this->load->helper(array('url', 'language'));
    $this->load->library('email');

  }
    // Pickup data get
	  public function Pickup_projects_get(){
        $input['join'] = array('pickup_projects','pickup_projects.project_id = projects.id');
        $input['limit'] = array(8,0);
        $input['order'] = array('created','DESC');
        $toppage = $this->Project->get_data($input);
        if($toppage){
            $this->response($toppage, 200);
        }else{
            $data = array([
                'status' => ' Get failed!'
            ]);
            $this->response($data, 400);
        }
    }
    // Project mới được admin chấp nhận đưa lên
    public function New_project_get(){
        $input['join'] = array('categories','categories.id = projects.category_id');
        $input['order'] = array('collection_start_date','DESC');
        $input['limit'] = array(4,0);
        $data = $this->Project->get_list($input);
        $this->response($data, 200);
    }
    // Project được admin add vào yêu thích
    public function Favourite_projects_get(){
        $input['join'] = array('favourite_projects','favourite_projects.project_id = projects.id');
        $input['limit'] = array(8,0);
        $input['order'] = array('created','DESC');
        $favourite = $this->Project->get_data($input);
        if($favourite){
            $this->response($favourite, 200);
        }else{
            $data = array([
                'status' => ' Get failed!'
            ]);
            $this->response($data, 400);
        }
    }
    // Tin tức mới nhất
    public function News_get(){
        $input['order'] = array('created','DESC');
        $input['limit'] = array(4,0);
        $data = $this->News->get_list($input);
        $this->response($data, 200);
    }
    // Project được admin add vào đề cử
    public function Recommends_projects_get(){
        $input['join'] = array('recommends_projects','recommends_projects.project_id = projects.id');
        $input['limit'] = array(8,0);
        $input['order'] = array('created','DESC');
        $favourite = $this->Project->get_data($input);
        if($favourite){
            $this->response($favourite, 200);
        }else{
            $data = array([
                'status' => ' Get failed!'
            ]);
            $this->response($data, 400);
        }
    }
    // Project hết hạn
    public function Expired_get(){
        $date = new DateTime();
        $date = $date->format('Y-m-d H:i:s');
        $input['where'] = array('collection_end_date <=' => $date);
        $test = $this->Project->get_list($input);

        $this->response($test, 200);
    }
    // Project sắp hết hạn
    public function Near_expired_get(){
        $a = array();

        $test = $this->Project->get_list();
        foreach ($test as $key => $value){
          if((($value->collected_amount*100)/$value->goal_amount >= 9)){
            array_push($a,$value);
          }
        }
        var_dump($a);
        var_dump(array_slice($a, 0, 4));
        exit;
    }
    // Project sắp gọi đủ vốn
    public function Project_goal_get(){
        if('2018-09-18 10:00:00' >= '2018-09-18 10:00:00'){
            var_dump('test');
        }
        $a = (int) '12312312312';
        $input['where'] = array(`collection_end_date` < $date);
        $test = $this->Project->get_list($input);
        $this->response($test, 200);
    }



}
