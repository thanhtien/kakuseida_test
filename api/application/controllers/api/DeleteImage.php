<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class DeleteImage extends BD_Controller {
    function __construct()
    {
        // Construct the parent class
        parent::__construct();

    }
    /**
     * [index_post Upload Image Admin User]
     * @return [type] [description]
     */
     public function delImage_post() {

         $linkImage = $this->post('linkImage');
         $linkImage= str_replace(base_url(),"",$linkImage);

         if (file_exists('./static/'.$linkImage)) {
           unlink('./static/'.$linkImage);
           $this->response('./static/'.$linkImage,200);
         }
     }

    public function delEmailTemPlate_post(){
        $linkImage = $this->post('linkImage');
        $linkImage= str_replace(base_url(),"",$linkImage);

        if (file_exists('./static/'.$linkImage)) {
            unlink('./static/'.$linkImage);
        	$this->response('./static/'.$linkImage,200);
        }
    }
}
