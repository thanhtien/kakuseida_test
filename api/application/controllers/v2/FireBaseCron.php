 <?php

defined('BASEPATH') OR exit('No direct script access allowed');
require("public/assets/socket/socket.io.php");
include(__DIR__.'/MailChimp.php');
use \DrewM\MailChimp\MailChimp;
use \DrewM\MailChimp\Batch;

class FireBaseCron extends BD_Controller {
    function __construct()
    {
        // Construct the parent class
        parent::__construct();

    }
    /**
     * [createComment_post Create Comment]
     * @return [type] [description]
    */
    //  Cronjob Project Enough Expired then annouce for owner This project enough Expired
    public function projectEnoughExpired_get(){
        $roomSocket = $this->config->item('roomSocket');
        $portRoomSocket = $this->config->item('portRoomSocket');
        // get key firebase
        $key_firebase = $this->config->item('key_firebase');
        // Get date
        $date = new DateTime();
        // format date
        $date = $date->format('Y-m-d H:i:s');
        // explode $date
        $parts = explode('-', $date);
        // add 5 day for check project enough expired
        $datePlusFive = date(
            'Y-m-d H:i:s',
            mktime(0, 0, 0, $parts[1], $parts[2] + 5, $parts[0])
        );
        // get projects enough expired
        $near_expired = $this->Project->getAllProjectEnoughExpired($date,$datePlusFive);
        $dataNearExpired = [];
        foreach ($near_expired as $key => $value) {
            if($value->status_expired === '0') {
                array_push($dataNearExpired,$value);
            }
        }


        foreach ($dataNearExpired as $key => $value) {
            // create Datetime
            $datetime = new DateTime();
            $datetime = $datetime->format('Y-m-d H:i:s');
            // get strtotime + change time zone
            $asia_timestamp = strtotime($datetime);
            date_default_timezone_set('UTC');
            // get Date vs time zone
            $date = date("Y-m-d H:i:s", $asia_timestamp);

            // array create notification info
            $dataNotification = array (
                'user_id' => $value->user_id,
                'user_action' => 'Admin',
                'title' =>  '終了間際のプロジェクト',
                'message' =>'「'.$value->project_name.'」の期間がもうすぐ終わってしまいます。',
                'link'=> "project-detail/".$value->id,
                'status' => 0,
                'thumbnail' => base_url()."images/2018/default/logo-icon.png",
                'created' => $date
            );
            // create notification info
            $createNotification = $this->Notification_info->create($dataNotification);

            $socketio = new SocketIO();
            // // array
            $data = array(
                'room' => 'project_'.$value->id,
                'title' => '終了間際のプロジェクト',
                'message' => '「'.$value->project_name.'」の期間がもうすぐ終わってしまいます。',
                'url'=> app_url()."project-detail/".$value->id,
                'ava' => base_url().'images/2018/default/logo-icon.png'
            );


            $socketio->send($roomSocket, $portRoomSocket, 'annouce_project',json_encode($data));

            $curl = curl_init();

            curl_setopt_array($curl, array(
              CURLOPT_URL => "https://fcm.googleapis.com/fcm/send",
              CURLOPT_RETURNTRANSFER => true,
              CURLOPT_ENCODING => "",
              CURLOPT_MAXREDIRS => 10,
              CURLOPT_TIMEOUT => 30,
              CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
              CURLOPT_CUSTOMREQUEST => "POST",
              CURLOPT_POSTFIELDS => "{   \"notification\": {\r\n        \"title\": \"終了間際のプロジェクト\",\r\n        \"body\": \"「".$value->project_name."」の期間がもうすぐ終わってしまいます。\",\r\n        \"click_action\": \"".app_url()."project-detail/".$value->id."\",\r\n        \"icon\": \"".base_url()."images/2018/default/logo-icon.png\"\r\n    },\r\n  \"to\" : \"/topics/project_".$value->id."\"\r\n}",
              CURLOPT_HTTPHEADER => array(
                "Authorization: key=".$key_firebase."",
                "Content-Type: application/json",
              ),
            ));

            $response = curl_exec($curl);
            $err = curl_error($curl);

            curl_close($curl);

            $updateProject = array(
                'status_expired' => '1'
            );

            $update = $this->Project->update($value->id,$updateProject);
        }
    }

    public function projectNearGoalAmount_get() {

        // get key firebase
        $key_firebase = $this->config->item('key_firebase');
        $roomSocket = $this->config->item('roomSocket');
        $portRoomSocket = $this->config->item('portRoomSocket');

        // Get date
        $date = new DateTime();
        // format date
        $date = $date->format('Y-m-d H:i:s');

        $project_goal = $this->Project->getEnoughAmountPagination($date);

        $dataNearGoalAmount = [];
        foreach ($project_goal as $key => $value) {
            if($value->status_goal_amount === '0') {
                array_push($dataNearGoalAmount,$value);
            }
        }


        foreach ($dataNearGoalAmount as $key => $value) {

            // create Datetime
            $datetime = new DateTime();
            $datetime = $datetime->format('Y-m-d H:i:s');
            // get strtotime + change time zone
            $asia_timestamp = strtotime($datetime);
            date_default_timezone_set('UTC');
            // get Date vs time zone
            $date = date("Y-m-d H:i:s", $asia_timestamp);

            // array create notification info
            $dataNotification = array (
                'user_id' => $value->user_id,
                'user_action' => 'Admin',
                'title' =>  'あと少しで目標100％になるプロジェクト',
                'message' =>$value->project_name.'プロジェクトの目標金額にもうすぐ到達します',
                'link'=> "project-detail/".$value->id,
                'status' => 0,
                'thumbnail' => base_url()."images/2018/default/logo-icon.png",
                'created' => $date
            );
            // create notification info
            $createNotification = $this->Notification_info->create($dataNotification);

            $socketio = new SocketIO();
            // // array
            $data = array(
                'room' => 'project_'.$value->id,
                'title' =>  'あと少しで目標100％になるプロジェクト',
                'message' =>$value->project_name.'プロジェクトの目標金額にもうすぐ到達します',
                'url' => app_url()."project-detail/".$value->id,
                'ava' => base_url()."images/2018/default/logo-icon.png"
            );

            $socketio->send($roomSocket, $portRoomSocket, 'annouce_project',json_encode($data));

            $curl = curl_init();

            curl_setopt_array($curl, array(
              CURLOPT_URL => "https://fcm.googleapis.com/fcm/send",
              CURLOPT_RETURNTRANSFER => true,
              CURLOPT_ENCODING => "",
              CURLOPT_MAXREDIRS => 10,
              CURLOPT_TIMEOUT => 30,
              CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
              CURLOPT_CUSTOMREQUEST => "POST",
              CURLOPT_POSTFIELDS => "{   \"notification\": {\r\n        \"title\": \"あと少しで目標100％になるプロジェクト\",\r\n        \"body\": \"".$value->project_name."プロジェクトの目標金額にもうすぐ到達します\",\r\n        \"click_action\": \"".app_url()."project-detail/".$value->id."\",\r\n        \"icon\": \"".base_url()."images/2018/default/logo-icon.png\"\r\n    },\r\n  \"to\" : \"/topics/project_".$value->id."\"\r\n}",
              CURLOPT_HTTPHEADER => array(
                "Authorization: key=".$key_firebase."",
                "Content-Type: application/json",
              ),
            ));

            $response = curl_exec($curl);
            $err = curl_error($curl);

            curl_close($curl);

            $updateProject = array(
                'status_goal_amount' => '1'
            );

            $update = $this->Project->update($value->id,$updateProject);
        }
    }

    public function DeleteDonateCron_get(){

        // get key firebase
        $key_firebase = $this->config->item('key_firebase');
        // get Backed
        $backed = $this->BackedProject->getAllBackedProjectExpired();
        // foreach Backed get data
        $dataDelete = [];
        foreach ($backed as $key => $value) {
            // create date by created
            $dateBacked = new DateTime($value->created);

            $curDate = new DateTime();
            $interval = $dateBacked->diff($curDate);
            $day = $interval->format('%a');
            if($day > 2 ){
                array_push($dataDelete,$value->id);
            }
        }
        if(isset($dataDelete) && $dataDelete){
            $id = implode(',', $dataDelete);
            $this->db->where("id in ($id)");
            // Delete talbe project with id
            $del = $this->db->delete('backed_projects');
        }
    }

    /**
     * [AnnouceFanClubDonate_get
     *  Annouce for Donater know Donate new month for fan club
     * ]
     */
    public function AnnouceFanClubDonate_get() {
        // get key firebase
        $key_firebase = $this->config->item('key_firebase');
        $roomSocket = $this->config->item('roomSocket');
        $portRoomSocket = $this->config->item('portRoomSocket');

        // get Project FanClub
        $project = $this->Project->getFanClubs();
        // create array
        $infoSocket = array();
        $infoEmail = array();

        // foreach $project
        foreach ($project as $key => $value) {

            $date1 = new DateTime($value->collection_start_date);
            $date2 = new DateTime(date('Y-m-d'));
            $time = $date1->diff($date2);

            $date = date_diff($date1,$date2);
            $date = $date->format("%a");

            $number_day = $value->number_month*30;

            if((int)$date > (int)$number_day) {
                $month = ($date/$number_day);
                $checkMonth = ceil($date/$number_day);
                if($month === (int)$checkMonth) {
                    $month = $checkMonth;
                } else {
                    $month = $checkMonth - 1;
                }
            }else{
                $month = 0;
            }

            if((int)$month === (int)0) {
                $numberDayAnnouce = $number_day - 5;
            }else{
                $numberDayAnnouce = $number_day*($month + 1) - 5;
            }
            $date1 = $date1->format('Y-m-d');
            $parts = explode('-', $date1);

            $dateAnnouce = date(
                'Y-m-d',
                mktime(0, 0, 0, $parts[1], $parts[2] + ($numberDayAnnouce - 1), $parts[0])
            );

            $currentDay = $date2->format('Y-m-d');

            if($currentDay === $dateAnnouce && count($infoSocket) < 50) {

                $infoFanClub = $this->ListDonateFanClub->getAllDonate($value->id,$month);

                foreach ($infoFanClub as $key => $info) {

                    if($info->status === '0'){
                        array_push($infoSocket,$info);
                        $newInfo = array(
                            'user_id' => $info->user_id,
                            'project_id' => $info->project_id,
                            'month' => $info->month,
                            'status' => $info->status,
                        );
                        if (in_array($newInfo, $infoEmail) !== true) {
                            array_push($infoEmail,$newInfo);
                        }
                    }
                }
            }
        }

        foreach ($infoSocket as $key => $value) {

            $userCheck = $value->user_id;
            $projectCheck = $value->project_id;
            $monthCheck = $value->month + 1;

            $checkDonate = $this->ListDonateFanClub->checkIsDonateSendMail($userCheck,$projectCheck,$monthCheck);
            if(isset($checkDonate) && $checkDonate){

            }else{
                $curUser =  $this->User->getUserId($value->user_id);
                $project = $this->Project->ProjectId($value->project_id);

                // // send mail for admin

                //use json_encode show data.
                $email = $curUser->email;
                $userName = $curUser->username;
                $projectName = $project->project_name;

                $this->load->library('email');
                $this->email->from('admin@kakuseida.conoha.io', 'KAKUSEIDA');
                $this->email->to('hodacquyenpx@gmail.com');

                $this->email->subject($projectName.'支援中のお客様へ');
                $this->email->message(
                    'KAKUSEIDA運営事務局です。<br>
                    KAKUSEIDAのサービスをご利用いただきありがとうございます。<br>
                    <br>
                    ご支援していただいている '.$projectName.' の定期課金の自動更新のお知らせとなります。<br>
                    本日より５日後に定期課金が自動更新されます。<br>
                    '.$projectName.' の支援を中断したい方は、<a href="'.app_url().'my-page/project-supported-fan/page=1">マイページの支援中のプロジェクト</a>から支援を中断してください。<br>
                    <br>
                    今後ともKAKUSEIDAをよろしくお願い致します。<br>'
                );

                $this->email->send();
            }
        }

        foreach ($infoSocket as $key => $value) {
            $userCheck = $value->user_id;
            $projectCheck = $value->project_id;
            $backingCheck = $value->backing_levels_id;
            $monthCheck = $value->month + 1;
            $checkDonate = $this->ListDonateFanClub->checkIsDonate($userCheck,$projectCheck,$backingCheck,$monthCheck);

            if(isset($checkDonate) && $checkDonate){

            }else{
                $dataUpdate = array(
                    'month' => $monthCheck,
                );
                $this->ListDonateFanClub->update($value->id,$dataUpdate);

            }
        }
    }

    public function closeFanClub_get(){
        $FanClubs = $this->Project->getFanClubsStop();
        $curentDay = new DateTime();
        $curentDay = $curentDay->format('Y-04-30');
        foreach ($FanClubs as $key => $value) {
            $data = array(
                'active' => 'cls'
            );
            if($curentDay === $value->date_fanclub_close){

                $updateFanClub = $this->Project->update($value->id,$data);
            }
        }
    }

    public function sendMailCloseFanClub_get(){
        $roomSocket = $this->config->item('roomSocket');
        $portRoomSocket = $this->config->item('portRoomSocket');
        // get key firebase
        $key_firebase = $this->config->item('key_firebase');
        //include Stripe PHP library
        require_once APPPATH."third_party/stripe/init.php";

        $FanClubs = $this->Project->getFanClubsStop();

        $listEmailDonate = array();
        $listStopDonate = array();
        foreach ($FanClubs as $key => $value) {
            $listDonate = $this->ListDonateFanClub->getAllDonateClose($value->id);
            foreach ($listDonate as $key => $value) {
                if(count($listEmailDonate) >= 50){
                    break;
                }else{
                    $newInfo = array(
                        'email' => $value->email,
                        'project_name' => $value->project_name,
                        'user_id' => $value->user_id,
                        'sub_fanclub_id' => $value->sub_fanclub_id,
                        'project_id' => $value->project_id,
                    );

                    array_push($listStopDonate,$newInfo);

                    if (in_array($newInfo, $listEmailDonate) !== true) {
                        array_push($listEmailDonate,$newInfo);
                    }
                }
            }
        }
        //set api key
        $key = $this->Setting->getSetting();
        $stripe = array(
            "secret_key"      => $key->secret_key,
            "publishable_key" => $key->public_key
        );

        \Stripe\Stripe::setApiKey($stripe['secret_key']);

        foreach ($listStopDonate as $key => $value) {
            $subscription = \Stripe\Subscription::retrieve($value['sub_fanclub_id']);
            if(isset($subscription) && $subscription->status !== 'canceled') {
                $cancelDonate = $subscription->cancel();
            }
        }
        foreach ($listEmailDonate as $key => $value) {

            $user_id = $value['user_id'];
            $project_id = $value['project_id'];
            $email = $value['email'];
            $projectName = $value['project_name'];

            $this->load->library('email');
            $this->email->from('admin@kakuseida.conoha.io', 'KAKUSEIDA');
            $this->email->to($email);

            $this->email->subject('「'.$projectName.'」プロジェクトの解散のお知らせ');
            $this->email->message(
                'KAKUSEIDA運営事務局です。<br>
                KAKUSEIDAをご利用いただきありがとうございます。<br>
                <br>
                お客様が加入している「'.$projectName.'」プロジェクトの解散についてのお知らせを致します。<br>
                「'.$projectName.'」プロジェクトは企画者の権限により解散されることとなりました。<br>
                解散に至った経緯、詳細、解散日時につきましては、企画者からの連絡をお待ちください。<br>
                解散後、当プロジェクトの定期課金は自動的に停止されます。<br>
                <br>
                今後ともKAKUSEIDAをよろしくお願い致します。<br>
                <br>
                KAKUSEIDA運営事務局<br>'
            );
            $sendMail = $this->email->send();
            // $sendMail = false;
            if($sendMail) {

                // create Datetime
                $curUser =  $this->User->getUserId($user_id);
                $tokenFireBase  = $curUser->token;
                $project = $this->Project->ProjectId($project_id);
                $datetime = new DateTime();
                $datetime = $datetime->format('Y-m-d H:i:s');
                // get strtotime + change time zone
                $asia_timestamp = strtotime($datetime);
                date_default_timezone_set('UTC');
                // get Date vs time zone
                $date = date("Y-m-d H:i:s", $asia_timestamp);

                // array create notification info
                $dataNotification = array (
                    'user_id' => $user_id,
                    'user_action' => 'Admin',
                    'title' =>  '「'.$projectName.'」は解散されることとなりました。 ',
                    'message' =>'「'.$projectName.'」は解散されることとなりました。',
                    'link'=> "project-detail/".$project_id,
                    'status' => 0,
                    'thumbnail' => base_url()."images/2018/default/logo-icon.png",
                    'created' => $date
                );
                $createNotification = $this->Notification_info->create($dataNotification);
                // create notification info
                $socketio = new SocketIO();
                // // array
                $data = array(
                    'room' => 'user_'.$user_id,
                    'title' =>  '「'.$projectName.'」は解散されることとなりました。',
                    'message' =>'「'.$projectName.'」は解散されることとなりました。',
                    'url' => app_url()."project-detail/".$project_id,
                    'ava' => base_url()."images/2018/default/logo-icon.png"
                );

                $socketio->send($roomSocket, $portRoomSocket, 'annouce_project',json_encode($data));

                $curl = curl_init();

                curl_setopt_array($curl, array(
                  CURLOPT_URL => "https://fcm.googleapis.com/fcm/send",
                  CURLOPT_RETURNTRANSFER => true,
                  CURLOPT_ENCODING => "",
                  CURLOPT_MAXREDIRS => 10,
                  CURLOPT_TIMEOUT => 30,
                  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                  CURLOPT_CUSTOMREQUEST => "POST",
                  CURLOPT_POSTFIELDS => "{   \"notification\": {\r\n        \"title\": \"「".$projectName."」は解散されることとなりました。\",\r\n        \"body\": \"「".$projectName."」は解散されることとなりました。\",\r\n        \"click_action\": \"".app_url()."project-detail/".$project_id."\",\r\n        \"icon\": \"".base_url()."images/2018/default/logo-icon.png\"\r\n    },\r\n  \"to\" : \"".$tokenFireBase."\"\r\n}",
                  CURLOPT_HTTPHEADER => array(
                    "Authorization: key=".$key_firebase."",
                    "Content-Type: application/json",
                  ),
                ));

                $response = curl_exec($curl);
                $err = curl_error($curl);

                curl_close($curl);

                $getAllDonate = $this->ListDonateFanClub->AllDonateClose($user_id,$project_id);
                $arrayDonate = array();
                foreach ($getAllDonate as $key => $value) {
                    array_push($arrayDonate,$value->id);
                }
                $dataDonate = array(
                    'status' => '1',
                    'status_join_fanclub' => '1'
                );
                $inDonate = implode(',', $arrayDonate);
                $update = $this->ListDonateFanClub->updateMoreFanClub($inDonate,$dataDonate);
            }
        }
    }

    public function testcron_get(){
        $this->load->library('email');
        $this->email->from('admin@kakuseida.conoha.io', 'KAKUSEIDA');
        $this->email->to('hodacquyenpx@gmail.com');

        $this->email->subject('test cron');
        $this->email->message(
            'tesst cron'
        );
        $sendMail = $this->email->send();
    }


    public function CloseProjectByAdmin_get(){
        $projectCls = $this->Project->getProjectCls();
        $arrayEmail = array();
        foreach ($projectCls as $key => $value) {
            if(isset($value->email_close) && $value->email_close) {
                $Owner = $this->User->getUserId($value->user_id);

                $email = explode(",",$value->email_close);

                foreach ($email as $key => $value2) {

                    $infoSendMail = array(
                        'email' => $value2,
                        'project_name' => $value->project_name,
                        'project_id' => $value->id,
                        'user_id' => $value->user_id,
                        'user_name' => $Owner->username,
                        'ownerEmail' => $Owner->email
                    );
                    if(count($arrayEmail) >= 2){
                        break;
                    }else{
                        if (in_array($infoSendMail, $arrayEmail) !== true) {
                               array_push($arrayEmail,$infoSendMail);
                        }
                    }
                }
            }
        }
        foreach ($arrayEmail as $key => $value) {

            $this->load->library('email');
            $this->email->from('admin@kakuseida.conoha.io', 'KAKUSEIDA');
            $this->email->to($value['email']);

            $this->email->subject('【KAKUSEIDA】本人確認のお知らせ');
            $this->email->message(
                '支援者の皆様<br />
                KAKUSEIDA運営事務局です<br /><br />
                支援していただいた以下のプロジェクトは途中で終了になりました。<br />
                プロジェクト名：'.$value['project_name'].' <br />
                企画者： '.$value['user_name'].' <br />
                企画したプロジェクトはキャンセルとなり、支援していただいたお金は全て支援者の方に返金されます。<br />
                返金スケジュールなど、詳細については追って御連絡させていただきます。<br />
                ご不便をかけて大変申し訳ありません。<br />

                今後ともKAKUSEIDAをよろしくお願い致します。'
            );
            $sendMail = $this->email->send();

            if($sendMail) {

                $projectDetail = $this->Project->getProjectId($value['project_id']);
                if($projectDetail->project_type === '1') {
                    $listDonate = $this->ListDonateFanClub->AllDonateClose($value['user_id'],$value['project_id'],1);
                    if(isset($listDonate) && $listDonate) {
                        foreach ($listDonate as $key => $value) {
                            //include Stripe PHP library
                            require_once APPPATH."third_party/stripe/init.php";

                            //set api key
                            $key = $this->Setting->getSetting();
                            $stripe = array(
                                "secret_key"      => $key->secret_key,
                                "publishable_key" => $key->public_key
                            );

                            \Stripe\Stripe::setApiKey($stripe['secret_key']);
                            $subscription = \Stripe\Subscription::retrieve($value->sub_fanclub_id);
                            if($subscription->status !== 'canceled'){
                                $cancelDonate = $subscription->cancel();
                                $dataDonate = array(
                                    'status_join_fanclub' => '1'
                                );
                                $updateDataDonate = $this->ListDonateFanClub->update($value->id,$dataDonate);
                            }else{
                                $dataDonate = array(
                                    'status_join_fanclub' => '1'
                                );
                                $updateDataDonate = $this->ListDonateFanClub->update($value->id,$dataDonate);
                            }
                        }
                    }
                }
                $arrayEmail = explode(",",$projectDetail->email_close);
                $newArrayEmail = array_diff($arrayEmail,array($value['email']));
                $listEmail = implode(",", $newArrayEmail);
                //
                $dataUser = array(
                    'email_close' => $listEmail
                );
                $this->Project->update($value['project_id'],$dataUser);
            }

        }
    }

    public function joinNotificationAllUser_get() {

    $key_firebase = $this->config->item('key_firebase');

        $listUser = $this->User->getUserJoinAllUser();

        if(isset($listUser) && $listUser) {
            foreach ($listUser as $key => $value) {

                $tokenUser = $value->token;
                // / use curl create topics (send notification = firebase)
                if(isset($tokenUser) && $tokenUser) {

                    $curl = curl_init();
                    curl_setopt_array($curl, array(
                        CURLOPT_URL => "https://iid.googleapis.com/iid/v1/".$tokenUser."/rel/topics/all_user",
                        CURLOPT_RETURNTRANSFER => true,
                        CURLOPT_ENCODING => "",
                        CURLOPT_MAXREDIRS => 10,
                        CURLOPT_TIMEOUT => 30,
                        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                        CURLOPT_CUSTOMREQUEST => "POST",
                        CURLOPT_POSTFIELDS => "",
                        CURLOPT_HTTPHEADER => array(
                        "authorization: key=".$key_firebase."",
                        "cache-control: no-cache"
                        ),
                    ));

                    $response = curl_exec($curl);
                    $err = curl_error($curl);

                    curl_close($curl);

                    $data = array(
                        'status_all_user' => '1'
                    );
                    $updateUser = $this->User->update($value->id,$data);

                }
            }
        }
    }

    public function getAllUser_get(){
        $user = $this->User->getUser();

        foreach ($user as $key => $value) {
            if($value->profileImageURL !== 'https://static.kakuseida.com/images/2018/default/default-profile_01.png'){
                $link = $value->profileImageURL;
                $arrayLink = explode('/', $link);
                if($arrayLink[2] === 'static.kakuseida.com'){
                    $this->load->helper('directory');
                    // $map = directory_map('./TeampleatEmail',1);
                    //         var_dump($status);
                    $map = directory_map('static/images/'.$arrayLink[4].'/'.$arrayLink[5].'/'.$arrayLink[6]);
                    $status = 0;
                    if(isset($map) && $map) {
                        foreach ($map as $key => $value2) {
                            if($value2 === 'medium_'.$arrayLink[7]){
                                $status = $status + 1;
                            }
                        }

                        if($status === 0) {

                            $filename = $arrayLink[7];

                            $this->load->library('image_lib');
                            //config
                            $config = array (
                                'image_library' => 'ImageMagick',
                                'library_path' => '/usr/bin',
                                'source_image' => './static/images/'.$arrayLink[4].'/'.$arrayLink[5].'/'.$arrayLink[6].'/'.$arrayLink[7],
                                'quality' => '80%',
                                'maintain_ratio' => false,
                                'x_axis' => 0,
                                'y_axis' => 0,
                                'new_image' => './static/images/'.$arrayLink[4].'/'.$arrayLink[5].'/'.$arrayLink[6].'/small_'.$filename,
                                'width' =>  50,
                                'height' => 50
                            );

                            $this->image_lib->initialize($config);
                            $this->image_lib->resize();

                            $config1 = array(
                                'image_library' => 'ImageMagick',
                                'library_path' => '/usr/bin',
                                'source_image' => './static/images/'.$arrayLink[4].'/'.$arrayLink[5].'/'.$arrayLink[6].'/'.$arrayLink[7],
                                'quality' => '80%',
                                'maintain_ratio' => false,
                                'x_axis' => 0,
                                'y_axis' => 0,
                                'new_image' => './static/images/'.$arrayLink[4].'/'.$arrayLink[5].'/'.$arrayLink[6].'/medium_'.$filename,
                                'width' =>  240,
                                'height' => 240
                            );

                            $this->image_lib->initialize($config1);
                            if (!$this->image_lib->resize()){
                                $error = array('error' => strip_tags($this->image_lib->display_errors()));
                                $this->response($error, 400);
                            } else {
                                $firstIndex = stripos($filename, '.');
                                $pathinfo =  pathinfo($filename, PATHINFO_EXTENSION);
                                $name = substr($filename, 0, $firstIndex);
                                $data = array(
                                    'base_url'=> base_url(),
                                    'forder' => 'images/'.$year.'/'.$username.'/profileImage/',
                                    'name_image' => $name.'.',
                                    'mime_type' => $pathinfo
                                );
                            }
                        }
                    }
                }

            }
        }
    }


    public function getAllProject_get(){
        $project = $this->Project->getAllProjectCurrent();

        foreach ($project as $key => $value) {
            if($value->thumbnail !== 'https://static.kakuseida.com/images/2018/default/noimage-01.png'){
                $link = $value->thumbnail;
                $arrayLink = explode('/', $link);
                if($arrayLink[2] === 'static.kakuseida.com'){
                    $this->load->helper('directory');
                    // $map = directory_map('./TeampleatEmail',1);
                    //         var_dump($status);
                    $map = directory_map('static/images/'.$arrayLink[4].'/'.$arrayLink[5].'/'.$arrayLink[6].'/'.$arrayLink[7]);
                    $status = 0;
                    if(isset($map) && $map) {
                        foreach ($map as $key => $value2) {
                            if($value2 === 'medium_'.$arrayLink[8]){
                                $status = $status + 1;
                            }
                        }

                        if($status === 0) {

                            $filename = $arrayLink[8];

                            $this->load->library('image_lib');

                            $config1 = array(
                                'image_library' => 'ImageMagick',
                                'library_path' => '/usr/bin',
                                'source_image' => './static/images/'.$arrayLink[4].'/'.$arrayLink[5].'/'.$arrayLink[6].'/'.$arrayLink[7].'/'.$arrayLink[8],
                                'quality' => '80%',
                                'maintain_ratio' => false,
                                'x_axis' => 0,
                                'y_axis' => 0,
                                'new_image' => './static/images/'.$arrayLink[4].'/'.$arrayLink[5].'/'.$arrayLink[6].'/'.$arrayLink[7].'/medium_'.$filename,
                                'width' =>  300,
                                'height' => 300
                            );

                            $this->image_lib->initialize($config1);
                            if (!$this->image_lib->resize()){
                                $error = array('error' => strip_tags($this->image_lib->display_errors()));
                                $this->response($error, 400);
                            } else {
                                $firstIndex = stripos($filename, '.');
                                $pathinfo =  pathinfo($filename, PATHINFO_EXTENSION);
                                $name = substr($filename, 0, $firstIndex);
                                $data = array(
                                    'base_url'=> base_url(),
                                    'forder' => 'images/123/321321/profileImage/',
                                    'name_image' => '321321.',
                                    'mime_type' => $pathinfo
                                );

                            }
                        }
                    }
                }

            }
        }
    }
    public function delAllImageMediumCurrent_get(){
        $project = $this->Project->getAllProjectCurrent();

        foreach ($project as $key => $value) {
            if($value->thumbnail !== 'https://static.kakuseida-test.tk/images/2018/default/noimage-01.png'){
                $link = $value->thumbnail;
                $arrayLink = explode('/', $link);
                if($arrayLink[2] === 'static.kakuseida-test.tk'){

                    $this->load->helper('directory');

                    // $map = directory_map('./TeampleatEmail',1);
                    //         var_dump($status);
                    $map = directory_map('static/images/'.$arrayLink[4].'/'.$arrayLink[5].'/'.$arrayLink[6].'/'.$arrayLink[7]);
                    $status = 0;
                    $map = glob('static/images/'.$arrayLink[4].'/'.$arrayLink[5].'/'.$arrayLink[6].'/'.$arrayLink[7].'/*');
                    if(isset($map) && $map) {
                        foreach ($map as $key => $value2) {
                            // var_dump($value2);
                            $file = 'static/images/'.$arrayLink[4].'/'.$arrayLink[5].'/'.$arrayLink[6].'/'.$arrayLink[7].'/medium_'.$arrayLink[8];

                            if($value2 === $file){
                                unlink($value2);
                            }

                        }
                    }
                }

            }
        }
    }

    public function sendMailTemplate_get(){

        $this->load->library('email');

        $template = $this->Template->getFirstTemplate();
        $arrayEmail = explode(",",$template->email_template);

        $listTemplate = array();
        if(isset($arrayEmail) && $arrayEmail) {
            foreach ($arrayEmail as $key => $value) {

                $data = array(
                    'email' => $value
                );

                if(count($listTemplate) >= 50){
                    break;
                }else {
                    array_push($listTemplate,$data);
                }
                // var_dump($template->id);

            }
        }
        foreach ($listTemplate as $key => $value) {
            var_dump($value);
            $newArrayEmail = array_diff($arrayEmail,array($value['email']));
            $listEmail = implode(",", $newArrayEmail);
            var_dump($listEmail);
            // $template = $this->Template->getFirstTemplate();
            // $arrayEmail = explode(",",$template->email_template);
            //
            // $this->email->from('admin@kakuseida.conoha.io', 'KAKUSEIDA');
            // $this->email->to($value['email']);
            // // $this->email->reply_to('admin@kakuseida.conoha.io', 'KAKUSEIDA');
            // $this->email->subject($template->name_template);
            // $this->email->message($template->message_template);
            //
            // if($this->email->send()){
            //
                // $newArrayEmail = array_diff($arrayEmail,array($value['email']));
                // $listEmail = implode(",", $newArrayEmail);
            //
            //     $dataTemplate = array(
            //         'email_template' => $listEmail
            //     );
            //     $this->Template->update($template->id,$dataTemplate);
            // }
        }
        // var_dump($listTemplate);
        // foreach ($template as $key => $value) {
        //     $arrayEmail = explode(",",$value->email_template);
        //
        //     var_dump($arrayEmail);
        // }
    }


    function TemplateMailChimp_get(){

        $MailChimp = new MailChimp('878c7c0ecbe17a05c26a87b4063ebf61-us20');

        $template = $this->Template->getAllEmailSubscription();
        foreach ($template as $key => $value) {

            $campaign_id = $value->campaign_id;
            $messageTemplate = $value->message_template;
            $list_id = $value->list_id;

            $arrayEmail = explode(",",$value->email_template);

            $listEmail = array();


            foreach ($arrayEmail as $key => $email) {

                if(count($listEmail) > 40){
                    break;
                } else {
                    array_push($listEmail,$email);
                }

            }

            $newArrayEmail = array_diff($arrayEmail,$listEmail);
            $listEmailString = implode(",", $newArrayEmail);
            $dataTemplate = array(
                'email_template' => $listEmailString
            );

            $update = $this->Template->update($value->id,$dataTemplate);

            foreach ($listEmail as $key => $email) {
                $result = $MailChimp->post("lists/$list_id/members", [
                    'email_address' => $email,
                    'status'        => 'subscribed',
                ]);

                if ($MailChimp->success()) {
                } else {
                    $emailError = $value->email_error.''.$email.',';

                    $dataError = array(
                        'email_error' => $emailError
                    );
                    $update = $this->Template->update($value->id,$dataError);
                }
            }

            if($value->email_template === ''){
                $result = $MailChimp->put("campaigns/".$campaign_id."/content", [
                    'html' => $messageTemplate
                ]);

                $dataUpdateTemplate = array(
                    'status_template' => '1'
                );
                $update = $this->Template->update($value->id,$dataUpdateTemplate);

                if($value->status_template === '1') {
                        $send = $MailChimp->post("campaigns/".$campaign_id."/actions/send", [
                            "is_ready" => true
                        ]);
                        if($send) {
                            $dataUpdateTemplate = array(
                                'status' => '1'
                            );
                            $update = $this->Template->update($value->id,$dataUpdateTemplate);
                        }
                }

            }

        }
    }

    public function ProjectExpiredAllInOrAllNothing_get() {

        $this->load->library('email');
        $key_firebase = $this->config->item('key_firebase');
        $roomSocket = $this->config->item('roomSocket');
        $portRoomSocket = $this->config->item('portRoomSocket');
        $email_admin = $this->config->item('email_admin');

        $projects = $this->Project->getProjectExpiredAllin();

        foreach ($projects as $key => $value) {

            $backed = $this->BackedProject->getBackedProjectByProjectId($value->id);

            $emailUser = array();

            if($value->status_all_in === '1') {

                if($value->status_send_owner === '0'){

                    $this->email->from('admin@kakuseida.conoha.io', 'KAKUSEIDA');
                    $this->email->to($value->email);
                    // $this->email->reply_to('admin@kakuseida.conoha.io', 'KAKUSEIDA');
                    $this->email->subject($value->username.'様が企画した「'.$value->project_name.'」プロジェクトの期間が終了になりました。');
                    $this->email->message(

                        $value->username.'様<br>
                        <br>
                        お世話になっております。<br>
                        KAKUSEIDA事務局です。<br>
                        <br>
                        '.$value->username.'様が企画した「'.$value->project_name.'」プロジェクトの期間が終了になりました。<br>
                        期間中に集まった支援総額は'.number_format($value->collected_amount).'円です。<br>
                        <br>
                        当事務局で金額を確認し、手数料を計算後に銀行口座に振り込みを行います。<br>
                        数日お待ちいただきますが、よろしくお願い致します。<br>
                        振込の際はまたご連絡させていただきます。<br>'

                    );

                    $datetime = new DateTime();
                    $datetime = $datetime->format('Y-m-d H:i:s');
                    // get strtotime + change time zone
                    $asia_timestamp = strtotime($datetime);
                    date_default_timezone_set('UTC');
                    // get Date vs time zone
                    $date = date("Y-m-d H:i:s", $asia_timestamp);

                    // array create notification info
                    $dataNotification = array (
                        'user_id'       => $value->user_id,
                        'user_action'   => 'Admin',
                        'title'         => '過去のプロジェクト',
                        'message'       => '企画した「'.$value->project_name.'」プロジェクトの期間が終了しました。ご確認ください。',
                        'link'          => "project-detail/".$value->id,
                        'status'        => 0,
                        'thumbnail'     => base_url()."images/2018/default/logo-icon.png",
                        'created'       => $date
                    );
                    // create notification info
                    $createNotification = $this->Notification_info->create($dataNotification);

                    $socketio = new SocketIO();
                    // // array
                    $data = array(
                        'room' => 'project_'.$value->id,
                        'title' => '過去のプロジェクト',
                        'message' => '企画した「'.$value->project_name.'」プロジェクトの期間が終了しました。ご確認ください。',
                        'url'=> app_url()."project-detail/".$value->id,
                        'ava' => base_url().'images/2018/default/logo-icon.png'
                    );


                    $socketio->send($roomSocket, $portRoomSocket, 'annouce_project',json_encode($data));

                    $curl = curl_init();

                    curl_setopt_array($curl, array(
                      CURLOPT_URL => "https://fcm.googleapis.com/fcm/send",
                      CURLOPT_RETURNTRANSFER => true,
                      CURLOPT_ENCODING => "",
                      CURLOPT_MAXREDIRS => 10,
                      CURLOPT_TIMEOUT => 30,
                      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                      CURLOPT_CUSTOMREQUEST => "POST",
                      CURLOPT_POSTFIELDS => "{   \"notification\": {\r\n        \"title\": \"\",\r\n        \"body\": \"企画した「".$value->project_name."」プロジェクトの期間が終了しました。ご確認ください。\",\r\n        \"click_action\": \"".app_url()."project-detail/".$value->id."\",\r\n        \"icon\": \"".base_url()."images/2018/default/logo-icon.png\"\r\n    },\r\n  \"to\" : \"/topics/project_".$value->id."\"\r\n}",
                      CURLOPT_HTTPHEADER => array(
                        "Authorization: key=".$key_firebase."",
                        "Content-Type: application/json",
                      ),
                    ));

                    $response = curl_exec($curl);
                    $err = curl_error($curl);

                    if($this->email->send()) {

                        $dataEditProject = array(
                            'status_send_owner' => '1'
                        );
                        $editBacked = $this->Project->update($value->id,$dataEditProject);
                    }
                }

                if($value->status_send_admin === '0'){

                    $this->email->from('admin@kakuseida.conoha.io', 'KAKUSEIDA');
                    $this->email->to($email_admin);
                    // $this->email->reply_to('admin@kakuseida.conoha.io', 'KAKUSEIDA');
                    $this->email->subject($value->username.'様が企画した「'.$value->project_name.'」プロジェクトの期間が終了になりました。');
                    $this->email->message(

                        $value->username.'の「'.$value->project_name.'」プロジェクトの期間が終了になりました。<br>
                        支援の合計：'.number_format($value->collected_amount).'円です。<br>
                        支援総額から振込手数料を引いた金額を企画者の銀行口座に振り込んでください。<br>'

                    );
                    if($this->email->send()) {
                        $dataEditBacked = array(
                            'status_send_admin' => '1'
                        );
                        $editBacked = $this->Project->update($value->id,$dataEditBacked);
                    }

                }

                if(isset($backed) && $backed) {

                    foreach ($backed as $key => $user) {

                        if(count($emailUser) >= 10 ) {
                            break;
                        }else {
                            array_push($emailUser,$user);
                        }

                    }

                    foreach ($emailUser as $key => $email) {
                        $this->email->from('admin@kakuseida.conoha.io', 'KAKUSEIDA');
                        $this->email->to($email->emailUser);
                        // $this->email->reply_to('admin@kakuseida.conoha.io', 'KAKUSEIDA');
                        $this->email->subject('【KAKUSEIDA】本人確認のお知らせ');
                        $this->email->message(

                            $email->username.'様<user_id>
                            <br>
                            お世話になっております。<br>
                            KAKUSEIDA事務局です。<br>
                            <br>
                            '.$email->username.'様が支援されました「'.$value->project_name.'」プロジェクトの期間が終了になりました。<br>
                            期間中に集まった支援総額は'.number_format($value->collected_amount).'円です。<br>
                            <br>
                            プロジェクトのリターン品を送付されていない場合は、企画者からの連絡をお待ちください。<br>
                            企画者より順次リターン品が発送されます。<br>'

                        );

                        $datetime = new DateTime();
                        $datetime = $datetime->format('Y-m-d H:i:s');
                        // get strtotime + change time zone
                        $asia_timestamp = strtotime($datetime);
                        date_default_timezone_set('UTC');
                        // get Date vs time zone
                        $date = date("Y-m-d H:i:s", $asia_timestamp);

                        // array create notification info
                        $dataNotification = array (
                            'user_id'       => $email->user_id,
                            'user_action'   => 'Admin',
                            'title'         => '過去のプロジェクト',
                            'message'       => '支援した「'.$value->project_name.'」プロジェクトの期間が終了しました。ご確認ください。',
                            'link'          => "project-detail/".$value->id,
                            'status'        => 0,
                            'thumbnail'     => base_url()."images/2018/default/logo-icon.png",
                            'created'       => $date
                        );
                        // create notification info
                        $createNotification = $this->Notification_info->create($dataNotification);

                        $socketio = new SocketIO();
                        // // array
                        $data = array(
                            'room' => 'user_'.$email->user_id,
                            'title' => '過去のプロジェクト',
                            'message' => '支援した「'.$value->project_name.'」プロジェクトの期間が終了しました。ご確認ください。',
                            'url'=> app_url()."project-detail/".$value->id,
                            'ava' => base_url().'images/2018/default/logo-icon.png'
                        );


                        $socketio->send($roomSocket, $portRoomSocket, 'annouce_project',json_encode($data));

                        $curl = curl_init();

                        curl_setopt_array($curl, array(
                          CURLOPT_URL => "https://fcm.googleapis.com/fcm/send",
                          CURLOPT_RETURNTRANSFER => true,
                          CURLOPT_ENCODING => "",
                          CURLOPT_MAXREDIRS => 10,
                          CURLOPT_TIMEOUT => 30,
                          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                          CURLOPT_CUSTOMREQUEST => "POST",
                          CURLOPT_POSTFIELDS => "{   \"notification\": {\r\n        \"title\": \"\",\r\n        \"body\": \"支援した「".$value->project_name."」プロジェクトの期間が終了しました。ご確認ください。\",\r\n        \"click_action\": \"".app_url()."project-detail/".$value->id."\",\r\n        \"icon\": \"".base_url()."images/2018/default/logo-icon.png\"\r\n    },\r\n  \"to\" : \"".$email->token."\"\r\n}",
                          CURLOPT_HTTPHEADER => array(
                            "Authorization: key=".$key_firebase."",
                            "Content-Type: application/json",
                          ),
                        ));

                        $response = curl_exec($curl);
                        $err = curl_error($curl);


                        if($this->email->send()) {
                            $dataEditBacked = array(
                                'status_finish_project' => '1'
                            );
                            $editBacked = $this->BackedProject->update($email->id,$dataEditBacked);
                        }

                    }
                }else{
                    $updateProject = array(
                        'status_finish_all_in' => '1'
                    );
                    $editBacked = $this->Project->update($value->id,$updateProject);
                }
            } else {

                if($value->status_send_owner === '0'){

                    $this->email->from('admin@kakuseida.conoha.io', 'KAKUSEIDA');
                    $this->email->to($value->email);
                    // $this->email->reply_to('admin@kakuseida.conoha.io', 'KAKUSEIDA');
                    $this->email->subject($value->username.'様が企画した「'.$value->project_name.'」プロジェクトの期間が終了になりました。');
                    $this->email->message(

                        $value->username.'様<br>
                        <br>
                        お世話になっております。<br>
                        KAKUSEIDA事務局です。<br>
                        <br>
                        '.$value->username.'様が企画した「'.$value->project_name.'」プロジェクトの期間が終了になりました。<br>
                        期間中に集まった支援総額は'.number_format($value->collected_amount).'円です。<br>
                        目標金額を達成できませんでしたので、全ての支援者に返金を行います。<br>
                        <br>
                        当事務局で金額を確認し、手数料を計算後に各支援者の銀行口座に振り込みを行います。<br>
                        全支援者への返金が完了した際には、ご連絡致します。<br>'

                    );

                    $datetime = new DateTime();
                    $datetime = $datetime->format('Y-m-d H:i:s');
                    // get strtotime + change time zone
                    $asia_timestamp = strtotime($datetime);
                    date_default_timezone_set('UTC');
                    // get Date vs time zone
                    $date = date("Y-m-d H:i:s", $asia_timestamp);

                    // array create notification info
                    $dataNotification = array (
                        'user_id'       => $value->user_id,
                        'user_action'   => 'Admin',
                        'title'         => '過去のプロジェクト',
                        'message'       => '企画した「'.$value->project_name.'」プロジェクトの期間が終了しました。ご確認ください。',
                        'link'          => "project-detail/".$value->id,
                        'status'        => 0,
                        'thumbnail'     => base_url()."images/2018/default/logo-icon.png",
                        'created'       => $date
                    );
                    // create notification info
                    $createNotification = $this->Notification_info->create($dataNotification);

                    $socketio = new SocketIO();
                    // // array
                    $data = array(
                        'room' => 'project_'.$value->id,
                        'title' => '過去のプロジェクト',
                        'message' => '企画した「'.$value->project_name.'」プロジェクトの期間が終了しました。ご確認ください。',
                        'url'=> app_url()."project-detail/".$value->id,
                        'ava' => base_url().'images/2018/default/logo-icon.png'
                    );


                    $socketio->send($roomSocket, $portRoomSocket, 'annouce_project',json_encode($data));

                    $curl = curl_init();

                    curl_setopt_array($curl, array(
                      CURLOPT_URL => "https://fcm.googleapis.com/fcm/send",
                      CURLOPT_RETURNTRANSFER => true,
                      CURLOPT_ENCODING => "",
                      CURLOPT_MAXREDIRS => 10,
                      CURLOPT_TIMEOUT => 30,
                      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                      CURLOPT_CUSTOMREQUEST => "POST",
                      CURLOPT_POSTFIELDS => "{   \"notification\": {\r\n        \"title\": \"\",\r\n        \"body\": \"企画した「".$value->project_name."」プロジェクトの期間が終了しました。ご確認ください。\",\r\n        \"click_action\": \"".app_url()."project-detail/".$value->id."\",\r\n        \"icon\": \"".base_url()."images/2018/default/logo-icon.png\"\r\n    },\r\n  \"to\" : \"/topics/project_".$value->id."\"\r\n}",
                      CURLOPT_HTTPHEADER => array(
                        "Authorization: key=".$key_firebase."",
                        "Content-Type: application/json",
                      ),
                    ));

                    $response = curl_exec($curl);
                    $err = curl_error($curl);

                    if($this->email->send()) {

                        $dataEditProject = array(
                            'status_send_owner' => '1'
                        );
                        $editBacked = $this->Project->update($value->id,$dataEditProject);
                    }
                }

                if($value->status_send_admin === '0'){

                    $this->email->from('admin@kakuseida.conoha.io', 'KAKUSEIDA');
                    $this->email->to($email_admin);
                    // $this->email->reply_to('admin@kakuseida.conoha.io', 'KAKUSEIDA');
                    $this->email->subject($value->username.'様が企画した「'.$value->project_name.'」プロジェクトの期間が終了になりました。');
                    $this->email->message(

                        $value->username.'の「'.$value->project_name.'」プロジェクトの期間が終了になりました。<br>
                        支援の合計：'.number_format($value->collected_amount).'円です。<br>
                        目標金額に達成しなかった為、支援者に返金をおこなってください<br>'

                    );
                    if($this->email->send()) {
                        $dataEditBacked = array(
                            'status_send_admin' => '1'
                        );
                        $editBacked = $this->Project->update($value->id,$dataEditBacked);
                    }

                }

                if(isset($backed) && $backed) {


                    foreach ($backed as $key => $user) {

                        if(count($emailUser) >= 20 ) {
                            break;
                        }else {
                            array_push($emailUser,$user);
                        }

                    }

                    foreach ($emailUser as $key => $email) {
                        $this->email->from('admin@kakuseida.conoha.io', 'KAKUSEIDA');
                        $this->email->to($email->emailUser);
                        // $this->email->reply_to('admin@kakuseida.conoha.io', 'KAKUSEIDA');
                        $this->email->subject('【KAKUSEIDA】本人確認のお知らせ');
                        $this->email->message(

                            $email->username.'様<user_id>
                            <br>
                            お世話になっております。<br>
                            KAKUSEIDA事務局です。<br>
                            <br>
                            '.$email->username.'様が支援されました「'.$value->project_name.'」プロジェクトの期間が終了になりました。<br>
                            期間中に集まった支援総額は'.number_format($value->collected_amount).'円です。<br>
                            残念ながら目標金額を達成できなかったため、支援者の方に返金を行います。<br>
                            <br>
                            当事務局で金額を確認し、手数料を計算後に銀行口座に振り込みを行います。<br>
                            数日お待ちいただきますが、よろしくお願い致します。<br>
                            振込の際はまたご連絡させていただきます。<br>'

                        );

                        $datetime = new DateTime();
                        $datetime = $datetime->format('Y-m-d H:i:s');
                        // get strtotime + change time zone
                        $asia_timestamp = strtotime($datetime);
                        date_default_timezone_set('UTC');
                        // get Date vs time zone
                        $date = date("Y-m-d H:i:s", $asia_timestamp);

                        // array create notification info
                        $dataNotification = array (
                            'user_id'       => $email->user_id,
                            'user_action'   => 'Admin',
                            'title'         => '過去のプロジェクト',
                            'message'       => '支援した「'.$value->project_name.'」プロジェクトの期間が終了しました。ご確認ください。',
                            'link'          => "project-detail/".$value->id,
                            'status'        => 0,
                            'thumbnail'     => base_url()."images/2018/default/logo-icon.png",
                            'created'       => $date
                        );
                        // create notification info
                        $createNotification = $this->Notification_info->create($dataNotification);

                        $socketio = new SocketIO();
                        // // array
                        $data = array(
                            'room' => 'user_'.$email->user_id,
                            'title' => '過去のプロジェクト',
                            'message' => '支援した「'.$value->project_name.'」プロジェクトの期間が終了しました。ご確認ください。',
                            'url'=> app_url()."project-detail/".$value->id,
                            'ava' => base_url().'images/2018/default/logo-icon.png'
                        );


                        $socketio->send($roomSocket, $portRoomSocket, 'annouce_project',json_encode($data));

                        $curl = curl_init();

                        curl_setopt_array($curl, array(
                          CURLOPT_URL => "https://fcm.googleapis.com/fcm/send",
                          CURLOPT_RETURNTRANSFER => true,
                          CURLOPT_ENCODING => "",
                          CURLOPT_MAXREDIRS => 10,
                          CURLOPT_TIMEOUT => 30,
                          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                          CURLOPT_CUSTOMREQUEST => "POST",
                          CURLOPT_POSTFIELDS => "{   \"notification\": {\r\n        \"title\": \"\",\r\n        \"body\": \"支援した「".$value->project_name."」プロジェクトの期間が終了しました。ご確認ください。\",\r\n        \"click_action\": \"".app_url()."project-detail/".$value->id."\",\r\n        \"icon\": \"".base_url()."images/2018/default/logo-icon.png\"\r\n    },\r\n  \"to\" : \"".$email->token."\"\r\n}",
                          CURLOPT_HTTPHEADER => array(
                            "Authorization: key=".$key_firebase."",
                            "Content-Type: application/json",
                          ),
                        ));

                        $response = curl_exec($curl);
                        $err = curl_error($curl);


                        if($this->email->send()) {
                            $dataEditBacked = array(
                                'status_finish_project' => '1'
                            );
                            $editBacked = $this->BackedProject->update($email->id,$dataEditBacked);
                        }

                    }
                }else{
                    $updateProject = array(
                        'status_finish_all_in' => '1'
                    );
                    $editBacked = $this->Project->update($value->id,$updateProject);
                }
            }
            break;
        }

    }

    public function BackedBlockUser_get(){
        $this->load->library('email');
        $user = $this->User->getAllUserBlock();
        foreach ($user as $key => $value) {

            $emailArray = array();

            $backed = $this->BackedProject->getProjectBlockByUser_id($value->id);
            var_dump($value->id);

            if(isset($backed) && $backed)  {
                if($value->status_block === '0') {

                    $this->email->from('admin@kakuseida.conoha.io', 'KAKUSEIDA');
                    $this->email->to($value->email);
                    // $this->email->reply_to('admin@kakuseida.conoha.io', 'KAKUSEIDA');
                    $this->email->subject($value->username.'様は、総合的な理由により、アカウントを停止されました。');
                    $this->email->message(
                        $value->username.'様<br>
                        <br>
                        お世話になっております。<br>
                        KAKUSEIDA事務局です。<br>
                        <br>
                        '.$value->username.'様は、総合的な理由により、アカウントを停止されました。<br>
                        本日以降、'.$value->username.'様はプロジェクトの企画、支援ができなくなります。<br>
                        支援中のリターン品につきましては正常に送付されます。<br>
                        <br>
                        KAKUSEIDAのご利用ありがとうございました。<br>'
                    );

                    if($this->email->send()){
                        $updateUser = array(
                            'status_block' => '1'
                        );
                        $this->User->update($value->id,$updateUser);
                    }
                }


                foreach ($backed as $key => $backing) {

                    $getUser = $this->User->getUserId($backing->IdUserOwner);
                    $newData = array(
                        'user_id'       =>  $backing->user_id,
                        'email'         =>  $backing->emailUser,
                        'username'      =>  $backing->username,
                        'usernameOnwer' =>  $getUser->username,
                        'emailOnwer'    =>  $getUser->email,
                        'project_id'    =>  $backing->project_id,
                        'project_name'  =>  $backing->project_name,
                    );
                    $checkListEmail = in_array($newData,$emailArray);
                    if(!$checkListEmail){
                        array_push($emailArray,$newData);
                    }
                }
            } else {
                // $array
            }

            foreach ($emailArray as $key => $email) {
                $this->email->from('admin@kakuseida.conoha.io', 'KAKUSEIDA');
                $this->email->to($email['emailOnwer']);
                // $this->email->reply_to('admin@kakuseida.conoha.io', 'KAKUSEIDA');
                $this->email->subject($email['username'].'様が総合的な理由によりアカウントを停止されました。');
                $this->email->message(

                    $email['usernameOnwer'].'様<br>
                    <br>
                    お世話になっております。<br>
                    KAKUSEIDA事務局です。<br>
                    <br>
                    '.$email['usernameOnwer'].'様が公開中のプロジェクト「'.$email['project_name'].'」に支援中の'.$email['username'].'様が総合的な理由によりアカウントを停止されました。<br>
                    本日より'.$email['username'].'様は追加の支援を行うことができません。<br>
                    アカウント停止前の支援は有効ですので、リターン品の送付をお願い致します。<br>
                    <br>
                    ご質問等ありましたらKAKUSEIDA運営事務局までお問い合わせください。<br>
                    今後ともKAKUSEIDAをよろしくお願い致します。<br>'
                );

                if($this->email->send()) {
                    $updateBacked = array(
                        'status_owner_block' => '1'
                    );
                    $this->BackedProject->updateBackedByProjectVsUser($email['user_id'],$email['project_id'],$updateBacked);
                }
            }
        }
    }

    public function ProjectUserBlock_get() {

        $this->load->library('email');
        $user = $this->User->getAllUserBlock();
        foreach ($user as $key => $value) {
            // var_dump($value->id);
            $project = $this->Project->getProjectBlockPublic($value->id);

            if(isset($project) && $project){

                foreach ($project as $key => $dataProject) {
                    if($dataProject->status_send_block_user === '0') {

                        $this->email->from('admin@kakuseida.conoha.io', 'KAKUSEIDA');
                        $this->email->to($dataProject->email);
                        // $this->email->reply_to('admin@kakuseida.conoha.io', 'KAKUSEIDA');
                        $this->email->subject($dataProject->username.'様は、総合的な理由により、アカウントを停止されました。');
                        $this->email->message(
                            $dataProject->username.'様<br>
                            <br>
                            お世話になっております。<br>
                            KAKUSEIDA事務局です。<br>
                            <br>
                            '.$dataProject->username.'様は、総合的な理由により、アカウントを停止されました。 <br>
                            公開中のプロジェクト「'.$dataProject->project_name.'」は非公開となり、支援金は支援者に返金されます。<br>
                            支援者への返金完了時にはご連絡致します。<br>
                            <br>
                            KAKUSEIDAのご利用ありがとうございました。<br>'
                        );

                        if($this->email->send()){
                            $updateProject = array(
                                'status_send_block_user' => '1'
                            );
                            $this->Project->update($dataProject->id,$updateProject);
                        }

                    }

                    $emailSend = array();

                    $backed = $this->BackedProject->getProjectBlockByProjectId($dataProject->id);

                    if(isset($backed) && $backed) {

                        foreach ($backed as $key => $value) {

                            $newData = array(
                                'user_id'       =>  $value->user_id,
                                'email'         =>  $value->emailUser,
                                'username'      =>  $value->username,
                                'username'      =>  $value->username,
                                'project_id'    =>  $value->project_id,
                                'project_name'  =>  $value->project_name
                            );
                            $checkListEmail = in_array($newData,$emailSend);
                            if(!$checkListEmail){
                                array_push($emailSend,$newData);
                            }
                        }
                    } else {
                        $updateProject = array(
                            'status_finish_block_user' => '1'
                        );
                        $this->Project->update($dataProject->id,$updateProject);
                    }

                    foreach ($emailSend as $key => $value) {

                        $this->email->from('admin@kakuseida.conoha.io', 'KAKUSEIDA');
                        $this->email->to($value['email']);
                        // $this->email->reply_to('admin@kakuseida.conoha.io', 'KAKUSEIDA');
                        $this->email->subject($value['username'].'様が支援されているプロジェクト「'.$value['project_name'].'」は、企画者による総合的な理由により中止となりました。');
                        $this->email->message(

                            $value['username'].'様<br>
                            <br>
                            お世話になっております。<br>
                            KAKUSEIDA事務局です。<br>
                            <br>
                            '.$value['username'].'様が支援されているプロジェクト「'.$value['project_name'].'」は、企画者による総合的な理由により中止となりました。<br>
                            全ての支援金は返金されます。<br>
                            <br>
                            当事務局で金額を確認し、手数料を計算後に銀行口座に振り込みを行います。<br>
                            数日お待ちいただきますが、よろしくお願い致します。<br>
                            振込の際はまたご連絡させていただきます。<br>'
                        );
                        if($this->email->send()) {
                            $updateBacked = array(
                                'status_block_user' => '1'
                            );
                            $this->BackedProject->updateBackedByProjectVsUser($value['user_id'],$value['project_id'],$updateBacked);
                        }
                    }
                }
            }
        }
    }

}
