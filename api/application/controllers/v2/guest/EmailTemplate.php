<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class EmailTemplate extends BD_Controller {
    function __construct() {
        // Construct the parent class
        parent::__construct();
    }

    public function getEmailTemplate_get(){
        $this->load->helper('directory');
        $map = directory_map('./static/images/TeampleatEmail');
        $countMap = count($map);
            // var_dump($map);
            $data = [];
            foreach ($map as $key => $value) {
                array_push($data,$value);

            }
        $newdata = array(
            'data' => $data
        );
        $this->response($newdata,200);
    }
}
