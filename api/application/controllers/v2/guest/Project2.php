<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Project2 extends BD_Controller {
    function __construct() {
        // Construct the parent class
        parent::__construct();
    }
    public function checkProjectReturn_get(){

        $project_id = $this->get('project_id');
        $backing_level_id = $this->get('backing_level_id');

        if(isset($project_id) && isset($backing_level_id)){

            $existproject = $this->Project->checkProjectId($project_id);
            $existprojectretrun = $this->BackingLevel->getBackingId($backing_level_id);

            if(isset($existproject) && $existproject){
                if($existproject->project_type === '0'){
                    $getTime = $this->getTime_get($existproject->id);
                    $existproject->format_collection_end_date = $getTime;
                    if(isset($existproject->format_collection_end_date['status']) && $existproject->format_collection_end_date['status'] === '終了' ){
                        $error = array(
                            'status'=>'Project Expired!'
                        );
                        $this->response($error,404);
                    }
                }
                if(isset($existprojectretrun) && $existprojectretrun){
                    if($existprojectretrun->project_id === $existproject->id) {

                        $project_return = $this->BackingLevel->getBackingId($backing_level_id);
                        $project_return->project_type = $existproject->project_type;
                        $this->response($project_return,200);

                    }else{
                        $error = array(
                            'status'=>'ProjectReturn or Project not found!'
                        );
                        $this->response($error,404);
                    }
                }else{
                    $error = array(
                        'status'=>'ProjectReturn not exists!'
                    );
                    $this->response($error,404);
                }
            }else{
                $error = array(
                    'status'=>'Project not exists!'
                );
                $this->response($error,404);
            }
        }else{
            $error = array(
                'status'=>'This id is not exists!'
            );
            $this->response($error,404);
        }

    }
}
?>
