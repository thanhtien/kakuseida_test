<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class AddressUsers extends UserController {
    function __construct()
    {
        // Construct the parent class
        parent::__construct();
        $this->auth();
    }

    /**
     * [user_profile_get Get User Current folow Auth(MY_Controller)]
     * @return [type] [description]
     */
    public function userProfile_get() {
        // Get Auth -> Get User_id
        $user = $this->user_data;
        $user_id =  $user->id;
        // Get Profile
        // $checkUser = $this->User->checkSocical();
        $curUser =  $this->User->getUserId($user_id);
        if(isset($curUser) && $curUser) {
            // Get Group Current(ion_auth)
            $group = $this->ion_auth->get_users_groups($user_id)->row();
            // Get Group Name
            $group_name = $group->name;
            // Count MY Project
            $numberProject = $this->Project->getProjectUserId($user_id);

            $numberPatron = $this->BackedProject->countBackedInfoDonate($user_id);
            // Create Object
            $safeUserObject = null;

            $listAddress = $this->AddressUser->getAddressUserId($user_id);

            $FormatImage = getImage($curUser->profileImageURL);

            $numberNotification = $this->Notification_info->getNumberNotification($user_id);

            $listFan = $this->ListDonateFanClub->getAllDonateFanClub($user_id);
            $numberPatronfanclub = $listFan['total'];
            $new = '';

            foreach ($listFan['data'] as $key => $value) {
                if($value->status_join_fanclub === '0'){
                    if($new === '') {
                        $new = $value->backing_levels_id;
                    }else{
                        $new = $new.','.$value->backing_levels_id;
                    }
                }
            }

            $safeUserObject = array(
                "id"                 =>     $curUser->id,
                "username"           =>     $curUser->username,
                "roles"              =>     $group_name,
                'profileImageURL'    =>     $curUser->profileImageURL,
                'medium_profileImageURL' => $FormatImage['medium'],
                'small_profileImageURL' =>  $FormatImage['small'],
                'created'            =>     $curUser->created_on,
                'sex'                =>     $curUser->sex,
                'birthday'           =>     $curUser->birthday,
                'self_description'   =>     $curUser->self_description,
                'url1'               =>     $curUser->url1,
                'url2'               =>     $curUser->url2,
                'url3'               =>     $curUser->url3,
                'address'            =>     $curUser->address,
                'job'                =>     $curUser->job,
                'email'              =>     $curUser->email,
                'bank_number'        =>     $curUser->bank_number,
                'bank_owner'         =>     $curUser->bank_owner,
                'bank_name'          =>     $curUser->bank_name,
                'bank_branch'        =>     $curUser->bank_branch,
                'bank_type'          =>     $curUser->bank_type,
                'country'            =>     $curUser->country,
                'eventProjectSocket' =>     $curUser->eventProjectSocket,
                'numberProject'      =>     count($numberProject),
                'patron'             =>     count($numberPatron),
                'patronfanclub'      =>     $numberPatronfanclub,
                'listFanClub'        =>     $new,
                'tokenRefest'        =>     $this->user_data->tokenRefest,
                'listAddress'        =>     $listAddress,
                'number_notification'=>     $numberNotification,
                'statusUnsubscribe'  =>     $curUser->unsubscribe,
                'unsubscribeProject' =>     $curUser->unsubscribeProject,
                'wish_list'          =>     $curUser->wish_list,
            );
            if(isset($curUser->customer_id) && $curUser->customer_id ) {
                $safeUserObject['number_card'] = '************'.$curUser->last4;
                $safeUserObject['exp_month'] = $curUser->card_month;
                $safeUserObject['exp_year'] = $curUser->card_year;
                $safeUserObject['brand'] = $curUser->card_brand;
                $safeUserObject['cvc'] = $curUser->cvc;
            }else{
                $safeUserObject['number_card'] = '';
                $safeUserObject['exp_month'] = '';
                $safeUserObject['exp_year'] = '';
                $safeUserObject['brand'] = '';
                $safeUserObject['cvc'] = '';
            }

            $this->response($safeUserObject, 200);

        }else{

            $error = array('status'=>'user is not exists!');
            $this->response($error, 404);
        }
    }

    /**
    * [createAddressUser_post Create Address for User]
    * @return [type] [Create Address]
    */
    public function createAddressUser_post(){
        // get User Id = auth

        $user = $this->user_data;
        $user_id =  $user->id;
        // get date
        $date = new DateTime();
        //fomat date
        $date = $date->format('Y-m-d H:i:s');
        // config of form_validation
        $config = [
            [
                'field' => 'postcode',
                'label' => 'Post Coe',
                'rules' => 'required',
                'errors' => [
                  'required' => '郵便番号を入力してください',
                ],
            ],
            [
                'field' => 'address_return',
                'label' => 'Address Return',
                'rules' => 'required',
                'errors' => [
                  'required' => '住所を入力してください',
                ],
            ],
            [
                'field' => 'phone',
                'label' => 'Phone',
                'rules' => 'required',
                'errors' => [
                  'required' => '電話番号を入力してください',
                ],
            ],
            [
                'field' => 'name_return',
                'label' => 'Name Return',
                'rules' => 'required',
                'errors' => [
                  'required' => '名前を入力してください',
                ],
            ],
            [
                'field' => 'email',
                'label' => 'Email',
                'rules' => 'required|valid_email',
                'errors' => [
                  'required' => 'メールアドレスを入力してください',
                  'valid_email' => 'メールアドレスが正しくないので、確認してください',
                ],
            ],
        ];
        // set data and rules for form_validation
        $data = $this->post();
        $this->form_validation->set_data($data);
        $this->form_validation->set_rules($config);
        // Validation = true
        if ($this->form_validation->run() !== false) {
            // $data for create AddressUser
            $checkListAddressUser = $this->AddressUser->getAddressUserId($user_id);
            $countListAddressUser = count($checkListAddressUser);
            if(isset($checkListAddressUser) && $checkListAddressUser){
                if($countListAddressUser >= 5){
                    $error = array(
                        'status'=>'住所登録数は最大5件となっております。新規追加の際には、既存の住所録の中から1件削除して追加してください。'
                    );
                    $this->response($error,400);
                }else{
                    $data = array(
                        'user_id'         =>    $user_id,
                        "postcode"        =>    $this->post('postcode'),
                        "address_return"  =>    $this->post('address_return'),
                        "phone"           =>    $this->post('phone'),
                        "name_return"     =>    $this->post('name_return'),
                        "email"           =>    $this->post('email'),
                        "chosen_default"  =>    0,
                        "created"         =>    $date,
                        "updated"         =>    $date
                    );
                }
            }else{
                $data = array(
                    'user_id'         =>     $user_id,
                    "postcode"        =>    $this->post('postcode'),
                    "address_return"  =>    $this->post('address_return'),
                    "phone"           =>    $this->post('phone'),
                    "name_return"     =>    $this->post('name_return'),
                    "email"           =>    $this->post('email'),
                    "chosen_default"  =>    1,
                    "created"         =>    $date,
                    "updated"         =>    $date
                );
            }
            // Create = model
            $idCreateAddress = $this->AddressUser->createAddressUser($data);
            // Success
            if($idCreateAddress){
                $default = $this->post('setDefault');
                if(isset($default) && $default === 'true'){
                    $this->chosenDefault2_get($idCreateAddress);
                }
                // Return data
                $userProfile = $this->userProfile_get();
                $this->response($userProfile,200);
                // Error
            }else{
                // Return error
                $error = array(
                    'status'=>'失敗！エラーがありましたので、住所がまだ保存されなません。再度試してください。'
                );
                $this->response($error,400);
            }
            // Form Validation error
        }else{
            // Return error of form validation
            $error = $this->form_validation->error_array();
            $this->response($error,400);
        }
    }

    /**
    * [chosenDefault_get Chosen 1 address is default for user]
    * @return [type] [description]
    */
    public function chosenDefault_get(){
        // Get id ( id of address_user)
        $id = $this->get('id');
        // check $id is exists
        if(isset($id) && $id){
            // Get User_id from Auth
            $user = $this->user_data;
            $user_id =  $user->id;
            $date = new DateTime();
            //fomat date
            $date = $date->format('Y-m-d H:i:s');
            // Get Address curent from address_user
            $address = $this->AddressUser->getAddressUserId($user_id,$id);
            // Check $address
            if(isset($address) && $address){
                // Check $address->user_id === $user_id
                if($address->user_id === trim($user_id)){
                    $listAddress = $this->AddressUser->getAddressUserId($user_id);
                    foreach ($listAddress as $key => $value) {
                        if($value->id === trim($id)){
                            $data = array(
                                'chosen_default' => 1,
                                "updated"        =>$date
                            );
                            $update = $this->AddressUser->editAddressUser($value->id,$data);
                        }else{
                            $data = array(
                                'chosen_default' => 0
                            );
                            $update = $this->AddressUser->editAddressUser($value->id,$data);
                        }
                    }
                    // $newListAddress = $this->AddressUser->getAddressUserId($user_id);
                    // $this->response($newListAddress,200);

                    $userProfile = $this->userProfile_get();
                    $this->response($userProfile,200);
                }else{
                    $error = array(
                        'status' => 'error!'
                    );
                    $this->response($error,400);
                }
            }else{
                $error = array(
                    'status' => 'This id is ánot exists!'
                );
                $this->response($error,400);
            }
        }else{
            $error = array(
                'status' => 'This id is not exists!'
            );
            $this->response($error,400);
        }
    }


    public function chosenDefault2_get($id){
        // check $id is exists
        if(isset($id) && $id){
            // Get User_id from Auth
            $user = $this->user_data;
            $user_id =  $user->id;
            $date = new DateTime();
            //fomat date
            $date = $date->format('Y-m-d H:i:s');
            // Get Address curent from address_user
            $address = $this->AddressUser->getAddressUserId($user_id,$id);
            // Check $address
            if(isset($address) && $address){
                // Check $address->user_id === $user_id
                if($address->user_id === trim($user_id)){
                    $listAddress = $this->AddressUser->getAddressUserId($user_id);
                    foreach ($listAddress as $key => $value) {
                        if($value->id == trim($id)){
                            $data = array(
                                'chosen_default' => 1,
                                "updated"        =>$date
                            );
                            $update = $this->AddressUser->editAddressUser($value->id,$data);
                        }else{
                            $data = array(
                                'chosen_default' => 0
                            );
                            $update = $this->AddressUser->editAddressUser($value->id,$data);
                        }
                    }
                    // $newListAddress = $this->AddressUser->getAddressUserId($user_id);
                    // $this->response($newListAddress,200);

                    $userProfile = $this->userProfile_get();
                    $this->response($userProfile,200);
                }else{
                    $error = array(
                        'status' => 'id nay khong thuoc user hien tai'
                    );
                    $this->response($error,400);
                }
            }else{
                $error = array(
                    'status' => 'This id is ánot exists!'
                );
                $this->response($error,400);
            }
        }else{
            $error = array(
                'status' => 'This id is not exists!'
            );
            $this->response($error,400);
        }
    }


    public function listAddressUser_get(){
        $user = $this->user_data;
        $user_id =  $user->id;
        // $default =
        // $default = $this->chosenDefault_get($id ='22');
        // var_dump($default);
        $curUser =   $this->User->getUserId($user_id);
        if(isset($curUser) && $curUser){
            $userProfile = $this->userProfile_get();
            $this->response($userProfile,200);
        }else{
            $error = array(
                'status' => 'This id is not exists!'
            );
            $this->response($error,400);
        }
    }

    public function editAddressUser_put(){
        $id = $this->put('id');
        if(isset($id) && $id){
            $user = $this->user_data;
            $user_id =  $user->id;
            $curUser =   $this->User->getUserId($user_id);
            if(isset($curUser) && $curUser){
                $checkAddressUser = $this->AddressUser->getAddressUserId($user_id,$id);
                if(isset($checkAddressUser) && $checkAddressUser){
                    $config = [
                        [
                            'field' => 'postcode',
                            'label' => 'Post Coe',
                            'rules' => 'required',
                            'errors' => [
                              'required' => 'Please input PostCode',
                            ],
                        ],
                        [
                            'field' => 'address_return',
                            'label' => 'Address Return',
                            'rules' => 'required',
                            'errors' => [
                              'required' => 'Please input Address Return',
                            ],
                        ],
                        [
                            'field' => 'phone',
                            'label' => 'Phone',
                            'rules' => 'required',
                            'errors' => [
                              'required' => 'Please input Phone ',
                            ],
                        ],
                        [
                            'field' => 'name_return',
                            'label' => 'Name Return',
                            'rules' => 'required',
                            'errors' => [
                              'required' => 'Please input Name Return ',
                            ],
                        ],
                        [
                            'field' => 'email',
                            'label' => 'Email',
                            'rules' => 'required|valid_email',
                            'errors' => [
                              'required' => 'メールアドレスを入力してください',
                              'valid_email' => 'メールアドレスが正しくないので、確認してください',
                            ],
                        ],
                    ];
                    $data = $this->put();
                    $this->form_validation->set_data($data);
                    $this->form_validation->set_rules($config);
                    if ($this->form_validation->run() !== false) {

                        $date = new DateTime();
                        //fomat date
                        $date = $date->format('Y-m-d H:i:s');

                        $data = array(
                            'postcode'      =>  $this->put('postcode'),
                            'address_return'=>  $this->put('address_return'),
                            'phone'         =>  $this->put('phone'),
                            'name_return'   =>  $this->put('name_return'),
                            'email'         =>  $this->put('email'),
                            'updated'       =>  $date
                        );

                        $updateAddressUser = $this->AddressUser->editAddressUser($id,$data);
                        if($updateAddressUser){
                            $default = $this->put('setDefault');
                            if(isset($default) && $default === 'true'){
                                $this->chosenDefault2_get($id);
                            }
                            $userProfile = $this->userProfile_get();
                            $this->response($userProfile,200);
                        }else{
                            $error = array(
                                'status' => 'Update AddressUser failed!'
                            );
                            $this->response($error,400);
                        }

                    }else{
                        $error = $this->form_validation->error_array();
                        $this->response($error,400);
                    }
                }else{
                    $error = array(
                        'status' => 'id này không thuộc user hiện tại'
                    );
                    $this->response($error,404);
                }
            }else{
                $error = array(
                    'status' => 'user không tồn tại'
                );
                $this->response($error,404);
            }
        }else{
            $error = array(
                'status' => 'id này không tồn tại'
            );
            $this->response($error,404);
        }
    }


    public function deleteAddressUser_delete($id){
        if(isset($id) && $id){
            $user = $this->user_data;
            $user_id =  $user->id;
            $curUser =   $this->User->getUserId($user_id);
            if(isset($curUser) && $curUser){
                $checkAddressUser = $this->AddressUser->getAddressUserId($user_id,$id);
                if(isset($checkAddressUser) && $checkAddressUser){
                    $checkDonate = $this->BackedProject->checkAddressDonate($id);
                    $checkDefault = $this->AddressUser->getAddressUserDefault($user_id,$id);
                    if(isset($checkDonate) && $checkDonate){
                        $error = array(
                            'status' => 'Address này đã có donate nên bạn không thể xóa!'
                        );
                        $this->response($error,400);
                    }else if(isset($checkDefault) && $checkDefault){
                        $error = array(
                            'status' => 'AddressUser này đang là mặc định, vui lòng thay đổi mặc định để xóa!'
                        );
                        $this->response($error,400);
                    }else{
                        $delete = $this->AddressUser->deleteAddressUser($id);
                        if(isset($delete) && $delete){
                            $userProfile = $this->userProfile_get();
                            $this->response($userProfile,200);
                        }else{
                            $error = array(
                                'status' => 'Delete AddressUser failed!'
                            );
                            $this->response($error,400);
                        }
                    }
                }else{
                    $error = array(
                        'status' => 'id này không thuộc user hiện tại'
                    );
                    $this->response($error,404);
                }
            }else{
                $error = array(
                    'status' => 'id này không tồn tại'
                );
                $this->response($error,404);
            }
        }else{
            $error = array(
                'status' => 'id này không tồn tại'
            );
            $this->response($error,404);
        }
    }
}
