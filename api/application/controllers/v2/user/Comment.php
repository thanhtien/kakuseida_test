<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Comment extends UserController {
    function __construct()
    {
        // Construct the parent class
        parent::__construct();
        $this->auth();
    }
    /**
     * [createComment_post Create Comment]
     * @return [type] [description]
     */
    public function createComment_post(){
        // get key firebase
        $key_firebase = $this->config->item('key_firebase');
        $roomSocket = $this->config->item('roomSocket');
        $portRoomSocket = $this->config->item('portRoomSocket');
        // get user
        $user = $this->user_data;
        $user_id =  $user->id;
        // get project id to post
        $projectId = $this->post('project_id');
        // create date
        $datetime = new DateTime();
        // format date
        $datetime = $datetime->format('Y-m-d H:i:s');
        // change time current -> time UTC
        $asia_timestamp = strtotime($datetime);
        date_default_timezone_set('UTC');
        $date = date("Y-m-d H:i:s", $asia_timestamp);

        // $date = new DateTime();
        // $date = $date->format('Y-m-d H:i:s');

        // Check exists projectId
        if(isset($projectId) &&  $projectId){
            // Get Curent User + Curent Project
            $curUser =   $this->User->getUserId($user_id);
            $curProject = $this->Project->ProjectId($projectId);
            // Check curent user + curent proejct exists
            if(isset($curUser) && isset($curProject)){
                // get data
                $comment = $this->post('comment');
                $page = $this->post('page');
                // assign data
                $data = array(
                    'user_id'       =>  $user_id,
                    'project_id'    =>  $projectId,
                    'comment'       =>  $comment,
                    'created'       =>  $date,

                );
                // create comment
                $param = $this->Comment_model->create($data);
                // get curent Comment
                $curComment = $this->Comment_model->getCommentId($param);

                if($curUser->id === $curProject->user_id){
                }else{
                    $date = new DateTime();
                    $date = $date->format('Y-m-d H:i:s');
                    // array create notification info
                    $dataNotification = array (
                        'user_id' => $curProject->user_id,
                        'user_action' => $curUser->username,
                        'title' => '～さんは「'.$curProject->project_name.'」にコメントしました',
                        'message' => $curUser->username.'さんが「'.$curProject->project_name.'」にコメントしました',
                        'link'=> "project-detail/".$projectId,
                        'status' => 0,
                        'thumbnail' => $curUser->profileImageURL,
                        'created' => $date
                    );
                    // create notification info
                    $createNotification = $this->Notification_info->create($dataNotification);

                    if(isset($param) && $param) {
                        require("public/assets/socket/socket.io.php");
                        $socketio = new SocketIO();
                        // // array
                        $data = array(
                            'room' => 'project_'.$projectId,
                            'title' => $curUser->username.' ～さんは「'.$curProject->project_name.'」にコメントしました。',
                            'message' => substr($comment,0,30),
                            'url'=> app_url()."project-detail/".$projectId,
                            'ava' => $curUser->profileImageURL
                        );

                        $socketio->send($roomSocket, $portRoomSocket, 'comment_project',json_encode($data));

                        $room = 'ownerchat_'.$param.'_'.$projectId;

                        if($curUser->eventProjectSocket != null){

                            if (strpos($curUser->eventProjectSocket, $room) !== false) {
                                $roomSocket = $curUser->eventProjectSocket;

                            }else {
                                $roomSocket = $curUser->eventProjectSocket.','.'ownerchat_'.$param.'_'.$projectId;
                            }

                        }else{
                            $roomSocket = 'ownerchat_'.$param.'_'.$projectId;
                        }

                        $dataUser = array(
                            'eventProjectSocket' => $roomSocket
                        );
                        $editUser = $this->User->update($user_id,$dataUser);

                    }
                }

                // count all comment
                $totalComment = $this->Comment_model->getAllComment($projectId);
                // show totalpage
                $totalPages = ceil($totalComment / 5);
                // get user current flow comment
                $user = $this->User->getUserId($curComment->user_id);
                // username of comment
                $curComment->username = $user->username;
                // check current comment ==== currentproject user_id then acpect owner

                // check total page > page current(5) then have get more and else
                if(trim($totalPages) > $page){
                    $curComment->getMore = true;
                }else{
                    $curComment->getMore = false;
                }
                // get image
                $curComment->profileImageURL = $user->profileImageURL;
                // before create comment then reppyl = []
                $curComment->reply = [];
                // response
                $this->response($curComment,200);
            }else{
                // annouce error
                $error = array(
                    'status' => 'Error not found!'
                );
                $this->response($error,404);
            }
        }else{
            // annouce error
            $error = array(
                'status' => 'Error not found!'
            );
            $this->response($error,404);
        }
    }
    /**
     * reply comment
     */
    public function replyComment_post() {
        // get key + room + port
        $key_firebase = $this->config->item('key_firebase');
        $roomSocket = $this->config->item('roomSocket');
        $portRoomSocket = $this->config->item('portRoomSocket');
        // require socket
        require("public/assets/socket/socket.io.php");
        // new socket
        $socketio = new SocketIO();
        // get user
        $user = $this->user_data;
        $user_id =  $user->id;

        // get projectId vs commentId
        $projectId = $this->post('project_id');
        $comment_id = $this->post('comment_id');
        // get Date
        $datetime = new DateTime();
        $datetime = $datetime->format('Y-m-d H:i:s');
        // get strtotime + change time zone
        $asia_timestamp = strtotime($datetime);
        date_default_timezone_set('UTC');
        // get Date vs time zone
        $date = date("Y-m-d H:i:s", $asia_timestamp);
        // $date = new DateTime();
        // $date = $date->format('Y-m-d H:i:s');

        // check projectId
        if(isset($projectId) &&  $projectId) {
            // get CurentUser and curenProject
            $curUser =   $this->User->getUserId($user_id);
            $curProject = $this->Project->ProjectId($projectId);
            // check curenProject and curUser
            if(isset($curUser) && isset($curProject)){
                // get comment
                $comment = $this->post('comment');
                // create array
                $data = array(
                    'user_id'       =>  $user_id,
                    'project_id'    =>  $projectId,
                    'comment_id'    =>  $comment_id,
                    'comment'       =>  $comment,
                    'created'       =>  $date,
                );
                // create reply
                $idReply = $this->Reply_Comment_model->create($data);
                // check reply
                if(isset($idReply) && $idReply) {
                    // get data reply vs idReply
                    $dataReply = $this->Reply_Comment_model->getReplyCommentId($idReply);
                    // get user (get user and image save in reply)
                    $user = $this->User->getUserId($dataReply->user_id);
                    // get username
                    $dataReply->username = $user->username;
                    // get image
                    $dataReply->profileImageURL = $user->profileImageURL;
                    // get comment by commentId
                    $commentProject = $this->Comment_model->getCommentId($comment_id);
                    // check curUser is owner ?
                    if($curUser->id === $curProject->user_id){
                        // owner true
                        // check commentProject getUserId != curUser
                        if($commentProject->user_id !== $curUser->id) {
                            $getUserComment = $this->User->getUserId($commentProject->user_id);

                            // get date
                            $date = new DateTime();
                            $date = $date->format('Y-m-d H:i:s');
                            // array create notification info
                            $dataNotification = array (
                                'user_id' => $commentProject->user_id,
                                'user_action' => $curUser->username,
                                'title' => $curUser->username.'さんが「'.$curProject->project_name.'」でのあなたのコメントに返信しました',
                                'message' => $curUser->username.'さんが「'.$curProject->project_name.'」でのあなたのコメントに返信しました',
                                'link'=> "project-detail/".$projectId,
                                'status' => 0,
                                'thumbnail' => $curUser->profileImageURL,
                                'created' => $date
                            );

                            // create notification info
                            $createNotification = $this->Notification_info->create($dataNotification);
                            // get token send notification
                            $token = $getUserComment->token;

                            // socket
                            $data2 = array(
                                'room' => 'ownerchat_'.$commentProject->id.'_'.$projectId,
                                'title' =>  $curUser->username.'さんが「'.$curProject->project_name.'」でのあなたのコメントに返信しました',
                                'message' => substr($comment,0,30),
                                'url'=> app_url()."project-detail/".$projectId,
                                'ava' => $curUser->profileImageURL
                            );

                            // send socket
                            $socketio->send($roomSocket, $portRoomSocket, 'comment_project',json_encode($data2));

                        }
                    } else {
                        // get date
                        $date = new DateTime();
                        $date = $date->format('Y-m-d H:i:s');

                        // array create notification info
                        if($commentProject->user_id !== $curUser->id) {
                            // create array
                            $dataNotification = array (
                                'user_id' => $commentProject->user_id,
                                'user_action' => $curUser->username,
                                'title' =>  $curUser->username.'さんが「'.$curProject->project_name.'」でのあなたのコメントに返信しました',
                                'message' => $curUser->username.'さんが「'.$curProject->project_name.'」でのあなたのコメントに返信しました',
                                'link'=> "project-detail/".$projectId,
                                'status' => 0,
                                'thumbnail' => $curUser->profileImageURL,
                                'created' => $date
                            );
                            // create notification info
                            $createNotification = $this->Notification_info->create($dataNotification);
                            // create array
                            $data2 = array (
                                'room' => 'ownerchat_'.$commentProject->id.'_'.$projectId,
                                'title' =>  $curUser->username.'さんが「'.$curProject->project_name.'」でのあなたのコメントに返信しました',
                                'message' => substr($comment,0,30),
                                'url'=> app_url()."project-detail/".$projectId,
                                'ava' => $curUser->profileImageURL
                            );
                            // send socket
                            $socketio->send($roomSocket, $portRoomSocket, 'donate_project',json_encode($data2));
                            // get token send notificaiton
                            $token = $curUser->token;
                            // curl send notificaiton

                        }
                        // create array
                        $dataNotification2 = array (
                            'user_id' => $curProject->user_id,
                            'user_action' => $curUser->username,
                            'title' =>  $curUser->username.'さんが「'.$curProject->project_name.'」にコメントしました',
                            'message' => $curUser->username.'さんが「'.$curProject->project_name.'」にコメントしました',
                            'link'=> "project-detail/".$projectId,
                            'status' => 0,
                            'thumbnail' => $curUser->profileImageURL,
                            'created' => $date
                        );
                        // create notification info
                        $createNotification2 = $this->Notification_info->create($dataNotification2);


                        // // array
                        $data = array(
                            'room' => 'project_'.$projectId,
                            'title' =>  $curUser->username.'さんが「'.$curProject->project_name.'」にコメントしました',
                            'message' => substr($comment,0,30),
                            'url'=> app_url()."project-detail/".$projectId,
                            'ava' => $curUser->profileImageURL
                        );
                        // send socket
                        $socketio->send($roomSocket, $portRoomSocket, 'donate_project',json_encode($data));
                        // send notification
                    }
                    $this->response($dataReply,200);
                }
            }else{
                $error = array(
                    'status' => 'Error not found!'
                );
                $this->response($error,404);
            }
        }else{
            $error = array(
                'status' => 'Error not found!'
            );
            $this->response($error,404);
        }
    }

    /**
     * [detailComment_get Detail Comment]
     * @return [type] [description]
     */
    public function detailComment_get(){

        $comment_id = $this->get('id');
        $user = $this->user_data;
        $user_id =  $user->id;

        $comment = $this->Comment_model->getCommentId($comment_id);
        $User = $this->User->getUserId($comment->user_id);

        $comment->username  = $User->username;
        $comment->profileImageURL  = $User->profileImageURL;
        $comment->time  = getTimeComment($comment->created);

        $data = array(
            'data' => $comment
        );

        $this->response($data,200);
    }


    /**
     * [editComment_put edit Comment]
     * @return [type] [description]
     */
    public function editComment_put(){
        // get info
        $comment_id = $this->put('id');
        $commentProject = $this->put('comment');
        // check comment != ''
        if($commentProject !== '') {
            // get user
            $user = $this->user_data;
            $user_id =  $user->id;
            // get comment
            $comment = $this->Comment_model->getCommentId($comment_id);
            $User = $this->User->getUserId($comment->user_id);
            // get info comment
            $comment->username  = $User->username;
            $comment->profileImageURL  = $User->profileImageURL;
            $comment->time  = getTimeComment($comment->created);
            // check comment vs current user
            // check owner comment
            if($user_id === $comment->user_id) {
                // change comment
                $dataComment = array(
                    'comment' => $commentProject,
                    'updated' => 1
                );
                $updateComment = $this->Comment_model->update($comment_id,$dataComment);
                if($updateComment){
                    $comment->comment = $commentProject;
                    $comment->updated = 1;
                    $data = array(
                        'data' => $comment
                    );

                    $this->response($data,200);
                }else{
                    $comment->comment = $commentProject;
                    $data = array(
                        'status' => 'edit comment fail'
                    );

                    $this->response($data,400);
                }

            }else{
                $data = array(
                    'status' => 'Error'
                );

                $this->response($data,404);
            }
        }else{
            $data = array(
                'status' => 'Error'
            );

            $this->response($data,404);
        }

    }
    /**
     * [removeComment_delete Remove Comment]
     * @param  [type] $comment_id [description]
     * @return [type]             [description]
     */
    public function removeComment_delete($comment_id){
        // get user
        $user = $this->user_data;
        $user_id =  $user->id;
        // get comment
        $comment = $this->Comment_model->getCommentId($comment_id);
        if(isset($comment) && $comment) {
            // get project
            $project = $this->Project->getProjectId($comment->project_id);
            // check owner project or owner comment delete comment
            if($comment->user_id === $user_id || $project->user_id ) {
                // del comment
                $delComment = $this->Comment_model->deleteComment($comment_id);
                // if success
                if($delComment){
                    // del reply in comment
                    $delReply = $this->Reply_Comment_model->RemoveReplyComments($comment_id);
                    // show message
                    $data = array(
                        'status' => 'Delete Success'
                    );
                    $this->response($data,200);
                // not delete comment
                }else{
                    $data = array(
                        'status' => 'Delete Fail'
                    );
                    $this->response($data,400);
                }
            // not owner project or owner comment
            }else{
                $data = array(
                    'status' => 'Not Found'
                );
                $this->response($data,400);
            }
        // not had comment
        } else {
            $data = array(
                'status' => 'Not Found Id'
            );
            $this->response($data,404);
        }
    }

    /**
     * [editReplyComment_put edit reply comment]
     * @return [type] [description]
     */
    public function editReplyComment_put() {
        // get info
        $reply_id = $this->put('id');
        $commentReply = $this->put('comment');
        // get user
        $user = $this->user_data;
        $user_id =  $user->id;
        // get reply
        $reply = $this->Reply_Comment_model->getReplyCommentId($reply_id);
        // get user
        $User = $this->User->getUserId($reply->user_id);
        // replace info  reply
        $reply->username  = $User->username;
        $reply->profileImageURL  = $User->profileImageURL;
        $reply->time  = getTimeComment($reply->created);

        // if owner
        if($user_id === $reply->user_id) {
            // change reply
            $dataReply = array(
                'comment' => $commentReply,
                'updated'  =>  1
            );
            // update reply
            $updateReply = $this->Reply_Comment_model->update($reply_id,$dataReply);
            // success
            if($updateReply){
                // send message
                $reply->comment = $commentReply;
                $data = array(
                    'data' => $reply
                );
                $this->response($data,200);
            // faild
            }else {
                $data = array(
                    'status' => 'Update Fail'
                );
                $this->response($data,400);
            }
        // not owner
        }else {
            $data = array(
                'status' => 'Not Found Id'
            );
            $this->response($data,404);
        }
    }
    /**
     * [removeReplyComment_delete Remove Reply]
     * @param  [type] $replyCommentId [description]
     * @return [type]                 [description]
     */
    public function removeReplyComment_delete($replyCommentId) {
        // get user
        $user = $this->user_data;
        $user_id =  $user->id;
        // get reply
        $replyComment = $this->Reply_Comment_model->getReplyCommentId($replyCommentId);
        // check reply
        if(isset($replyComment) && $replyComment) {
            // get project
            $project = $this->Project->getProjectId($replyComment->project_id);
            // check owner project or owner comment
            if($replyComment->user_id === $user_id || $project->user_id ) {
                // del reply
                $delReply = $this->Reply_Comment_model->deleteReplyComment($replyCommentId);
                // success
                if($delReply){
                    $data = array(
                        'status' => 'Delete Success'
                    );
                    $this->response($data,200);
                // fail
                }else{
                    $data = array(
                        'status' => 'Delete Fail'
                    );
                    $this->response($data,400);
                }
            // not owner
            }else{
                $data = array(
                    'status' => 'Not Found'
                );
                $this->response($data,400);
            }
        // not exists reply
        } else {
            $data = array(
                'status' => 'Not Found Id'
            );
            $this->response($data,404);
        }
    }
}
