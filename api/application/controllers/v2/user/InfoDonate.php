<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class InfoDonate extends UserController {
    function __construct()
    {
        parent::__construct();
        $this->auth();
    }

    public function ListDonate_get(){
        $start = $this->get('start');
        $limit = '10';
        if(!$start || $start =='0'){
            $start ='0';
        }else{
            $start = $start*$limit;
        }
        $user = $this->user_data;

        $user_id =  $user->id;
        $curUser =   $this->User->getUserId($user_id);
        $countProject = $this->BackedProject->BackedInfoDonate($user_id);

        $totalNews = count($countProject);
        $totalPages = ceil($totalNews / $limit);

        $input['limit']  = array($limit,$start);
        $projectSubsidized = $this->BackedProject->BackedInfoDonateNormal($user_id,$input);
        if(isset($projectSubsidized) && $projectSubsidized){
            foreach ($projectSubsidized as $key => $value) {
                $project = $this->Project->getProjectId($value->project_id);
                if(isset($project) && $project) {
                    $value->thumbnail = $project->thumbnail;
                    $value->project_name = $project->project_name;
                    $value->project_type = $project->project_type;
                    $value->active = $project->active;
                    $backingLevel = $this->BackingLevel->getLevelId($value->backing_level_id);
                    $value->now_count = $backingLevel->now_count;

                    $value->return_amount = $backingLevel->return_amount;
                    if($value->type === 'Credit' && $value->status === 'succeeded'){
                        $value->statusPay = '決済済み';
                    }else if($value->type === 'Bank' && $value->status === 'succeeded'){
                        $value->statusPay = '振込済み';
                    }else if($value->status === 'unsuccess'){
                        $value->statusPay = '振込待ち';
                    }
                }
            }
        }
        if($curUser){
            $data = array(
                'page_count'=>$totalPages,
                'data'=>$projectSubsidized
            );
        }
        $this->response($data, 200);

    }


    public function listDonateChart_get(){
        $user = $this->user_data;
        $user_id =  $user->id;

        $curUser = $this->User->getUserId($user_id);

        $project_id = $this->input->get('project_id');
        $first_day = $this->input->get('first_day');
        $last_day = $this->input->get('last_day');
        if($project_id && $first_day && $last_day && $curUser){

            $project = $this->Project->getProjectId($project_id);

            if(isset($project) && $project){

                if($project->user_id === $user_id){
                    // $listDonate = $this->BackedProject->DonateDate($project_id,$first_day,$last_day);

                    $day_first_day = date('d', strtotime($first_day));
                    $day_last_day = date('d', strtotime($last_day));


                    $month_first_day = date('m', strtotime($first_day));


                    $year_first_day = date('Y', strtotime($first_day));

                    $datetime1 = new DateTime($first_day);
                    $datetime2 = new DateTime($last_day);
                    $interval = $datetime1->diff($datetime2);
                    $num =  $interval->format('%a');

                    $labels = array();
                    $data = array();

                    $today = strtotime(date("Y-m-d"));
                    $date = new DateTime($first_day);
                    // $first_day = date_format($date ,"Y-m-t");

                    for ($i = 0; $i <= $num; $i++)
                    {
                        $fullday = date('Y/m/d', strtotime("+$i day", strtotime($first_day)));
                        // $date = strtotime($year_first_day . "-" . $month_first_day . "-" . ($i + 1));
                        //
                        // $day=strftime("%A", $date);
                        // $fullday =  $year_first_day.'-'.$month_first_day.'-'.($i + 1);
                        //
                        $listDonate = $this->BackedProject->DonateDate($project_id,$fullday);

                        $period_array = array();
                        foreach ($listDonate as $row)
                        {
                            $period_array[] = intval($row->invest_amount); //can it be float also?
                        }
                        $total = array_sum($period_array);
                        array_push($labels,$fullday);
                        array_push($data,$total);
                    }
                    $newdata = array(
                        'labels' => $labels,
                        'data'  => $data
                    );

                    $this->response($newdata,200);

                }else{
                    $error = array(
                        'status' => 'Error'
                    );
                    $this->response($error,404);
                }

            }else {
                $error = array(
                    'status' => 'Error'
                );
                $this->response($error,404);
            }
        }else{
            $error = array(
                'status' => 'Error'
            );
            $this->response($error,404);
        }

    }


    /**
     * [listDonateChartTotal_get Total Donate ]
     * @return [type] [description]
     */
    public function listDonateChartTotal_get() {
        // get user
        $user = $this->user_data;
        // get userId
        $user_id =  $user->id;

        // get User by user_id
        $curUser = $this->User->getUserId($user_id);
        // get project_id is transmitted
        $project_id = $this->get('project_id');

        // check project_id vs curUser
        if($project_id && $curUser){

            // get Project
            $project = $this->Project->getProjectId($project_id);

            // check Project
            if(isset($project) && $project){

                // check onwer project vs curentuser
                if($project->user_id === $user_id){

                    // get date
                    $first_day = $this->get('last_day');

                    // get time by firts collection_start_date
                    $time = strtotime($project->collection_start_date);
                    // get lastday by time
                    $last_day = date( 'Y-m-d', $time );

                    // get date by firstday last_day
                    $datetime1 = new DateTime($first_day);
                    $datetime2 = new DateTime($last_day);
                    // function diff calculate time
                    $interval = $datetime1->diff($datetime2);
                    // format time
                    $day =  $interval->format('%a');

                    // craete total + create array data
                    $total = 0;
                    $labels = array();
                    $data = array();
                    // if $day <= 10
                    if($day <= 10) {
                        // for
                        for ($i = 0; $i <= $day; $i++) {
                            // create date flow for + last_day
                            $fullday = date('Y/m/d', strtotime("+$i day", strtotime($last_day)));
                            // $listDonate all data had Donate in $fullday
                            $listDonate = $this->BackedProject->DonateDate($project_id,$fullday);
                            // craete array
                            $period_array = array();
                            // foreach array
                            foreach ($listDonate as $row)
                            {
                                // use intval get invest_amount
                                $period_array[] = intval($row->invest_amount); //can it be float also?
                            }
                            // sum array get total
                            $total2 = array_sum($period_array);
                            // check total current flow $i had total !== 0
                            if($total2 !== 0){
                                // $total = $total current + $total2(0)
                                $total = $total + $total2;
                            }
                            // array push day
                            array_push($labels,$fullday);
                            // array push data
                            array_push($data,$total);
                        }
                    // if day > 10
                    }else{
                        // divide $day for 10
                        $divide = ceil($day/10);
                        // for with $day $i = $i + $divide
                        for ($i = 0; $i <= $day; $i = $i + $divide) {
                            // amount $i + $divide -1 (use operation of $date2)
                            $totalDay = $i + $divide - 1;
                            // var_dump($totalDay);
                            // var_dump($day);
                            // $date1 = first day in model
                            $date1 = date('Y/m/d', strtotime("+$i day", $time));
                            // $date2 = lastday in model

                            if($totalDay >= $day){
                                $date2 = $first_day;
                            }else{
                                $date2 = date('Y/m/d', strtotime("+$totalDay day", $time));
                            }
                            // get list donate between date1 -> date2
                            $listDonate = $this->BackedProject->DonateDateTotal($project_id,$date1,$date2);
                            // craete array
                            $period_array = array();
                            // foreach array
                            foreach ($listDonate as $row) {
                                // use intval get invest_amount
                                $period_array[] = intval($row->invest_amount); //can it be float also?
                            }
                            // $total òf $period_array($invest_amount)
                            $total2 = array_sum($period_array);
                            // if $total2 # 0
                            if($total2 !== 0){
                                // create new total
                                $total = $total + $total2;
                            }
                            // check last date == vs $day(day calculate first day vs last day)
                            if($totalDay >= $day){
                                // arraypush date2
                                array_push($labels,$date2);
                            }else{
                                // arraypush date1
                                array_push($labels,$date1);
                            }
                            // arraypush total
                            array_push($data,$total);
                        }
                    }
                    // create array assign $labels, $data
                    $newdata = array(
                        'labels' => $labels,
                        'data'  => $data
                    );
                    // response
                    $this->response($newdata,200);
                // onwer project != curentuser
                }else{
                    $error = array(
                        'status' => 'Error'
                    );
                    $this->response($error,404);
                }
            // Not exists Project
            }else {
                $error = array(
                    'status' => 'Error1'
                );
                $this->response($error,404);
            }
        // Not exists project_id and currentUser
        }else{
            $error = array(
                'status' => 'Error2'
            );
            $this->response($error,404);
        }

    }

    /**
     * [delInfoCard_get del info card of current user]
     * @return [type] [description]
     */
    public function delInfoCard_get(){
        // get user
        $user = $this->user_data;
        $user_id =  $user->id;

        $currentUser = $this->User->getUserId($user_id);

        if(isset($currentUser) && $currentUser) {
            if(isset($currentUser->customer_id) && $currentUser->customer_id){
                $dataUpdateUser = array(
                    'hash_token' => null,
                    'last4' => null,
                    'card_year' => null,
                    'card_month' => null,
                    'card_brand' => null,
                    'customer_id' => null,
                );
                $updateUser = $this->User->editUser($currentUser->id,$dataUpdateUser);
                if($updateUser){
                    $success = array(
                        'status' => 'クレジットカード情報が削除されました'
                    );
                    $this->response($success,200);
                }else{
                    $error = array(
                        'status' => 'xóa card thất bại!'
                    );
                    $this->response($error,400);
                }
            }else{
                $error = array(
                    'status' => 'bạn chưa có nhập thông tin card!'
                );
                $this->response($error,400);
            }

        }else{
            $error = array(
                'status' => 'không tìm thấy user này!'
            );
            $this->response($error,404);
        }
    }
}
