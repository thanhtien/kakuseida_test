<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Subscribe extends UserController {
    function __construct()
    {
        // Construct the parent class
        parent::__construct();
        $this->auth();
    }
    // unsubscribe notification when logout or expired token
    public function unSubscribeNotification_get() {
        $key_firebase = $this->config->item('key_firebase');
        // get id
        $user = $this->user_data;
        $user_id =  $user->id;
        // get user
        $curUser =   $this->User->getUserId($user_id);
        $tokenSubscribe = $curUser->token;

        // check user
        if(isset($curUser) && $curUser){
            // get notification to user_id
            $notification = $this->Notification_action->getNotificationById($user_id);
            $dataUnSubscribe = [];
            foreach ($notification as $key => $value) {
                array_push($dataUnSubscribe,$value->topics.'_'.$value->action);
            }


            foreach ($dataUnSubscribe as $key => $value) {
                // unsubscrite notification
                $curl = curl_init();

                curl_setopt_array($curl, array(
                    CURLOPT_URL => "https://iid.googleapis.com/iid/v1:batchRemove",
                    CURLOPT_RETURNTRANSFER => true,
                    CURLOPT_ENCODING => "",
                    CURLOPT_MAXREDIRS => 10,
                    CURLOPT_TIMEOUT => 30,
                    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                    CURLOPT_CUSTOMREQUEST => "POST",
                    CURLOPT_POSTFIELDS => "{\r\n   \"to\": \"/topics/".$value."\",\r\n   \"registration_tokens\": [\"".$tokenSubscribe."\"]\r\n}",
                    CURLOPT_HTTPHEADER => array(
                        "Authorization: key=".$key_firebase."",
                        "Content-Type: application/json",
                    ),
                ));

                $response = curl_exec($curl);
                $err = curl_error($curl);

                curl_close($curl);
            }
            $success = array(
                'status' => 'OK'
            );
            $this->response($success,200);
        // else
        }else{
            $error = array(
                'status' => 'Id of user not found!'
            );
            $this->response($error,404);
        }
    }

    /**
     * unSubscribe Email
     */
    public function unSubscribeEmail_get($type){
        // check has type
        if(isset($type) && $type){
            // get user
            $user = $this->user_data;
            $user_id =  $user->id;
            // get user data
            $curUser = $this->User->getUserId($user_id);
            // check curuser exists
            if(isset($curUser) && $curUser){
                // check type is advertisement
                if($type === 'ads') {

                    $subscription = $this->Subscription->getDataEmail($curUser->email);
                    if(isset($subscription) && $subscription){
                        if($subscription->unsubscribe === '0'){
                            $dataUnSubscribe = array(
                                'unsubscribe' => 1
                            );
                            $updateUnSubscribe = $this->Subscription->unSubscribeMail($curUser->email,$dataUnSubscribe);
                            $success = array(
                                'statusUnsubscribe' => '1'
                            );
                        }else{
                            $dataUnSubscribe = array(
                                'unsubscribe' => 0
                            );
                            $updateUnSubscribe = $this->Subscription->unSubscribeMail($curUser->email,$dataUnSubscribe);
                            $success = array(
                                'statusUnsubscribe' => '0'
                            );
                        }
                        $this->response($success,200);
                    }
                }else if($type === 'projects') {
                    $dataUnSubscribe = array(
                        'unsubscribe' => 1
                    );
                    $updateUnSubscribe = $this->User->editUserUnSubscribe($curUser->email,$dataUnSubscribe);
                    $success = array(
                        'status' => 'unSubscribe success!'
                    );
                    $this->response($success,200);
                }
            }else{
                $success = array(
                    'status' => 'Not Found!'
                );
                $this->response($success,404);
            }
        }
    }

    /**
     * unSubscribe mail project
     */
    public function unSubscribeEmailProject_get($project_id){
        // check has type
        if(isset($project_id) && $project_id){
            // get user
            $user = $this->user_data;
            $user_id =  $user->id;
            // get user data
            $curUser = $this->User->getUserId($user_id);
            $project = $this->Project->getProjectId($project_id);
            // check curuser exists
            if(isset($curUser) && $curUser && isset($project) && $project){
                // get backed
                $checkBacked = $this->BackedProject->checkBackedDonate($user_id,$project_id);
                // check has backed
                if($checkBacked > 0 ){
                    // check unsubscribe = null
                    if($project->unsubscribe === null || $project->unsubscribe === ''){
                        // create array
                        $data = array();
                        // push array
                        array_push($data,$user_id);
                        // json array
                        $unsubscribeProject = json_encode($data);
                        // create array save json array
                        $dataUpdate = array(
                            'unsubscribe' => $unsubscribeProject
                        );
                        // save
                        $this->Project->update($project_id,$dataUpdate);

                    // has element in array unSubscribe
                    }else{
                        // json decode get array
                        $data = json_decode($project->unsubscribe);
                        // check $project_id in array
                        if(in_array($user_id,$data)){
                            // get location of projectid in array
                            $location = array_search($user_id,$data);
                            // remove element with location in array
                            unset($data[$location]);
                        // not exists project id in array
                        }else{
                            // push array
                            array_push($data,$user_id);
                        }
                        // json encode -> string save database
                        $unsubscribeProject = json_encode($data);
                        // create array
                        $dataUpdate = array(
                            'unsubscribe' => $unsubscribeProject
                        );
                        // update
                        $this->Project->update($project_id,$dataUpdate);
                    }
                }else{
                    // check unsubscribe = null
                    if($project->subscribe === null){
                        // create array
                        $data = array();
                        // push array
                        array_push($data,$user_id);
                        // json array
                        $subscribeProject = json_encode($data);
                        // create array save json array
                        $dataUpdate = array(
                            'subscribe' => $subscribeProject
                        );
                        // save
                        $this->Project->update($project_id,$dataUpdate);
                    // has element in array unSubscribe
                    }else{
                        // json decode get array
                        $data = json_decode($project->subscribe);
                        // check $project_id in array
                        if(in_array($user_id,$data)){
                            // get location of projectid in array
                            $location = array_search($user_id,$data);
                            // remove element with location in array
                            unset($data[$location]);
                        // not exists project id in array
                        }else{
                            // push array
                            array_push($data,$user_id);
                        }
                        // json encode -> string save database
                        $subscribeProject = json_encode($data);
                        // create array
                        $dataUpdate = array(
                            'subscribe' => $subscribeProject
                        );
                        // update
                        $this->Project->update($project_id,$dataUpdate);
                    }
                    // $error = array(
                    //     'status' =>'dsadsa'
                    // );
                    // $this->response($error,400);
                }
            }else{
                $success = array(
                    'status' => 'Not Found!'
                );
                $this->response($success,404);
            }
        }
    }

    /**
     * un subscribe project + ads
     */
    public function unSubscribeEmail2_post(){
        // get id
        $user = $this->user_data;
        $user_id =  $user->id;
        // get user
        $curUser =   $this->User->getUserId($user_id);
        // check $curUser exists
        if(isset($curUser) && $curUser) {
            $curUser->statusUnsubscribe = $curUser->unsubscribe;
            // get data from this post
            $unsubscribe = $this->post('statusUnsubscribe');
            $unsubscribeProject = $this->post('unsubscribeProject');
            // check exists unsubscribe
            if(isset($unsubscribe) && isset($unsubscribeProject)){
                // create array
                $dataSubScrtiption = array(
                    'unsubscribe' => $unsubscribe
                );
                // create array
                $dataUser = array(
                    'unsubscribeProject' => $unsubscribeProject
                );
                // change status subscribe in Subscription Email (ADS)
                $unsubscribeAds = $this->Subscription->unSubscribeMail($curUser->email,$dataSubScrtiption);
                // change status subscribe in Subscription Email (Project)
                $unsubscribe = $this->User->editUser($curUser->id,$dataUser);
                // check success
                if($unsubscribeAds && $unsubscribe) {
                    // get new user
                    $newCurUser =   $this->User->getUserId($user_id);
                    $data = array(
                        'statusUnsubscribe'  =>  $newCurUser->unsubscribe,
                        'unsubscribeProject' =>  $newCurUser->unsubscribeProject
                    );
                    $this->response($data,200);
                // error
                } else {
                    // show error
                    $error = array(
                        'status' => 'Unsubscribe Mail Error!'
                    );
                    $this->response($error,400);
                }
            // not found data
            } else {
                // show error
                $error = array(
                    'status' => 'error'
                );
                $this->response($error,404);
            }
        // not found user
        } else {
            // show error
            $error = array(
                'status' => 'error'
            );
            $this->response($error,404);
        }
    }





    // Donate Use Stripe;
    public function donateStripe_post() {
        $roomSocket = $this->config->item('roomSocket');
        $portRoomSocket = $this->config->item('portRoomSocket');
        $key_firebase = $this->config->item('key_firebase');

        $user = $this->user_data;
        $user_id =  $user->id;
        $curUser =   $this->User->getUserId($user_id);
        $email = $curUser->email;
        $_POST = $this->post();
        //check whether stripe token is not empty
        //get token, card and user info from the form
        $token  = $this->post('stripeToken');
        $cvc  = $this->post('cvc');
        if(!isset($cvc)){
            $error = array(
                'status' => 'Please check cvc'
            );
            $this->response($error,400);
        }
        $project_id = $_POST['project_id'];
        $backing_level_id = $_POST['backing_level_id'];
        // $comment = '”'.$curUser->username.'"は"'.$projectName.'('.$project_id.')"の"'.$backing_level_id.'"に支援しました。';
        $comment =  'comment';
        $invest_amount = $_POST['invest_amount'];


        if(isset($project_id) && isset($backing_level_id)){
            $date = new DateTime();
            $date = $date->format('Y-m-d H:i:s');

            $existproject = $this->Project->checkProjectId($project_id);

            $existprojectretrun = $this->BackingLevel->getBackingId($backing_level_id);

            if(isset($existproject) && $existproject){
                $number_month = $existproject->number_month;
                $timestart = $existproject->collection_start_date;
                $project_type = $existproject->project_type;

                if($existproject->project_type === '0'){
                    if($existproject->collection_end_date < $date){
                        $error = array(
                            "status" => "error!"
                        );
                        $this->response($error,500);
                    }
                }

                if($user_id === $existproject->user_id){
                    $error = array(
                        "status" => "error!"
                    );
                    $this->response($error,500);
                }

                if(isset($existprojectretrun) && $existprojectretrun){
                    if($existprojectretrun->project_id === $existproject->id) {
                        //include Stripe PHP library
                        require_once APPPATH."third_party/stripe/init.php";

                        //set api key
                        $key = $this->Setting->getSetting();
                        $stripe = array(
                            "secret_key"      => $key->secret_key,
                            "publishable_key" => $key->public_key
                        );

                        \Stripe\Stripe::setApiKey($stripe['secret_key']);

                        //add customer to stripe
                        if(isset($curUser->customer_id) && $curUser->customer_id && !isset($token)){
                            $customer = (object) array('id' => $curUser->customer_id);
                            $key = $this->Setting->getSetting();
                            //set api key
                            $stripe = array (
                                "secret_key"      => $key->secret_key,
                                "publishable_key" => $key->public_key
                            );
                            \Stripe\Stripe::setApiKey($stripe['secret_key']);

                            $customer = \Stripe\Customer::update(
                                $curUser->customer_id,
                                [
                                    'metadata' => ['cvc' => '2222'],
                                ]
                            );
                            var_dump($customer);
                            // $customer = \Stripe\Customer::retrieve($curUser->customer_id);
                            // var_dump($customer);
                            // $customerId = $customer['default_source'];
                            // $card = $customer->sources->retrieve($customerId);
                            // // var_dump($card);
                            // var_dump($card->brand);
                            // var_dump($cvc);
                            exit;
                            if($cvc !== $card->cvc){
                                $error = array(
                                    'status' => 'cvc fail'
                                );
                                $this->response($error,400);
                            }
                        }else if(isset($curUser->customer_id) && $curUser->customer_id && isset($token) || !isset($curUser->customer_id) && isset($token)) {
                            try {
                                $customer = \Stripe\Customer::create(array(
                                    'email' => $email,
                                    'source'  => $token
                                ));
                            } catch(Stripe_CardError $e) {
                                $error1 = $e->getMessage();
                                $error = array(
                                    'status'=>$error1
                                );
                                $this->response($error,400);
                            } catch (Stripe_InvalidRequestError $e) {
                                // Invalid parameters were supplied to Stripe's API
                                $error2 = $e->getMessage();
                                $error = array(
                                    'status'=>$error2
                                );
                                $this->response($error,400);
                            } catch (Stripe_AuthenticationError $e) {
                                // Authentication with Stripe's API failed
                                $error3 = $e->getMessage();
                                $error = array(
                                    'status'=>$error3
                                );

                                $this->response($error,400);
                            } catch (Stripe_ApiConnectionError $e) {
                                // Network communication with Stripe failed
                                $error4 = $e->getMessage();
                                $error = array(
                                    'status'=>$error4
                                );
                                $this->response($error,400);
                            } catch (Stripe_Error $e) {
                                // Display a very generic error to the user, and maybe send
                                // yourself an email
                                $error5 = $e->getMessage();
                                $error = array(
                                    'status'=>$error5
                                );
                                $this->response($error,400);
                            } catch (Exception $e) {
                                // Something else happened, completely unrelated to Stripe
                                $errrr = json_decode($e->getHttpBody());
                                if($errrr->error->code === "token_already_used"){
                                    $error = array(
                                        'status'=>'このクレジットカード情報は存在しました。'
                                    );
                                }else if($errrr->error->code === "email_invalid"){
                                    $error = array(
                                        'status'=>'メールアドレスを正しく入力してください'
                                    );
                                }

                                $this->response($error,400);
                            }
                        }
                        //item information
                        $itemName = $project_id;
                        $itemNumber = $backing_level_id;
                        $itemPrice = $this->post('invest_amount');
                        $currency = "JPY";
                        $orderID = $project_id;

                        //charge a credit or a debit card
                        try {
                            $charge = \Stripe\Charge::create(array(
                                'customer' => $customer->id,
                                'amount'   => $itemPrice,
                                'currency' => $currency,
                                'description' => $itemNumber,
                                'metadata' => array(
                                'item_id' => $itemNumber
                                )
                            ));
                            //retrieve charge details
                            $chargeJson = $charge->jsonSerialize();

                            //check whether the charge is successful
                            if($chargeJson['amount_refunded'] == 0 && empty($chargeJson['failure_code']) && $chargeJson['paid'] == 1 && $chargeJson['captured'] == 1)
                            {
                                //order details
                                $amount = $chargeJson['amount'];
                                $balance_transaction = $chargeJson['balance_transaction'];
                                $currency = $chargeJson['currency'];
                                $status = $chargeJson['status'];
                                $date = date("Y-m-d H:i:s");


                                //insert tansaction data into the database

                                $addressDefault = $this->AddressUser->getAddressUserDefault($user_id);
                                if(!$addressDefault){
                                    $error = array(
                                        'status' => 'error!'
                                    );
                                    $this->response($error,500);
                                }
                                $dataDB = array(
                                    'project_id' => $project_id,
                                    'user_id' => $user_id,
                                    'id_address' => $addressDefault->id,
                                    'postcode'  =>  $addressDefault->postcode,
                                    'address_return'  =>  $addressDefault->address_return,
                                    'phone'  =>  $addressDefault->phone,
                                    'name_return'  =>  $addressDefault->name_return,
                                    'backing_level_id' => $backing_level_id,
                                    'invest_amount' => $invest_amount,
                                    'stripe_charge_id' => $charge->id,
                                    'type'=>'Credit',
                                    'comment' => $comment,
                                    'status' => $status,
                                    'created' => $date,
                                    'manual_flag' => '0'
                                );

                                if ($this->db->insert('backed_projects', $dataDB)) {
                                    if($this->db->insert_id() && $status == 'succeeded'){

                                        if($project_type === '1') {
                                            $time = getMonthProject($timestart);
                                            $time = ceil($time/$number_month);
                                            $dateTime = new DateTime();
                                            $dateTime = $dateTime->format('Y-m-d H:i:s');
                                            $dataListDonate = array(
                                                'user_id' => $user_id,
                                                'project_id' => $project_id,
                                                'backing_levels_id' => $backing_level_id,
                                                'month' => $time,
                                                'created' => $dateTime
                                            );
                                            $this->db->insert('listuserdonate_fanclub', $dataListDonate);
                                        }

                                        $dataBackingLevel = $this->BackingLevel->getBackingId($backing_level_id);
                                        $now_count = $dataBackingLevel->now_count;
                                        $dataUpdate = array(
                                            'now_count'=>$now_count + 1,
                                        );
                                        $this->BackingLevel->update($backing_level_id,$dataUpdate);
                                        $dataUserUpdate = array(
                                            'customer_id'=>$customer->id
                                        );
                                        $projectId = $this->Project->getProjectId($project_id);
                                        $collected_amount = $projectId->collected_amount;
                                        $now_countProject = $projectId->now_count;
                                        $dataUpdateProject = array(
                                            'now_count'=>$now_countProject + 1,
                                            'collected_amount'=>$collected_amount + $itemPrice,
                                        );
                                        $updateProject = $this->Project->update($project_id,$dataUpdateProject);
                                        $updateUser = $this->User->update($user_id,$dataUserUpdate);
                                        $listUserDonate = array(
                                            'user_id'=>$user_id,
                                            'project_id'=>$project_id,
                                            'created'=>$date
                                        );
                                        $checkIsDonate = $this->ListUserDonate->checkIsDonate($user_id,$project_id);
                                        if(!$checkIsDonate){
                                            $createList = $this->ListUserDonate->createListUserDonate($listUserDonate);
                                        }

                                        // create Datetime
                                        $datetime = new DateTime();
                                        $datetime = $datetime->format('Y-m-d H:i:s');
                                        // get strtotime + change time zone
                                        $asia_timestamp = strtotime($datetime);
                                        date_default_timezone_set('UTC');
                                        // get Date vs time zone
                                        $date = date("Y-m-d H:i:s", $asia_timestamp);

                                        // get data for socket + create notification_info + firebase
                                        $userDonate = $curUser->username;
                                        $investAmountDonate = $invest_amount;
                                        $imageUserDonate = $curUser->profileImageURL;
                                        $projectId = $project_id;
                                        $projectName = $existproject->project_name;
                                        // array create notification info
                                        $dataNotification = array (
                                            'user_id' => $existproject->user_id,
                                            'user_action' => $userDonate,
                                            'title' => '支援があります',
                                            'message' => ''.$userDonate.' さんが 「'.substr($projectName,0,30).'」に'.number_format($investAmountDonate).'円支援しました',
                                            'link'=> "my-page/statistic/".$projectId,
                                            'status' => 0,
                                            'thumbnail' => $imageUserDonate,
                                            'created' => $date
                                        );
                                        // create notification info
                                        $createNotification = $this->Notification_info->create($dataNotification);
                                        // check exists
                                        if($createNotification) {
                                            // use SocketIO annouce for user
                                            require("public/assets/socket/socket.io.php");
                                            $socketio = new SocketIO();
                                            // // array
                                            $data = array(
                                                'room' => 'project_'.$projectId,
                                                'title' => '支援があります',
                                                'message' => $userDonate."さんが「".substr($projectName,0,30)."」に".number_format($investAmountDonate)."円支援しました",
                                                'url'=> app_url()."my-page/statistic/".$projectId,
                                                'ava' => $imageUserDonate
                                            );
                                            $socketio->send($roomSocket, $portRoomSocket, 'donate_project',json_encode($data));

                                            $curl = curl_init();

                                            curl_setopt_array($curl, array(
                                              CURLOPT_URL => "https://fcm.googleapis.com/fcm/send",
                                              CURLOPT_RETURNTRANSFER => true,
                                              CURLOPT_ENCODING => "",
                                              CURLOPT_MAXREDIRS => 10,
                                              CURLOPT_TIMEOUT => 30,
                                              CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                                              CURLOPT_CUSTOMREQUEST => "POST",
                                              CURLOPT_POSTFIELDS => "{   \"notification\": {\r\n        \"title\": \"支援があります\",\r\n        \"body\": \"".$userDonate."さんが「".substr($projectName,0,30)."」」に ".number_format($investAmountDonate)."円支援しました\",\r\n        \"click_action\": \"".app_url()."my-page/statistic/".$projectId."\",\r\n        \"icon\": \"".base_url()."images/2018/default/logo.png\"\r\n    },\r\n  \"to\" : \"/topics/project_".$projectId."\"\r\n}",
                                              CURLOPT_HTTPHEADER => array(
                                                "Authorization: key=".$key_firebase,
                                                "Content-Type: application/json",
                                              ),
                                            ));

                                            $response = curl_exec($curl);
                                            $err = curl_error($curl);

                                            curl_close($curl);
                                            // Session send message for admin
                                        }

                                        $success = array(
                                            'status'=>'Donate Success!'
                                        );
                                        $this->response($success,200);
                                    }
                                }
                            }
                        } catch(Stripe_CardError $e) {
                            $error1 = $e->getMessage();
                            $error = array(
                                'status'=>$error1);
                                $this->response($error,400);
                        } catch (Stripe_InvalidRequestError $e) {
                            // Invalid parameters were supplied to Stripe's API
                            $error2 = $e->getMessage();
                            $error = array(
                                'status'=>$error2);
                                $this->response($error,400);
                        } catch (Stripe_AuthenticationError $e) {
                            // Authentication with Stripe's API failed
                            $error3 = $e->getMessage();
                            $error = array(
                                'status'=>$error3);
                                $this->response($error,400);
                        } catch (Stripe_ApiConnectionError $e) {
                            // Network communication with Stripe failed
                            $error4 = $e->getMessage();
                            $error = array(
                                'status'=>$error4
                            );
                            $this->response($error,400);
                        } catch (Stripe_Error $e) {
                        // Display a very generic error to the user, and maybe send
                        // yourself an email
                        $error5 = $e->getMessage();
                        $error = array(
                            'status'=>$error5);
                            $this->response($error,400);
                        } catch (Exception $e) {
                            // Something else happened, completely unrelated to Stripe
                            $error6 = $e->getMessage();
                            $error = array(
                                'status'=>$error6);
                            $this->response($error,400);
                        }

                    }else{
                        $error = array(
                            'status'=>'ProjectReturn or Project not found!'
                        );
                        $this->response($error,404);
                    }
                }else{
                    $error = array(
                        'status'=>'ProjectReturn not exists!'
                    );
                    $this->response($error,404);
                }
            }else{
                $error = array(
                    'status'=>'Project not exists!'
                );
                $this->response($error,404);
            }
        }else{
            $error = array(
                'status'=>'This id is not exists!'
            );
            $this->response($error,404);
        }
    }

}
