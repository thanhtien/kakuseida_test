<?php

defined('BASEPATH') OR exit('No direct script access allowed');
use \Firebase\JWT\JWT;
class TestFanClub extends UserController {
    function __construct() {
        // Construct the parent class
        parent::__construct();
        $this->auth();
    }


    // Donate Use Stripe;
    public function testCreate_post() {
        // $user = $this->user_data;
        // $user_id =  $user->id;
        // $curUser =   $this->User->getUserId($user_id);
        // $email = $curUser->email;
        // $_POST = $this->post();
        //check whether stripe token is not empty
        //get token, card and user info from the form
        // $token  = $this->post('stripeToken');
        // $project_id = $_POST['project_id'];
        // $backing_level_id = $_POST['backing_level_id'];
        // // $comment = '”'.$curUser->username.'"は"'.$projectName.'('.$project_id.')"の"'.$backing_level_id.'"に支援しました。';
        // $comment =  'comment';
        // $invest_amount = $_POST['invest_amount'];


        //include Stripe PHP library
        require_once APPPATH."third_party/stripe/init.php";

        //set api key
        $key = $this->Setting->getSetting();
        $stripe = array(
            "secret_key"      => $key->secret_key,
            "publishable_key" => $key->public_key
        );

        \Stripe\Stripe::setApiKey($stripe['secret_key']);
        $a = \Stripe\Product::create([
            "name" => 'test new 30 day',
            "type" => "service",
        ]);

        \Stripe\Plan::create([
          "nickname" => "Standard Monthly",
          "product" => $a->id,
          "amount" => 2000,
          "currency" => "JPY",
          "interval" => "day",
          "interval_count" => 30,
          "usage_type" => "licensed",
        ]);
    }




    public function stopDonateFanClub_get(){

    }

}
