<?php defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
require_once APPPATH . '/libraries/REST_Controller.php';
require_once APPPATH . '/libraries/JWT.php';
require_once APPPATH . '/libraries/BeforeValidException.php';
require_once APPPATH . '/libraries/ExpiredException.php';
require_once APPPATH . '/libraries/SignatureInvalidException.php';
use \Firebase\JWT\JWT;

class BD_Controller extends REST_Controller
{
	private $user_credential;
    public function auth()
    {
        $date = new DateTime();
        $this->load->library(array('ion_auth', 'form_validation'));
        // Configure limits on our controller methods
        // Ensure you have created the 'limits' table and enabled 'limits' within application/config/rest.php
        $this->methods['users_get']['limit'] = 500; // 500 requests per hour per user/key
        $this->methods['users_post']['limit'] = 100; // 100 requests per hour per user/key
        $this->methods['users_delete']['limit'] = 50; // 50 requests per hour per user/key

        //JWT Auth middleware
        $headers = $this->input->get_request_header('authorization');
        $kunci = $this->config->item('thekey'); //secret key for encode and decode
        $token= "token";

       	if (!empty($headers)) {
        	if (preg_match('/Bearer\s(\S+)/', $headers , $matches)) {
            $token = $matches[1];
        	}
    	}
        try {
			JWT::$leeway = 6000;
			$date = new DateTime();
			$decoded = JWT::decode($token, $kunci, array('HS256'));
			$decoded->iat = $date->getTimestamp();
			$decoded->exp = $date->getTimestamp() + 60*60*24*30*3;
			$tokenRefest = JWT::encode($decoded, $kunci);
			$decoded->tokenRefest = $tokenRefest;
            $this->user_data = $decoded;
        } catch (Exception $e) {
            $invalid = ['status' => $e->getMessage()]; //Respon if credential invalid
            $this->response($invalid, 401);//401
        }
    }

	function getTime_get($project_id) {

        if(isset($project_id) && $project_id){

            $project = $this->Project->getProjectId($project_id);
            if(isset($project) && $project){

                $date1 = new DateTime($project->collection_end_date);
                $date2 = new DateTime(date('Y-m-d H:i:s'));
                if($date1 > $date2){
                  $time = $date1->diff($date2);

                  $date = date_diff($date1,$date2);
                  $date = $date->format("%a");
                  $format_collection_end_date = array(
                    'date' =>$date,
                    'hour' => $time->h,
                    'minutes' => $time->i,
                  );
                }else{
                  $format_collection_end_date = array(
                    'status'=>'終了'
                  );
                }
                return $format_collection_end_date;
            }else{

                $error = array(
                    'status' => 'id is not exists!'
                );
                return $error;

            }
        }
        $error = array(
            'status' => 'id is not exists!'
        );
        return $error;
    }

	function getTimeProject_get($collection_end_date) {

		if(isset($collection_end_date) && $collection_end_date){
			$date1 = new DateTime($collection_end_date);
			$date2 = new DateTime(date('Y-m-d H:i:s'));
			if($date1 > $date2){
				$time = $date1->diff($date2);

				$date = date_diff($date1,$date2);
				$date = $date->format("%a");
				$format_collection_end_date = array(
					'date' =>$date,
					'hour' => $time->h,
					'minutes' => $time->i,
				);
			}else{
				$format_collection_end_date = array(
					'status'=>'終了'
				);
			}
			return $format_collection_end_date;

		}

	}
}
class UserController extends BD_Controller
{
	private $user_credential;
    public function auth()
    {
        $date = new DateTime();
        $this->load->library(array('ion_auth', 'form_validation'));
        // Configure limits on our controller methods
        // Ensure you have created the 'limits' table and enabled 'limits' within application/config/rest.php
        $this->methods['users_get']['limit'] = 500; // 500 requests per hour per user/key
        $this->methods['users_post']['limit'] = 100; // 100 requests per hour per user/key
        $this->methods['users_delete']['limit'] = 50; // 50 requests per hour per user/key

        //JWT Auth middleware
        $headers = $this->input->get_request_header('authorization');
        $kunci = $this->config->item('thekey'); //secret key for encode and decode
        $token= "token";

       	if (!empty($headers)) {
        	if (preg_match('/Bearer\s(\S+)/', $headers , $matches)) {
            $token = $matches[1];
        	}
    	}
        try {
			JWT::$leeway = 6000;
			$date = new DateTime();
			$decoded = JWT::decode($token, $kunci, array('HS256'));
			$decoded->iat = $date->getTimestamp();
			$decoded->exp = $date->getTimestamp() + 60*60*24*30*3;
			$tokenRefest = JWT::encode($decoded, $kunci);
			$decoded->tokenRefest = $tokenRefest;
            $this->user_data = $decoded;
        } catch (Exception $e) {
            $invalid = ['status' => $e->getMessage()]; //Respon if credential invalid
            $this->response($invalid, 401);//401
        }
    }

	function getTime_get($project_id) {

        if(isset($project_id) && $project_id){

            $project = $this->Project->getProjectId($project_id);
            if(isset($project) && $project){

                $date1 = new DateTime($project->collection_end_date);
                $date2 = new DateTime(date('Y-m-d H:i:s'));
                if($date1 > $date2){
                  $time = $date1->diff($date2);

                  $date = date_diff($date1,$date2);
                  $date = $date->format("%a");
                  $format_collection_end_date = array(
                    'date' =>$date,
                    'hour' => $time->h,
                    'minutes' => $time->i,
                  );
                }else{
                  $format_collection_end_date = array(
                    'status'=>'終了'
                  );
                }
                return $format_collection_end_date;
            }else{

                $error = array(
                    'status' => 'id is not exists!'
                );
                return $error;

            }
        }
        $error = array(
            'status' => 'id is not exists!'
        );
        return $error;
    }

	function getTimeProject_get($collection_end_date) {

		if(isset($collection_end_date) && $collection_end_date){
			$date1 = new DateTime($collection_end_date);
			$date2 = new DateTime(date('Y-m-d H:i:s'));
			if($date1 > $date2){
				$time = $date1->diff($date2);

				$date = date_diff($date1,$date2);
				$date = $date->format("%a");
				$format_collection_end_date = array(
					'date' =>$date,
					'hour' => $time->h,
					'minutes' => $time->i,
				);
			}else{
				$format_collection_end_date = array(
					'status'=>'終了'
				);
			}
			return $format_collection_end_date;

		}

	}

	/**
	 * [user_profile_get Get User Current folow Auth(MY_Controller)]
	 * @return [type] [description]
	 */
	public function profileUser_get($user_id) {
		// Get Profile
		// $checkUser = $this->User->checkSocical();
		$curUser =  $this->User->getUserId($user_id);
		if(isset($curUser) && $curUser) {
            // Get Group Current(ion_auth)
            $group = $this->ion_auth->get_users_groups($user_id)->row();
            // Get Group Name
            $group_name = $group->name;
            // Count MY Project
            $numberProject = $this->Project->getProjectUserId($user_id);

            $numberPatron = $this->BackedProject->countBackedInfoDonate($user_id);
            // Create Object
            $safeUserObject = null;

            $listAddress = $this->AddressUser->getAddressUserId($user_id);

            $FormatImage = getImage($curUser->profileImageURL);

            $numberNotification = $this->Notification_info->getNumberNotification($user_id);

            $listFan = $this->ListDonateFanClub->getAllDonateFanClub($user_id);
            $numberPatronfanclub = $listFan['total'];
            $new = '';

            foreach ($listFan['data'] as $key => $value) {
                if($value->status_join_fanclub === '0'){
                    if($new === '') {
                        $new = $value->backing_levels_id;
                    }else{
                        $new = $new.','.$value->backing_levels_id;
                    }
                }
            }

            $safeUserObject = array(
                "id"                 =>     $curUser->id,
                "username"           =>     $curUser->username,
                "roles"              =>     $group_name,
                'profileImageURL'    =>     $curUser->profileImageURL,
                'medium_profileImageURL' => $FormatImage['medium'],
                'small_profileImageURL' =>  $FormatImage['small'],
                'created'            =>     $curUser->created_on,
                'sex'                =>     $curUser->sex,
                'birthday'           =>     $curUser->birthday,
                'self_description'   =>     $curUser->self_description,
                'url1'               =>     $curUser->url1,
                'url2'               =>     $curUser->url2,
                'url3'               =>     $curUser->url3,
                'address'            =>     $curUser->address,
                'job'                =>     $curUser->job,
                'email'              =>     $curUser->email,
                'bank_number'        =>     $curUser->bank_number,
                'bank_owner'         =>     $curUser->bank_owner,
                'bank_name'          =>     $curUser->bank_name,
                'bank_branch'        =>     $curUser->bank_branch,
                'bank_type'          =>     $curUser->bank_type,
                'country'            =>     $curUser->country,
                'eventProjectSocket' =>     $curUser->eventProjectSocket,
                'numberProject'      =>     count($numberProject),
                'patron'             =>     count($numberPatron),
                'patronfanclub'      =>     $numberPatronfanclub,
                'listFanClub'        =>     $new,
                'tokenRefest'        =>     $this->user_data->tokenRefest,
                'listAddress'        =>     $listAddress,
                'number_notification'=>     $numberNotification,
                'statusUnsubscribe'  =>     $curUser->unsubscribe,
                'unsubscribeProject' =>     $curUser->unsubscribeProject,
                'wish_list'          =>     $curUser->wish_list,
            );
            if(isset($curUser->customer_id) && $curUser->customer_id ) {
                $safeUserObject['number_card'] = '************'.$curUser->last4;
                $safeUserObject['exp_month'] = $curUser->card_month;
                $safeUserObject['exp_year'] = $curUser->card_year;
                $safeUserObject['brand'] = $curUser->card_brand;
                $safeUserObject['cvc'] = $curUser->cvc;
            }else{
                $safeUserObject['number_card'] = '';
                $safeUserObject['exp_month'] = '';
                $safeUserObject['exp_year'] = '';
                $safeUserObject['brand'] = '';
                $safeUserObject['cvc'] = '';
            }

            $this->response($safeUserObject, 200);

        }else{

            $error = array('status'=>'user is not exists!');
            $this->response($error, 404);
        }
    }
}
