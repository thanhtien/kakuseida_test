<?php if(!defined('BASEPATH')) exit('No direct script allowed');

class BackedProject extends BD_Model{
	var $table = 'backed_projects';
	// Get All BackedProject (Data Donate)
	function getBackedProject(){
		$this->db->from($this->table);
		$a = $this->db->get();
		return $a->result();
	}

	function getBackedProjectById($id){
		$this->db->from($this->table);
		$this->db->where('id',$id);
		$a = $this->db->get();
		return $a->row();
	}
  // function getBackedProject(){
  //     $this->db->select('*');
  //     $this->db->from($this->table);
  //     $this->db->join('backing_levels','backing_levels.id = backed_projects.backing_level_id');
  //     $query = $this->db->get();
  //     return $query->result();
  // }

	// Get All Data Donate with user_id
	function countBackedProject($id){
		$this->db->from('listuserdonate');
		$this->db->where('user_id',$id);
		$a = $this->db->get();
		return $a->result();
	}

	function countBackedInfoDonate($user_id){
		$this->db->from($this->table);
		$this->db->where('user_id',$user_id);
		$a = $this->db->get();
		return $a->result();
	}

	function getOwnerProjectDonate($user_id,$input = null){
		$this->db->select('backed_projects.id,backed_projects.user_id,backed_projects.project_id,projects.project_name');
		$this->db->select('backed_projects.user_id as `user_id_current`');
		$this->db->from($this->table);
		$this->db->where('backed_projects.user_id',$user_id);
		$this->db->join('projects','projects.id = backed_projects.project_id');
		$this->db->group_by('backed_projects.project_id');
        $total = $this->db->count_all_results();

		$this->db->select('backed_projects.id,backed_projects.user_id,backed_projects.project_id,projects.project_name,projects.thumbnail,projects.user_id,users.email');
		$this->db->select('backed_projects.user_id as `user_id_current`');
		$this->db->from($this->table);
		$this->db->where('backed_projects.user_id',$user_id);
		$this->db->join('projects','projects.id = backed_projects.project_id');
		$this->db->join('users','users.id = projects.user_id');
		$this->db->group_by('backed_projects.project_id');
		if($input){
			$this->db->limit($input['limit'][0], $input['limit'][1]);
		}
		$project = $this->db->get();
		$project = $project->result();
		return ["total" => $total, "project" => $project];

	}

	function checkBackedDonate($user_id,$project_id){
		$this->db->from($this->table);
		$this->db->where('user_id',$user_id);
		$this->db->where('project_id',$project_id);
		$a = $this->db->get();
		return $a->num_rows();
	}

	function BackedDonate($user_id,$project_id,$backing_level_id){
		$this->db->from($this->table);
		$this->db->where('user_id',$user_id);
		$this->db->where('project_id',$project_id);
		$this->db->where('backing_level_id',$backing_level_id);
		$a = $this->db->get();
		return $a->row();
	}

  	public function checkAddressDonate($id){
		if(isset($id) && $id){
			$this->db->from($this->table);
		    $this->db->where('id_address',$id);
		    $para = $this->db->get();
			$para = $para->row();
			if($para){
				return $para;
			}else{
				return false;
			}
		}else{
			return false;
		}
	}

	// Get All Data Donate with project_id
	function BackedProjectProjectId($id){
		$this->db->from($this->table);
		$this->db->where('project_id',$id);
		$a = $this->db->get();
		return $a->result();
	}

	function BackedInfoDonate($user_id,$input = null){
		$this->db->from($this->table);
		$this->db->where('user_id',$user_id);
		$this->db->order_by('created','DESC');
		if($input){
			$this->db->limit($input['limit'][0], $input['limit'][1]);
		}
		$a = $this->db->get();
		return $a->result();
	}

	function BackedInfoDonateNormal($user_id,$input = null){
		$this->db->select($this->table.'.*,projects.project_type');
		$this->db->from($this->table);
		$this->db->join('projects','projects.id = backed_projects.project_id');
		$this->db->where('backed_projects.user_id',$user_id);
		$this->db->where('projects.project_type','0');
		$this->db->order_by('created','DESC');
		if($input){
			$this->db->limit($input['limit'][0], $input['limit'][1]);
		}
		$a = $this->db->get();
		return $a->result();
	}

	// Get All Project + limit with user_id
	function dataBackedProject($id,$input = null){
		$this->db->select('projects.*');
		$this->db->from('listuserdonate');
	   	$this->db->join('projects','projects.id = listuserdonate.project_id');
	   	if($input){
		   	$this->db->limit($input['limit'][0], $input['limit'][1]);
	   	}
	   	$this->db->where('listuserdonate.user_id',$id);
	   	$this->db->order_by("listuserdonate.created", "desc");
	   	$backed = $this->db->get();
	   	return $backed->result();
  	}
	// Get All Data Donate with project_id
  	function getBackedProjectId($id,$input = null){
    	$this->db->from($this->table);
    	$this->db->where('project_id',$id);
		$this->db->order_by('created','DESC');
		if($input){
			$this->db->limit($input['limit'][0], $input['limit'][1]);
		}
    	$a = $this->db->get();
    	return $a->result();
  	}

	function getBackedFanClubById($id,$input = null){
		$this->db->from($this->table);
		$this->db->where('project_id',$id);
		$this->db->order_by('created','DESC');
		$this->db->where('created >=', $input['first_day']);
		$this->db->where('created <=', $input['last_day']);
		if(isset($input['limit']) && $input['limit']){
			$this->db->limit($input['limit'][0], $input['limit'][1]);
		}
		$a = $this->db->get();
		return $a->result();
	}

	function getAllBackedProjectExpired(){
		// Get date
		$date = new DateTime();
	    $date = $date->format('Y-m-d H:i:s');
		$this->db->from($this->table);
		$this->db->where('type','Bank');
		$this->db->where('status','unsuccess');
		$this->db->where('created <', $date);
		$a = $this->db->get();
		return $a->result();
	}

	function DonateDate($project_id,$first_day) {
		if(isset($project_id) && isset($first_day)  && $project_id && $first_day) {
			// $date = date_create($first_day);
			$date = new DateTime($first_day);
			$first_day = date_format($date,"Y-m-d 00:00:00");
			$last_day = date_format($date,"Y-m-d 23:59:59");
			$this->db->select('invest_amount');
			$this->db->from($this->table);
			$this->db->where('project_id',$project_id);
			$this->db->where('created >=', $first_day);
			$this->db->where('created <=', $last_day);
			$a = $this->db->get();
			return $a->result();
		}
	}


	function DonateDateTotal($project_id,$first_day,$last_day) {
		if(isset($project_id) && isset($first_day) && isset($last_day)  && $last_day && $project_id && $first_day) {
			// $date = date_create($first_day);
			$date1 = new DateTime($first_day);
			$date2 = new DateTime($last_day);
			$first_day = date_format($date1,"Y-m-d 00:00:00");
			$last_day = date_format($date2,"Y-m-d 23:59:59");
			$this->db->select('invest_amount');
			$this->db->from($this->table);
			$this->db->where('project_id',$project_id);
			$this->db->where('created >=', $first_day);
			$this->db->where('created <=', $last_day);
			$a = $this->db->get();
			return $a->result();
		}
	}

	function getInfoDonater($project_id,$first_day,$last_day) {
		if(isset($project_id) && isset($first_day) && isset($last_day)  && $last_day && $project_id && $first_day) {
			// $date = date_create($first_day);
			$date1 = new DateTime($first_day);
			$date2 = new DateTime($last_day);
			$first_day = date_format($date1,"Y-m-d 00:00:00");
			$last_day = date_format($date2,"Y-m-d 23:59:59");
			$this->db->from($this->table);
			$this->db->where('project_id',$project_id);
			$this->db->where('created >=', $first_day);
			$this->db->where('created <=', $last_day);
			$this->db->where('status', 'succeeded');
			$this->db->where('first_status', '0');
			$a = $this->db->get();
			return $a->result();
		}
	}


	function getBackedProjectByProjectId($project_id){
		$this->db->select('backed_projects.*,users.username,users.token,users.email as emailUser');
		$this->db->from($this->table);
		$this->db->join('users','users.id = backed_projects.user_id');
		$this->db->where('backed_projects.project_id',$project_id);
		$this->db->where('backed_projects.status_finish_project','0');
		$a = $this->db->get();
		return $a->result();
	}

	function getProjectBlockByProjectId($project_id){
		$this->db->select('backed_projects.*,users.username,users.token,users.email as emailUser,projects.project_name');
		$this->db->from($this->table);
		$this->db->join('users','users.id = backed_projects.user_id');
		$this->db->join('projects','projects.id = backed_projects.project_id');
		$this->db->where('backed_projects.project_id',$project_id);
		$this->db->where('backed_projects.status_block_user','0');
		$this->db->order_by('backed_projects.user_id','DESC');
		$a = $this->db->get();
		return $a->result();
	}

	function getProjectBlockByUser_id($user_id){
		$this->db->select('backed_projects.*,users.username,users.token,users.email as emailUser,projects.project_name,projects.user_id as IdUserOwner');
		$this->db->from($this->table);
		$this->db->join('users','users.id = backed_projects.user_id');
		$this->db->join('projects','projects.id = backed_projects.project_id');
		$this->db->where('backed_projects.user_id',$user_id);
		$this->db->where('backed_projects.status_owner_block','0');
		$this->db->where('projects.opened','yes');
		$this->db->where('projects.active','yes');
		$this->db->order_by('backed_projects.user_id','DESC');
		$a = $this->db->get();
		return $a->result();
	}

	public function updateBackedByProjectVsUser($user_id,$project_id,$data) {

		if(isset($user_id) && $user_id && isset($project_id) && $project_id){

    		$update = $this->db->update($this->table, $data, array('user_id' => $user_id,'project_id' => $project_id));

			if(isset($update) && $update){
				return true;
			}else{
				return false;
			}

		}else{

			return false;

		}

	}
}
