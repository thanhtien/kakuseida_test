<?php if(!defined('BASEPATH')) exit('No direct script allowed');

class Category extends BD_Model{
  var $table = 'categories';
  // Get all Category
  function getCategory(){
		$this->db->from($this->table);
		$a = $this->db->get();
		return $a->result();
	}
  function getCategoryId($id){
        $this->db->select('*');
		$this->db->from($this->table);
		$this->db->where('id', $id);
		$a = $this->db->get();
		return $a->row();
  }
  // Get Category with slug
  function getCategorySlug($slug){
		$this->db->select('*');
		$this->db->from($this->table);
		$this->db->where('slug', $slug);
		$a = $this->db->get();
		return $a->row();
	}
}
