<?php if(!defined('BASEPATH')) exit('No direct script allowed');

class M_main extends CI_Model{
	var $table = 'users';
	// function get_user($q) {
	// 	return $this->db->get_where('m_user',$q);
	// }
	function getall() {
		$query = $this->db->get($this->table);
		return $query;
    }
    function getdatavsemail($email) {
        if($email){
            $query = $this->db->where('email',$email)->get($this->table);
            return $query;
        }

    }
    function get_list($input = array())
    {
        //xu ly ca du lieu dau vao
        $this->get_list_set_input($input);
        
        //thuc hien truy van du lieu
        $query = $this->db->get($this->table);
        //echo $this->db->last_query();
        return $query->result();
    }
    protected function get_list_set_input($input = array())
    {

        // Thêm điều kiện cho câu truy vấn truyền qua biến $input['where']
        //(vi du: $input['where'] = array('email' => 'hocphp@gmail.com'))
        if ((isset($input['where'])) && $input['where'])
        {
            $this->db->where($input['where']);
        }

        //tim kiem like
        // $input['like'] = array('name' => 'abc');
        if ((isset($input['like'])) && $input['like'])
        {
            $this->db->like($input['like'][0], $input['like'][1]);
        }

        // Thêm điều kiện limit cho câu truy vấn thông qua biến $input['limit']
        //(ví dụ $input['limit'] = array('10' ,'0'))
        if (isset($input['limit'][0]) && isset($input['limit'][1]))
        {
            $this->db->limit($input['limit'][0], $input['limit'][1]);
        }

    }
    function get_total($input = array())
    {
        $this->get_list_set_input($input);

        $query = $this->db->get($this->table);

        return $query->num_rows();
    }
    function update($id,$data){

        if( isset($id) && count($id) > 0 ){

        $this->db->where('id',$id);

        if($this->db->update($this->table,$data)) {

        return true;

        } else { return false; }

        } else { return false; }

	}



}
