<?php if(!defined('BASEPATH')) exit('No direct script allowed');

class News extends BD_Model{
	var $table = 'news';
	// Get News with id
  function getNewId($id){
      $this->db->from($this->table);
      $this->db->where('id',$id);
	  $this->db->where('status','1');
      $param = $this->db->get();
	  if(isset($param)){
		  return $param->row();
	  }else{
		  return false;
	  }
  }
}
