<?php if(!defined('BASEPATH')) exit('No direct script allowed');

class News_project extends BD_Model{
	var $table = 'projects';

  function News_data_projects(){
			//
      $this->db->select($this->table.'.*,users.username,categories.name');
      $this->db->from($this->table);
      $this->db->join('users','users.id = projects.user_id');
      $this->db->join('categories','categories.id = projects.category_id');
      $this->db->order_by('created', 'DESC');
      $this->db->limit(11, 0);
      $query = $this->db->get();
      return $query->result();
  }
	function get_banking_level(){
		$this->db->select('*');
		$this->db->from('backing_levels');
		$query = $this->db->get();
		return $query->result();
	}

}
