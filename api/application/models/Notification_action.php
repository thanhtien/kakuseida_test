<?php if(!defined('BASEPATH')) exit('No direct script allowed');

class Notification_action extends BD_Model{
	var $table = 'notification_action';
	// Get News with id

	public function getNotificationById($user_id){
		if(isset($user_id) && $user_id){
			$this->db->from($this->table);
			$this->db->where('user_id',$user_id);
			$param = $this->db->get();
			if(isset($param)){
				return $param->result();
			}else{
				return [];
			}

		}else{
			return [];
		}
	}

	public function DeleteNotification($id,$user_id){
		if(isset($id) && $id && isset($user_id) && $user_id){
			$this->db->where('action', $id);
			$this->db->where('user_id', $user_id);
			$param = $this->db->delete($this->table);
			if(isset($param)){
				return true;
			}else{
				return false;
			}
		}else{
			return false;
		}

	}
}
