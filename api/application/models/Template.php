<?php if(!defined('BASEPATH')) exit('No direct script allowed');

class Template extends BD_Model{
	var $table = 'template';


    function getFirstTemplate(){
		$this->db->from($this->table);
		$this->db->where('status', 0);
		$template = $this->db->get();
		return $template->row();
	}

	function getAllEmailSubscription(){
        $this->db->select('*');
        $this->db->where('status','0');
        $query = $this->db->get($this->table);
        return $query->result();
    }

	function getTemplateById($id){
		$this->db->select('*');
		$this->db->where('id',$id);
		$query = $this->db->get($this->table);
		return $query->row();
	}
}
