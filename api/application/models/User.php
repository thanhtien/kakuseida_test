<?php if(!defined('BASEPATH')) exit('No direct script allowed');

class User extends BD_Model{
	var $table = 'users';
	function getUser(){
		$this->db->from($this->table);
		$data = $this->db->get();
		return $data->result();
	}
	function getUserId($id){
		$this->db->select('users.*,subscription.unsubscribe');
		$this->db->select('users.id as `id`');
		$this->db->from($this->table);
		$this->db->where('users.id',$id);
		$this->db->join('subscription','subscription.email = users.email');
		$user = $this->db->get();
		return $user->row();
	}

	function checkSocical($id){
		$this->db->from($this->table);
		$user = $this->db->get();
		return $user->row();
	}

	function getUserSocialId($id) {
		$this->db->from($this->table);
		$this->db->where('id',$id);
		$user = $this->db->get();
		return $user->row();
	}

	function activeSocial($token_active,$data) {
		$this->db->select('*');
		$this->db->from($this->table);
		$this->db->where('activation_code',$token_active);
		$user = $this->db->update($this->table,$data);
		if($user){
			return $id;
		}else{
			return false;
		}
		$user = $this->db->get();
		return $user->row();
	}

	function getAllUserBlock() {
		$this->db->select('*');
		$this->db->from($this->table);
		$this->db->where('active','0');
		$user = $this->db->get();
		return $user->result();
	}

	function getUsersBankNumber($bank_number){
		$this->db->select('*');
		$this->db->from($this->table);
		$this->db->where('bank_number', $bank_number);
		$data = $this->db->get();
		return $data->row();
	}
	function getUsersStripecvc($last4){
		$this->db->select('*');
		$this->db->from($this->table);
		$this->db->where('last4', $last4);
		$data = $this->db->get();
		return $data->result();
	}


	function getDataEmail($email) {
		if($email){
			$query = $this->db->where('email',$email)->get($this->table);
			return $query->result();
		}else{
			return false;
		}
	}

	function getDataUser($name) {
		if($name){
			$query = $this->db->where('username',$name)->get($this->table);
			return $query->row();
		}else{
			return false;
		}
	}
	function editUser($id,$data){
		if(isset($id) && isset($data)){
			$this->db->from($this->table);
			$this->db->where('id',$id);
			$user = $this->db->update($this->table,$data);
			if($user){
				return $id;
			}else{
				return false;
			}
		}else{
			return false;
		}
	}

	function editUserUnSubscribe($email,$data){
		if(isset($email) && isset($data)){
			$this->db->from($this->table);
			$this->db->where('email',$email);
			$user = $this->db->update($this->table,$data);
			if($user){
				return true;
			}else{
				return false;
			}
		}else{
			return false;
		}
	}

	function checkIdFacebook($facebookId){
		if($facebookId){
			$query = $this->db->where('facebook_id',$facebookId)->get($this->table);
			if(isset($query) && $query){
				return $query->row();
			}else{
				return false;
			}
		}else{
			return false;
		}
	}

	function checkIdTwitter($Twitter){
		if($Twitter){
			$query = $this->db->where('twitter_id',$Twitter)->get($this->table);
			if(isset($query) && $query){
				return $query->row();
			}else{
				return false;
			}
		}else{
			return false;
		}
	}

	public function checkUsername($username){
		if($username){
			$query = $this->db->where('username',$username)->get($this->table);
			if(isset($query) && $query){
				return $query->row();
			}else{
				return false;
			}
		}else{
			return false;
		}
	}

	public function getUserJoinAllUser(){
		$this->db->from($this->table);
		$this->db->where('status_all_user','0');
		$this->db->where('token IS NOT NULL', null, false);
		$this->db->limit(10,0);
		$data = $this->db->get();
		return $data->result();
	}

	public function checkUser($email){
		$this->db->select('users.email,users.active');
		$this->db->from($this->table);
		$this->db->where('email',$email);
		$data = $this->db->get();
		return $data->row();
	}
}
