<?php
	defined('BASEPATH') OR exit('No direct script access allowed');

	class Home extends CI_Controller {

        public function __construct() {
            parent::__construct();
             if (!$this->session->userdata['is_admin']) {
                redirect(site_url('login'));
            }
        }

		public function index()
		{
			$this->load->view('site/page');
		}


		function elfinder_init()
		{
            $this->load->helper('path');
            $opts = array(
                // 'debug' => true,
                'roots' => array(
                array(
                    'driver' => 'LocalFileSystem',
                    'path'   => set_realpath('images'),
                    'URL'    => base_url('/images') . '/'

                )
                )
            );
		  $this->load->library('elfinder_lib', $opts);
		}

	}

	/* End of file Home.php */
	/* Location: ./application/controllers/Home.php */
?>
