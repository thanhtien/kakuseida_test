<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->helper(array('form', 'html', 'file', 'path'));
        $this->load->library('form_validation');
        $this->load->library('session');
        //$this->load->model('pages_model');
    }

    public function index() {
        if ($this->session->userdata('is_admin')) {
            redirect(site_url('/'));
        }
        $data = [];
        if ($this->input->post('email')) {
            $remember = true;
            $email = $this->input->post('email');
            $pass = $this->input->post('password');
            $this->load->library('form_validation');
            $this->form_validation->set_rules('password', 'Password', 'trim|required');
            $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');
            if ($this->form_validation->run() !== false) {

                if ($this->ion_auth->login($email, $pass,$remember)) {
                    if (!$this->ion_auth->is_admin()) {
                        $this->session->set_flashdata('error', 'この画面は管理者しか閲覧できません');
                        redirect('login');
                    } else {
                        $user = $this->ion_auth->user()->row();
                        $this->session->set_userdata('is_admin', true);
                        $this->session->set_userdata('user_info', array(
                            'email' => $user->email,
                            'id' => $user->id,
                            'name' => $user->username));
                        redirect("/");
                    }
                } else {
                    $this->session->set_flashdata('error', 'ユーザー名かパスワードが正しくない');
                    redirect('login');
                }
            } else {
                $this->session->set_flashdata('error', 'メールアドレスを正しく入力してください');
            }
        }
        $this->load->view('login',$data);
    }

    public function signout() {
        $this->session->unset_userdata("is_admin");
        $this->session->unset_userdata("user_info");
        redirect("/");
    }

}
