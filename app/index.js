const express = require('express');
const bodyParser = require('body-parser')
const path = require('path');
const app = express();
const request = require('request');
var fs = require('fs');

if (fs.existsSync(__dirname+'/build/index.html')) {

  app.get('/project-detail/*', function(resApp, response) {

    const filePath = path.resolve(__dirname, './build', 'index.html')
    fs.readFile(filePath, 'utf8', function (err,data) {
      if (err) {
        return console.log(err);
      }
      var url = `https://api.kakuseida.com/v2/guest/Meta/projectDetail/${resApp.params[0]}`;

      request(url, { json: true }, (err, res, body) => {
        if (err) { return console.log(err); }

        data = data.replace(/\$OG_TITLE/g, body.title);
        data = data.replace(/\$OG_DESCRIPTION/g, body.description);
        result = data.replace(/\$OG_IMAGE/g, body.thumbnail);
        response.send(result);
      });

    });
  });

  app.get('/new-detail/*', function(resApp, response) {

    const filePath = path.resolve(__dirname, './build', 'index.html')
    fs.readFile(filePath, 'utf8', function (err,data) {
      if (err) {
        return console.log(err);
      }

      var url = `https://api.kakuseida.com/v2/guest/Meta/newDetail/${resApp.params[0]}`;

      request(url, { json: true }, (err, res, body) => {
        if (err) { return console.log(err); }
        data = data.replace(/\$OG_TITLE/g, body.title);
        data = data.replace(/\$OG_DESCRIPTION/g, body.description);
        result = data.replace(/\$OG_IMAGE/g, body.thumbnail);
        response.send(result);
      });

    });
  });

  app.get('/*', function (req, res , next) {
    if (req.headers.host.match(/^www/) !== null ) {
      res.redirect('http://' + req.headers.host.replace(/^www\./, '') + req.url);
    } else {
      const filePath = path.resolve(__dirname, './build', 'index.html');
      fs.readFile(filePath, 'utf8', function (err,data) {
        if (err) {
          return console.log(err);
        }

        data = data.replace(/\$OG_TITLE/g, "kakuseida");
        data = data.replace(/\$OG_DESCRIPTION/g, "kakuseida Clound System");
        data = data.replace(/\$OG_IMAGE/g, "https://static.kakuseida.com/images/2018/default/error.jpg?_=1544412382");
        res.send(data);

      });

    }
  });
}else {
  app.get('/*', function (req, res , next) {
    if (req.headers.host.match(/^www/) !== null ) {
      res.redirect('http://' + req.headers.host.replace(/^www\./, '') + req.url);
    } else {
      app.use(express.static(path.join(__dirname, 'default')));
      res.sendFile(path.join(__dirname, 'default', 'index.html'));
    }
  });
}

app.listen(90);
