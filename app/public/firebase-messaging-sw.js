importScripts('https://www.gstatic.com/firebasejs/4.8.1/firebase-app.js');
importScripts('https://www.gstatic.com/firebasejs/4.8.1/firebase-messaging.js');
firebase.initializeApp({
  "messagingSenderId": "837376760710"
});


const messaging = firebase.messaging();
messaging.setBackgroundMessageHandler(payload => {
  const title = payload.title;
  const options = {
      body: payload.body,
      icon: payload.icon
  };
  self.registration.showNotification(title, options);
});
