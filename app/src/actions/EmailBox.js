import axios from 'axios';
import History from '../history.js';
import {
  LIST_PROJECT_MAIL,
  UNAUTH_USER,
  OWNER_PROJECT_PROFILE,
  SEND_EMAIL_BOX,
  RECIVE_EMAIL,
  SENDED_EMAIL,
  STATUS_MAILBOX,
  CHANGE_STATUS_EMAIL
} from './types';
var CONFIG = require('../config/common');

if (process.env.NODE_ENV === "development") {
  CONFIG = CONFIG.CONFIG.DEV;
}else {
  CONFIG = CONFIG.CONFIG.PRODUCT;
}
const ROOT_URL = CONFIG.ROOT_URL;
const token = 'Bearer '+ localStorage.getItem('token');
//Config Axios
const config = {
  headers: {
    "Authorization" : token
  }
};


export const ProjectEmailBox = (start) => {
  return (dispatch) => {

      //Config Axios
      if (start < 0) {
        start = 0;
      }

      axios.get(`${ROOT_URL}api/Users/getAllOwnerDonater?page=${start}` , config)
        .then(response => {

          dispatch({
            type: LIST_PROJECT_MAIL,
            payload: response.data,
            page:start
          });

          var number = Number(start)+1;
          window.history.replaceState({urlPath:''},"",`page=${number}`);

        }).catch((error) => {


          if (error) {

            if (error.response.data.status === 'Expired token') {
              localStorage.removeItem('token')
              setTimeout(function () {
                History.push('/signin');
              }, 1000);
              dispatch({ type: UNAUTH_USER });
            } else if(error.response.status === 400) {
              alert(error.response.data.status);
            }
            else if(error.response.status === 401) {
              History.push('/unauthorized');
            }
            else if(error.response.status === 404) {
              History.push('/404');
            }
            else {
              alert(error.response.data.status);
            }
          }

        });
    };
};



export const getProfileOwner = (id) => {
  return (dispatch) => {

      axios.get(`${ROOT_URL}v2/user/MailBoxs/getInfoOwner?project_id=${id}` , config)
        .then(response => {

          dispatch({
            type: OWNER_PROJECT_PROFILE,
            payload: response.data
          });



        }).catch((error) => {


          if (error) {

            if (error.response.data.status === 'Expired token') {
              localStorage.removeItem('token')
              setTimeout(function () {
                History.push('/signin');
              }, 1000);
              dispatch({ type: UNAUTH_USER });
            } else if(error.response.status === 400) {
              alert(error.response.data.status);
            }
            else if(error.response.status === 401) {
              History.push('/unauthorized');
            }
            else if(error.response.status === 404) {
              History.push('/404');
            }
            else {
              alert(error.response.data.status);
            }
          }

        });
    };
};

export const getProfileOwnerUserID = (id) => {
  return (dispatch) => {

      axios.get(`${ROOT_URL}v2/user/MailBoxs/getInfoUser?user_id=${id}` , config)
        .then(response => {

          dispatch({
            type: OWNER_PROJECT_PROFILE,
            payload: response.data
          });



        }).catch((error) => {


          if (error) {

            if (error.response.data.status === 'Expired token') {
              localStorage.removeItem('token')
              setTimeout(function () {
                History.push('/signin');
              }, 1000);
              dispatch({ type: UNAUTH_USER });
            } else if(error.response.status === 400) {
              alert(error.response.data.status);
            }
            else if(error.response.status === 401) {
              History.push('/unauthorized');
            }
            else if(error.response.status === 404) {
              History.push('/404');
            }
            else {
              alert(error.response.data.status);
            }
          }

        });
    };
};

export const SendEmailToOwnner = (data,loading,message) => {
  return (dispatch) => {
      loading();
      axios.post(`${ROOT_URL}v2/user/MailBoxs/sendMailBox` , data ,config)
        .then(response => {
          dispatch({
            type: SEND_EMAIL_BOX,
            payload: response.data,
            message:'メッセージが送信できました。'
          });
          loading();

          History.push('/my-page/mail-sended/page=0');

        })
        .catch((error) => {
          if (error) {
            loading();
            if (error.response.data.status === 'Expired token') {
              localStorage.removeItem('token')
              setTimeout(function () {
                History.push('/signin');
              }, 1000);
              dispatch({ type: UNAUTH_USER });
            } else if(error.response.status === 400) {
              alert(error.response.data.status);
            }
            else if(error.response.status === 401) {
              History.push('/unauthorized');
            }
            else if(error.response.status === 404) {
              History.push('/404');
            }
            else {
              alert(error.response.data.status);
            }
          }
        });
    };
};


export const goToReply = (data) => {
  return (dispatch) => {
    dispatch({
      type: CHANGE_STATUS_EMAIL,
      payload: data.id,
    });

    History.push(`/my-page/email-box/new-user/${data.user_id_send}/project/${data.project_id}`);

    axios.get(`${ROOT_URL}/v2/user/MailBoxs/checkReadMailBox?id=${data.id}` ,config);

  };
};

export const goToRead = (data) => {
  return (dispatch) => {
    dispatch({
      type: CHANGE_STATUS_EMAIL,
      payload: data.id,
    });


    axios.get(`${ROOT_URL}/v2/user/MailBoxs/checkReadMailBox?id=${data.id}` ,config);

  };
};

export const reciveEmail = (start ,loading) => {
  return (dispatch) => {
      //Config Axios
      if (start < 0) {
        start = 0;
      }

      loading();
      axios.get(`${ROOT_URL}v2/user/MailBoxs/getMailReceive?page=${start}`  ,config)
        .then(response => {

          dispatch({
            type: RECIVE_EMAIL,
            payload: response.data,
            page:start
          });


          var number = Number(start)+1;
          window.history.replaceState({urlPath:''},"",`page=${number}`);
          loading();

        })
        .catch((error) => {
          if (error) {
            loading();

            if (error.response.data.status === 'Expired token') {
              localStorage.removeItem('token')
              setTimeout(function () {
                History.push('/signin');
              }, 1000);
              dispatch({ type: UNAUTH_USER });
            } else if(error.response.status === 400) {
              alert(error.response.data.status);
            }
            else if(error.response.status === 401) {
              History.push('/unauthorized');
            }
            else if(error.response.status === 404) {
              History.push('/404');
            }
            else {
              alert(error.response.data.status);
            }
          }
        });
    };
};

export const sendedEmail = (start ,loading) => {
  return (dispatch) => {
      //Config Axios
      if (start < 0) {
        start = 0;
      }

      loading();
      axios.get(`${ROOT_URL}v2/user/MailBoxs/getMailSend?page=${start}`  , config)
        .then(response => {

          dispatch({
            type: SENDED_EMAIL,
            payload: response.data,
            page:start
          });


          var number = Number(start)+1;
          window.history.replaceState({urlPath:''},"",`page=${number}`);
          loading();

        })
        .catch((error) => {
          if (error) {
            loading();

            if (error.response.data.status === 'Expired token') {
              localStorage.removeItem('token')
              setTimeout(function () {
                History.push('/signin');
              }, 1000);
              dispatch({ type: UNAUTH_USER });
            } else if(error.response.status === 400) {
              alert(error.response.data.status);
            }
            else if(error.response.status === 401) {
              History.push('/unauthorized');
            }
            else if(error.response.status === 404) {
              History.push('/404');
            }
            else {
              alert(error.response.data.status);
            }
          }
        });
    };
};


export const FlashMassage = () => {
  return(dispatch) => {
    dispatch({
      type: STATUS_MAILBOX,
    });
  }
}
