import axios from 'axios';
import History from '../history.js';
import {
    FETCH_PROJECT,FETCH_PROJECTS_NEW,LOADING_POST_STOP,
    LOADING_POST_START,FETCH_PROJECTS_ENOUGH_LIST,FETCH_PROJECTS_GOAL_LIST,EXPIRED_PAGINATION,NEWS_LIST,CAT_SLUG_LIST,NEW_DETAIL,FANCLUB_PAGINATION,PROJECT_S_PAGINATION
} from './types';





var CONFIG = require('../config/common');

if (process.env.NODE_ENV === "development") {
  CONFIG = CONFIG.CONFIG.DEV;
}else {
  CONFIG = CONFIG.CONFIG.PRODUCT;
}
const ROOT_URL = CONFIG.ROOT_URL;
function getPickUp() {
  return axios.get(ROOT_URL+'/API_Top/API/Pickup_projects');
}

function getFaProject() {
  return axios.get(ROOT_URL+'/API_Top/API/Favourite_projects');
}

function getRecommentProject() {
  return axios.get(ROOT_URL+'API_Top/API/Recommends_projects');
}

function getNews() {
  return axios.get(ROOT_URL+'/API_Top/API/News');
}

function getNewsProject() {
  return axios.get(ROOT_URL+'/API_Top/API/New_project');
}

function getProjectExpired() {
  return axios.get(ROOT_URL+'/API_Top/API/Expired');
}

function getProjectEnoughtGoalAmount() {
  return axios.get(ROOT_URL+'/API_Top/API/enough_goal_amount');
}
function getProjectEnoughExpired() {
  return axios.get(ROOT_URL+'/API_Top/API/enough_expired');
}

function getProjectFanClub() {
  return axios.get(ROOT_URL+'/API_Top/API/FanClub_projects');
}
function getProject100() {
  return axios.get(ROOT_URL+'/API_Top/API/full_goal_amount');
}

//Get List
export const fetchProject = () => {
  return (dispatch) => {
    axios.all([getPickUp(), getNews() , getFaProject() , getRecommentProject() , getNewsProject() , getProjectExpired(),getProjectEnoughtGoalAmount(),getProjectEnoughExpired(),getProjectFanClub(),getProject100()])
    .then(axios.spread(function (pickupProject, news , faProject , recommentProject , newsProject, Expired,EnoughtGoalAmount,EnoughExpired,FanClub,project100) {
      const data = [];
      data.push(pickupProject.data);
      data.push(news.data);
      data.push(faProject.data);
      data.push(recommentProject.data);
      data.push(newsProject.data);
      data.push(Expired.data);
      data.push(EnoughtGoalAmount.data);
      data.push(EnoughExpired.data);
      data.push(FanClub.data);
      data.push(project100.data);

      dispatch({
        type: FETCH_PROJECT,
        payload: data
      });
    }))
    .catch((error) => {

      if (error) {
        alert(error.response.data.status);
      }

    });
  };
};




//Get List
export const projectsNews = (start) => {

  return (dispatch) => {
    dispatch({
      type: LOADING_POST_START,
      payload: true
    });
    if (start < 0) {
      start = 0;
    }
    axios.get(ROOT_URL+'API_Top/API/newProject?start='+start)
    .then(response =>{

      dispatch({
        type: LOADING_POST_STOP,
        payload: false,
      });

      dispatch({
          type: FETCH_PROJECTS_NEW,
          payload: response.data,
          page:start
      });
      var number = Number(start)+1;

      window.history.replaceState({urlPath:''},"",`page=${number}`);


    })
    .catch(function (error) {
      dispatch({
        type: LOADING_POST_STOP,
        payload: false
      });
      if (error) {
        if(error.response.status === 404) {
          History.push('/404');
        }else {
          alert(error.response.data.status);
        }
      }
    });
  };
};



//Get List
export const projectsFanClub = (start) => {

  return (dispatch) => {
    dispatch({
      type: LOADING_POST_START,
      payload: true
    });
    if (start < 0) {
      start = 0;
    }
    axios.get(ROOT_URL+'API_Top/API/ListFanClub?start='+start)
    .then(response =>{

      dispatch({
        type: LOADING_POST_STOP,
        payload: false,
      });

      dispatch({
          type: FANCLUB_PAGINATION,
          payload: response.data,
          page:start
      });
        var number = Number(start)+1;

      window.history.replaceState({urlPath:''},"",`page=${number}`);

    })
    .catch(function (error) {
      dispatch({
        type: LOADING_POST_STOP,
        payload: false
      });
      if (error) {
        if(error.response.status === 404) {
          History.push('/404');
        }else {
          alert(error.response.data.status);
        }
      }
    });
  };
};



export const ProjectEnoughExpired = (start) => {

  return (dispatch) => {
    dispatch({
      type: LOADING_POST_START,
      payload: true
    });
    if (start < 0) {
      start = 0;
    }
    axios.get(ROOT_URL+'API_Top/API/enoughExpired?start='+start)
    .then(response =>{

      dispatch({
        type: LOADING_POST_STOP,
        payload: false
      });

      dispatch({
          type: FETCH_PROJECTS_ENOUGH_LIST,
          payload: response.data,
          page:start
      });


      var number = Number(start)+1;

      window.history.replaceState({urlPath:''},"",`page=${number}`);


    })
    .catch(function (error) {

      dispatch({
        type: LOADING_POST_STOP,
        payload: false
      });

      if (error) {
        if(error.response.status === 404) {
          History.push('/404');
        }else {
          alert(error.response.data.status);
        }
      }

    });
  };
};



export const ProjectEnoughtGoalAmountList = (start) => {

  return (dispatch) => {
    dispatch({
      type: LOADING_POST_START,
      payload: true
    });
    if (start < 0) {
      start = 0;
    }
    axios.get(ROOT_URL+'API_Top/API/enoughGoalAmount?start='+start)
    .then(response =>{
      dispatch({
        type: LOADING_POST_STOP,
        payload: false
      });
        dispatch({
            type: FETCH_PROJECTS_GOAL_LIST,
            payload: response.data,
            page:start
        });
        var number = Number(start)+1;
        window.history.replaceState({urlPath:''},"",`page=${number}`);

    })
    .catch(function (error) {

      dispatch({
        type: LOADING_POST_STOP,
        payload: false
      });

      if (error) {
        if(error.response.status === 404) {
          History.push('/404');
        }else {
          alert(error.response.data.status);
        }
      }

    });
  };
};



export const expiredPaginationlist = (start) => {

  return (dispatch) => {
    dispatch({
      type: LOADING_POST_START,
      payload: true
    });
    if (start < 0) {
      start = 0;
    }
    axios.get(ROOT_URL+'API_Top/API/expiredPagination?start='+start)
    .then(response =>{
      dispatch({
        type: LOADING_POST_STOP,
        payload: false
      });
        dispatch({
            type: EXPIRED_PAGINATION,
            payload: response.data,
            page:start
        });
        var number = Number(start)+1;
        window.history.replaceState({urlPath:''},"",`page=${number}`);

    })
    .catch(function (error) {

      dispatch({
        type: LOADING_POST_STOP,
        payload: false
      });

      if (error) {
        if(error.response.status === 404) {
          History.push('/404');
        }else {
          alert(error.response.data.status);
        }
      }

    });
  };
};



export const project100List = (start) => {

  return (dispatch) => {
    dispatch({
      type: LOADING_POST_START,
      payload: true
    });
    if (start < 0) {
      start = 0;
    }
    axios.get(ROOT_URL+'API_Top/API/fullGoalAmount?page='+start)
    .then(response =>{
      dispatch({
        type: LOADING_POST_STOP,
        payload: false
      });
        dispatch({
            type: PROJECT_S_PAGINATION,
            payload: response.data,
            page:start
        });
        var number = Number(start)+1;
        window.history.replaceState({urlPath:''},"",`page=${number}`);

    })
    .catch(function (error) {

      dispatch({
        type: LOADING_POST_STOP,
        payload: false
      });

      if (error) {
        if(error.response.status === 404) {
          History.push('/404');
        }else {
          alert(error.response.data.status);
        }
      }

    });
  };
};

export const Newslist = (start) => {

  return (dispatch) => {
    dispatch({
      type: LOADING_POST_START,
      payload: true
    });
    if (start < 0) {
      start = 0;
    }
    axios.get(ROOT_URL+'API_Top/API/news_list?start='+start)
    .then(response =>{
      dispatch({
        type: LOADING_POST_STOP,
        payload: false
      });
        dispatch({
            type: NEWS_LIST,
            payload: response.data
        });

        var number = Number(start)+1;
        window.history.replaceState({urlPath:''},"",`page=${number}`);

    })
    .catch(function (error) {

      dispatch({
        type: LOADING_POST_STOP,
        payload: false
      });

      if (error) {
        if(error.response.status === 404) {
          History.push('/404');
        }else {
          alert(error.response.data.status);
        }
      }

    });
  };
};




export const catSlugPaginationlist = (start,slug) => {
  return (dispatch) => {
    dispatch({
      type: LOADING_POST_START,
      payload: true
    });
    axios.get(ROOT_URL+'API_Top/API/project_category/'+slug+'/?start='+start)
    .then(response =>{
      dispatch({
        type: LOADING_POST_STOP,
        payload: false
      });
      dispatch({
          type: CAT_SLUG_LIST,
          payload: response.data,
          page:start
      });

      var number = Number(start)+1;
      if (response.data.data.length !== 0) {
        window.history.replaceState({urlPath:''},"",`page=${number}`);
      }



    })
    .catch(function (error) {
      dispatch({
        type: LOADING_POST_STOP,
        payload: false
      });
      if (error) {
        if(error.response.status === 404) {
          History.push('/404');
        }else {
          alert(error.response.data.status);
        }
      }
    });
  };
};



export const NewsDetail = (id) => {

  return (dispatch) => {
    dispatch({
      type: LOADING_POST_START,
      payload: true
    });
    axios.get(ROOT_URL+'API_Top/API/new_detail/'+id)
    .then(response =>{
      dispatch({
        type: LOADING_POST_STOP,
        payload: false
      });
        dispatch({
            type: NEW_DETAIL,
            payload: response.data
        });
    })
    .catch(function (error) {

      dispatch({
        type: LOADING_POST_STOP,
        payload: false
      });

      if (error) {
        if(error.response.status === 404) {
          History.push('/404');
        }else {
          alert(error.response.data.status);
        }
      }

    });
  };
};
