import axios from 'axios';
import History from '../history.js';
import {
  LIST_WISHLIST_PAGE,
  LOADING_POST_START,
  LOADING_POST_STOP,
  UNAUTH_USER,
  LEAVE_WISH_LIST_PAGE
} from './types';
var CONFIG = require('../config/common');

if (process.env.NODE_ENV === "development") {
  CONFIG = CONFIG.CONFIG.DEV;
}else {
  CONFIG = CONFIG.CONFIG.PRODUCT;
}
const ROOT_URL = CONFIG.ROOT_URL;
const token = 'Bearer '+ localStorage.getItem('token');
//Config Axios
const config = {
  headers: {
    "Authorization" : token
  }
};

export const leaveToWishList = (project_id,page) => {
  return (dispatch) => {


    dispatch({
      type: LOADING_POST_START,
      payload: true
    });

      var url = `${ROOT_URL}v2/user/Projects/leaveWishListPaginate`;

      var data = {
        project_id:project_id,
        page:page
      }

      axios.post(url, data ,config)
        .then(response => {

          var calcPage = page;
          dispatch({
            type: LOADING_POST_STOP,
            payload: false
          });
          if (response.data.page_count !== 0) {

            if (response.data.status) {
              window.history.replaceState({urlPath:''},"",`page=${calcPage}`);
            }

          }
          dispatch({
            type: LEAVE_WISH_LIST_PAGE,
            payload: response.data,
            page:calcPage
          });

          console.log(response.data);
        }).catch((error) => {
          dispatch({
            type: LOADING_POST_STOP,
            payload: false
          });

          if (error) {

            if (error.response.data.status === 'Expired token') {
              localStorage.removeItem('token')
              setTimeout(function () {
                History.push('/signin');
              }, 1000);
              dispatch({ type: UNAUTH_USER });
            } else if(error.response.status === 400) {
              alert(error.response.data.status);
            }
            else if(error.response.status === 401) {
              History.push('/unauthorized');
            }
            else if(error.response.status === 404) {
              History.push('/404');
            }
            else {
              alert(error.response.data.status);
            }
          }
        });
    };
};


export const wishList = (start) => {
  return (dispatch) => {

      dispatch({
        type: LOADING_POST_START,
        payload: true
      });

      //Config Axios
      if (start < 0) {
        start = 0;
      }
      var url = `${ROOT_URL}v2/user/Projects/listWishList?page=${start}`;

      axios.get(url, config)
        .then(response => {
          dispatch({
            type: LOADING_POST_STOP,
            payload: false
          });
          dispatch({
            type: LIST_WISHLIST_PAGE,
            payload:response.data,
            page:start
          });

          var number = Number(start)+1;
          window.history.replaceState({urlPath:''},"",`page=${number}`);



        }).catch((error) => {

          dispatch({
            type: LOADING_POST_STOP,
            payload: false
          });

          if (error) {
            if (error.response.data.status === 'Expired token') {
              localStorage.removeItem('token')
              setTimeout(function () {
                History.push('/signin');
              }, 1000);
              dispatch({ type: UNAUTH_USER });
            } else if(error.response.status === 400) {
              alert(error.response.data.status);
            }
            else if(error.response.status === 401) {
              History.push('/unauthorized');
            }
            else {
              alert(error.response.data.status);
            }
          }
        });
    };
};
