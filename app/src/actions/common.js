import axios from 'axios';
import History from '../history.js';
import {
    FOOTER_CAT,
    SEARCH_FORM,
    LOADING_POST_STOP,
    LOADING_POST_START,
    HEADER_AUTH,
    UNAUTH_USER,
    UPDATE_SEARCH_TEXT,
    CONTACT_FORM,
    NOTI_LIST,
    CHANGE_NUMBER_NOTI,
    READ_NOTI,
    CHANGE_NUMBER_NOTI_RECIVE,
    WISH_LIST
} from './types';

import {initialize} from 'redux-form';
import { info } from 'react-notification-system-redux';
var CONFIG = require('../config/common');

if (process.env.NODE_ENV === "development") {
  CONFIG = CONFIG.CONFIG.DEV;
}else {
  CONFIG = CONFIG.CONFIG.PRODUCT;
}
const ROOT_URL = CONFIG.ROOT_URL;



const token = 'Bearer '+ localStorage.getItem('token');
const config = {
  headers: {
    "Authorization" : token
  }
};

export const readNotification = (id) => {

  return (dispatch) => {
    axios.get(ROOT_URL+`/v2/user/Notification/clickNotificationSeen?id=${id}`,config)
    .then(response =>{
      dispatch({
        type: READ_NOTI,
        payload:id
      });
    })
  };
};

export const reciveNotification = () => {
  return (dispatch) => {
    dispatch({
      type: CHANGE_NUMBER_NOTI_RECIVE
    });
  };
};





export const DeleteNumberNotification = (loading) => {

  return (dispatch) => {
    loading();
    axios.get(ROOT_URL+'/v2/user/Notification/clickNotification',config)
    .then(response =>{

      dispatch({
        type: NOTI_LIST,
        payload: response.data
      });
      loading();
      dispatch({
        type: CHANGE_NUMBER_NOTI
      });

    })
    .catch(function (error) {
      loading();
      alert(error.response.data.status);
    });
  };
};


export const GetWishList = (loading) => {

  return (dispatch) => {

    loading();
    axios.get(ROOT_URL+'/v2/user/Projects/listWishList?page=0',config)
    .then(response =>{
      dispatch({
        type: WISH_LIST,
        payload: response.data
      });
      dispatch({
        type: CHANGE_NUMBER_NOTI
      });

    })
  };
};


export const fetchCat = () => {
  return (dispatch) => {
    axios.get(ROOT_URL+'API_Top/API/category')
    .then(response =>{
      dispatch({
          type: FOOTER_CAT,
          payload: response.data
      });
    })
  };
};

function getprofile() {
  return axios.get(ROOT_URL+'api/Users/user_profile', config);
}


export const profileUser = () => {

  return (dispatch) => {
    axios.all([getprofile()])
    .then(response =>{

      if (response[0].data.tokenRefest) {
        localStorage.setItem("token", response[0].data.tokenRefest);
      }
      dispatch({
        type: HEADER_AUTH,
        payload: response[0].data
      });


    })
    .catch(function (error) {
      if (error) {


        if (error.response.data.block) {
          alert('様は、総合的な理由により、アカウントを停止されました。');
          localStorage.removeItem('token')
          setTimeout(function () {
            History.push('/signin');
          }, 1000);
          dispatch({ type: UNAUTH_USER });
        }


        localStorage.removeItem('token')
        setTimeout(function () {
          History.push('/signin');
        }, 1000);
        dispatch({ type: UNAUTH_USER });

      }else {
        History.push('/connect-error');
        return false;
      }
    });
  };
};


export const searchForm = (searchText) => {
  return (dispatch) => {
    dispatch({
      type: LOADING_POST_START,
      payload: true
    });
    axios.get(ROOT_URL+'API_Top/API/search?search='+searchText.search)
    .then(response =>{
      dispatch({
        type: LOADING_POST_STOP,
        payload: false
      });
      dispatch({
        type: SEARCH_FORM,
        payload: response.data,
        searchText:searchText
      });
      History.push('/search-result');

    })
    .catch(function (error) {
      dispatch({
        type: LOADING_POST_STOP,
        payload: false
      });
      if (error) {
        if(error.response.status === 400) {
          alert(error.response.data.status);
        }
        else {
          alert(error.response.data.status);
        }
      }
    });
  };
};


export const InitialData = (data) => {
  return (dispatch) => {
    dispatch(initialize('searchForm', data));
  };
};


export const updateSearch = (data) => {
  return (dispatch) => {
    dispatch({
      type: UPDATE_SEARCH_TEXT,
      payload: data
    });
  };
};



export const pushSocket = (notificationOpts) => {
  return (dispatch) => {
    dispatch(info(notificationOpts));
  };
};




export const contactForm = (data) => {

  return (dispatch) => {
    dispatch({
      type: LOADING_POST_START,
      payload: true
    });

    axios.post(ROOT_URL+'API_Top/API/contactForm',data)
    .then(response =>{

      dispatch({
        type: LOADING_POST_STOP,
        payload: false
      });
      dispatch({
        type: CONTACT_FORM,
        payload: response.data,
      });


    })
    .catch(function (error) {
      dispatch({
        type: LOADING_POST_STOP,
        payload: false
      });
      if (error) {
        if(error.response.status === 400) {
          alert(error.response.data.status);
        }
        else {
          alert(error.response.data.status);
        }
      }
    });
  };
};

export const resetStatusContact = (data) => {

  return (dispatch) => {
    dispatch({
      type: CONTACT_FORM,
      payload: data,
    });
  };
};
