import axios from 'axios';
import {
  PROJECT_DETAIL,
  LOADING_POST_STOP,
  PROJECT_DETAIL_MYPAGE,
  LOADING_POST_START,
  UNAUTH_USER,
  ADD_COMMENT,
  ADD_REPLY,
  ADD_MORE_COMMENT,
  ADD_MORE_REPLY,
  ADD_MORE_REPORT,
  NEWS_DETAIL_REPORT,
  DELETE_COMMENT,
  EDIT_COMMENT,
  DELETE_REPLY,
  EDIT_REPLY,
  LEAVE_FAN_PROJECT_DETAIL,
  ADD_WISH_LIST,
  LEAVE_WISH_LIST
} from './types';
import History from '../history.js';
var CONFIG = require('../config/common');

if (process.env.NODE_ENV === "development") {
  CONFIG = CONFIG.CONFIG.DEV;
}else {
  CONFIG = CONFIG.CONFIG.PRODUCT;
}
const ROOT_URL = CONFIG.ROOT_URL;
const token = 'Bearer '+ localStorage.getItem('token');

//Config Axios
const config = {
  headers: {
    "Authorization" : token
  }
};

export const leaveToWishList = (project_id,loading) => {
  return (dispatch) => {


      loading();

      var url = `${ROOT_URL}v2/user/Projects/leaveWishList`;

      var data = {
        project_id:project_id
      }

      axios.post(url, data ,config)
        .then(response => {
          loading();

          dispatch({
            type: LEAVE_WISH_LIST,
            payload: project_id
          });


        }).catch((error) => {
          loading();

          if (error) {

            if (error.response.data.status === 'Expired token') {
              localStorage.removeItem('token')
              setTimeout(function () {
                History.push('/signin');
              }, 1000);
              dispatch({ type: UNAUTH_USER });
            } else if(error.response.status === 400) {
              alert(error.response.data.status);
            }
            else if(error.response.status === 401) {
              History.push('/unauthorized');
            }
            else if(error.response.status === 404) {
              History.push('/404');
            }
            else {
              alert(error.response.data.status);
            }
          }
        });
    };
};

export const addToWishList = (project_id,loading) => {
  return (dispatch) => {


      loading();

      var url = `${ROOT_URL}v2/user/Projects/addWishList`;

      var data = {
        project_id:project_id
      }

      axios.post(url, data ,config)
        .then(response => {
          loading();

          dispatch({
            type: ADD_WISH_LIST,
            payload: response.data
          });


        }).catch((error) => {
          loading();

          if (error) {

            if (error.response.data.status === 'Expired token') {
              localStorage.removeItem('token')
              setTimeout(function () {
                History.push('/signin');
              }, 1000);
              dispatch({ type: UNAUTH_USER });
            } else if(error.response.status === 400) {
              alert(error.response.data.status);
            }
            else if(error.response.status === 401) {
              History.push('/unauthorized');
            }
            else if(error.response.status === 404) {
              History.push('/404');
            }
            else {
              alert(error.response.data.status);
            }
          }
        });
    };
};



export const stopDonateFanClubFromProjectDetail = (project_id,backing_levels_id,loadingLeave) => {
  return (dispatch) => {


      loadingLeave();


      var url = `${ROOT_URL}v2/user/FanClub/stopDonateFanClub?project_id=${project_id}&backing_levels_id=${backing_levels_id}`;


      axios.get(url, config)
        .then(response => {
          loadingLeave();



          dispatch({
            type: LEAVE_FAN_PROJECT_DETAIL,
            payload: backing_levels_id
          });





        }).catch((error) => {
          loadingLeave();

          if (error) {

            if (error.response.data.status === 'Expired token') {
              localStorage.removeItem('token')
              setTimeout(function () {
                History.push('/signin');
              }, 1000);
              dispatch({ type: UNAUTH_USER });
            } else if(error.response.status === 400) {
              alert(error.response.data.status);
            }
            else if(error.response.status === 401) {
              History.push('/unauthorized');
            }
            else if(error.response.status === 404) {
              History.push('/404');
            }
            else {
              alert(error.response.data.status);
            }
          }
        });
    };
};

export const ProjectDetailNewsGet = (id) => {

  return (dispatch) => {
    dispatch({
      type: LOADING_POST_START,
      payload: true
    });
    axios.get(`${ROOT_URL}/v2/guest/ListReportDaily/ReportDetail?id=${id}`)
      .then(response => {
        dispatch({
          type: LOADING_POST_STOP,
          payload: false
        });

        dispatch({ type: NEWS_DETAIL_REPORT,payload:response.data });
      }).catch((error) => {
        dispatch({
          type: LOADING_POST_STOP,
          payload: false
        });

        if (error.response.status === 404) {
          History.push('/PageNotFound');
        }

        if (error.response.status === 400) {
          History.push('/unauthorized');
        }


      });
  };
};


export const ProjectDetailGet = (id) => {

  return (dispatch) => {
    dispatch({
      type: LOADING_POST_START,
      payload: true
    });
    axios.get(`${ROOT_URL}/API_Top/API/project_detail?id=`+id)
      .then(response => {
        dispatch({
          type: LOADING_POST_STOP,
          payload: false
        });

        dispatch({ type: PROJECT_DETAIL,payload:response.data });
      }).catch((error) => {

        dispatch({
          type: LOADING_POST_STOP,
          payload: false
        });

        if (error.response.status === 404) {
          History.push('/PageNotFound');
        }

        if (error.response.status === 400) {
          History.push('/unauthorized');
        }


      });
  };
};


export const sendProjectUnActive = (id) => {


  return (dispatch) => {
    dispatch({
      type: LOADING_POST_START,
      payload: true
    });

    axios.get(`${ROOT_URL}/api/Users/sendProjectUnActive/?id=`+id,config)
      .then(response => {

        window.location.href = "/my-page/post-project/page=1";

      }).catch((error) => {

        dispatch({
          type: LOADING_POST_STOP,
          payload: false
        });
        if (error.response.data.status === 'Expired token') {
          localStorage.removeItem('token')


          setTimeout(function () {
            History.push('/signin');
          }, 1000);
          dispatch({ type: UNAUTH_USER });
        } else if(error.response.status === 400) {
          History.push('/unauthorized');
        }
        else if(error.response.status === 404) {
          History.push('/notFoundPage');
        }
        else if(error.response.status === 401) {
          History.push('/unauthorized');
        } else {
          console.log(error.response.data);
        }
      });
  };
};

export const ProjectDetailMypageGet = (id) => {



  return (dispatch) => {
    dispatch({
      type: LOADING_POST_START,
      payload: true
    });

    axios.get(`${ROOT_URL}/api/Users/project_detail?id=`+id,config)
      .then(response => {

        dispatch({
          type: LOADING_POST_STOP,
          payload: false
        });

        dispatch({ type: PROJECT_DETAIL_MYPAGE,payload:response.data });

      }).catch((error) => {

        dispatch({
          type: LOADING_POST_STOP,
          payload: false
        });
        if (error.response.data.status === 'Expired token') {
          localStorage.removeItem('token')


          setTimeout(function () {
            History.push('/signin');
          }, 1000);
          dispatch({ type: UNAUTH_USER });
        } else if(error.response.status === 400) {
          History.push('/unauthorized');
        }
        else if(error.response.status === 404) {
          History.push('/notFoundPage');
        }
        else if(error.response.status === 401) {
          History.push('/unauthorized');
        } else {
          console.log(error.response.data.status);
        }


      });
  };
};


export const CommentBoxSocket = (data) => {
  return (dispatch) => {
    dispatch({ type: ADD_COMMENT,payload:data });
  }
}

export const CommentBox = (data,LoadingInput,socket) => {


  return (dispatch) => {
    LoadingInput();

    axios.post(`${ROOT_URL}v2/user/Comment/createComment`,data,config)
      .then(response => {

        LoadingInput();


        dispatch({ type: ADD_COMMENT,payload:response.data });

        socket.emit("user-chat", response.data);


      }).catch((error) => {
        LoadingInput();
        if (error.response.data.status === 'Expired token') {
          localStorage.removeItem('token')
          setTimeout(function () {
            History.push('/signin');
          }, 1000);
          dispatch({ type: UNAUTH_USER });
        } else if(error.response.status === 400) {
          History.push('/unauthorized');
        }
        else if(error.response.status === 404) {
          History.push('/notFoundPage');
        }
        else if(error.response.status === 401) {
          History.push('/unauthorized');
        } else {
          console.log(error.response.data.status);
        }
      });
  };
};

export const CommentBoxEdit = (data,LoadingInput) => {



  return (dispatch) => {
    LoadingInput();

    axios.put(`${ROOT_URL}v2/user/Comment/editComment`, data , config)
      .then(response => {

        LoadingInput();


        dispatch({ type: EDIT_COMMENT,payload:response.data});

        // socket.emit("user-chat", response.data);


      }).catch((error) => {

        LoadingInput();
        if (error.response.data.status === 'Expired token') {
          localStorage.removeItem('token')
          setTimeout(function () {
            History.push('/signin');
          }, 1000);
          dispatch({ type: UNAUTH_USER });
        } else if(error.response.status === 400) {
          History.push('/unauthorized');
        }
        else if(error.response.status === 404) {
          History.push('/notFoundPage');
        }
        else if(error.response.status === 401) {
          History.push('/unauthorized');
        } else {
          console.log(error.response.data.status);
        }
      });
  };
};


export const ReplyBoxEdit = (data,LoadingInput) => {


  return (dispatch) => {
    LoadingInput();

    axios.put(`${ROOT_URL}v2/user/Comment/editReplyComment`, data , config)
      .then(response => {

        LoadingInput();


        dispatch({ type: EDIT_REPLY,payload:response.data});

        // socket.emit("user-chat", response.data);


      }).catch((error) => {

        LoadingInput();
        if (error.response.data.status === 'Expired token') {
          localStorage.removeItem('token')
          setTimeout(function () {
            History.push('/signin');
          }, 1000);
          dispatch({ type: UNAUTH_USER });
        } else if(error.response.status === 400) {
          History.push('/unauthorized');
        }
        else if(error.response.status === 404) {
          History.push('/notFoundPage');
        }
        else if(error.response.status === 401) {
          History.push('/unauthorized');
        } else {
          console.log(error.response.data.status);
        }
      });
  };
};



export const firebaseCURL = (data) => {



  return (dispatch) => {
    axios.post(`${ROOT_URL}v2/Firebase/Curl`,data,config).then(response => {});
  };
};


export const ReplyBoxSocket = (data) => {

  return (dispatch) => {
    dispatch({ type: ADD_REPLY,payload:data });
  };
};

export const ReplyBox = (data,LoadingInput,socket,closeAdd) => {



  return (dispatch) => {
    LoadingInput();

    axios.post(`${ROOT_URL}v2/user/Comment/replyComment`,data,config)
      .then(response => {

        LoadingInput();
        closeAdd();

        dispatch({ type: ADD_REPLY,payload:response.data });
        socket.emit("user-chat-reply", response.data);

      }).catch((error) => {

        LoadingInput();
        closeAdd();


        if (error.response.data.status === 'Expired token') {
          localStorage.removeItem('token')


          setTimeout(function () {
            History.push('/signin');
          }, 1000);
          dispatch({ type: UNAUTH_USER });
        } else if(error.response.status === 400) {
          History.push('/unauthorized');
        }
        else if(error.response.status === 404) {
          History.push('/notFoundPage');
        }
        else if(error.response.status === 401) {
          History.push('/unauthorized');
        } else {
          console.log(error.response.data.status);
        }


      });
  };
};


export const LoadMoreComment = (page,projectId,LoadingInput) => {




  return (dispatch) => {
    LoadingInput();

    axios.get(`${ROOT_URL}v2/guest/LoadComment/loadComment?project_id=${projectId}&page=${page}`,config)
      .then(response => {

        LoadingInput();

        dispatch({ type: ADD_MORE_COMMENT,payload:response.data });

        console.log(response.data);

      }).catch((error) => {

        LoadingInput();

        if (error.response.data.status === 'Expired token') {
          localStorage.removeItem('token')


          setTimeout(function () {
            History.push('/signin');
          }, 1000);
          dispatch({ type: UNAUTH_USER });
        } else if(error.response.status === 400) {
          History.push('/unauthorized');
        }
        else if(error.response.status === 404) {
          History.push('/notFoundPage');
        }
        else if(error.response.status === 401) {
          History.push('/unauthorized');
        } else {
          console.log(error.response.data.status);
        }


      });
  };
};


export const loadMoreReply = (project_id,comment_id,_loadingReply) => {



  return (dispatch) => {
    _loadingReply();

    axios.get(`${ROOT_URL}v2/guest/LoadComment/loadMoreReply?project_id=${project_id}&comment_id=${comment_id}`,config)
      .then(response => {


        dispatch({ type: ADD_MORE_REPLY,payload:response.data });
        //Hide Button Loading
        _loadingReply();

      }).catch((error) => {

        _loadingReply();

        if (error.response.data.status === 'Expired token') {
          localStorage.removeItem('token')


          setTimeout(function () {
            History.push('/signin');
          }, 1000);
          dispatch({ type: UNAUTH_USER });
        } else if(error.response.status === 400) {
          History.push('/unauthorized');
        }
        else if(error.response.status === 404) {
          History.push('/notFoundPage');
        }
        else if(error.response.status === 401) {
          History.push('/unauthorized');
        } else {
          console.log(error.response.data.status);
        }


      });
  };
};






export const loadMoreNewsReport = (project_id,page,toogleLoading) => {



  return (dispatch) => {
    toogleLoading();

    axios.get(`${ROOT_URL}v2/guest/ListReportDaily/loadMoreListReport?project_id=${project_id}&page=${page}`,config)
      .then(response => {
        dispatch({ type: ADD_MORE_REPORT,payload:response.data });

        toogleLoading();
      }).catch((error) => {
        toogleLoading();

      });
  };
};



export const deleteComment = (id,loading) => {


  return (dispatch) => {
    loading();

    axios.delete(`${ROOT_URL}v2/user/Comment/removeComment/${id}` , config)
      .then(response => {
        loading();

        dispatch({ type: DELETE_COMMENT,payload:id });

      }).catch((error) => {
        loading();
        if (error.response.data.status === 'Expired token') {
          localStorage.removeItem('token')
          setTimeout(function () {
            History.push('/signin');
          }, 1000);
          dispatch({ type: UNAUTH_USER });
        } else if(error.response.status === 400) {
          History.push('/unauthorized');
        }
        else if(error.response.status === 404) {
          History.push('/notFoundPage');
        }
        else if(error.response.status === 401) {
          History.push('/unauthorized');
        } else {
          console.log(error);
        }
      });
  };
};


export const deleteReply = (id,commnetId,loading) => {



  return (dispatch) => {
    loading();

    axios.delete(`${ROOT_URL}v2/user/Comment/removeReplyComment/${id}` , config)
      .then(response => {
        loading();

        dispatch({ type: DELETE_REPLY,replyId:id,commnetId:commnetId });

      }).catch((error) => {
        loading();
        if (error.response.data.status === 'Expired token') {
          localStorage.removeItem('token')
          setTimeout(function () {
            History.push('/signin');
          }, 1000);
          dispatch({ type: UNAUTH_USER });
        } else if(error.response.status === 400) {
          History.push('/unauthorized');
        }
        else if(error.response.status === 404) {
          History.push('/notFoundPage');
        }
        else if(error.response.status === 401) {
          History.push('/unauthorized');
        } else {
          console.log(error);
        }
      });
  };
};
