export const AUTH_USER = 'auth_user';
export const UNAUTH_USER = 'unauth_user';
export const AUTH_ERROR = 'auth_error';
export const SEND_EMAIL_FORGOT = 'send_email_forgot';
export const FETCH_FEATURE = 'fetch_feature';
export const FETCH_FEATURE_POST = 'fetch_feature_post';
export const SEND_RESET = 'send_reset';
export const SEND_RESET_FAILE = 'send_reset_faile';
export const SEND_SUB_EMAIL = 'send_sub_email';
export const SEND_SUB_EMAIL_FAILE = 'send_sub_email_faile';
export const TOKEN_OK = 'token_ok';
export const TOKEN_FAILE = 'token_faile';
export const FETCH_PROJECT = 'FETCH_PROJECT';
export const FOOTER_CAT = 'fetch_footer_cat'
export const HEADER_AUTH = 'HEADER_AUTH';
export const SEARCH_FORM = 'search_form'
export const LOADING_POST_START = 'loading_post_start'
export const LOADING_POST_STOP = 'loading_post_stop'
export const SEND_EMAIL_FORGOT_FAILE = 'send_email_faile'
export const LOAD = 'load_acc'
export const LOAD_USER = 'LOAD_USER'
export const UPLOAD_IMAGE = 'UPLOAD_IMAGE'
export const UPLOAD_IMAGE_PROGESS = 'UPLOAD_IMAGE_PROGESS'
export const DISABLE = 'DISABLE'
export const EDIT_AUTH = 'EDIT_AUTH'
export const FETCH_PROJECTS_NEW = 'FETCH_PROJECTS_NEW'
export const FETCH_PROJECTS_ENOUGH_LIST = 'FETCH_PROJECTS_ENOUGH_LIST'
export const FETCH_PROJECTS_GOAL_LIST = 'FETCH_PROJECTS_GOAL_LIST'
export const EXPIRED_PAGINATION = 'EXPIRED_PAGINATION'
export const NEWS_LIST = 'NEWS_LIST'
export const CAT_SLUG_LIST = 'CAT_SLUG_LIST'
export const UPDATE_SEARCH_TEXT = 'UPDATE_SEARCH_TEXT'
export const PROJECT_DETAIL = 'PROJECT_DETAIL'
export const GET_PROJECT_POST = 'GET_PROJECT_POST'
export const GET_PROJECT_DONATED = 'GET_PROJECT_DONATED'
export const DIS_STATUS_API = 'DIS_STATUS_API'
export const ADD_NEW_PROJECT_OK = 'ADD_NEW_PROJECT_OK'
export const CONFIRM_OK = 'CONFIRM_OK'
export const GET_PROJECT_DEFAULT_EDIT = 'GET_PROJECT_DEFAULT_EDIT'
export const EDIT_PROJECT_OK = 'EDIT_PROJECT_OK'
export const CONFIRM_EDIT_PROJECT_OK = 'CONFIRM_EDIT_PROJECT_OK'
export const EDIT_PROJECT_NO_CONFIRM_OK = 'EDIT_PROJECT_NO_CONFIRM_OK'
export const UPLOAD_IMAGE_PROGESS_RESET = 'UPLOAD_IMAGE_PROGESS_RESET'
export const LOAD_BACKER = 'LOAD_BACKER'
export const CREDIT_MONTH_YEAR = 'CREDIT_MONTH_YEAR'
export const DONATE_OK = 'DONATE_OK'
export const SETTING_VISA_OK = 'SETTING_VISA_OK'
export const SETTING_ATM_OK = 'SETTING_ATM_OK'
export const SETTING_VISA_OK_FIRST = 'SETTING_VISA_OK_FIRST'
export const DISABLE_DONATE = 'DISABLE_DONATE'
export const GET_STATICTIS_OK = 'GET_STATICTIS_OK'
export const PROJECT_DETAIL_MYPAGE = 'PROJECT_DETAIL_MYPAGE'
export const PASSWORD_CHANGE_OK = 'PASSWORD_CHANGE_OK'
export const CONTACT_FORM = 'CONTACT_FORM'
export const NEW_DETAIL = 'NEW_DETAIL'
export const EDIT_AUTH_DONATE = 'EDIT_AUTH_DONATE'
export const ADD_COMMENT = 'ADD_COMMENT'
export const ADD_REPLY = 'ADD_REPLY'
export const ADD_MORE_COMMENT = 'ADD_MORE_COMMENT'
export const ADD_MORE_REPLY = 'ADD_MORE_REPLY'
export const ADD_MORE_REPORT = 'ADD_MORE_REPORT'
export const NEWS_DETAIL_REPORT = 'NEWS_DETAIL_REPORT'
export const EDIT_PROJECT_NEWS_DATA = 'EDIT_PROJECT_NEWS_DATA'
export const FETCH_PROJECTS_NEWS = 'FETCH_PROJECTS_NEWS'
export const SUB_EMAIL = 'SUB_EMAIL'
export const PAGEVIEW_REPORT = 'PAGEVIEW_REPORT'
export const DONATE_REPORT = 'DONATE_REPORT'
export const DONATE_REPORT_TOTAL = 'DONATE_REPORT_TOTAL'
export const NOTI_LIST = 'NOTI_LIST'
export const LIST_NOTICATION_PAGE = 'LIST_NOTICATION_PAGE'
export const CHANGE_NUMBER_NOTI = 'CHANGE_NUMBER_NOTI'
export const READ_NOTI = 'READ_NOTI'
export const CHANGE_NUMBER_NOTI_RECIVE = 'CHANGE_NUMBER_NOTI_RECIVE'
export const LIST_EDIT_PUBLIC = 'LIST_EDIT_PUBLIC'
export const DELETE_COMMENT = 'DELETE_COMMENT'
export const EDIT_COMMENT = 'EDIT_COMMENT'
export const DELETE_REPLY = 'DELETE_REPLY'
export const EDIT_REPLY = 'EDIT_REPLY'
export const FANCLUB_PAGINATION = 'FANCLUB_PAGINATION'
export const GET_PROJECT_DONATED_FAN = 'GET_PROJECT_DONATED_FAN'
export const LEAVE_FAN = 'LEAVE_FAN'
export const LEAVE_FAN_PROJECT_DETAIL = 'LEAVE_FAN_PROJECT_DETAIL'
export const REMOVE_VISA_OK = 'REMOVE_VISA_OK'
export const SETTING_VISA_NON_LOGIN = 'SETTING_VISA_NON_LOGIN'
export const CONFIRM_RETURN = 'CONFIRM_RETURN'
export const WISH_LIST = 'WISH_LIST'
export const ADD_WISH_LIST = 'ADD_WISH_LIST'
export const LEAVE_WISH_LIST = 'LEAVE_WISH_LIST'
export const LIST_WISHLIST_PAGE = 'LIST_WISHLIST_PAGE'
export const LEAVE_WISH_LIST_PAGE = 'LEAVE_WISH_LIST_PAGE'
export const LIST_NOTICATION_HEADER = 'LIST_NOTICATION_HEADER'
export const LIST_NOTICATION_STATIC = 'LIST_NOTICATION_STATIC'
export const DETAIL_NOTICATION_HEADER = 'DETAIL_NOTICATION_HEADER'
export const LIST_PROJECT_MAIL = 'LIST_PROJECT_MAIL'
export const OWNER_PROJECT_PROFILE = 'OWNER_PROJECT_PROFILE'
export const SEND_EMAIL_BOX = 'SEND_EMAIL_BOX'
export const RECIVE_EMAIL = 'RECIVE_EMAIL'
export const SENDED_EMAIL = 'SENDED_EMAIL'
export const STATUS_MAILBOX = 'STATUS_MAILBOX'
export const CHANGE_STATUS_EMAIL = 'CHANGE_STATUS_EMAIL'
export const PROJECT_S_PAGINATION = 'PROJECT_S_PAGINATION'
