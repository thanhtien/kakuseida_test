/* @flow */

import React, { Component } from 'react';
import { Link } from 'react-router-dom';


export default class Breadcrumbs extends Component {
  render() {
    const {linkBreacrm} = this.props;
    return (
      <div className="bread-crums">
        <ul>
          <li>
            <Link to={'/'}><i className="icss-anim icss-home"></i> Home /</Link>
          </li>
          {
            linkBreacrm ? linkBreacrm.map( (item,i) => {
              return (
                item.last === false ?
                <li key={i}>
                  <Link to={item.link}>{item.label} /</Link>
                </li> :
                <li key={i}>
                  <span>{item.label}</span>
                </li>
              )
            }) : null

          }
        </ul>
      </div>
    );
  }
}
