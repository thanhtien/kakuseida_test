export const CONFIG = {
  NotificationItem: { // Override the notification item
    DefaultStyle: { // Applied to every notification, regardless of the notification level
      height:'auto',
      overflow:'hidden',
      borderTop:"2px solid #0071BA"
    },
    title:{
      color:"#0071BA"
    },
    info: { // Applied only to the success notification item
      color: '#333',
      borderColor:"#1b9b5"
    }
  }
}
