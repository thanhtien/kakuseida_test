/* @flow */

import React, { Component } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faSearch } from '@fortawesome/free-solid-svg-icons';
import Breadcrumbs from '../Breadcrumbs';

export default class SearchNotFound extends Component {
  constructor(props) {
    super(props);
    this.state = {
      heightCalc:0
    };
  }
  componentDidMount() {
    var footer = document.getElementById('footer').offsetHeight;
    var header = document.getElementById('header').offsetHeight;
    var notFound = 0;

    if (document.getElementById('notFound')) {
      notFound = document.getElementById('notFound').offsetHeight;
    }
    var h = window.innerHeight;
    if (h > (footer+header+notFound) ) {
      this.setState({
        heightCalc:h - (footer+header+notFound)
      })
    }

    window.addEventListener("resize", this.updateDimensions.bind(this));


  }
  updateDimensions(){
    var footer = document.getElementById('footer').offsetHeight;
    var header = document.getElementById('header').offsetHeight;
    var notFound = 0;
    if (document.getElementById('notFound')) {
      notFound = document.getElementById('notFound').offsetHeight;
    }

    var h = window.innerHeight;
    if (h > (footer+header+notFound) ) {
      this.setState({
        heightCalc:h - (footer+header+notFound)
      })
    }
  }
  render() {

    const {heightCalc} = this.state;
    return (
        <div>
          <div id="notFound" className="banner">
            <div className="wraper">
                <div className="search">
                    <FontAwesomeIcon icon={faSearch} />
                    <p className="search-p">検索結果はありません</p>
                </div>
                <h1 className="banner-title">{this.props.title}</h1>

            </div>
          </div>
          <div style={{height:heightCalc+"px"}}  className="height wraper">
            <div className="cover-Breadcrumbs"><Breadcrumbs linkBreacrm={this.props.linkBreacrm}></Breadcrumbs></div>
          </div>
        </div>
    );
  }
}
