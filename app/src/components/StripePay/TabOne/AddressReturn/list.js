/* @flow */

import React, { Component } from 'react';
import Rodal from 'rodal';
import { connect } from 'react-redux'
import * as actions from '../../../../actions/editSystem';
import Loading from '../../../common/loading';
import Form from './formEdit';
 class List extends Component {

   constructor(props) {
     super(props);
     this.state = {
       showForm:false,
       IdChose:null,
       setChoseDefault:false
     };
   }

  hideModal() {
    this.props.close();
  }

  closeForm() {
    this.setState({
      showForm:false,
      setChoseDefault:false
    })
    this.props.InitialDataAddressReturnEdit(null)
  }

  editAddress(event,item) {
    event.stopPropagation();

    if (item.chosen_default === '1') {
      this.setState({
        showForm:true,
        setChoseDefault:true
      })
    }else {
      this.setState({
        showForm:true,
        setChoseDefault:false
      })
    }


    this.props.InitialDataAddressReturnEdit(item);

  }

  /**
   * [chosenDefault Set Default Address]
   * @param  {[type]} id [Address ID]
   */
  chosenDefault(id) {
    if (id) {
      this.props.SetDefaultAddressStepOne(id);
      document.body.style.overflow = "auto";
    }

  }
  setId(event,id) {
    event.stopPropagation();
    this.setState({
      IdChose:id
    })
  }

  /**
   * [_renderAction logic action Address return]
   * @param  {[type]} chosen_default [Default Contact Return]
   * @return {[type]}                [Action]
   */
  _renderAction(chosen_default , item) {
    const {IdChose} = this.state;
    if (IdChose) {
      if (IdChose.toString() === item.id.toString()) {
        return(
          <div className="div-action" >
            <span onClick={ (e) => this.setId(e,item.id) } className="action-table fake-chekcbox active">
              デフォルトにする
            </span>

            <span onClick={ (event) => this.editAddress(event,item) }  className="action-table">
              <i className="icss-pencil-b"></i>
              編集
            </span>
          </div>
        )
      }
      else {
        return (
          <div className="div-action">
            <span onClick={ (e) => this.setId(e,item.id) } className="action-table fake-chekcbox">
              デフォルトにする
            </span>
            <span style={{color:'#0A5C93'}} onClick={ (event) => this.editAddress(event,item) }  className="action-table">
              <i className="icss-pencil-b"></i>
              編集
            </span>
          </div>
        )
      }

    }
    else {
      if (chosen_default === '1') {
        return(
          <div className="div-action" >
            <span onClick={ (e) => this.setId(e,item.id) } className="action-table fake-chekcbox active">
              デフォルトにする
            </span>

            <span onClick={ (event) => this.editAddress(event,item) }  className="action-table">
              <i className="icss-pencil-b"></i>
              編集
            </span>
          </div>
        )
      }
      else {
        return(
          <div className="div-action">
            <span onClick={ (e) => this.setId(e,item.id) } className="action-table fake-chekcbox">
              デフォルトにする
            </span>
            <span style={{color:'#0A5C93'}} onClick={ (event) => this.editAddress(event,item) }  className="action-table">
              <i className="icss-pencil-b"></i>
              編集
            </span>
          </div>
        )
      }
    }
  }


  render() {
    const { status , data , loading} = this.props;
    const {showForm,setChoseDefault} = this.state;
    return (
      <Rodal
        measure="%" width={80} height={80}
        visible={status}
        onClose={this.hideModal.bind(this)}
      >
        {
          loading ? <Loading></Loading> : null
        }


        {
          showForm ?
          <div style={{overflow:'auto' , height:'100%'}} className="cover-no-blank inbox">
            <div style={{width:'100%'}} className="add-donate-address">
              <Form
                setChoseDefault={setChoseDefault}
                closeForm={() => this.closeForm()}
                close={this.props.close}>
              </Form>
            </div>
          </div>  :
          <div style={{overflow:'auto',height:'100%' , 'paddingBottom':'10px'}}>
            <table className="static padding">
              <caption>お届け先情報</caption>
              <thead>
                <tr>
                  <th scope="col">名前 </th>
                  <th scope="col">メールアドレス  </th>
                  <th scope="col">電話番号</th>
                  <th scope="col">郵便番号</th>
                  <th scope="col">住所</th>
                  <th className="set-width" scope="col">デフォルト設置／編集</th>
                </tr>
              </thead>
              <tbody>
                {
                  data.map( (item , i ) => {
                    return(
                      <tr className={item.chosen_default === '1' ? 'active' : null}  key={i} onClick={ (event) => this.editAddress(event,item) }>
                        <td data-label="名前">
                          {
                            item.name_return
                          }
                        </td>
                        <td data-label="メールアドレス ">
                          {
                            item.email
                          }
                        </td>
                        <td data-label="電話番号">
                          {item.phone}
                        </td>
                        <td data-label="郵便番号">
                          {item.postcode}

                        </td>
                        <td data-label="住所">
                          {item.address_return}
                        </td>
                        <td data-label="デフォルト設置／編集">
                          {
                            this._renderAction(item.chosen_default,item)
                          }
                        </td>
                      </tr>
                    )
                  })
                }
              </tbody>
            </table>
            <div className="clear-fix"></div>
            <div style={{marginTop: '25px'}} className="submit-box custom-donate">
              <span style={{cursor:"pointer"}} onClick={() => this.chosenDefault(this.state.IdChose)} className="button-submit">保存</span>
            </div>
          </div>
        }
      </Rodal>
    );
  }
}


const mapStateToProps = (state) => {
    return {
      loading:state.common.loading
    }
}
export default connect(mapStateToProps,actions, null, { withRef: true })(List);
