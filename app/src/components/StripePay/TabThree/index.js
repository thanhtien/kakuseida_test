/* @flow */

import React, { Component } from 'react';
import Form from './Form';
export default class TabThree extends Component {
  /**
   * [_renderHTML description]
   * @param  {[type]} profileUser [Data Default]
   * @return {[type]}             [HTML React]
   */
  _renderHTML() {
    const {invest_amount,project_id,backing_level_id,profile,dataBacker} = this.props;
    return (
      <div className="page-tab">
        <Form
          dataBacker={dataBacker}
          data={profile}
          backing_level_id={backing_level_id}
          project_id={project_id}
          invest_amount={invest_amount}
        />
            
        </div>
    )
  }
  render() {
    return (
      <div className="wraper">
        {
          this._renderHTML()
        }
      </div>
    );
  }
}
