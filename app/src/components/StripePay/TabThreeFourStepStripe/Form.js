/* @flow */

import React, { Component } from 'react';
import { connect } from 'react-redux';
import * as actions from '../../../actions/DonateAction';
import ProjectReturn from './ProjectReturn';

import {  Link } from 'react-router-dom';


 class Form extends Component {

  /**
   * [confirmToDonate Confirm Donate]
   * @return {[type]} [Donate True Or False]
   */
  confirmToDonate() {
    const {invest_amount,project_id,backing_level_id,dataBacker,initialValues} = this.props;

    this.props.Donate(dataBacker.project_type,initialValues.cvc,invest_amount,project_id,backing_level_id);
  
  }

  _renderAddress(addressList) {

    if (addressList) {
      for (var i = 0; i < addressList.length; i++) {
        if (addressList[i].chosen_default === "1") {
          return (
            <div className="form-area form-area-3">
                <p className="page-tab-label">お届け先情報</p>
                <div className="form-box">
                    <div className="form-field form-field-1">
                        <p className="form-field-name">名前</p>
                        <div className="form-field-content">
                          <p>
                            {addressList[i].name_return}
                          </p>
                        </div>
                    </div>
                    <div className="form-field form-field-2">
                        <p className="form-field-name">郵便番号</p>
                        <div className="form-field-content">
                          <p>
                            {addressList[i].postcode}
                          </p>
                        </div>
                    </div>

                    <div className="form-field form-field-4">
                        <p className="form-field-name">住所</p>
                        <div className="form-field-content">
                          <p>
                            {addressList[i].address_return}
                          </p>
                        </div>
                    </div>
                    <div className="form-field form-field-5">
                        <p className="form-field-name">電話番号</p>
                        <div className="form-field-content">
                          <p>
                            {addressList[i].phone}
                          </p>
                        </div>
                    </div>

                    <div className="form-field form-field-5">
                        <p className="form-field-name">メールアドレス</p>
                        <div className="form-field-content">
                          <p>
                            {addressList[i].email}
                          </p>
                        </div>
                    </div>




                </div>
            </div>
          )
        }
      }
    }

  }

  render() {
    const {initialValues , dataBacker} = this.props;

    return (
      <div>
        {
          initialValues ?
          <form>
            <div className="form-area">
                <p className="page-tab-label">クレジットカード</p>
                <div className="form-box">
                    <div className="form-field">
                        <p className="form-field-name">カード番号</p>
                        <div className="form-field-content">
                          <p>
                            {initialValues.number_card}
                          </p>
                        </div>
                    </div>
                    <div className="form-field">
                        <p className="form-field-name">有効期限</p>
                        <div className="form-field-content">
                          <p>
                            <span>{initialValues.exp_month}</span>/ <span>{initialValues.exp_year}</span>
                          </p>
                        </div>
                    </div>

                    <div className="form-field">
                        <p className="form-field-name">セキュリティーコード</p>
                        <div className="form-field-content">
                          <p>
                            <span>{initialValues.cvc}</span>
                          </p>
                        </div>
                    </div>

                    <p>
                      クレジットカードの情報を変更したい場合は、<Link to="/my-page/setting-user/credit-accounts">こちら</Link>をクリックしてください。
                    </p>


                </div>
            </div>

            {
              this._renderAddress(initialValues.listAddress)
            }
            {
              <ProjectReturn data={dataBacker}></ProjectReturn>
            }



            {/*<input className="btn-submit" type="submit" value="確認画面"/>*/}
            <span style={{cursor:"pointer"}} onClick={()=>this.confirmToDonate()} className="btn-submit">
              確認画面
            </span>
          </form> : null
        }
      </div>
    );
  }
}


const mapStateToProps = (state) => {
    return {
      initialValues: state.Donate.profileUpdate,
    }
}

// You have to connect() to any reducers that you wish to connect to yourself
export default connect(mapStateToProps, actions )(Form);
