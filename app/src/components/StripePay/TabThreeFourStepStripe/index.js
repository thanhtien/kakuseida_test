/* @flow */

import React, { Component } from 'react';
import Form from './Form';
export default class TabThree extends Component {
  /**
   * [_renderHTML description]
   * @param  {[type]} profileUser [Data Default]
   * @return {[type]}             [HTML React]
   */
  _renderHTML() {
    const {invest_amount,project_id,backing_level_id,dataBacker} = this.props;
    return (
      <div className="page-tab">
            <p className="page-tab-head">お支払い情報を入力してください （クレジットカード）</p>
            <div className="page-tab-content">
                <div className="page-tab-form">

                  <Form
                    dataBacker={dataBacker}
                    backing_level_id={backing_level_id}
                    project_id={project_id}
                    invest_amount={invest_amount}
                  />

                </div>
            </div>
        </div>
    )
  }
  render() {
    return (
      <div className="wraper">
        {
          this._renderHTML()
        }
      </div>
    );
  }
}
