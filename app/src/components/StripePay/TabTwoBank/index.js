/* @flow */

import React, { Component } from 'react';
import Form from './Form';
export default class TabTwo extends Component {

  ChangeStep() {
    this.props.ChangeStep(3);
  }
  /**
   * [_renderHTML description]
   * @param  {[type]} profileUser [Data Default]
   * @return {[type]}             [HTML React]
   */
  _renderHTML(profileUser) {
    return (
      <div className="page-tab">
            <p className="page-tab-head">お支払い情報を入力してください （銀行口座情報）</p>
            <div className="page-tab-content">
                <div className="page-tab-form">

                  <Form profileUser={profileUser} ChangeStep={ () => this.ChangeStep() }/>

                </div>
            </div>
        </div>
    )
  }
  render() {
    const {profileUser} = this.props;
    return (
      <div className="wraper">
        {
          profileUser ? this._renderHTML(profileUser) : null
        }
      </div>
    );
  }
}
