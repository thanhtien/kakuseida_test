
const validate = (values) => {
  const errors = {};

  if (!values.bank_name) {
    errors.bank_name = '銀行名を入力してください';
  }
  if (!values.bank_branch) {
    errors.bank_branch = '支店名を入力してください';
  }
  if (!values.bank_type) {
    errors.bank_type = '口座種類を入力してください';
  }
  if (!values.bank_number) {
    errors.bank_number = '口座番号を入力してください';
  }
  if (!values.bank_owner) {
    errors.bank_owner = '口座カナ名義を入力してください';
  }
  return errors;
};
export default validate;
