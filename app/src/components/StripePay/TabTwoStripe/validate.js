import {
    CREDIT_MONTH_YEAR
} from '../../../actions/types';
import valid from 'card-validator';
import creditCardType from 'credit-card-type';


const validate = (values , dispatch) => {
  const errors = {};
  var patt = new RegExp("^[A-Za-z][A-Za-z0-9]$");




  if (!values.username) {
    errors.username = 'ユーザー名を入力してください';
  }else if(values.username){
    if (values.username.length<=2 || values.username.length>= 32) {
      errors.username = 'ユーザー名（英数字3-32文字）';
    }
  };

  if (!values.cvc) {
    errors.cvc = "セキュリティーコードが正しくありません";
  }



  if(patt.test(values.username) === true){
    errors.username = '注：ログイン名の中にスペース、特殊文字が入らないでください。ログイン名の行頭に数字を入力しないください。';
  }



  if (!values.name_return) {
    errors.name_return = "名前入力して下い" ;
  }
  if (!values.postcode) {
    errors.postcode = "郵便番号入力して下い" ;
  }
  if (!values.address_return) {
    errors.address_return = "住所入力して下い" ;
  }
  if (!values.phone) {
    errors.phone = "電話番号入力して下い" ;
  }




  if (!values.number_card) {
    errors.number_card = "クレジットカード情報は正しくありません。" ;
  }
  var visaCards = creditCardType(values.number_card);
  var numberValidation = valid.number(values.number_card);

  if (visaCards.length === 0) {
    errors.number_card = "クレジットカード情報は正しくありません。" ;
  }


  if (!numberValidation.isPotentiallyValid) {
    errors.number_card = "クレジットカード情報は正しくありません。" ;
  }






  if (!values.exp_month) {

    dispatch.dispatch({
      type:CREDIT_MONTH_YEAR,
      payload:"有効期限の月を入力してください"
    })
  }else {
    dispatch.dispatch({
      type:CREDIT_MONTH_YEAR,
      payload:null
    })
  }

  if (!values.exp_year) {
    dispatch.dispatch({
      type:CREDIT_MONTH_YEAR,
      payload:"有効期限の年を入力してください"
    })
  }else {
    dispatch.dispatch({
      type:CREDIT_MONTH_YEAR,
      payload:null
    })
  }

  if (!values.exp_year || !values.exp_month) {
    if (!values.exp_month) {
      dispatch.dispatch({
        type:CREDIT_MONTH_YEAR,
        payload:"有効期限の月を入力してください"
      })
    }
    if (!values.exp_year ) {
      dispatch.dispatch({
        type:CREDIT_MONTH_YEAR,
        payload:"有効期限の年を入力してください"
      })
    }
  }

  if (!values.exp_month  && !values.exp_year) {
    dispatch.dispatch({
      type:CREDIT_MONTH_YEAR,
      payload:null
    })

  }else {
    var minMonth = new Date().getMonth() + 1;
    var minYear = new Date().getFullYear();
    var month = parseInt(values.exp_month, 10);
    var year = parseInt(values.exp_year, 10);
    if (year.toString().length === 2) {
      year = year + 2000;
    }
    if ( year > minYear || (year === minYear && month >= minMonth)) {
      dispatch.dispatch({
        type:CREDIT_MONTH_YEAR,
        payload:null
      })
    }else {
      dispatch.dispatch({
        type:CREDIT_MONTH_YEAR,
        payload:"有効期限が正しくありません"
      })
    }
  }

  if (values.exp_year  && isNaN(values.exp_year)) {
    dispatch.dispatch({
      type:CREDIT_MONTH_YEAR,
      payload:"有効期限が正しくありません"
    })
  }

  if (values.exp_month  && isNaN(values.exp_month)) {
    dispatch.dispatch({
      type:CREDIT_MONTH_YEAR,
      payload:"有効期限が正しくありません"
    })
  }


  return errors;
};
export default validate;
