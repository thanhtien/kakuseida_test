/* @flow */

import React, { Component } from 'react';

export default class Nav extends Component {

  ChangeStep(step){
    const {disable} = this.props;
    if (!disable) {
      this.props.ChangeStep(step);
    }
  }
  //For Nav Stripe
  _renderNavStripe(confirmCheckPay) {
    const {step,disable} = this.props;
    return(
      confirmCheckPay === ""  ?
      <ul id="topNavStripe" className="animated fadeIn">
          <li onClick={ () => this.ChangeStep(1) } className={ step === 1 && disable === false ? "active" : null }>１．リターン　選択</li>
          <li onClick={ () => this.ChangeStep(2) } className={ step === 2 && disable === false ? "active" : null }>２．お支払情報の入力</li>
          <li className={ step === 3 && disable === false ? "active" : null }>３．申込み内容の確認</li>
          <li  className={ disable === true ? "active" : null }>４．完了</li>
      </ul>:


      <ul id="topNavStripe" className="animated fadeIn">
          <li onClick={ () => this.ChangeStep(1) } className={ step === 1 && disable === false ? "active" : null }>１．リターン　選択</li>
          <li onClick={ () => this.ChangeStep(3) } className={ step === 3 && disable === false ? "active" : null }>２．お支払情報の確認</li>
          <li  className={ disable === true ? "active" : null }>３．完了</li>
      </ul>
    )
  }
  //For Nav Bank
  _renderNavBank(confirmBank) {
    const {step,disable} = this.props;
    
    return(
      confirmBank === null  ?
      <ul id="topNavStripe" className="animated fadeIn">
          <li onClick={ () => this.ChangeStep(1) } className={ step === 1 && disable === false ? "active" : null }>１．リターン　選択</li>
          <li onClick={ () => this.ChangeStep(2) } className={ step === 2 && disable === false ? "active" : null }>２．お支払情報の入力</li>
          <li className={ step === 3 && disable === false ? "active" : null }>３．申込み内容の確認</li>
          <li  className={ disable === true ? "active" : null }>４．完了</li>
      </ul>:
      <ul id="topNavStripe" className="animated fadeIn">
          <li onClick={ () => this.ChangeStep(1) } className={ step === 1 && disable === false ? "active" : null }>１．リターン　選択</li>
          <li onClick={ () => this.ChangeStep(3) } className={ step === 3 && disable === false ? "active" : null }>２．お支払情報の確認</li>
          <li  className={ disable === true ? "active" : null }>３．完了</li>
      </ul>
    )
  }
  /**
   * [_renderNav Logic For Render Nav]
   * @param  {[type]} typePay [type for Detec]
   * @return {[type]}         [Navigation]
   */
  _renderNav(typePay) {
    const {profileUser} = this.props;

    if (typePay === 'stripe') {
      if (profileUser) {
        var confirmCheckPay = profileUser.number_card;
      }
      return this._renderNavStripe(confirmCheckPay)

    }else if( typePay === 'bank') {
      if (profileUser) {
        var confirmBank= profileUser.bank_number

      }
      return this._renderNavBank(confirmBank)
    }
  }

  render() {
    const {typePay} = this.props;



    return (
      <div className="nav-tab">
        <div className="wraper">
          {
            this._renderNav(typePay)
          }
        </div>
    </div>
    );
  }
}
