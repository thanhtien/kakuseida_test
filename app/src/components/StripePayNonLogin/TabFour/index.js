/* @flow */
import React, { Component } from 'react';
export default class tabFour extends Component {
  render() {
    const {typePay} = this.props;
    return (
      <div className="wraper">
        <div className="page-tab">
          {
            typePay === 'bank' ?
            <div className="page-tab-head">
               リターン選択ができました。誠にありがとうございました。<br/>
               振り込みが完了しましたら、<a href="mailto:info@kakuseida.com">info@kakuseida.com</a>にご連絡してください。
              システムの管理者が確認次第、ご支援が有効となります。
            </div> : <p className="page-tab-head">お支払いができました。誠にありがとうございました。</p>
          }
            <div className="page-tab-content">
                <p className="btn-back"><a href="/">トップページへ</a></p>
            </div>
        </div>
      </div>
    );
  }
}
