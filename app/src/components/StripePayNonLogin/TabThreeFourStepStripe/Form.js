/* @flow */

import React, { Component } from 'react';
import { connect } from 'react-redux';
import * as actions from '../../../actions/DonateAction';
import ProjectReturn from './ProjectReturn';

 class Form extends Component {

  /**
   * [confirmToDonate Confirm Donate]
   * @return {[type]} [Donate True Or False]
   */
  confirmToDonate() {
    const {project_id,backing_level_id,visa_info_nonlogin} = this.props;
    var data = {
      "project_id":project_id,
      "backing_level_id":backing_level_id,
      "email":visa_info_nonlogin.email,
      "name":visa_info_nonlogin.name,
      "customerId":visa_info_nonlogin.customerId
    }

    this.props.DonateNonLogin(data);

  }

  _renderAddress(inforUser) {

    return (
      <div className="form-area form-area-3">
          <p className="page-tab-label">ユーザー情報</p>
          <div className="form-box">
              <div className="form-field form-field-1">
                  <p className="form-field-name">名前</p>
                  <div className="form-field-content">
                    <p>
                      {inforUser.name}
                    </p>
                  </div>
              </div>
              <div className="form-field form-field-1">
                  <p className="form-field-name">名前</p>
                  <div className="form-field-content">
                    <p>
                      {inforUser.email}
                    </p>
                  </div>
              </div>

          </div>
      </div>
    )

  }

  render() {
    const {dataBacker , visa_info_nonlogin} = this.props;

    return (
      <div>
        {
          visa_info_nonlogin ?
          <form>
            <div className="form-area">
                <p className="page-tab-label">クレジットカード</p>
                <div className="form-box">
                    <div className="form-field">
                        <p className="form-field-name">カード番号</p>
                        <div className="form-field-content">
                          <p>
                            {visa_info_nonlogin.number_card}
                          </p>
                        </div>
                    </div>
                    <div className="form-field">
                        <p className="form-field-name">有効期限</p>
                        <div className="form-field-content">
                          <p>
                            <span>{visa_info_nonlogin.exp_month}</span>/ <span>{visa_info_nonlogin.exp_year}</span>
                          </p>
                        </div>
                    </div>

                    <div className="form-field">
                        <p className="form-field-name">セキュリティーコード</p>
                        <div className="form-field-content">
                          <p>
                            <span>{visa_info_nonlogin.cvc}</span>
                          </p>
                        </div>
                    </div>




                </div>
            </div>

            {
              this._renderAddress(visa_info_nonlogin)
            }
            {
              <ProjectReturn data={dataBacker}></ProjectReturn>
            }



            {/*<input className="btn-submit" type="submit" value="確認画面"/>*/}
            <span style={{cursor:"pointer"}} onClick={()=>this.confirmToDonate()} className="btn-submit">
              確認画面
            </span>
          </form> : null
        }
      </div>
    );
  }
}




// You have to connect() to any reducers that you wish to connect to yourself
export default connect(null, actions )(Form);
