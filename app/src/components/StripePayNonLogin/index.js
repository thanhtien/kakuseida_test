/* @flow */

import React, { Component } from 'react';
import Nav from './nav';
import TabOne from './TabOne';
import TabTwoStripe from './TabTwoStripe';

import TabThreeFourStepStripe from './TabThreeFourStepStripe';

import TabFour from './TabFour';
import { connect } from 'react-redux';
import * as actions from '../../actions/DonateAction';
import History from '../../history.js';
import Loading from '../common/loading';
import smoothScroll from '../common/smoothScroll';



class StripePay extends Component {

  /**
   * [constructor description]
   * @param {[type]} props [description]
   */
  constructor(props) {
    super(props);
    this.state = {
      step:1
    };
  }



  /**
   * [ChangeStep Change Step Donate]
   * @param {[type]} step [Step Number]
   */
  ChangeStep(step ) {
    this.setState({
      step:step
    },function () {
      smoothScroll.scrollTo('topNavStripe');
    });

  }
  /**
   * [ChangeStepTop Change Step Donate]
   * @param {[type]} step [Step Number]
   */
  ChangeStepTop(step) {
    this.setState({
      step:step
    });
  }
  /**
   * @param {[idProject]} project id
   * @param {[backerId]} return project id
   * @return {[type]} [Get Backer]
   */
  componentDidMount() {


    const {projectID , backerId} = this.props.match.params;
    this.props.getBackerNonLogin(projectID,backerId);

    if (this.props.disable) {
      this.props.disable_donate({
        type: 'DISABLE_DONATE',
      });
    }
    document.title = "リターン選択｜KAKUSEIDA";

  }

  /**
   * [componentWillMount LifeCirle]
   * @param {[idProject]} project id
   * @param {[backerId]} return project id
   * @return {[type]} [Get Backer]
   */

  componentWillMount() {
    const {projectID , backerId} = this.props.match.params;
    this.props.getBackerNonLogin(projectID,backerId);
  }


  render() {
    const { dataBacker, loading, profile, disable , typePay  , visa_info_nonlogin} = this.props;
    const {projectID, backerId} = this.props.match.params;
    const {step} = this.state;

    if (dataBacker) {

      if (dataBacker.now_count === dataBacker.max_count) {
        History.push('/unauthorized');
      }
    }



    if (loading) {

      return( <Loading></Loading> );
    }





    if (!dataBacker) {
      return( <Loading></Loading> );
    }


    return (
      <div className="cover-content">


        {/* Nav */}
        <Nav
          ChangeStep={ (value) => this.ChangeStepTop(value) }
          step={step}
          typePay={typePay}
          profileUser={profile}
          disable={disable}
        />
        {/* Nav */}

        {/* Tab One */}
        <div className={ step === 1 && disable === false ? "active pd-area animated fadeIn":"pd-area"} id="tab1">
          {
            dataBacker  ?
            <TabOne
              ChangeStep={ (value) => this.ChangeStep(value) }
              data={dataBacker}
              profileUser={profile}
            />
            : null
          }
        </div>
        {/* Tab One */}

        {/* Tab Two */}
        {
          typePay === 'stripe' ?
          <div className={ step === 2 && disable === false ? "active pd-area animated fadeIn":"pd-area"} id="tab2">
            {
              <TabTwoStripe
                projectID={projectID}
                backerId={backerId}
                ChangeStep={ (value) => this.ChangeStepTop(value) }
                data={dataBacker} />
            }
          </div>
          : null
        }
        {/* Tab Two */}






        <div className={ step === 3 && disable === false ? "active pd-area animated fadeIn":"pd-area"} id="tab3">





          {

            dataBacker  && typePay === 'stripe' ?
            <TabThreeFourStepStripe
              visa_info_nonlogin={visa_info_nonlogin}
              dataBacker={dataBacker}
              backing_level_id={backerId}
              project_id={projectID}
              invest_amount={dataBacker.invest_amount}
              ChangeStep={ (value) => this.ChangeStep(value) }
            />:
            null
          }

        </div>
        {/* Tab Three */}

        <div className={ disable === true ? "active pd-area animated fadeIn":"pd-area"} id="tab4">
          {
            <TabFour projectID={projectID} typePay={typePay}/>
          }
        </div>
        {/* Tab Four */}

      </div>
    );
  }
}
const mapStateToProps = (state) => {
  return {
    dataBacker: state.Donate.backerData,
    disable: state.Donate.disable,
    typePay: state.Donate.typePay,
    loading:state.common.loading,
    visa_info_nonlogin:state.common.visa_info_nonlogin
  }
}
export default connect(mapStateToProps, actions)(StripePay);
