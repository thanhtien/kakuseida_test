
import React, { Component } from 'react';
import { Field, reduxForm , reset } from 'redux-form';
import { connect } from 'react-redux';
import * as actions from '../../actions';
import Loading from '../common/loading';
import AlertAuth from './AlertAuth';
class forgotPassword extends Component {

  constructor(props) {
    super(props);
    this.state = {
      loading:false
    };
  }
  handleFormSubmit(data) {
    this.props.forgot(data);
  }
  componentDidMount() {
    document.title = "パスワードを思い出せない｜KAKUSEIDA";
  }

  //Render Each Input
  renderField = ({ input, label,placeholder,type, meta: { touched, error } }) => (
      <div className="cover-input" style={{float: 'left',position: 'relative'}}>
        <input className="form-control" {...input} placeholder={label} type={type} />
        {touched && error && <span style={{display: 'block',color:'red'}}>{error}</span>}
      </div>
  );
  //render Error
  renderError() {
    if (this.props.errorMessage) {
        return (
            <div className="alert alert-danger">
                <string>Oops! {this.props.errorMessage}</string>
            </div>
        );
    }
  }
  render() {
    const { handleSubmit,sendEmail,loading,sendEmailError } = this.props;
    return (
      <div className="area sign-area forgot">
        <div className="wraper">
          {
            loading ? <Loading></Loading> : null
          }
          {
            sendEmail ?
            <AlertAuth
              title={'パスワード変更の案内を登録されているメールアドレスに送信しました。'}
              messOne={'メールに記載されたURLをクリックの上、パスワード変更を続けてください。'}
              messTwo={'※KAKUSEIDAからの確認メールが届かない場合は、迷惑メールボックス（SPAMBOX)をご確認していただき、'}
              messThree={'KAKUSEIDAからのメール受信を許可するよう設定してください。'}
            />:
            <div className="login-panel">
                <div className="col">
                    <h2 className="title">パスワードを思い出せない</h2>
                    <div className="form-register">
                        <h1 style={{textAlign: 'center',color:'red'}}>
                          {
                            sendEmailError ? sendEmailError.status : null
                          }
                        </h1>
                          <form       onSubmit={handleSubmit(this.handleFormSubmit.bind(this))}>
                            <div className="form-field">
                              <p className="form-label">
                                登録されているメールアドレスをご入力ください。<br/>再設定用のURLをメールにて送信させていただきます。


                              </p>
                              <div className="form-one">
                                <Field
                                  className="form-control"
                                  name="email"
                                  label="Email"
                                  component={this.renderField}
                                  type="text"
                                />
                                <input className="one-row-btn" type="submit" value="送信"/>
                                {this.renderError()}
                              </div>
                            </div>
                          </form>

                    </div>
                </div>
            </div>
            }
            {/* Form*/}
          </div>
      </div>
    );
  }
}

const validate = values => {
    const errors = {};
    if (!values.email) {
        errors.email = 'メールアドレスを記入してください';
    }
    else if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(values.email)) {
        errors.email = 'メールアドレスを正しくしてください';
    }
    return errors;
};

const afterSubmit = (result, dispatch) => {
  return (
    dispatch(reset('forgot'))
  )
};

const mapStateToProps = (state) => {
    return {
      sendEmail: state.auth.sendEmail,
      loading: state.common.loading,
      sendEmailError:state.auth.sendEmailError
    }
};

export default reduxForm({
    form: 'forgot',
    onSubmitSuccess: afterSubmit,
    validate
})(connect(mapStateToProps,actions)(forgotPassword));
