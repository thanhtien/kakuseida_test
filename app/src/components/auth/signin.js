import React, { PureComponent } from 'react';
import { connect } from 'react-redux';

import SigninLoginForm from './signinLoginForm';
import SigninSubscribe from './signinSubscribe';
import Loading from '../common/loading';
import AlertAuth from './AlertAuth';



class Signin extends PureComponent {
  componentWillMount() {
    if (this.props.authenticated) {
      this.props.history.push('/');
    }
    document.title = "サインイン｜KAKUSEIDA";
  }


  componentWillUpdate(nextProps) {
    if (nextProps.authenticated) {
      this.props.history.push('/');
    }
  }





  render() {
      const {loading , sendSubEmail } = this.props;


      return (
        <div className="area sign-area">
          <div className="wraper">
              {
                loading ? <Loading /> : null
              }
              {
                sendSubEmail ?
                <AlertAuth
                  title={'メールをご確認ください'}
                  messOne={'メールに記載されたURLをクリックの上、新規会員登録を続けてください。'}
                  messTwo={'※KAKUSEIDAからの確認メールが届かない場合は、迷惑メールボックス（SPAMBOX)をご確認していただき、'}
                  messThree={'KAKUSEIDAからのメール受信を許可するよう設定してください。'}
                />
                :
                <div className="login-panel">
                    <div className="col-l">
                        <h2 className="title">ログイン</h2>
                        <SigninLoginForm />

                    </div>
                    <div className="col-r">
                        <h2 className="title">新規会員登録</h2>
                        <SigninSubscribe />
                    </div>

                </div>
              }

              {/* .login-panel */}

          </div>
        </div>
      );
    }
}

const mapStateToProps = (state) => {
  return {
    authenticated: state.auth.authenticated,
    loading:state.common.loading,
    sendSubEmail:state.auth.sendSubEmail
  }
};

export default connect(mapStateToProps)(Signin);
