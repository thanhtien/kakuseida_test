/* @flow */

import React, { Component } from 'react';
import { Field, reduxForm ,reset} from 'redux-form';
import * as actions from '../../actions';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import Twitter from './Twitter';
import FacebookLoginButton from './FacebookLoginButton';
import CreateUserSocial from './createUserSocial';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faFacebookF } from '@fortawesome/fontawesome-free-brands';
class SigninLoginForm extends Component {

  constructor(props) {
    super(props);
    this.state = {
      dataUserLogin:null,
      modalSocial:false
    };
  }

  //Render Each Input
  renderField = ({ input, label,placeholder,type, meta: { touched, error } }) => (
    <div className="form-field">
      <p className="form-label">{label}</p>
      <input className="form-control" {...input} placeholder={label} type={type} />
      {touched && error && <span style={{color:'red',display: 'block'}} className="text-danger">{error}</span>}
    </div>
  );
  //render Error
  renderError() {
    if (this.props.errorMessage) {
        return (
          <p style={{color:'red', textAlign: 'center'}}>
            {this.props.errorMessage}
          </p>
        );
    }
  }

  handleFormSubmit({ email, password }) {
    this.props.signinUser({ email, password })
  }

  setDataUserSocial(data) {

    this.setState({
      dataUserLogin:data,
      modalSocial:true
    })
    document.body.className = 'block-body';
  }
  hidePopupSocial(){
    this.setState({
      dataUserLogin:null,
      modalSocial:false
    })
    document.body.className = '';
  }
  justhideSocial(){
    this.setState({
      modalSocial:false
    })
    document.body.className = '';
  }
  justshowSocial(){
    this.setState({
      modalSocial:true
    })
    document.body.className = 'block-body';
  }

  onFacebookLogin = (loginStatus, resultObject) => {

    if (loginStatus === true) {
      this.props.facebookAccessToken(resultObject.authResponse.accessToken , (data)=>this.setDataUserSocial(data));
    } else {
      alert('Facebookのログインエラー');
    }
  }

  render() {

    const { handleSubmit , twitterToken } = this.props;
    const {modalSocial , dataUserLogin} = this.state;
    return (
      <div className="form-login">

          <CreateUserSocial
            dataUserLogin={dataUserLogin}
            hidePopup={()=>this.hidePopupSocial()}
            justhideSocial={()=>this.justhideSocial()}
            justshowSocial={()=>this.justshowSocial()}
            modalSocial={modalSocial}>
          </CreateUserSocial>

          <form onSubmit={handleSubmit(this.handleFormSubmit.bind(this))}>
            <Field
              className="form-control"
              name="email"
              label="Username/Email"
              component={this.renderField}
              type="text"
            />
            <Field
              label="Password"
              className="form-control"
              name="password"
              component={this.renderField}
              type="password" />

              <div className="cover-360">
                {this.renderError()}
                <p className="form-link">
                  <Link to={'forgot-password'}>パスワードを思出せない場合</Link>
                </p>
                <p className="form-submit">
                  <input type="submit" value="ログイン"/>
                </p>
              </div>
          </form>
          <div className="separator login-here-separator"></div>

          <p className="title-social">
            外部サービスでログインする
          </p>
          <FacebookLoginButton onLogin={this.onFacebookLogin}>
            <span className="facebook-button">
              <FontAwesomeIcon
                icon={faFacebookF} style={{fontSize: 17,marginRight: 20}} />
               Facebookでログイン
            </span>
          </FacebookLoginButton>
          <Twitter
            text={'Twitterでログイン'}
            setDataUserSocial={(data)=>this.setDataUserSocial(data)}
            twitterToken={twitterToken}
          />


      </div>
    );
  }
}

const validate = values => {
    const errors = {};
    if (!values.email) {
        errors.email = 'メールアドレスを記入してください';
    }


    if (!values.password) {
        errors.password = 'パスワードを記入してください';
    }
    return errors;
};


const mapStateToProps = (state) => {
  return {
    errorMessage: state.auth.error,
  }
};
const afterSubmit = (result, dispatch) => {
  return (
    dispatch(reset('signin'))
  )
};

export default reduxForm({
    form: 'signin',
    onSubmitSuccess:afterSubmit,
    validate,

})(connect(mapStateToProps, actions)(SigninLoginForm));
