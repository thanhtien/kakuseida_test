/* @flow */

import React, { Component } from 'react';
import { Field, reduxForm } from 'redux-form';
import * as actions from '../../actions';
import { connect } from 'react-redux';
import Twitter from './Twitter';
import FacebookLoginButton from './FacebookLoginButton';
import CreateUserSocial from './createUserSocial';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faFacebookF } from '@fortawesome/fontawesome-free-brands'
 class SigninSubscribe extends Component {


   constructor(props) {
     super(props);
     this.state = {
       dataUserLogin:null,
       modalSocial:false
     };
   }
  handleFormSubmit(formdata) {
    this.props.emailSubscribe(formdata)
  }
  renderField = ({ input, label,placeholder,type, meta: { touched, error } }) => (
      <div className="cover-input" style={{float:'left'}}>
        <input className="form-control" {...input} placeholder={placeholder} type={type} />
        {touched && error && <span style={{color:'red',display: 'block'}} className="text-danger">{error}</span>}
      </div>
  );
  renderError() {

    if (this.props.errorMessage) {
        return (
          <p style={{color:'red',display:'block',float:'left'}}>
            {this.props.errorMessage}
          </p>
        );
    }
  }


  setDataUserSocial(data) {

    this.setState({
      dataUserLogin:data,
      modalSocial:true
    })
    document.body.className = 'block-body';
  }
  hidePopupSocial(){
    this.setState({
      dataUserLogin:null,
      modalSocial:false
    })
    document.body.className = '';
  }
  justhideSocial(){
    this.setState({
      modalSocial:false
    })
    document.body.className = '';
  }
  justshowSocial(){
    this.setState({
      modalSocial:true
    })
    document.body.className = 'block-body';
  }

  onFacebookLogin = (loginStatus, resultObject) => {

    if (loginStatus === true) {
      this.props.facebookAccessToken(resultObject.authResponse.accessToken , (data)=>this.setDataUserSocial(data));
    } else {
      alert('Facebookのログインエラー');
    }
  }
  render() {
    const {handleSubmit,twitterToken} = this.props;
    const {modalSocial , dataUserLogin} = this.state;


    return (
      <div className="form-register">
        <CreateUserSocial
          dataUserLogin={dataUserLogin}
          hidePopup={()=>this.hidePopupSocial()}
          justhideSocial={()=>this.justhideSocial()}
          justshowSocial={()=>this.justshowSocial()}
          modalSocial={modalSocial}>
        </CreateUserSocial>
          <form onSubmit={handleSubmit(this.handleFormSubmit.bind(this))}>
              <div className="form-field">
                  <p className="form-label">メールアドレスで新規登録</p>
                  <div className="form-one">
                      <Field
                        className="form-control"
                        name="email"
                        placeholder="Email"
                        component={this.renderField}
                        type="text"
                      />

                      <input className="one-row-btn" type="submit" value="確認メールを送信"/>
                      {this.renderError()}
                  </div>
              </div>
          </form>
          <div className="separator"></div>

          <div className="register-side">


              <FacebookLoginButton onLogin={this.onFacebookLogin}>
                <span className="facebook-button">

                  <FontAwesomeIcon icon={faFacebookF} style={{fontSize: 17,marginRight: 20}} />
                    Facebookを利用して新規登録する
                </span>
              </FacebookLoginButton>
              <Twitter
                text={'Twitterを利用して新規登録する'}
                setDataUserSocial={(data)=>this.setDataUserSocial(data)}
                twitterToken={twitterToken}
              />
            <p>※承認なくFacebook、Twitterへポストすることはありません</p>
          </div>
      </div>
    );
  }
}

const validate = values => {
    const errors = {};
    if (!values.email) {
        errors.email = 'メールアドレスを記入してください';
    }
    else if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(values.email)) {
        errors.email = 'メールアドレスを正しくしてください';
    }
    return errors;
};
const mapStateToProps = (state) => {
  return {
    errorMessage: state.auth.sendSubEmailError,
  }
};

export default reduxForm({
    form: 'emailSubscribe',
    validate
})(connect(mapStateToProps, actions)(SigninSubscribe));
