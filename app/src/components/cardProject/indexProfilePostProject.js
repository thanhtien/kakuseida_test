/* @flow */

import React, { Component } from 'react';
import moment from 'moment';
import NumberFormat from 'react-number-format';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faTag , faUser } from '@fortawesome/free-solid-svg-icons';
import { Link } from 'react-router-dom';
import LinesEllipsis from 'react-lines-ellipsis'
import { LazyLoadImage } from 'react-lazy-load-image-component';
import History from '../../history';
export default class CardProject extends Component {
  _renderProjectEndDateTitle(data) {
    if (data) {
      if (data.format_collection_end_date) {
        if ('status' in data.format_collection_end_date) {
          return '終了';
        }
        if (data.format_collection_end_date.date > 0) {
          return data.format_collection_end_date.date + "日";
        }else {
          return data.format_collection_end_date.hour+"時"+data.format_collection_end_date.minutes+"分";
        }
      }
    }
  }

  _renderProjectEndDate(data) {
    if (data) {
      if (data.format_collection_end_date) {
        if ('status' in data.format_collection_end_date) {
          return '終了';
        }
        if (data.format_collection_end_date.date > 0) {
          return <span>{data.format_collection_end_date.date} 日</span>;
        }else {
          return <span>{data.format_collection_end_date.hour}時{data.format_collection_end_date.minutes}分</span>;
        }
      }
    }
  }
  componentDidMount() {
    window.addEventListener("resize", this.updateDimensions.bind(this));
    if (this.img) {
      this.setState({
        heightImage:(Math.round((this.img.offsetWidth*2)/3)).toString()
      })
    }
  }

  updateDimensions() {
    if (this.img) {
      this.setState({
        heightImage:(Math.round((this.img.offsetWidth*2)/3)).toString()
      })
    }
  }
  /**
   * [_renderLabel Label Project]
   * @param  {[type]} item [data project]
   * @return {[type]}      [label name]
   */
  _renderLabel(item,typeProject) {

    if (typeProject === '0') {
      var day = moment(item.collection_end_date).diff(new Date(), 'days');
      if (day < 0) {
        return (
          <p style={{background:"#82D9EE"}} className="label-no-active">
            終了
          </p>
        )
      }

      if (item.active === 'no' && item.opened === 'no') {
        return(
          <div>
            <p className="label-no-active">
              下書き
            </p>
            <span onClick={ () => this.deleteProject(item) } className="trash-function">
              <i className="icono-crossCircle"></i>
            </span>
          </div>
        )
      } else if((item.active === 'no' && item.opened === 'yes')) {
        return(
          <p style={{background:'#0e6eb8'}} className="label-no-active">
            申請中
          </p>
        )
      }
      else if((item.active === 'cls' && item.opened === 'yes')) {
        return(
          <p style={{background:'#000',color:"#fff"}} className="label-no-active">
            途中終了
          </p>
        )
      } else {
        return (
          <p className="label-active">
            公開中
          </p>
        )
      }
    }
    else {

      if(item.active === 'yes'){
        if(item.status_fanclub === 'progress'){

          return (
            <p className="label-active">
              公開中
            </p>
          )
        }else if(item.status_fanclub === 'wait'){

          return (
            <p className="label-active label-wait">
              解散承認待ち
            </p>
          )
        }else if(item.status_fanclub === 'stop'){
          return (
            <p style={{background:"#82D9EE"}} className="label-no-active">
              解散済み
            </p>
          )
        }

      }
      else if(item.active ==='no' && item.opened === 'no'){
          return(
            <div>
              <p className="label-no-active">
                下書き
              </p>
              <span onClick={ () => this.deleteProject(item) } className="trash-function">
                <i className="icono-crossCircle"></i>
              </span>
            </div>
          )
      }
      else if(item.active ==='no' && item.opened === 'yes'){
        return(
          <p style={{background:'#0e6eb8'}} className="label-no-active">
            申請中
          </p>
        )
      }
      else if(item.active ==='blk'){
        return (
          <p style={{background:'#000',color:"#fff"}} className="label-no-active">
            解散済み
          </p>
        )
      }else if(item.active ==='cls'){
        return (
          <p style={{background:'#000',color:"#fff"}} className="label-no-active">
            解散済み
          </p>
        )
      }
    }


  }



  /**
   * [_renderLink Link Project]
   * @param  {[type]} item [data project]
   * @return {[type]}      [Link Url]
   */
   _renderLink(item) {

     var day = moment(item.collection_end_date).diff(new Date(), 'days');
     if (day < 0) {
       return (
         <Link to={"/my-page/statistic/"+item.id}>
           <LazyLoadImage
            alt={"thumbnail"}
            effect="blur"
            src={item.thumbnail} />
         </Link>
       )
     }

     if (item.active === 'no' && item.opened === 'no') {
       return(
         <a href={"/my-page/edit-project/"+item.id}>
           <LazyLoadImage
            alt={"thumbnail"}
            effect="blur"
            src={item.thumbnail} />
         </a>
       )
     } else if((item.active === 'no' && item.opened === 'yes')) {
       return(
         <Link to={"/my-page/project-detail/"+item.id}>
           <LazyLoadImage
            alt={"thumbnail"}
            effect="blur"
            src={item.thumbnail} />
         </Link>
       )
     } else {
       return (
         <Link to={"/my-page/statistic/"+item.id}>
           <LazyLoadImage
            alt={"thumbnail"}
            effect="blur"
            src={item.thumbnail} />
         </Link>
       )
     }
   }
  /**
   * [deleteProject Delete UnPulic]
   * @return {[type]} [description]
   */
  deleteProject(item) {

    if (window.confirm(`${item.project_name}というプロジェクトを削除しますか。`)) {
      this.props.deleteProject(item,this.props.pageLoad);
    }

  }
  _capitalizeFirstLetter(string) {
    if (string) {
      return string.charAt(0).toUpperCase() + string.slice(1);
    }
  }

  /**
   * [_renderLink Link Project]
   * @param  {[type]} item [data project]
   * @return {[type]}      [Link Url]
   */
   clickRibbon(item) {

     var day = moment(item.collection_end_date).diff(new Date(), 'days');
     if (day < 0) {
       History.push(`/my-page/statistic/${item.id}`);
     }

     if (item.active === 'no' && item.opened === 'no') {
       History.push("/my-page/edit-project/"+item.id);

     } else if((item.active === 'no' && item.opened === 'yes')) {
       History.push("/my-page/project-detail/"+item.id);
     } else {
       History.push("/my-page/statistic/"+item.id);
     }
   }

   _renderLinkTitle(item){
     var day = moment(item.collection_end_date).diff(new Date(), 'days');
     if (day < 0) {
       return (
         <Link to={'/my-page/statistic/'+item.id}>
           <pre>
             <LinesEllipsis
               text={item.project_name}
               maxLine='2'
               ellipsis='...'
               trimRight
               basedOn='letters'
             />
           </pre>
         </Link>
       )
     }

     if (item.active === 'no' && item.opened === 'no') {
       return(
         <a href={"/my-page/edit-project/"+item.id}>
           <pre>
             <LinesEllipsis
               text={item.project_name}
               maxLine='2'
               ellipsis='...'
               trimRight
               basedOn='letters'
             />
           </pre>

         </a>
       )
     } else if((item.active === 'no' && item.opened === 'yes')) {
       return(
         <Link to={"/my-page/project-detail/"+item.id}>
           <pre>
             <LinesEllipsis
               text={item.project_name}
               maxLine='2'
               ellipsis='...'
               trimRight
               basedOn='letters'
             />
           </pre>

         </Link>
       )
     } else {
       return (
         <Link to={"/my-page/statistic/"+item.id}>
           <pre>
             <LinesEllipsis
               text={item.project_name}
               maxLine='2'
               ellipsis='...'
               trimRight
               basedOn='letters'
             />
           </pre>

         </Link>
       )
     }
   }
  _renderTemplate(item) {
    const numberWidth = Math.round((Number(item.collected_amount)/Number(item.goal_amount))*100);

    return(
      <div style={{position:"relative",overflow:'initial'}} className="col">
        <div style = {{height:this.img !== undefined ? this.state.heightImage+'px' : '0px' , overflow:'hidden'}} ref={ (img) => this.img = img} className="img">
          {
            this._renderLink(item)
          }
          {
            this._renderLabel(item,item.project_type)
          }
          {
            item.project_type === '1' ?
            <div style={{cursor:'pointer'}} onClick={ ()=>this.clickRibbon(item) } className="ribbon ribbon-top-right"><span>定期課金のプロジェクト</span></div>
            : null
          }

        </div>

        <div className="boxCol">

            <div title={item.project_name} className="postTitle">
              {
                this._renderLinkTitle(item)
              }
            </div>


            <p  className="rPeople">
                <span title={item.category.name} className="note"><Link to={'/categories/'+item.category.slug+'/page=1'}>
                <FontAwesomeIcon icon={faTag} />{' '+item.category.name}</Link></span>

                {
                  item.user ?
                  <span title={item.user.name} className="people">
                    <span className="name-user" style={{cursor:'pointer'}}>
                      <FontAwesomeIcon icon={faUser} />
                    {' '+ this._capitalizeFirstLetter(item.user.name) }</span></span> :null
                }
            </p>





            {
              item.project_type === '1' ?
              <div className="meter fan">

              </div> :
              <div className="meter">
                <div className="countNum" style={numberWidth >=100 ? {width:"100%"} : {width:numberWidth+"%"}}>
                  <span className="numTr">{numberWidth}%</span>
                </div>
              </div>

            }
            <div className={item.project_type === '1' ? "rowBot fan" : "rowBot"} >
                <div className="rowBot-l">
                    <p title={item.collected_amount} className="price tooltip">

                        <span className="small-title">現在</span>
                        <span className="number">
                          <span >
                            <NumberFormat value={item.collected_amount} displayType={'text'} thousandSeparator={true} suffix={'円'} />
                          </span>
                        </span>

                    </p>
                    <p title={item.now_count.now_count} className="pNumber">
                        <span className="small-title">支援者数</span>
                        <span className="number">{item.now_count.now_count}人<span>
                    </span></span></p>
                </div>
                <div className="rowBot-r">
                    <p title={this._renderProjectEndDateTitle(item)}  className="date">

                      <span className="small-title">残り</span>
                      <span className="number">{this._renderProjectEndDate(item)}<span>
                      </span></span>
                    </p>
                </div>
            </div>
        </div>
        {/* BOXCOL */}
      </div>
    );
  }
  render() {
    const {item} = this.props;

    return (
      this._renderTemplate(item)
    );
  }
}
