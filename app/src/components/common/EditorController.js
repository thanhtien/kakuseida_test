/* @flow */

import React, { Component } from 'react';
import { Editor } from 'react-draft-wysiwyg';
import { EditorState, convertToRaw , ContentState } from 'draft-js';
import 'react-draft-wysiwyg/dist/react-draft-wysiwyg.css';
import axios from 'axios';

import draftToHtml from "draftjs-to-html";
import { unemojify } from "node-emoji";
import htmlToDraft from 'html-to-draftjs';



var CONFIG = require('../../config/common');

if (process.env.NODE_ENV === "development") {
  CONFIG = CONFIG.CONFIG.DEV;
}else {
  CONFIG = CONFIG.CONFIG.PRODUCT;
}

const ROOT_URL = CONFIG.ROOT_URL;
export default class EditorController extends Component {

  constructor(props) {
    super(props);
    if (this.props.DefaultProjectContent) {
      const contentBlock = htmlToDraft(this.props.DefaultProjectContent);
      if (contentBlock) {
        const contentState = ContentState.createFromBlockArray(contentBlock.contentBlocks);
        const editorState = EditorState.createWithContent(contentState);
        this.state = {
          editorState,
        };
      }
    }else {
      this.state = {
        editorState: EditorState.createEmpty(),
      }
      this.props.onChange(
        draftToHtml(convertToRaw(this.state.editorState.getCurrentContent()))
      );
    }

  }






  clearForm: Function = editorState => {
    const contentBlock = htmlToDraft(editorState);
    if (contentBlock) {
      const contentState = ContentState.createFromBlockArray(contentBlock.contentBlocks);
      const editorState = EditorState.createWithContent(contentState);
      this.setState({
        editorState
      });
    }
 };

  onEditorStateChange: Function = editorState => {

   const { onChange, value } = this.props;

   const newValue = unemojify(
     draftToHtml(convertToRaw(editorState.getCurrentContent()))
   );




   if (value !== newValue) {
     onChange(newValue);
   }

   this.setState({
     editorState
   });

 };





  /**
   * [uploadCallback in editor]
   * @return {[string]}      [Link]
   */
  uploadCallback(file){
    return new Promise(
      (resolve, reject) => {
        let formData = new FormData();
        formData.append('files',file);
        const token = 'Bearer '+ localStorage.getItem('token');

        //Config Axios
        const config = {
            headers: {
              "Authorization" : token
            }
          }

        axios.post(`${ROOT_URL}api/Upload/thumbnailEditor`, formData, config)
          .then(response => {
            const url = response.data.base_url+response.data.forder+response.data.name_image+response.data.mime_type
            resolve({ data: { link: url } });
            console.log(response);
          }).catch((error) => {
            reject(error.toString());
            if (error.response.data.status === 'Expired token') {
              var confirm = window.confirm(error.response.data.status);
              if (confirm===true) {
                localStorage.removeItem('token')
                setTimeout(function () {
                  History.push('/signin');
                }, 1000);
              }
            }else {
              alert(error.response.data.error);
            }
          });
      }
    ).catch(err => {
      return err;
    });
  }


  render() {
    return (
      <Editor

        editorState={this.state.editorState}
        toolbarClassName="toolbarClassName"
        wrapperClassName="wrapperClassName"
        editorClassName="editorClassName"
        onEditorStateChange={this.onEditorStateChange}
        readOnly={this.props.disable}
        localization={{
          locale: 'ja',
        }}
        toolbar={{
          image: {
            uploadEnabled: true,
            previewImage:true,
            alt: { present: true, mandatory: false },
            uploadCallback: this.uploadCallback
          },
        }}
      />
    );
  }
}
