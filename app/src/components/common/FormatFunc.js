/* @flow */

import React, { Component } from 'react';
import moment from 'moment';


export default class MyComponent extends Component {


  _renderTime(time) {
    var stillUtc = moment.utc(time).toDate();

    var localTime = new Date(moment(stillUtc).local().format('YYYY-MM-DD HH:mm:ss').replace(' ', 'T'));

    var aaaa = Number(localTime.getFullYear());
    var gg = Number(localTime.getDate());
    var mm = (Number(localTime.getMonth()) + 1);
    var hours = Number(localTime.getHours());
    var minutes = Number(localTime.getMinutes());
    var timeReturn;

    if (Number(minutes) < 10) {
      timeReturn = `${aaaa}年${mm}月${gg}日 ${hours}:0${minutes}`;
    }else {
      timeReturn = `${aaaa}年${mm}月${gg}日 ${hours}:${minutes}`;
    }
    return timeReturn;
  }

  render() {
    const {date} = this.props;
    return (
      <span className="format-date">{this._renderTime(date)}</span>
    );
  }
}
