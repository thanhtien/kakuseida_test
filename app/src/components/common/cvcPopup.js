/* @flow */

import React, { Component } from 'react';
import Rodal from 'rodal';
var CONFIG = require('../../config/common');

if (process.env.NODE_ENV === "development") {
  CONFIG = CONFIG.CONFIG.DEV;
}else {
  CONFIG = CONFIG.CONFIG.PRODUCT;
}
export default class MyComponent extends Component {

  constructor(props) {
    super(props);
    this.state = {
      tooglePopup:false
    };
  }

  showPopupCVC(){
    this.setState({
      tooglePopup:true
    })

  }
  hidePopup(e){
    e.preventDefault();
    this.setState({
      tooglePopup:false
    })
  }
  render() {
    console.log();
    let w = 500;
    let h = 251;
    if (window.innerWidth < 768) {
      w = 300;
      h = 150;
    }
    return (
      <div className="question"  >
        <i onClick={ ()=>this.showPopupCVC() } className="icss-anim icss-question-circle"></i>
          <Rodal  width={w} height={h} visible={this.state.tooglePopup} onClose={(e)=>this.hidePopup(e)}>
            <img src={`${CONFIG.MAIN_URL}/img/common/debit.jpg`} alt=""/>
          </Rodal>
      </div>
    );
  }
}
