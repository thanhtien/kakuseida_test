/*global FB*/

import React, { Component } from 'react';

import { Link } from 'react-router-dom';

var CONFIG = require('../../config/common');

if (process.env.NODE_ENV === "development") {
  CONFIG = CONFIG.CONFIG.DEV;
}else {
  CONFIG = CONFIG.CONFIG.PRODUCT;
}

class Footer extends Component {



  componentDidMount() {

    window.fbAsyncInit = function() {
      FB.init({
        appId      : CONFIG.FB_KEY,
        xfbml      : true,
        version    : 'v2.1'
      });
    };


    (function(d, s, id) {
      var js, fjs = d.getElementsByTagName(s)[0];
      if (d.getElementById(id)) return;
      js = d.createElement(s); js.id = id;
      js.src = "https://connect.facebook.net/ja_JP/sdk.js#xfbml=1&version=v3.0";
      fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));


     var event = document.createEvent('Event');

     event.initEvent('fb_init', true, true);

     document.addEventListener('fb_init', function (e) {
       if (window.FB) {
         FB.XFBML.parse()
       }
     }, false);

     document.dispatchEvent(event);



     window.twttr = (function(d, s, id) {
     var js, fjs = d.getElementsByTagName(s)[0],  t = window.twttr || {};
     if (d.getElementById(id)) return t;
     js = d.createElement(s); js.id = id;
     js.src = "https://platform.twitter.com/widgets.js";
     fjs.parentNode.insertBefore(js, fjs);
     t._e = []; t.ready = function(f) {
       t._e.push(f);
     };
     return t;
     }(document, "script", "twitter-wjs"));

    if (window.twttr.widgets) {
      window.twttr.widgets.createFollowButton(
        'kakuseitokyo',
        document.getElementById('container'),
        {
          size: 'small',
          showScreenName: "false",
          showCount: false,
          "lang":"ja"
        }
      );
    }

  }

  render() {
    const {data} = this.props;
    return (
      <div id="footer">
          <div className="wraper">
              <div className="subRow">
                  <div className="colLeft colLeft1">
                      <p className="title">プロジェクトを探す</p>
                      <ul className="listCat">
                          {
                            data ? data.map( (item , i) => {
                              return(
                                <li key={i}>
                                  <a href={'/categories/'+item.slug+'/page=1'}>
                                    {item.name}
                                  </a>
                                </li>
                              )
                            }) : null
                          }
                      </ul>
                  </div>
                  <div className="colLeft colLeft2">
                      <p className="title">KAKUSEIDAとは？</p>
                      <div className="row">
                          <ul className="listCat">
                              <li><Link to={'/kakuseida/term'} >利用規約</Link></li>
                              <li><Link to={'/kakuseida/privacy-policy'} >プライバシーポリシー</Link></li>
                              <li><Link to={'/kakuseida/legal'} >特定商取引法に基づく表記</Link></li>
                              <li><Link to={'/kakuseida/guideline'} >企画者ガイドライン</Link></li>
                              <li><Link to={'/kakuseida/kakuseida-towa'} >KAKUSEIDAとは？</Link></li>
                              <li><Link to={'/kakuseida/Contact/'} >お問い合わせ</Link></li>
                          </ul>
                      </div>
                  </div>

                  <ul className="sns">
                      <li>
                        <div className="fb-like"
                         data-href="https://www.facebook.com/bpotech/"
                         data-layout="button"
                         data-action="like"
                         data-show-faces="true">
                       </div>
                      </li>
                      <li id="container">
                        <a
                          className="twitter-follow-button"
                          href="https://twitter.com/kakuseitokyo"
                          data-show-count="false"
                          data-show-screen-name="false"
                          data-lang="ja"
                          data-size="small">
                        </a>

                      </li>

                  </ul>
              </div>
          </div>
          {/* WRAPER */}
          <p id="copyright">© 2018 KAKUSEI Inc.</p>
      </div>
    );
  }
}


export default Footer
