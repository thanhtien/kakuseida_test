/* @flow */

import React, { Component } from 'react';
import { Link  } from 'react-router-dom';


export default class MenuPc extends Component {

  getJsonFromUrl(url) {
    return url.split("/");
  }


  render() {
    const {categories} = this.props;
    var urlParese = this.getJsonFromUrl(window.location.href);

    return (
      <ul className="menu pc">
        <li>
          <Link className={ urlParese[3] === "" ? 'nav-link active' : 'nav-link' } to="/" >HOME</Link>
        </li>
        <li>
            <div className={ urlParese[3] === "categories" ? 'top-link active' : 'top-link' }>
              プロジェクトをさがす
              <div className="subMenu">
                  <p className="titleSub">プロジェクトを探す</p>
                  <ul className="menuSub">
                    {
                      categories ? categories.map( (item , i) => {
                        return(
                          <li key={i}>
                            <a className="nav-link" href={'/categories/'+item.slug+'/page=1'}>{'ー '+item.name}</a>
                          </li>
                        )
                      }) : null
                    }
                  </ul>
                  <ul className="menuSub2">
                      {
                        document.getElementById('area2') ? <li><span className="link-face" onClick={()=>this.anchorToSlider()} style={{cursor:"pointer"}} >人気のプロジェクト</span></li> :null
                      }
                      <li><Link to={'/project-enough-goal-amount-list/page=1'} >あと少しで目標100％になるプロジェクト</Link></li>
                      <li><Link to={'/project-enough-expired-list/page=1'} >終了間近のプロジェクト</Link></li>
                  </ul>
              </div>
            </div>

        </li>
        <li>
          <div
            className={ urlParese[3] === "kakuseida" ? 'top-link active' : 'top-link' }    onClick={e => e.preventDefault()}>
            KAKUSEIDAとは？
            <div className="subMenu">
              <ul className="menuSub just-link">
                <li><Link to={'/kakuseida/term'} >利用規約</Link></li>
                <li><Link to={'/kakuseida/privacy-policy'} >プライバシーポリシー</Link></li>
                <li><Link to={'/kakuseida/legal'} >特定商取引法に基づく表記</Link></li>
                <li><Link to={'/kakuseida/kakuseida-towa'} >KAKUSEIDAとは？</Link></li>
                <li><Link to={'/kakuseida/guideline'} >企画者ガイドライン</Link></li>
                <li><Link to={'/kakuseida/Contact/'} >お問い合わせ</Link></li>
              </ul>
            </div>
          </div>

        </li>
        <li>
            <div
              className={ urlParese[3] === "operating-company" ? 'top-link active' : 'top-link' }    >
              運営会社
              <div className="subMenu">
                <ul className="menuSub just-link">
                  <li><Link to={'/operating-company/company'} >会社概要</Link></li>
                  <li><Link to={'/operating-company/kakuseitowa'} >カクセイとは</Link></li>
                  <li>

                      <a href="https://kakusei.com" target="_blank" rel="noopener noreferrer">
                        カクセイグループHP
                      </a>

                  </li>
                  <li><Link to={'/operating-company/daihyou-aisatsu'} >代表者挨拶</Link></li>
                  <li><Link to={'/operating-company/access'} >事業所案内（MAP・アクセス）</Link></li>
                </ul>
              </div>
            </div>
        </li>
        <li>
          <Link className={ urlParese[3] === "first-user" ? 'nav-link active' : 'nav-link' } to="/first-user" >初めての方へ</Link>
        </li>
      </ul>
    );
  }
}
