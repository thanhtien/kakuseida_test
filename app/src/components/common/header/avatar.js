/* @flow */

import React, { Component } from 'react';
import { Link  } from 'react-router-dom';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import {faDonate , faEdit , faCog , faSignOutAlt,faGem,faEnvelope} from '@fortawesome/free-solid-svg-icons';


export default class Avatar extends Component {
  render() {
    const {profile} = this.props;
    return (
      <ul>
        <li className="name-row">
            <p className="name">
              <Link to="/my-page/project-supported/page=1">{profile.username}<span>マイページへ</span></Link>
            </p>
        </li>

        <li className="support-row">
          <p className="support-project">
            <FontAwesomeIcon icon={faDonate} />

            <Link to="/my-page/project-supported/page=1">
              支援したプロジェクト
            </Link>
          </p>
        </li>
        <li className="support-row">
          <p className="fan-project">
            <FontAwesomeIcon icon={faGem} />
            <Link to="/my-page/project-supported-fan/page=1">定期支援中のプロジェクト</Link>
          </p>
        </li>
        <li className="post-row">
          <p className="post-project">
            <FontAwesomeIcon icon={faEdit} />
            <Link to="/my-page/post-project/page=1">自分のプロジェクト</Link>
          </p>
        </li>
        <li className="post-row">
          <p className="post-project">
            <FontAwesomeIcon icon={faEnvelope} />
            <Link to="/my-page/emailBox/page=0">メッセージ</Link>
          </p>
        </li>
        <li className="setting-row">
          <p className="setting">
            <FontAwesomeIcon icon={faCog} />
            <Link to="/my-page/setting-user/edit-profile">設定</Link></p></li>
        <li className="logout-row"><p className="logout"><FontAwesomeIcon icon={faSignOutAlt} /><Link to={'/signout'} >ログアウト</Link></p></li>
      </ul>
    );
  }
}
