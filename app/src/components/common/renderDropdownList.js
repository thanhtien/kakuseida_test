import React from 'react';
import DropdownList from 'react-widgets/lib/DropdownList'

const renderDropdownList = ({   input, data, valueField,disable, textField,meta: { touched, error } , ...rest }) => {

  return(
    <div name={`position-${input.name}`}>
      <DropdownList
        disabled={disable}
        {...input}
        data={data}
        valueField={valueField}
        textField={textField}
        onBlur = {function(data) { return input.onBlur(data.id)} }
        onChange={function(data) { return input.onChange(data.id)}}
        {...rest}
        />
      {touched && error && <span style={{color:'red'}} className="text-danger">{error}</span>}
    </div>
  )
};
export default renderDropdownList;
