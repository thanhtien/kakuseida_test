import Dropzone from 'react-dropzone';
import React, { Component } from 'react';

import * as actions from '../../actions/editSystem';
import { connect } from 'react-redux'


class renderDropzone extends Component {

  constructor(props) {
    super(props)
    this.state = {
      accepted: null,
      rejected: null,
      imageError:false
    }
  }


  setImageError(status) {
    this.setState({
      imageError:status
    })
  }

  //OnDrop
  onDrophere(accepted, rejected) {
    this.setState({ accepted, rejected });
    this.props.UploadImage(accepted[0],this.props.formName,this.props.feild,this.props.path,(status)=>this.setImageError(status) , this.props.imagePreview);
  }

  _renderImagePreview(accepted,imagePreview,imageError){

    if (accepted && imageError === false) {
      return(
        <p className="cover-image-preview">
          <img src={accepted[0].preview} alt="preview"/>
        </p>
      )
    }else if( !accepted && imagePreview ){
      return(
        <p className="cover-image-preview">
          <img src={imagePreview} alt="preview"/>
        </p>
      )
    }else if( imageError === true && imagePreview  ){
      return (
        <p className="cover-image-preview">
          <img src={imagePreview} alt="preview"/>
        </p>
      )
    }else {
      return (
        <div className="imageUpload">
          <div>
            <p >ドラッグ&ドロップで<br/>ファイルをアップロードする</p>
          </div>
        </div>
      )
    }
  }
  render() {
    const { disable, imagePreview } = this.props;
    const { imageError } = this.state;

    return (
      <div>
        <Dropzone
          disabled={disable}
          disabledClassName={"disabledClassName"}
          acceptClassName="acceptClassName"
          rejectClassName={"rejectedFile"}
          activeClassName={"activeDropZone"}
          className="dropzoneStyle"
          accept="image/jpeg, image/png"
          onDrop={(accepted, rejected) => this.onDrophere(accepted, rejected) }
          multiple={false}
        >

          {
            this._renderImagePreview(this.state.accepted ,  imagePreview , imageError , imagePreview)
          }


        </Dropzone>


      </div>
    );
  }
}

const mapStateToProps = (state) => {
    return {
      disable: state.uploadImage.disable,
    }
}

export default connect(mapStateToProps, actions )(renderDropzone);
