

/* @flow */

import React, { Component } from 'react';


export default class renderFieldCheckbox extends Component {

  render() {
    const { input , type } = this.props;
    return (
      <div className="cover-checkbox" name={`position-${input.name}`}>
        <input

          id={`${input.name}`}
          {...input}
          type={type}
          value={`${input.value}`}
        />
        <label className="label-checkbox" htmlFor={`${input.name}`}>
          デフォルトにする
        </label>
      </div>
    );
  }
}
