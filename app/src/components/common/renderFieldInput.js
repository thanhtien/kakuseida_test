

import React from 'react'
const renderField = (
  { input,placeholder,type,autoFocus,maxlength,disable, meta: { touched, error  } }) => (

  <div className={input.name === 'cvc' ? "input-render cvc-input"  : "input-render" }  name={`position-${input.name}`} >

    <input disabled={disable} maxLength={maxlength} className="string optional" autoFocus={autoFocus} style={{width:"100%"}} {...input} placeholder={placeholder} type={type} />
    {touched && error && <span style={ input.name === 'cvc' ? {color:'red',height: 0} : {color:'red'} } className="text-danger">{error}</span>}

  </div>

);

export default renderField;
