/* @flow */

import React, { Component } from 'react';
import { connect } from 'react-redux'
import * as actions from '../../../../actions/EmailBox';
import Loading from '../../../common/loading';
import SiderbarMenu from '../SiderbarMenu';
import Title from '../Title';
import ListProject from './ListProject';


class ChoseListProject extends Component {

  componentDidMount() {
    const {page} = this.props.match.params;
    var PageString = page.replace('page=', '');

    if (!this.props.data) {
      this.props.ProjectEmailBox(Number(PageString)-1);
    }
    document.title = "メッセージ新規作成｜KAKUSEIDA";

  }

  handlePageClick = (data) => {
    let selected = data.selected;
    this.props.ProjectEmailBox(selected);
  }

  render() {
    const {data , pageLoad} = this.props;

    if (!data) {
      return (<Loading></Loading>)
    }
    return (
      <div className="area cover-send-mail">
        <Title title={'メッセージ新規作成'}></Title>
        {/* Title */}

        <div className="wraper">
          <div className="cover-no-blank">



            <SiderbarMenu />
            {/* SiderBar */}

            <div className="right-content-mail">
              <p className="title-composer">支援者になっているプロジェクトから選ぶ</p>
              <ListProject
                handlePageClick={ (data)=>this.handlePageClick(data)}
                data={data}
                pageLoad={pageLoad} />
              {/* SiderBar */}

            </div>

            {/* Mail List */}

            <div className="clear-fix"></div>
          </div>

        </div>
        {/* Wraper */}

      </div>
    );
  }
}

const mapStateToProps = (state) => {
    return {
      data: state.MailBox.EmailList.data,
      pageLoad:state.MailBox.EmailList.page
    }
}
export default connect(mapStateToProps, actions )(ChoseListProject);
