/* @flow */

import React, { Component } from 'react';

import ReactPaginate from 'react-paginate';
import EmailContent from '../EmailContent';
import FormatFunc from '../../../common/FormatFunc';


export default class MailList extends Component {

  handlePageClick = (data) => {
    let selected = data.selected;
    this.props.handlePageClick(selected);
  };
  render() {

    const {data,pageLoad} = this.props;

    return (
      <div>
        {
          data && data.data.length > 0 ?
          <ul className="message-send-list">


            {


              data.data.map( (item , i ) => {

                return(
                  <li key={i}>
                    <p className="title-project-name">{item.project_name}</p>
                    <div className="cover-avatar-small">
                      <img src={item.profileImageURL} alt={item.username}/>
                    </div>
                    <div className="cover-right">
                      <div className="text-up">
                        <p className="title-name" titl={item.username}>{item.username}</p>
                        <p className="title-time">{ <FormatFunc date={item.created}></FormatFunc> }</p>
                      </div>
                      <div className="message-mail">
                        <EmailContent data={item.message}></EmailContent>
                      </div>
                    </div>
                  </li>
                )
              })

            }
          </ul> :
          <p className="none-email">
            メッセージはありません
          </p>
        }

        {
          data && data.data.length > 0 ?
          <div className="cover-paginate custom-suport">
            {
              <ReactPaginate
                previousLabel={"«"}
                nextLabel={"»"}
                breakLabel={<span>...</span>}
                breakClassName={"break-me"}
                pageCount={data.totalPages}
                marginPagesDisplayed={2}
                pageRangeDisplayed={2}
                forcePage={Number(pageLoad)}
                onPageChange={this.handlePageClick}
                containerClassName={"pagination"}
                subContainerClassName={"pages pagination"}
                activeClassName={"active"} />
            }
          </div> : null
        }


        {/* PAGINATE */}


      </div>


    );
  }
}
