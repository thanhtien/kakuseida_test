/* @flow */

import React, { Component } from 'react';
import SiderbarMenu from '../SiderbarMenu';
import MailList from './MailList';
import Title from '../Title';
import * as actions from '../../../../actions/EmailBox';
import Loading from '../../../common/loading';
import { connect } from 'react-redux'
import FlashMassage from 'react-flash-message';


class MailBox extends Component {

  constructor(props) {
    super(props);
    this.state = {
      loading:false
    };
  }
  ToogleLoading(){
    this.setState({
      loading:!this.state.loading
    })
  }
  componentDidMount() {
    const {page} = this.props.match.params;
    const {data} = this.props;

    var PageString = page.replace('page=', '');

    if (data && !data.data) {
      this.props.sendedEmail(Number(PageString)-1,()=>this.ToogleLoading());
    }
    document.title = "送信済メッセージ｜KAKUSEIDA";


  }


  handlePageClick(data){
    this.props.sendedEmail(Number(data),()=>this.ToogleLoading());
  }
  _renderStatusAPI() {
    const {message} = this.props;
    setTimeout( () => {
      this.props.FlashMassage();
    }, 2000);
    return(
      <div id="push-notification">
        <FlashMassage duration={2000}>
          <div className="alert">
            {message}
          </div>
        </FlashMassage>
      </div>
    )
  }


  render() {
    const {data,pageLoad,status_send_mail} = this.props;
    const {loading} = this.state;

    if (loading) {
      return(
        <Loading></Loading>
      )
    }

    return (
      <div className="area cover-send-mail">
        <Title title={'送信済メッセージ'}></Title>
        {/* Title */}

        <div className="wraper">
          <div className="cover-no-blank">
            {
              status_send_mail === true ?
              this._renderStatusAPI() : null
            }



            <SiderbarMenu active={'emailSended'}/>
            {/* SiderBar */}

            <div className="right-content-mail">
              {


                <MailList
                  pageLoad={pageLoad}
                  handlePageClick={(page)=>this.handlePageClick(page)}
                  data={data.data} />
              }

              {/* SiderBar */}

            </div>

            {/* Mail List */}

            <div className="clear-fix"></div>
          </div>

        </div>
        {/* Wraper */}

      </div>
    );
  }
}

const mapStateToProps = (state) => {
    return {
      data:state.MailBox.SendedEmail,
      pageLoad:state.MailBox.SendedEmail.page,
      message:state.MailBox.message,
      status_send_mail:state.MailBox.status_send_mail,
    }
}
export default connect(mapStateToProps, actions )(MailBox);
