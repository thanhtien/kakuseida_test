/* @flow */

import React, { Component } from 'react';

import ReactPaginate from 'react-paginate';
import EmailContent from './EmailContent';
import History from '../../../history';
import FormatFunc from '../../common/FormatFunc';

export default class MyComponent extends Component {

  handlePageClick = (data) => {
    let selected = data.selected;
    this.props.handlePageClick(selected);
  };
  goToReply(data,status){
    if (status === '0') {
      this.props.goToReply(data);
    }else {
      History.push(`/my-page/email-box/new-user/${data.user_id_send}/project/${data.project_id}`);
    }
  }
  goRead(data){
    this.props.goToRead(data);
  }
  render() {

    const {data,pageLoad} = this.props;

    return (
      <div>
        {
          data && data.data.length > 0 ?
          <ul className="message-send-list">
            {
              data.data.map( (item , i ) => {


                return(
                  <li
                    style={{cursor:'pointer'}}
                    key={i}
                    className={item.status === '0' ? "bold-text" : null}>
                    <p className="title-project-name">{item.project_name}</p>
                    <div className="cover-avatar-small">
                      <img src={item.profileImageURL} alt={item.username}/>
                    </div>
                    <div className="cover-right">
                      <div className="text-up">
                        <p className="title-name" title={item.username}>
                          {item.username}
                        </p>
                        <p className="title-time">{<FormatFunc date={item.created} ></FormatFunc>}</p>
                      </div>
                      <div className="message-mail">
                        <EmailContent data={item.message}></EmailContent>

                      </div>
                      <div className="action-button">
                        <button className="reply-email" onClick={()=>this.goToReply(item,item.status)} >返信</button>

                      {
                        item.status === '0' ? <button className="reply-email" onClick={()=>this.goRead(item,item.status)} >既読</button> : null
                      }


                      </div>



                    </div>


                  </li>
                )
              })

            }
          </ul> :
          <p className="none-email">
            メッセージはありません
          </p>
        }

        {
          data && data.data.length > 0 ?
          <div className="cover-paginate custom-suport">
            {
              <ReactPaginate
                previousLabel={"«"}
                nextLabel={"»"}
                breakLabel={<span>...</span>}
                breakClassName={"break-me"}
                pageCount={data.totalPages}
                marginPagesDisplayed={2}
                pageRangeDisplayed={2}
                forcePage={Number(pageLoad)}
                onPageChange={this.handlePageClick}
                containerClassName={"pagination"}
                subContainerClassName={"pages pagination"}
                activeClassName={"active"} />
            }
          </div> : null
        }


        {/* PAGINATE */}


      </div>


    );
  }
}


// <td data-label="支援者名">
//   <div className="avatar-and-name">
//
//     <p className="avataer-mail"><img src={item.avatar} alt="avatar"/></p>
//     <div className="text-name" title={item.from}>
//       <LinesEllipsis
//         text={item.from}
//         maxLine='1'
//         ellipsis='...'
//         trimRight
//         basedOn='letters'
//       />
//     </div>
//   </div>
//
//
// </td>
//
//
//
//
// <td data-label="リターン配送" title={item.message}>
//
//   <LinesEllipsis
//     text={item.message}
//     maxLine='1'
//     ellipsis='...'
//     trimRight
//     basedOn='letters'
//   />
// </td>
// <td data-label="リターン配送">
//   {
//     displaydate
//   }
// </td>
// <td data-label="リターン配送">
//   {
//     item.status === '1' ? 'doc roi' : 'chua doc'
//   }
// </td>
