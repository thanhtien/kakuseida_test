/* @flow */

import React, { Component } from 'react';
import SiderbarMenu from './SiderbarMenu';
import MailList from './MailList';
import Title from './Title';
import * as actions from '../../../actions/EmailBox';
import Loading from '../../common/loading';
import { connect } from 'react-redux'



class MailBox extends Component {

  constructor(props) {
    super(props);
    this.state = {
      loading:false
    };
  }
  ToogleLoading(){
    this.setState({
      loading:!this.state.loading
    })
  }
  componentDidMount() {
    const {page} = this.props.match.params;
    const {data} = this.props;

    var PageString = page.replace('page=', '');

    if (data && !data.data) {
      this.props.reciveEmail(Number(PageString)-1,()=>this.ToogleLoading());
    }
    document.title = "受信メッセージ｜KAKUSEIDA";
  }


  handlePageClick(data){
    this.props.reciveEmail(Number(data),()=>this.ToogleLoading());
  }
  goToReply(data){
    this.props.goToReply(data);
  }

  goToRead(data){
    this.props.goToRead(data)
  }


  render() {
    const {data,pageLoad} = this.props;
    const {loading} = this.state;

    if (loading) {
      return(
        <Loading></Loading>
      )
    }

    return (
      <div className="area cover-send-mail">
        <Title title={'受信メッセージ'}></Title>
        {/* Title */}

        <div className="wraper">
          <div className="cover-no-blank">



            <SiderbarMenu active={'emailBox'}/>
            {/* SiderBar */}

            <div className="right-content-mail">
              {


                <MailList
                  goToReply={(data)=>this.goToReply(data)}
                  goToRead={(data)=>this.goToRead(data)}
                  pageLoad={pageLoad}
                  handlePageClick={(page)=>this.handlePageClick(page)}
                  data={data.data} />
              }

              {/* SiderBar */}

            </div>

            {/* Mail List */}

            <div className="clear-fix"></div>
          </div>

        </div>
        {/* Wraper */}

      </div>
    );
  }
}

const mapStateToProps = (state) => {
    return {
      data:state.MailBox.reciveEmail,
      pageLoad:state.MailBox.reciveEmail.page,
    }
}
export default connect(mapStateToProps, actions )(MailBox);
