/* @flow */

import React, { Component } from 'react';

import { Link } from 'react-router-dom';
import FormatFunc from '../common/FormatFunc';

export default class MyComponent extends Component {



  render() {
    const {data} = this.props;


    return (
      <ul className="personal-panel-width personal-panel">
        {
          data.map( (item , i) =>  {
            return(
              <li key={i}>
                <Link className="notification" to={`/my-page/admin-notification/${item.id}`} style={{background: "#fff"}}>
                  <div className="notification-text notification-text2" >
                    {
                  
                      <FormatFunc date={item.created}></FormatFunc>
                    }

                  </div>
                    <div className="notification-text">
                      {
                        item.title
                      }
                    </div>

                </Link>
              </li>
            )
          })
        }
      </ul>
    );
  }
}
