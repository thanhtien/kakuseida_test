/* @flow */

import React, { Component } from 'react';
import { connect } from 'react-redux'
import { reduxForm  , Field } from 'redux-form'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import {faExclamationCircle} from '@fortawesome/free-solid-svg-icons';
import * as actions from '../../../../actions/editSystem';
import AsideLeft from '../aside';
import FlashMassage from 'react-flash-message';

import renderFieldInput from '../../../common/renderFieldInput';
import validate from './validate';

import Loading from '../../../common/loading';


class TableEdit extends Component {

  //Submit
  handleFormSubmit(data) {
    this.props.SettingATM(data);
  }

  _renderStatusAPI() {
    setTimeout( () => {
      this.props.Dis_Status_Api(false);
    }, 2000);
    return(
      <div id="push-notification">
        <FlashMassage duration={2000}>
          <div className="alert">
            成功！変更が保存されました。
          </div>
        </FlashMassage>
      </div>
    )
  }

  //Render
  render() {

    const {handleSubmit,statusAPI} = this.props;
    if (this.props.loading) {
      return(
        <Loading></Loading>
      )
    }


    return (
      <div>
        <AsideLeft/>
        <div className="main">
          {
            statusAPI ? this._renderStatusAPI(): null
          }


          <h3>銀行口座情報</h3>
            <span className="check-it-out">
              <FontAwesomeIcon icon={faExclamationCircle} style={{marginRight: "6px"}}/>
                銀行口座の登録について <br/>
                ・プロジェクトオーナー様：プロジェクト終了時にカクセイだ運営事務所から支援金を振り込む際に必要になります。<br/>
                ・支援者の様：支援したプロジェクト不成立の場合、カクセイだ運営事務所から支援金を返金する際に必要となります。<br/>
            </span>
            <form onSubmit={handleSubmit(this.handleFormSubmit.bind(this))}>
              <table className="show my-table">
                <tbody>
                    <tr className="my-tr">
                        <th className="my-th">
                            <label className="string required">銀行名 <span className="must-icon"></span></label>
                        </th>
                        <td className="my-td">
                          <Field
                            name="bank_name"
                            component={renderFieldInput}
                            type="text"
                            placeholder="銀行名"
                          />
                        </td>
                    </tr>
                    <tr className="my-tr">
                        <th className="my-th">
                            <label className="string required">支店名 <span className="must-icon"></span></label>
                        </th>
                        <td className="my-td">
                          <Field
                            name="bank_branch"
                            component={renderFieldInput}
                            type="text"
                            placeholder="支店名"
                          />
                        </td>
                    </tr>
                    <tr className="my-tr">
                        <th className="my-th">
                            <label className="string required">口座種類 <span className="must-icon"></span></label>
                        </th>
                        <td className="my-td">
                          <Field
                            name="bank_type"
                            component={renderFieldInput}
                            type="text"
                            placeholder="口座種類"
                          />
                        </td>
                    </tr>
                    <tr className="my-tr">
                        <th className="my-th">
                            <label className="string required">口座番号 <span className="must-icon"></span></label>
                        </th>
                        <td className="my-td">
                          <Field
                            name="bank_number"
                            component={renderFieldInput}
                            type="text"
                            placeholder="口座番号"
                          />
                        </td>
                    </tr>
                    <tr className="my-tr">
                        <th className="my-th">
                            <label className="string required">口座カナ名義 <span className="must-icon"></span></label>
                        </th>
                        <td className="my-td">
                          <Field
                            name="bank_owner"
                            component={renderFieldInput}
                            type="text"
                            placeholder="口座カナ名義"
                          />
                        </td>
                    </tr>
                </tbody>
              </table>
              <div className="submit-box"><input type="submit" name="commit" value="変更" data-disable-with="変更"/></div>
            </form>
        </div>
      </div>
    );
  }
}







TableEdit = reduxForm({
  form: 'ATM',
  validate,
  destroyOnUnmount: false
})(TableEdit)
const mapStateToProps = (state) => {
    return {
      initialValues: state.common.profile,
      loading:state.common.loading,
      statusAPI:state.common.statusAPI
    }
}

// You have to connect() to any reducers that you wish to connect to yourself
TableEdit = connect(mapStateToProps, actions )(TableEdit);



export default TableEdit
