/* @flow */
import React, { Component } from 'react';
import NavProfile from '../../nav';
import HeaderProfile from '../../header';
import TableEdit from './tableEditAddress';
import { connect } from 'react-redux'
import Loading from '../../../common/loading';
import List from './list';
import Add from './add';
class AddressReturn extends Component {

  constructor(props) {
    super(props);
    this.state = {
      statusList:false,
      statusAdd:false,
    };
  }
  componentDidMount() {
    document.title = "お届け先情報｜KAKUSEIDA";
  }

  /**
   * [openList Open List Address]
   * @return {[type]} [Open List]
   */

  openList() {
    this.setState({
      statusList:true
    })
  }
  /**
   * [closeList Close List Address]
   * @return {[type]} [description]
   */
  closeList() {
    this.setState({
      statusList:false
    })
    this.refs.childList.getWrappedInstance().closeForm()
  }

  /**
   * [openList Open Add Address]
   * @return {[type]} [Open List]
   */

  openAdd() {
    this.setState({
      statusAdd:true
    })
  }
  /**
   * [closeList Close Add Address]
   * @return {[type]} [description]
   */
  closeAdd() {
    this.setState({
      statusAdd:false
    })
  }
  render() {
    const {profile} = this.props;
    if (!profile) {
      return <Loading></Loading>
    }

    return (
      <div className="area my-page">
        <div className="wraper">
          <HeaderProfile></HeaderProfile>
          {/* Profile Detail */}
          <div className="nav-here">
            {
              <NavProfile patron={profile.patron} patronfanclub={profile.patronfanclub} numberProject={profile.numberProject}/>
            }
          </div>
          {/* NAV Detail */}
          <div className="clear-fix"></div>
        </div>
        <div className="white-page">
          <div className="cover-no-blank">

            <div className="wraper">

              {
                <TableEdit
                  addressList = {profile.listAddress}
                   openList={() => this.openList()} closeList={() => this.closeList()}
                   openAdd={() => this.openAdd()} closeAdd={() => this.closeAdd()}
                />
              }




              <div className="clear-fix"></div>
            </div>
          </div>
        </div>


          <List
            ref={'childList'}
            data={profile.listAddress}
            open={() => this.openList()}
            close={() => this.closeList()}
            status={this.state.statusList}>
          </List>

        {/* List Address */}

        <Add
          open={() => this.openAdd()}
          close={() => this.closeAdd()}
          status={this.state.statusAdd}>
        </Add>
        {/* List Address */}
      </div>
    );
  }
}

const mapStateToProps = (state) => {
    return {
      profile:state.common.profile
    }
}
export default connect(mapStateToProps)(AddressReturn);
