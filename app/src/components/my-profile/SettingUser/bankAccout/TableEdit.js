/* @flow */

import React, { Component } from 'react';
import { connect } from 'react-redux'
import { reduxForm  , Field , reset } from 'redux-form'

import * as actions from '../../../../actions/editSystem';
import AsideLeft from '../aside';
import NumberFormat from './NumberFormat';
import renderFieldInput from '../../../common/renderFieldInput';
import CvcPop from '../../../common/cvcPopup';
import validate from './validate';
import Loading from '../../../common/loading';
import FlashMassage from 'react-flash-message';
import Rodal from 'rodal';

var CONFIG = require('../../../../config/common');

if (process.env.NODE_ENV === "development") {
  CONFIG = CONFIG.CONFIG.DEV;
}else {
  CONFIG = CONFIG.CONFIG.PRODUCT;
}

class TableBankEdit extends Component {


    constructor(props) {
      super(props);
      this.state = {
        bank_ref:false,
        showpass:false,
        reset:false
      };
    }

  /**
   * [handleFormSubmit Submit Form Create Project]
   * @param  {[type]} data [Data TO Server]
   */
  handleFormSubmit(data) {

    if (!this.props.errorMonthYear) {
      const {SettingViSaDonate} = this.props;
      SettingViSaDonate(data);
    }
    this.setState({
      removeCredit:false
    })
  }

  /**
   * [showpass Show hide CVC]
   * @return {[type]} [CVC HIDE/SHOW]
   */
   showpass(e) {
     e.stopPropagation();
     this.setState({
       showpass:!this.state.showpass
     })
   }

  _renderStatusAPI() {
    const {removeCredit} = this.state;
    setTimeout( () => {
      this.props.Dis_Status_Api(false);
    }, 2000);
    return(
      <div id="push-notification">
        <FlashMassage duration={2000}>
          <div className="alert">
            {
              removeCredit ? "クレジットカード情報が削除されました" : "成功！変更が保存されました。"
            }
          </div>
        </FlashMassage>
      </div>
    )
  }

  showPopupBank() {
    this.setState({
      bank_ref:true
    })
  }
  hideModal() {
    this.setState({
      bank_ref:false
    })
  }

  removeClient() {
    this.setState({
      reset:true
    })
    this.props.dispatch(reset('Bank-accounts'));

  }

  removeCredit(){

    this.setState({
      removeCredit:true
    })

    this.props.removeCredit( () => this.removeClient() );
  }


  _renderButton(data){

    if (this.state.removeCredit === false) {
      return(<span onClick={ ()=>this.removeCredit() } className="btn-preview delete-btn-cvc">削除する</span>)
    }
    if (data === '') {
      return (
        <span className="btn-preview delete-btn-cvc disable">削除する</span>
      );
    }else {
      return(

           ( this.state.removeCredit !== false && this.state.removeCredit  )  ?
          <span className="btn-preview delete-btn-cvc disable">削除する</span> :
            <span onClick={ ()=>this.removeCredit() } className="btn-preview delete-btn-cvc">削除する</span>

      )
    }

  }

  render() {
    const {handleSubmit ,initialValues,errorMonthYear,statusAPI} = this.props;
    const {bank_ref,showpass} = this.state;

    if (this.props.loading) {
      return(
        <Loading></Loading>
      )
    }


    if (initialValues) {

      if ( initialValues.number_card !== "" ) {
        if (initialValues.number_card.indexOf('*') === -1) {
          initialValues.number_card = "************ "+initialValues.number_card;
        }
      }


    }


    return (
      <div>
        <AsideLeft/>
        <div className="main">
          {
            statusAPI ? this._renderStatusAPI(): <div id="push-notification"></div>
          }
          <h3>クレジットカード情報</h3>
            <span className="check-it-out">
              どの会社のクレジットカードが登録可能か<span onClick={() => this.showPopupBank()} className="show-popup-bank" >こちら</span>でご覧ください。
            </span>
            <Rodal width={500} height={300} visible={bank_ref} onClose={this.hideModal.bind(this)}>
              <div style={{display:'block'}} className="page-content">
                <div style={{padding:0}} className="pannel-white">
                    <h4  className="pannel-title">登録可能のクレジットカード</h4>
                    <div className="brand-list">
                        <ul>
                            <li><span ><img src={CONFIG.MAIN_URL+"/img/project-detail/brand-1.png"} alt=""/></span></li>
                            <li><span ><img src={CONFIG.MAIN_URL+"/img/project-detail/brand-2.png"} alt=""/></span></li>
                            <li><span ><img src={CONFIG.MAIN_URL+"/img/project-detail/brand-3.png"} alt=""/></span></li>
                            <li><span ><img src={CONFIG.MAIN_URL+"/img/project-detail/brand-5.png"} alt=""/></span></li>
                        </ul>
                    </div>
                </div>

              </div>

            </Rodal>
            <form onSubmit={handleSubmit(this.handleFormSubmit.bind(this))}>
              <table className="show my-table">
                <tbody>

                    <tr className="my-tr">
                        <th className="my-th">
                            <label className="string required">カード番号 <span className="must-icon"></span></label>
                        </th>
                        <td className="my-td">
                          <Field
                            name="number_card"
                            component={renderFieldInput}
                            type="text"
                            placeholder="4242-4242-4242-4242"

                          />
                        </td>
                    </tr>

                    <tr className="my-tr">
                        <th className="my-th">
                            <label className="string required">有効期限<span className="must-icon"></span></label>
                        </th>
                        <td className="my-td">
                          <div className="moth-year">
                            <Field
                              name="exp_month"
                              component={renderFieldInput}
                              type="text"
                              placeholder="月"
                              maxlength={2}
                              normalize={NumberFormat}
                            />

                            <Field
                              name="exp_year"
                              component={renderFieldInput}
                              type="text"
                              placeholder="年"
                              maxlength={4}
                              normalize={NumberFormat}
                            />
                            <span className="month-year-text">
                              mm/yy
                            </span>


                          </div>

                          <span className="text-danger" style={{color: "red"}}>
                            {
                              errorMonthYear ? errorMonthYear : null
                            }
                          </span>


                        </td>



                    </tr>

                    <tr className="my-tr ">
                        <th className="my-th">
                            <label className="string required">セキュリティーコード<span className="must-icon"></span></label>
                        </th>
                        <td className="my-td" >
                          <div style={{position: 'relative'}}>

                            <Field
                              name="cvc"
                              component={renderFieldInput}
                              type={ showpass ? 'text' : 'password' }
                              placeholder="（例）123"
                            />
                            <CvcPop></CvcPop>



                          <span style={{right: '15px'}} className='show-pass' onClick={ (e) => this.showpass(e) }>

                              {
                                showpass ?
                                <i className="icss-anim icss-eye"></i> :
                                <i className="icss-anim icss-eye-slash"></i>
                              }

                            </span>

                          </div>

                        </td>

                    </tr>


                </tbody>
              </table>
              <div className="submit-box submit-box-create-project">
                <input type="submit" name="commit" value="保存する" data-disable-with="変更"/>

                {
                  initialValues?
                  this._renderButton(initialValues.number_card):null
                }

              </div>

            </form>
        </div>
      </div>
    );
  }
}







TableBankEdit = reduxForm({
  form: 'Bank-accounts',
  validate,
  destroyOnUnmount: false
})(TableBankEdit)
const mapStateToProps = (state) => {

  return {
    initialValues: state.common.profile,
    errorMonthYear:state.common.errorMonthYear,
    loading:state.common.loading,
    statusAPI:state.common.statusAPI
  }
}

// You have to connect() to any reducers that you wish to connect to yourself
TableBankEdit = connect(mapStateToProps, actions )(TableBankEdit);



export default TableBankEdit
