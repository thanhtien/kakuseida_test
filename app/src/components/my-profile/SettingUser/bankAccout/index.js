/* @flow */
import React, { Component } from 'react';
import NavProfile from '../../nav';
import HeaderProfile from '../../header';
import TableEdit from './TableEdit';
import { connect } from 'react-redux'
import Loading from '../../../common/loading';

class BankAccount extends Component {
  componentDidMount() {

    document.title = "クレジットカード情報｜KAKUSEIDA";

  }
  render() {
    const {profile} = this.props;

    return (
      <div className="area my-page">
        <div className="wraper">
          <HeaderProfile></HeaderProfile>
          {/* Profile Detail */}
          <div className="nav-here">
            {
              profile ? <NavProfile patron={profile.patron} patronfanclub={profile.patronfanclub} numberProject={profile.numberProject}/> : <Loading></Loading>
            }
          </div>
          {/* NAV Detail */}
          <div className="clear-fix"></div>
        </div>
        <div className="white-page">
          <div className="cover-no-blank">

            <div className="wraper">
              <TableEdit></TableEdit>
              <div className="clear-fix"></div>
            </div>
          </div>
        </div>

      </div>
    );
  }
}

const mapStateToProps = (state) => {
    return {
      profile:state.common.profile
    }
}
export default connect(mapStateToProps)(BankAccount);
