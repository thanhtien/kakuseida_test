/* @flow */

import React, { Component } from 'react';
import { connect } from 'react-redux'
import { Field, reduxForm  , reset  } from 'redux-form'
import * as actions from '../../../../actions/editSystem';
import AsideLeft from '../aside';
import Loading from '../../../common/loading';
import renderFieldInput from '../../../common/renderFieldInput';
import 'react-draft-wysiwyg/dist/react-draft-wysiwyg.css';
import FlashMassage from 'react-flash-message';


class AddressReturnEdit extends Component {


  constructor(props) {
    super(props);
    this.state = {
      loadingGoogle:false
    };
  }

  handleFormSubmit(data){
    this.props.ChangePassword(data);
  }

  _renderStatusAPI() {
    setTimeout( () => {
      this.props.Dis_Status_Api(false);
    }, 2000);
    return(
      <div id="push-notification">
        <FlashMassage duration={2000}>
          <div className="alert">
            成功！変更が保存されました。
          </div>
        </FlashMassage>
      </div>
    )
  }



  render() {
    const { handleSubmit , statusAPI } = this.props;
    if (this.props.loading) {
      return(
        <Loading></Loading>
      )
    }



    return (
      <div>
        <AsideLeft/>
        <div className="main">
          {
            statusAPI ? this._renderStatusAPI(): null
          }
          <h3>パスワード変更</h3>
          <span className="check-it-out">

          </span>

          <form onSubmit={handleSubmit(this.handleFormSubmit.bind(this))}>

            <table className="show my-table">
              <tbody>
                  <tr className="my-tr">
                      <th className="my-th">
                          <label className="string required">新しいパスワード</label>
                      </th>
                      <td className="my-td">
                        <Field
                          name="password"
                          component={renderFieldInput}
                          type="password"
                          placeholder=""
                        />
                      </td>
                  </tr>
                  <tr className="my-tr">
                      <th className="my-th">
                          <label className="string required">新しいパスワード（確認）</label>
                      </th>
                      <td className="my-td">
                        <Field
                          name="re_password"
                          component={renderFieldInput}
                          type="password"
                          placeholder="上と同じパスワードを入力してください"
                        />
                      </td>
                  </tr>

              </tbody>
            </table>

          <div className="submit-box">
            <input disabled={this.state.loadingGoogle} type="submit" name="commit" value="変更" data-disable-with="変更"/>
          </div>
        </form>
        </div>

      </div>
    );
  }
}


const validate = values => {
    const errors = {};

    if (!values.password) {
      errors.password = "新しいパスワードを入力してください" ;
    }else {

      if (values.password.length < 6) {
        errors.password = "新しいパスワード（英数字6文字以上）" ;
      }
    }
    if (!values.re_password) {
      errors.re_password = "新しいパスワード（確認）を入力してください" ;
    }

    if (values.password && values.re_password) {
      if (values.password !== values.re_password) {
        errors.re_password = "パスワード(確認)とパスワードの入力が一致しません";
      }
    }




    return errors;
};

const afterSubmit = (result, dispatch) => {
  return (
    dispatch(reset('change-password'))
  )
};

AddressReturnEdit = reduxForm({
  form: 'change-password',
  validate,
  onSubmitSuccess:afterSubmit,
  destroyOnUnmount: false
})(AddressReturnEdit)
const mapStateToProps = (state) => {
  return {
    loading:state.common.loading,
    statusAPI:state.common.statusAPI
  }
}

// You have to connect() to any reducers that you wish to connect to yourself
AddressReturnEdit = connect(mapStateToProps, actions )(AddressReturnEdit);



export default AddressReturnEdit
