function centsToDollaString(x){
  return x.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,");
}
const NumberFormat = value => {
  var ValueFormat = value.replace(/[^0-9]+/g, "");
  
  return centsToDollaString(ValueFormat)
}

export default NumberFormat
