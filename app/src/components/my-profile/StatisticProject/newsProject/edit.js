/* @flow */
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Field, reduxForm  , reset , getFormValues } from 'redux-form'
import * as actions from '../../../../actions/editSystem';

import Loading from '../../../common/loading';
import EditorField from '../../../common/EditorField';
import renderFieldInput from '../../../common/renderFieldInput';
import RenderDropzone from '../../../common/renderDropzone';
import PreviewProject from './preview';
import validate from './validate';
import Rodal from 'rodal';
import scrollToFirstError from './scrollToFirstError';
import scrollToFirstErrorPreview from './scrollToFirstPreview';
import Breadcrumbs from '../../../Breadcrumbs';



class EditReport extends Component {
  constructor(props) {
    super(props)
    this.state = {
      dataPreview:null,
      tooglePreview:false
    }

  }

  componentDidMount() {
    document.title = "報告編集｜KAKUSEIDA";
    this.props.InitialDataReportNew(this.props.match.params.new_id);
  }

  /**
   * [handleFormSubmit Submit Form Create Project]
   * @param  {[type]} data [Data TO Server]
   */

  handleFormSubmit(data){
    data.id = this.props.match.params.new_id;
    this.props.editNewProject(data,this.props.match.params.id);

  }
  handleFormSubmitPublic(){
    var check = scrollToFirstErrorPreview(validate(this.props.dataPreview),this.props.dispatch);
    if (check !== true) {
      var data = this.props.dataPreview;
      data.status = 1;
       data.project_id = this.props.match.params.id;
      this.props.editNewProject(data,this.props.match.params.id);
    }

  }

  /**
   * [preview Generate HTML AND DATA FOR PREVIEW PROJECT]
   * @return {[type]} [LAYOUT PROJECT]
   */
  preview(){
    //check Validate Form same as submit and Scrollto error feild
    var check = scrollToFirstErrorPreview(validate(this.props.dataPreview),this.props.dispatch);
    if (check !== true) {
      this.setState({
        dataPreview:this.props.dataPreview,
        tooglePreview:true
      })
      var content = document.getElementById("content");
      content.className = 'full-content-popup';
      document.body.className = 'block-body';
    }
  }

  previewClose(){
    this.setState({
      tooglePreview:false
    })
    var content = document.getElementById("content");
    content.className = '';
    document.body.className = '';
  }

  render() {
    const { handleSubmit,initialValues} = this.props;
    const {id} = this.props.match.params;

    var linkBreacrm = [
      {last:false,label:'最新の活動報告',link:`/my-page/statistic/${id}`},
      {last:true,label:'報告編集',link:null}
    ]
    if (this.props.loading) {
      return(
        <Loading></Loading>
      )
    }
    return (
      <div className="area my-page">
        <div className="wraper">
          <div className="cover-Breadcrumbs project-detail-bread">
            <Breadcrumbs linkBreacrm={linkBreacrm}></Breadcrumbs>
          </div>
          <div className="white-page">
            <div className="wraper">
              <div className="cover-no-blank new-project-here">
                <div className="wraper" style={{'overflow': 'hidden'}}>
                  {
                    initialValues ?
                    <div className="main main-write">
                      <form onSubmit={handleSubmit(this.handleFormSubmit.bind(this))}>
                        <h3 className="h3">報告編集</h3>
                        <div className="cover-table active">
                          <table className={"my-table show"} >
                            <tbody>
                              <tr className="my-tr">
                                  <th className="my-th">
                                      <label className="string required">タイトル <span className="must-icon"></span></label>
                                  </th>
                                  <td className="my-td" colSpan="3">

                                    <Field
                                      name="title"
                                      component={renderFieldInput}
                                      type="text"
                                      placeholder="タイトル"
                                      nameError="title"
                                      maxlength={150}
                                    />
                                  <span className="warning">
                                    ※最大150文字
                                  </span>

                                  </td>
                              </tr>
                              <tr className="my-tr">
                                  <th className="my-th">
                                    <label className="file optional">
                                      画像（サムネイル表示）
                                    </label>
                                  </th>
                                  <td className="my-td" colSpan="3">
                                    <div className="upload-box just-one">
                                        <div className="input-file-outer">
                                          <div className="dropzone">
                                            
                                            {/*DropZone*/}
                                              <RenderDropzone
                                                imagePreview={initialValues.thumbnail}
                                                path="thumbnail"
                                                formName="edit_report_project"
                                                feild="thumbnail">
                                              </RenderDropzone>
                                              <p className="note">アップロードできる画像形式は <span style={{color: 'red'}}>*.jpeg と *.png</span>のみです</p>

                                            {/*DropZone*/}
                                            <Field style={{width:"100%"}} name="thumbnail" component="input" type="hidden" />

                                          </div>
                                        </div>
                                    </div>
                                    <span className="warning">
                                      {"※画像のサイズは600px＊400pxで最高です。容量 2MBまで 。"}
                                    </span>
                                    <span className="warning">※サムネイル表示</span>
                                  </td>
                              </tr>
                              <tr className="my-tr">
                                  <th className="my-th">
                                      <label className="integer optional" >コンテンツ </label>
                                  </th>
                                  <td className="my-td" colSpan="3">
                                    <div>
                                      <EditorField
                                          key="field"
                                          name="content"
                                          id="inputEditorText"
                                          disabled={false}
                                          placeholder="Type here"
                                          DefaultProjectContent={initialValues.content}
                                        />
                                    </div>
                                  </td>
                              </tr>
                          </tbody>
                          </table>
                        </div>
                        {/*TAB 1*/}
                        <div className="submit-box submit-box-create-project">
                          <input type="submit" name="commit" value="保存" data-disable-with="保存する"/>
                          <span className='btn-preview' onClick={()=> this.handleFormSubmitPublic()} >公開</span>
                          <span className='btn-preview' onClick={()=> this.preview()} >プレビュー</span>
                        </div>
                      </form>
                    </div> : null
                  }

                </div>
              </div>
            </div>
          </div>
        </div>
        <Rodal width={95} height={90} measure={'%'} visible={this.state.tooglePreview} onClose={this.previewClose.bind(this)}>
          <div className="scroll-modal">
            {
              this.state.tooglePreview ? <PreviewProject new_detail={this.state.dataPreview}/> : null
            }
          </div>
        </Rodal>
      </div>
    );
  }
}


const afterSubmit = (result, dispatch) => {
  return (
    dispatch(reset('edit_report_project'))
  )
};

EditReport = reduxForm({
  form: 'edit_report_project',
  destroyOnUnmount: false,
  onSubmitFail: (errors , dispatch) => scrollToFirstError(errors , dispatch),
  onSubmitSuccess:afterSubmit,
  validate

})(EditReport);

const mapStateToProps = (state) => {
  const dataPreview = getFormValues('edit_report_project')(state);
  return {
    dataPreview:dataPreview,
    data: state.uploadImage.data,
    loading:state.common.loading,
    initialValues:state.ProjectDetail.dataEdit



  }
}

// You have to connect() to any reducers that you wish to connect to yourself
EditReport = connect(mapStateToProps, actions)(EditReport);

export default EditReport
