/* @flow */

import React, { Component } from 'react';
import Banner from './banner';


export default class NewDetail extends Component {

  htmlReturn(data) {
    return{
      __html:data
    }
  }

  render() {
    const {new_detail} = this.props;


    if (new_detail) {
      var d = new Date();
      var date = d.getDate();
      var month = d.getMonth() + 1;
      var year = d.getFullYear();
      var time =  year.toString()+'年'+month.toString()+'月'+date.toString()+'日';

    }

    return (
      <div className="about_box bg">
        <div>
          <Banner></Banner>
          {/* .Banner */}

          <div style={{display:'block'}} className="pd-area">



            <div style={{background:'#fff'}} className="wraper padding">
              <div className="title-green">
                <p className="green-time">{time}</p>
                <p className="title-page">{new_detail.title}</p>

              </div>
              <p className="des-thumnail">

              </p>

              {
                new_detail.content ?
                <div className="div-content" dangerouslySetInnerHTML={this.htmlReturn(new_detail.content)} ></div> : <p style={{textAlign:'center', border: "3px dotted #0071BA",padding:"10px"}}>コンテンツがまだありません。</p>
              }
              {/* .WYSIWYG */}
              <div className="button-bottom">
                <button className="button prev">前へ</button>
                <span className="back-button-list">一覧へ</span>
              </div>
            </div>

          </div>
        </div>

      </div>
    );
  }
}
