/* @flow */

import React, { Component } from 'react';
import NumberFormat from 'react-number-format';
import InforDonor from './InforDonor';

import LinesEllipsis from 'react-lines-ellipsis';

export default class ProjectReturn extends Component {

  render() {
    const {data,UserType,id_donate,onClose,sendPackage} = this.props;

    return (

        <div className="pd-area" style={{display:'block',width:'100%'}}>

          <div className="return-list">
            <ul>
              <li>
                  <div className="return-list-img"><span><img src={data.thumnail} alt=""/></span></div>
                  <div className="return-list-content">
                    {
                      data.name ?
                      <h1 className="name-return" title={data.name}>

                        <pre>
                          <LinesEllipsis
                            text={data.name}
                            maxLine={2}
                            ellipsis={<span style={{color:'#147efb',cursor:'pointer'}}>...</span>}
                            trimRight
                            basedOn='letters'
                          />
                        </pre>
                      </h1> : <h1 className="name-return" title={`リターン品 ${id_donate}`}>リターン品{id_donate}</h1>
                    }
                      <div className="return-list-content-number">

                          {
                            this.props.number_month ?
                            <NumberFormat value={data.invest_amount} displayType={'text'} thousandSeparator={true} suffix={`円/${this.props.number_month}月`} /> :
                            <NumberFormat value={data.invest_amount} displayType={'text'} thousandSeparator={true} suffix={'円'} />
                          }
                      </div>
                      <div className="return-list-text" style={{whiteSpace:"pre-line"}}>{data.return_amount}</div>
                      {
                        data.schedule ?
                        <div className="return-list-status">
                          <p className="return-list-s return-list-s-1">支援者数：　<span>{data.now_count}人</span></p>
                            {data.schedule ? <p className="return-list-s return-list-s-2">
                              お届け予定：<span>{data.schedule}</span>
                            </p> :  null}

                        </div> : null
                      }

                  </div>




              </li>
            </ul>
          </div>
          {
            /**
             * Infor Donor Layout
             */
            <InforDonor
              sendPackage={sendPackage}
              onClose={onClose}
              id_donate={id_donate}
              UserType={UserType}
              data={data}>
            </InforDonor>
          }



      </div>


    );
  }
}
