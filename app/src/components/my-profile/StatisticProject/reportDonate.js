/* @flow */

import React, { Component } from 'react';
import { Line } from 'react-chartjs-2';
import LoadingScroll from '../../common/loadingScroll';
import RenderDateTimePicker from './RenderDateTimePicker';

export default class ReportDonate extends Component {

  constructor(props) {
    super(props);
    this.state = {
      start:0,
      end:0
    };
  }

  onchangeStart(data) {
    this.setState({
      start:data
    })
  }

  onchangeEnd(data) {
    this.setState({
      end:data
    })
  }

  returnTime(time) {
    var month = time.getMonth() + 1;
    var date = time.getDate();
    if (month < 10) {
      month = '0' + month;
    }
    if (date < 10) {
      date = '0' + date;
    }
    var timeReturn = `${time.getFullYear()}-${month}-${date}`;
    return timeReturn;
  }

  MoreReport() {
    if (this.state.start !== 0 && this.state.end !== 0) {
      const {projectID , setLoadingChartDonate} = this.props;
      this.props.getDonateChart(projectID,setLoadingChartDonate,this.returnTime(this.state.start),this.returnTime(this.state.end));
    }else {
      alert("日付を選択してください");
    }

  }

  render() {
    const {donateChart,loadingChartDonate} = this.props;

    var  data = null;

    if (donateChart) {
       data = {
        labels: donateChart.labels,
        datasets: [
          {
            label: '日次入金額',
            fill: false,
            lineTension: 0.5,
            backgroundColor: '#fff',
            borderColor: '#0071BA',
            borderCapStyle: 'butt',
            borderDash: [],
            borderDashOffset: 0.0,
            borderJoinStyle: 'miter',
            pointBorderColor: '#fd8b02',
            pointBackgroundColor: '#fff',
            pointBorderWidth: 1,
            pointHoverRadius: 5,
            pointHoverBackgroundColor: 'rgba(75,192,192,1)',
            pointHoverBorderColor: 'rgba(220,220,220,1)',
            pointHoverBorderWidth: 4,
            pointRadius: 3,
            pointHitRadius: 50,
            data: donateChart.data
          }
        ]
      };
    }else {
      data = {
       labels: [0],
       datasets: [
         {
           label: '日次入金額',
           fill: false,
           lineTension: 0.5,
           backgroundColor: '#fff',
           borderColor: '#0071BA',
           borderCapStyle: 'butt',
           borderDash: [],
           borderDashOffset: 0.0,
           borderJoinStyle: 'miter',
           pointBorderColor: '#fd8b02',
           pointBackgroundColor: '#fff',
           pointBorderWidth: 1,
           pointHoverRadius: 5,
           pointHoverBackgroundColor: 'rgba(75,192,192,1)',
           pointHoverBorderColor: 'rgba(220,220,220,1)',
           pointHoverBorderWidth: 4,
           pointRadius: 3,
           pointHitRadius: 50,
           data: [0]
         }
       ]
     };
    }

    return (

      <div className="wraper" >
          <div className="cover-title-and-filter">
            <h2 className="title-report">日次入金額</h2>
            <div className="filter-tool">
              <div className="time-input">
                <RenderDateTimePicker onChange={ (data) => this.onchangeStart(data)  } value={this.state.start} />
              </div>
              <div className="time-input">
                <RenderDateTimePicker onChange={ (data) => this.onchangeEnd(data) } value={this.state.end} />
              </div>
              <div className="time-input">
                <button className="button-report" onClick={ () => this.MoreReport() }>検索</button>
              </div>


            </div>
          </div>
        <div className="loading-chart" style={ loadingChartDonate ? {background:"rgba(255,255,255,0.7)"} : {display:"none"} }>
          {
            loadingChartDonate ? <LoadingScroll></LoadingScroll> : null
          }
        </div>
        {
          data ? <Line ref="chart" data={data}/> : null
        }
      </div>
    );
  }
}
