/**
 * [validate For Validate Form New Project]
 * @param  {[type]} values [values Form]
 * @return {[type]} Error  [Error For Validate match]
 */

 function validateUrl(value) {
   return /https?:\/\/(?:(youtu|y2u)\.be\/|(?:[a-z]{2,3}\.)?youtube\.com\/watch(?:\?|#\t)v=)([\w-]{11}).*/ig.test(value);
 }

const validate = values => {
    const errors = {};

    if (!values.project_name) {
      errors.project_name = 'プロジェクト名を入力してください。';
    }



    if (!values.goal_amount) {
      errors.goal_amount = '目標金額を入力してください';

    }

    if (values.goal_amount) {

      if( Number(values.goal_amount.replace(/,/g, "")) < 10000) {
        errors.goal_amount = '目標金額は、10,000円以上の金額で入力してください。';
      }
    }




    if (values.thumbnail_movie_code) {
      if (!validateUrl(values.thumbnail_movie_code)) {
        errors.thumbnail_movie_code = '動画のURLを入力してください';
      }
    }

    if (!values.category_id) {
      errors.category_id = 'カテゴリを選択してください';
    }

    if (!values.collection_end_date) {
      errors.collection_end_date = '募集終了日 を記入してください';
    }



    const project_returnErrors = []
    if (values.project_return) {
      values.project_return.forEach((projectReturn, memberIndex) => {
        const memberErrors = {}
        if (!projectReturn || !projectReturn.invest_amount) {
          memberErrors.invest_amount = '支援額を入力してください';
          project_returnErrors[memberIndex] = memberErrors;
        }
        if (!projectReturn || !projectReturn.name ) {
          memberErrors.name = 'リターン品の名前を入力してください';
          project_returnErrors[memberIndex] = memberErrors;
        }
      })
      if (project_returnErrors.length) {
        errors.project_return = project_returnErrors
      }
    }





    return errors;
};

export default validate;
