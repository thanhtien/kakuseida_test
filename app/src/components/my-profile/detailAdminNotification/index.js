/* @flow */

import React, { Component } from 'react';
import Banner from './banner';
import * as actions from '../../../actions/editSystem';
import { connect } from 'react-redux';
import Loading from '../../common/loading';
import Social from './social';
import Breadcrumbs from '../../Breadcrumbs';
import FormatFunc from '../../common/FormatFunc';

var CONFIG = require('../../../config/common');
if (process.env.NODE_ENV === "development") {
  CONFIG = CONFIG.CONFIG.DEV;
}else {
  CONFIG = CONFIG.CONFIG.PRODUCT;
}

class NewDetail extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loading:false
    };
  }
  componentDidMount() {
    const {id} = this.props.match.params;
    this.props.notificationListAdminHeaderDetail(id,()=>this._setLoading());
  }
  htmlReturn(data) {
    return{
      __html:data
    }
  }
  backTo() {
    this.props.history.goBack();
  }
  _setLoading(){
    this.setState({
      loading:!this.state.loading
    })
  }
  render() {
    const {new_detail} = this.props;
    const {id} = this.props.match.params;
    const {loading} = this.state;

    const shareLink = CONFIG.MAIN_URL+'new-detail/'+id;
    var linkBreacrm;

    if (new_detail) {


      var label;


      document.title = `${new_detail.title}｜KAKUSEIDA`;

      if (new_detail.title > 10) {
        label = new_detail.title.substring(0,10) + '...';
      }else {
        label = new_detail.title.substring(0,10);

      }
      linkBreacrm = [
        {last:true,label:label,link:null}
      ]
    }

    if (loading) {
      return(<Loading></Loading>)
    }


    return (
      <div className="about_box bg">
      {
        new_detail ?
        <div>
          <Banner></Banner>
          {/* .Banner */}
          <div className="wraper"><div className="cover-Breadcrumbs"><Breadcrumbs linkBreacrm={linkBreacrm} ></Breadcrumbs></div></div>

          <div style={{display:'block'}} className="pd-area">



            <div style={{background:'#fff'}} className="wraper padding">
              <Social shareLink={shareLink}></Social>
              <div className="title-green">
                <p className="green-time">{<FormatFunc date={new_detail.created}></FormatFunc>}</p>
                <p className="title-page">{new_detail.title}</p>

              </div>
              <p className="des-thumnail">

              </p>

              {
                new_detail.message ?
                <div className="div-content" dangerouslySetInnerHTML={this.htmlReturn(new_detail.message)} ></div> : <p style={{textAlign:'center', border: "3px dotted #0071BA",padding:"10px"}}>コンテンツがまだありません。</p>
              }
              {/* .WYSIWYG */}
              <div className="button-bottom">

                <button onClick={ () => this.backTo() } className="button prev">前へ</button>

              </div>
            </div>

          </div>
        </div> : <Loading></Loading>
      }

      </div>
    );
  }
}
const mapStateToProps = (state) => {
  return {
    new_detail: state.common.notificationListAdminHeaderDetail

  }
}

export default connect(mapStateToProps, actions)(NewDetail);
