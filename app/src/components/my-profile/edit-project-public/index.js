/* @flow */

import React, { Component } from 'react';
import HeaderProfile from '../header';
import { connect } from 'react-redux'
import * as actions from '../../../actions/editSystem';
import Breadcrumbs from '../../Breadcrumbs';
import TableEdit from './table';
import Loading from '../../common/loading';


class EditPublic extends Component {

  componentDidMount() {
    const {id} = this.props.match.params;
    const {project_default_edit} = this.props;
    this.props.ListEditPublic(id,0);
    this.props.getProjectDefaultEdit(id);
    if (project_default_edit) {
      console.log(project_default_edit.project_name);

      document.title = `「${project_default_edit.project_name}」プロジェクトの情報更新一覧｜KAKUSEIDA`;
    }

  }
  handlePageClick(data){
    const {id} = this.props.match.params;
    let selected = data.selected;
    this.props.ListEditPublic(id,selected);
  };

  render() {
    if (this.props.loading) {
      return(
        <Loading></Loading>
      )
    }

    const {id} = this.props.match.params;
    const {data,pageload,project_default_edit} = this.props;

    if (project_default_edit) {
      var linkBreacrm = [
        {last:false,label:"自分のプロジェクト",link:'/my-page/post-project/page=1'},
        {last:true,label:`「${project_default_edit.project_name}」プロジェクトの情報更新一覧`,link:null}
      ]
    }

    return (
      <div className="area my-page">
        <div className="wraper">
          <HeaderProfile ></HeaderProfile>
          {/* Profile Detail */}
          <div className="clear-fix"></div>
        </div>

        {/* Nav Tab */}
        <div className="wraper">
          <div className="cover-Breadcrumbs"><Breadcrumbs linkBreacrm={linkBreacrm}></Breadcrumbs></div>
          {/* Breadcrumbs */}
        </div>
        {/* Nav Tab */}

        <div className="white-page">
          <div className="cover-no-blank">
            <div className="wraper">
              {
                data && project_default_edit ?
                <TableEdit
                  project_default_edit={project_default_edit}
                  id={id}
                  pageload={pageload}
                  data={data}
                  handlePageClick={(data)=>this.handlePageClick(data)}
                ></TableEdit> : null
              }

              {/* Table */}

            </div>
          </div>
        </div>
        {/* White Page */}

      </div>
    );
  }
}






const mapStateToProps = (state) => {

  return {
    loading:state.common.loading,
    data:state.EditPublicProject.PublicEditProjectList.data,
    pageload:state.EditPublicProject.PublicEditProjectList.page,
    project_default_edit: state.common.project_default_edit,

  }
}

// You have to connect() to any reducers that you wish to connect to yourself
EditPublic = connect(mapStateToProps, actions , null , { withRef: true })(EditPublic);


export default EditPublic
