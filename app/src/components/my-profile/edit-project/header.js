/* @flow */

import React, { Component } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import {faBars} from '@fortawesome/free-solid-svg-icons';




 class HeaderNewProject extends Component {

  render() {
    const { typeProject } = this.props;
    return (
      <div className="cover-all-here">

        <h1 className="new-project-title">
          <i><FontAwesomeIcon icon={faBars} /></i>
          <span>{ typeProject === '0' ? '普通のプロジェクト' : '定期課金のプロジェクト' }編集画面</span>
          <strong>公開前</strong>
        </h1>
        <div className="clear-fix"></div>
      </div>
    );
  }
}


export default HeaderNewProject;
