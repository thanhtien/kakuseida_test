/* @flow */
import React, { Component } from 'react';
import Tab from '../componentCommon/tab';
import HeaderEditProject from './header';
import EditProjectForm from './EditProjectForm';
import { connect } from 'react-redux';
import Loading from '../../common/loading';
import * as actions from '../../../actions/editSystem';
import smoothScroll from '../../../components/common/smoothScroll';
import PreviewProject from '../project-preview';
import Rodal from 'rodal';
import Breadcrumbs from '../../Breadcrumbs';
import TypeProject from './typeProject';


class EditProject extends Component {

  constructor(props) {
    super(props);
    this.state = {
      preview_status:false,
      dataPreview:null,
      profile:null
    };
  }

  componentDidMount() {
    const {id} = this.props.match.params;
    this.props.getProjectDefaultEdit(id);
    document.title = "プロジェクト編集画面｜KAKUSEIDA";

  }


  previewClose(){
    this.setState({
      preview_status:false
    })
    var content = document.getElementById("content");
    content.className = '';
    document.body.className = '';

  }
  setDataPreview() {
    this.setState({
      preview_status:true,
    })
  }
  previewOpen(data,profile){
    this.setState({
      dataPreview:data,
      profile:profile
    }, () => {
      this.setDataPreview();
      var content = document.getElementById("content");
      content.className = 'full-content-popup';
      document.body.className = 'block-body';
    })

  }


  render() {
    const {data} = this.props;
    const {id} = this.props.match.params;
    if (data) {
      smoothScroll.scrollTo('header');
    }
    if (this.props.loading) {
      return(
        <Loading></Loading>
      )
    }

    var linkBreacrm = [
      {last:false,label:"自分のプロジェクト",link:'/my-page/post-project/page=1'},
      {last:true,label:"プロジェクト編集画面",link:null}
    ]



    return (

      <div className="area my-page">
        <div className="wraper">
          <div className="cover-Breadcrumbs"><Breadcrumbs linkBreacrm={linkBreacrm}></Breadcrumbs></div>
          {
            data ?
            <TypeProject typeProject={data.project_type} ></TypeProject>
            :null
          }

          {
            data ?
            <HeaderEditProject typeProject={data.project_type}/>
            : null
          }
          {/* Profile Detail */}

          {/* NAV Detail */}
          <div className="clear-fix"></div>
        </div>
        <div className="white-page">
          <div className="wraper">

            <div className="write-nav">
              <Tab></Tab>
            </div>
            <div className="cover-no-blank new-project-here">
              <div className="wraper">
                {
                  data ?
                  <EditProjectForm
                  idProject={id}
                  dataProject={data} previewOpen={(data,profile)=>this.previewOpen(data,profile)}/>:null
                }
                <div className="clear-fix"></div>
              </div>
            </div>
          </div>
        </div>
        <Rodal width={95} height={90} measure={'%'} visible={this.state.preview_status} onClose={this.previewClose.bind(this)}>
          <div className="scroll-modal">
            {
              this.state.preview_status ?
              <PreviewProject profile={this.state.profile} data={this.state.dataPreview}></PreviewProject> : null
            }
          </div>
        </Rodal>

      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    data: state.common.project_default_edit,
    loading:state.common.loading,

  }
}
export default connect(mapStateToProps, actions)(EditProject);
