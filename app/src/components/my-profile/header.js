/* @flow */
import React, { Component } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import {faTrophy} from '@fortawesome/free-solid-svg-icons';
import { connect } from 'react-redux';
import { Link  } from 'react-router-dom';
import * as actions from '../../actions/editSystem';
import Rodal from 'rodal';

import NotificationAdmin from './NotificationAdmin';
class headerProfile extends Component {
  constructor(props) {
    super(props);
    this.state = {
      disableButton:false,
      tooglePopup:false,
      statusUnsubscribe:false,
      unsubscribeProject:false

    }
  }
  disableButton() {
    this.setState({
      disableButton:!this.state.disableButton
    })
  }



  tooglePopupSettingEmail() {
    this.setState({
      tooglePopup:!this.state.tooglePopup
    })
  }

  hideModalAndConfirm(){
    const {subcriber} = this.props;
    var data = {
      "unsubscribeProject": null,
      "statusUnsubscribe":null
    }
    if (this.state.statusUnsubscribe) {
      data.statusUnsubscribe = 0
    }else {
      data.statusUnsubscribe = 1
    }
    if (this.state.unsubscribeProject) {
      data.unsubscribeProject = 0
    }else {
      data.unsubscribeProject = 1
    }

    this.props.changeSubcriber(()=> this.hideModal(),data,subcriber);

  }
  //Hide Modal And Set Data Default
  hideModal() {
    const {profile} = this.props;
    this.setState({
      tooglePopup:false
    })

    //Set Default Data Checkbox
    if (profile.statusUnsubscribe === '0') {
      this.setState({
        statusUnsubscribe:true,

      })
    }else {
      this.setState({
        statusUnsubscribe:false,
      })
    }

    if (profile.unsubscribeProject === '0') {
      this.setState({
        unsubscribeProject:true,

      })
    }else {
      this.setState({
        unsubscribeProject:false,
      })
    }
  }
  //Toogle Checkbox System
  mailSystem(e) {
    this.setState({
      statusUnsubscribe:!this.state.statusUnsubscribe
    })
  }
  //Toogle Checkbox Project
  mailProject(e) {
    this.setState({
      unsubscribeProject:!this.state.unsubscribeProject
    })
  }

  componentDidMount() {
    const {profile,notificationListAdminHeaderData} = this.props;
    //initial Data Checkbox
    if (!notificationListAdminHeaderData) {
      this.props.notificationListAdminHeader();
    }


    if (profile) {

      if (profile.statusUnsubscribe === '0') {
        this.setState({
          statusUnsubscribe:true,

        })
      }
      if (profile.unsubscribeProject === '0') {
        this.setState({
          unsubscribeProject:true,
        })
      }
    }
  }

  render() {
    const {profile,subcriber,notificationListAdminHeaderData} = this.props;
    const {tooglePopup,statusUnsubscribe,unsubscribeProject} = this.state;
    var widthModal;
    var heightModal;
    if (window.innerWidth <= 320) {
      widthModal = 300;
      heightModal= 320;
    }else {
      widthModal = 375;
      heightModal = 290;
    }


    return (
      <div className="cover-all-here">
        <div style={ subcriber ? {position:'relative',zIndex:'99999999999'} : null }>
          {
            <Rodal  width={widthModal} height={heightModal} visible={tooglePopup} onClose={this.hideModal.bind(this)}>
              <p className="text-center-popup">配信希望するメールをチェックして<br/>確認ボタンを押してください</p>
              {
                profile ?
                <div className="cover-checkbox-pop">
                  <label className="container-checkbox">システムから全ての報告通知メール

                    {
                      <input
                        type="checkbox"
                        value={statusUnsubscribe}
                        onChange={(e)=>this.mailSystem(e)}
                        checked={ statusUnsubscribe }
                        />
                    }
                    <span className="checkmark"></span>
                  </label>
                  <label className="container-checkbox">支援したプロジェクトに関する通知メール
                      {
                        <input
                          type="checkbox"
                          value={unsubscribeProject}
                          onChange={(e)=>this.mailProject(e)}
                          checked={ unsubscribeProject }
                        />
                      }
                    <span className="checkmark"></span>
                  </label>

                </div> : null
              }

              <button onClick={() => this.hideModalAndConfirm()} className="confirm-button">確認</button>
              <button style={{"background":"#ddd"}} onClick={() => this.hideModal()} className="confirm-button cancel">キャンセル</button>
            </Rodal>
          }
        </div>

        {
          profile !== null ?
          <div>
            <div className="image">
              <img src={profile.profileImageURL} alt="ava"/>
            </div>
            {/* IMAGE */}
            <div className="profile-detail">
              <p className="name">{profile.username}</p>
              <p  className="number">
                <FontAwesomeIcon icon={faTrophy} />
                <span>{`${profile.patron}件の支援したプロジェクトと ${profile.numberProject}件の自分のプロジェクト`}</span>
              </p>
              <div className="cover-header-button">
                <p className="btn-mypage"><Link to={'/my-page/add-new-project/'}>プロジェクト新規作成</Link></p>

                <p className="btn-mypage subscriber" style={ subcriber ? {position:'relative',zIndex:'99999999'} : null } >
                  <button
                    className={profile.statusUnsubscribe === '0' || profile.unsubscribeProject === '0' ? "un-active" : null }
                    onClick={ ()=> this.tooglePopupSettingEmail() } >

                      {profile.statusUnsubscribe === '0' || profile.unsubscribeProject === '0' ? "メール設定配信済" : "メール配信設定" }
                  </button>
                </p>
                {
                  subcriber ? <div className="dark-overlay"></div> : null
                }

              </div>

            </div>
          </div> : null
        }
        <div className="clear-fix"></div>

        <div className="admin-noti">
          {
            notificationListAdminHeaderData ?
            <NotificationAdmin
              data={notificationListAdminHeaderData} />
            : null
          }
        </div>

        <div className="clear-fix"></div>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    profile:state.common.profile,
    notificationListAdminHeaderData:state.common.notificationListAdminHeader,
  }
}

export default connect(mapStateToProps,actions)(headerProfile);
