/* @flow */

import React, { Component } from 'react';
import {  NavLink } from 'react-router-dom';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import {faDonate , faEdit , faCog ,faGem} from '@fortawesome/free-solid-svg-icons';

export default class Nav extends Component {
  render() {
    const {patron, patronfanclub , numberProject} = this.props;

    return (
      <ul className="navjs padding-30">
        <li>
          <NavLink to="/my-page/project-supported/page=1" className='support-row' activeClassName='active'>
            <FontAwesomeIcon icon={faDonate} />

            <span>
              支援したプロジェクト
            </span>
            <i className="number-propject">{patron}</i>


          </NavLink>
        </li>
        <li>
          <NavLink to="/my-page/project-supported-fan/page=1" className='support-row' activeClassName='active'>
            <FontAwesomeIcon icon={faGem} />

            <span>
              定期支援中のプロジェクト
            </span>
            <i className="number-propject">{patronfanclub}</i>


          </NavLink>
        </li>
        <li>
          <NavLink to="/my-page/post-project/page=1" className="post-row" activeClassName='active'>
            <FontAwesomeIcon icon={faEdit} />

            <span>
              自分のプロジェクト
            </span>
            <i className="number-propject">{numberProject}</i>
          </NavLink>
        </li>

        <li>
          <NavLink to="/my-page/setting-user" className="setting-row" activeClassName='active'>
            <FontAwesomeIcon icon={faCog} />
            <span>設定</span>
          </NavLink>
        </li>
      </ul>
    )
  }
}
