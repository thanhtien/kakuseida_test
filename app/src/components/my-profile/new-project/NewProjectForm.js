/* @flow */
import React, { Component } from 'react';
import NormalProject from './NormalProject';
import FanClubProject from './FanClubProject';


class TableEdit extends Component {



  render() {

    const {previewOpen , typeProject} = this.props;
    return (
      <div className="main main-write">



        {
          typeProject === 'normal' ? <NormalProject previewOpen={previewOpen} /> : null
        }

        {
          typeProject === 'fanclub' ? <FanClubProject previewOpen={previewOpen} /> : null
        }


      </div>
    );
  }
}




export default TableEdit
