/* @flow */
import React, { Component } from 'react';
import HeaderNewProject from './header-new-project';
import NewProjectForm from './NewProjectForm';
import Tab from '../componentCommon/tab';
import smoothScroll from '../../../components/common/smoothScroll';
import Rodal from 'rodal';
import PreviewProject from '../project-preview';
import Breadcrumbs from '../../Breadcrumbs';
import TypeProject from './typeProject';
class NewProject extends Component {


  constructor(props) {
    super(props);
    this.state = {
      preview_status:false,
      dataPreview:null,
      profile:null,
      typeProject:'normal'
    };
  }


  typeProjectChange(data){
    this.setState({
      typeProject:data
    })
  }

  componentDidMount() {
    smoothScroll.scrollTo('header');
    document.title = "プロジェクト新規作成｜KAKUSEIDA";

  }
  previewClose(){
    this.setState({
      preview_status:false
    })
    var content = document.getElementById("content");
    content.className = '';
    document.body.className = '';
  }
  setDataPreview() {
    this.setState({
      preview_status:true,
    })
  }
  previewOpen(data,profile){
    this.setState({
      dataPreview:data,
      profile:profile
    }, () => {
      this.setDataPreview();
      var content = document.getElementById("content");
      content.className = 'full-content-popup';
      document.body.className = 'block-body';
    })
  }


  render() {
    var linkBreacrm = [
      {last:false,label:"自分のプロジェクト",link:'/my-page/post-project/page=1'},
      {last:true,label:"プロジェクト新規作成",link:null}
    ]
    const {typeProject} = this.state;
    return (
      <div className="area my-page">
        <div className="wraper">
          <div className="cover-Breadcrumbs"><Breadcrumbs linkBreacrm={linkBreacrm}></Breadcrumbs></div>

          <TypeProject
            typeProject={typeProject}
            typeProjectChange={(data)=>this.typeProjectChange(data)}>
          </TypeProject>

          <HeaderNewProject typeProject={typeProject}/>
          {/* Profile Detail */}

          {/* NAV Detail */}
          <div className="clear-fix"></div>
        </div>
        <div className="white-page">
          <div className="wraper">
            <div className="write-nav">
              <Tab />
            </div>
            <div className="cover-no-blank new-project-here">
              <div className="wraper">
                <NewProjectForm
                  typeProject={typeProject}

                  previewOpen={(data,profile)=>this.previewOpen(data,profile)}
                />

                <div className="clear-fix"></div>
              </div>
            </div>
          </div>
        </div>

        <Rodal width={95} height={90} measure={'%'} visible={this.state.preview_status} onClose={this.previewClose.bind(this)}>
          <div className="scroll-modal">
            {
              this.state.preview_status ?
                <PreviewProject profile={this.state.profile} data={this.state.dataPreview} /> : null
            }
          </div>
        </Rodal>

      </div>
    );
  }
}
export default NewProject;
