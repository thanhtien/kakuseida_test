/* @flow */

import React, { Component } from 'react';
import {  NavLink } from 'react-router-dom';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import {faBars, faPencilAlt} from '@fortawesome/free-solid-svg-icons';

export default class Nav extends Component {
  render() {
    return (
      <ul className="navjs">
        <li>
          <NavLink to="" className='manage-row active' activeClassName='active'>
            <i><FontAwesomeIcon icon={faBars} /></i><span>管理画面</span>
          </NavLink>
        </li>
        <li>
          <NavLink to="" className="write-row" activeClassName='active'>
            <i><FontAwesomeIcon icon={faPencilAlt} /></i><span>編集する</span>
          </NavLink>
        </li>
      </ul>
    )
  }
}
