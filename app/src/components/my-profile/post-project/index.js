import React, { Component } from 'react';
import NavProfile from '../nav';
import HeaderProfile from '../header';
import { connect } from 'react-redux'
import * as actions from '../../../actions/editSystem';
import Loading from '../../common/loading';
import LoadingScroll from '../../common/loadingScroll';
import ReactPaginate from 'react-paginate';
import ProjectList from '../../projectList/profileUserPostProject';
import Blank from '../blank';
import FlashMassage from 'react-flash-message';


class PostProject extends Component {

  componentDidMount() {
    const {page} = this.props.match.params;
    var PageString = page.replace('page=', '');
    const {postProject} = this.props;
    if (!postProject) {
      this.props.getProjectPostNow(Number(PageString)-1);
    }
    document.title = "自分のプロジェクト｜KAKUSEIDA";

  }

  handlePageClick = (data) => {
    let selected = data.selected;
    this.props.getProjectPostNow(selected);
  };

  _renderStatusAPI() {
    setTimeout( () => {
      this.props.dispatch({
        type: 'CONFIRM_EDIT_PROJECT_OK',
        payload:null,
        status:false
      });
    }, 1500);
    return(
      <div id="push-notification">
        <FlashMassage duration={1500}>
          <div className="alert">
            成功！変更が保存されました。
          </div>
        </FlashMassage>
      </div>
    )
  }



  renderContentOrBlanK(){
    const {postProject,loading,pageLoad,deleteProject} = this.props;
    
    if (postProject) {
      if (postProject.post_project.length !== 0) {
        return(
          <div className="">
            <div className="cover-no-blank">
              <ProjectList pageLoad={pageLoad} deleteProject={deleteProject} data={postProject.post_project}/>
              <div className="cover-paginate">
                {
                  loading ?
                  <div className="loading-io">
                    <LoadingScroll></LoadingScroll>
                  </div>:
                  null
                }
                {
                  loading ? <Loading></Loading> : null
                }
                <ReactPaginate
                  previousLabel={"«"}
                  nextLabel={"»"}
                  breakLabel={<span>...</span>}
                  breakClassName={"break-me"}
                  pageCount={postProject.page_count}
                  marginPagesDisplayed={2}
                  pageRangeDisplayed={2}
                  forcePage={Number(pageLoad)}
                  onPageChange={this.handlePageClick}
                  containerClassName={"pagination"}
                  subContainerClassName={"pages pagination"}
                  activeClassName={"active"} />
              </div>
              <div className="clear-fix"></div>
            </div>


            <div className="clear-fix"></div>
          </div>
        )
      }else {
        return (
          <Blank data="自分のプロジェクトはまだありません。"></Blank>
        )
      }
    }else {
      return (
        <Loading></Loading>
      )
    }
  }

  render() {
    const {profile,edit_new_status_flast} = this.props;

    return (
      <div className="area my-page">
        <div className="wraper">
          <HeaderProfile></HeaderProfile>
          {
            edit_new_status_flast === true ? this._renderStatusAPI() : null
          }
          {/* Profile Detail */}
          <div className="nav-here">
            {
              profile ? <NavProfile patron={profile.patron} patronfanclub={profile.patronfanclub} numberProject={profile.numberProject}/> : null
            }

          </div>
          {/* NAV Detail */}
          <div className="clear-fix"></div>
        </div>
        <div className="">
          <div className="wraper">
            {this.renderContentOrBlanK()}
          </div>
        </div>

      </div>
    );
  }
}

const mapStateToProps = (state) => {
    return {
      postProject: state.common.postProject.data,
      loading:state.common.loading,
      profile:state.common.profile,
      edit_new_status_flast:state.common.edit_new_status_flast,
      pageLoad:state.common.postProject.page
    }
}
export default connect(mapStateToProps, actions )(PostProject);
