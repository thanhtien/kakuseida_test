import React, { Component } from 'react';


export default class InforProject extends Component {
  render() {
    const {data} = this.props;

    return (
      <div className="pd-box pd-box-1">
          <h4 className="pd-box-title">ファンディングの企画者について</h4>
          <div className="pd-box-content">
              <div className="pd-box-content-panel">
                  <p className="pd-box-content-panel-avatar"><img src={data.user.profileImageURL} alt=""/></p>
                  <div className="pd-box-content-panel-text-wraper">
                      <p className="pd-box-text">氏名 ：　{data.user.name}</p>
                      <p className="pd-box-text">現在地 ：{data.user.address}</p>
                      <p className="pd-box-text">職業 ：　{data.user.job}</p>
                  </div>
              </div>
              <div className="pd-box-content-intro">
                  <p className="pd-box-content-intro-title">はじめにご挨拶：</p>
                  <p className="pd-box-text">{data.description}</p>
                  <p>

                  </p>
                  <p className="pd-box-btn end-grey"><span className="info-button" onClick={()=>this.props.tagChange(1)} >プロジェクト詳細 ＞</span></p>
              </div>
          </div>
      </div>
    );
  }
}
