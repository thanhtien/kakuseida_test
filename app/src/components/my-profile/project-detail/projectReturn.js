/* @flow */

import React, { Component } from 'react';
import NumberFormat from 'react-number-format';
import History from '../../../history.js';
import { LazyLoadImage } from 'react-lazy-load-image-component';
import LinesEllipsis from 'react-lines-ellipsis';


export default class ProjectReturn extends Component {

  goDonate(item) {

    var url = "/projects/"+item.project_id+"/backers/"+item.id;
    History.push(url);
  }
  _renderCount(max,now) {
    var check = max - now;

    if (check === 0) {
      return (<p className="out-of-stock" style={{color:'red',fontWeight: '600'}} >OUT OF STOCK</p>)
    }
    else {
      return (<p >残り件数:<span><NumberFormat value={check} displayType={'text'} thousandSeparator={true} suffix={'件'} /></span></p>)
    }
  }


  _renderButton(item) {

    const {owner,currentUser} = this.props;

    if (Number(currentUser) === Number(owner)) {
      return(
        null
      )
    }

    if (item.max_count !== null) {
      if ((Number(item.max_count)  === Number(item.now_count))) {
        return(
          null
        )
      }
    }

    return (
      <button onClick={ () => this.goDonate(item) } ></button>
    )



  }

  _renderButtonBlue(item) {

    const {owner,currentUser} = this.props;

    if (Number(currentUser) === Number(owner)) {
      return(
        null
      )
    }

    if (item.max_count !== null) {
      if ((Number(item.max_count)  === Number(item.now_count))) {
        return(
          null
        )
      }
    }

    return (
      <p className="return-list-btn" style={{cursor:'pointer'}} onClick={ () => this.goDonate(item) } ><span className="button-return-click">このリターンを選択する</span></p>
    )



  }

  render() {
    const {data} = this.props;

    return (
      <ul>
        {
          data.map((item,i)=>{

            return(
              <li key={i}>
                  {
                    this._renderButton(item)
                  }
                  <p className="return-list-img">
                    <span>
                        <LazyLoadImage
                          alt={"thumbnail"}
                          effect="blur"
                          src={item.thumnail} />
                    </span>
                  </p>
                  <div className="return-list-content">
                      <div className="return-list-content-number">

                        {
                          item.name ?
                          <h1 className="name-return" title={item.name}>

                            <pre>
                              <LinesEllipsis
                                text={item.name}
                                maxLine={2}
                                ellipsis={<span style={{color:'#147efb',cursor:'pointer'}}>...</span>}
                                trimRight
                                basedOn='letters'
                              />
                            </pre>
                          </h1> : <h1 className="name-return" title={`リターン品 ${item.id}`}>リターン品 {item.id}</h1>
                        }


                          {
                            this.props.number_month ?
                            <NumberFormat value={item.invest_amount} displayType={'text'} thousandSeparator={true} suffix={`円/${this.props.number_month}月`} /> :
                            <NumberFormat value={item.invest_amount} displayType={'text'} thousandSeparator={true} suffix={'円'} />
                          }
                        {
                          item.max_count ?
                            <div className="max_count">
                              {
                                this._renderCount(Number(item.max_count), Number(item.now_count))
                              }
                            </div> :
                          null
                        }
                      </div>
                      <p className="return-list-text" style={{whiteSpace:"pre-line"}}>{item.return_amount}</p>
                      <div className="return-list-status">
                          <p className="return-list-s return-list-s-1">支援者数：　<span>{item.now_count}人</span></p>

                            {item.schedule ? <p className="return-list-s return-list-s-2">
                              お届け予定：<span>{item.schedule}</span>
                            </p> :  null}

                      </div>
                      {
                        this._renderButtonBlue(item)
                      }


                  </div>
              </li>
            )
          })
        }
      </ul>
    );
  }
}
