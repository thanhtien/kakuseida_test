/* @flow */
import React, { Component } from 'react';
import NavProfile from '../nav';
import HeaderProfile from '../header';
import { connect } from 'react-redux'
import * as actions from '../../../actions/editSystem';
import Loading from '../../common/loading';
import LoadingScroll from '../../common/loadingScroll';
import ReactPaginate from 'react-paginate';
import NumberFormat from 'react-number-format';
import LinesEllipsis from 'react-lines-ellipsis';
import History from '../../../history';
import Blank from '../blank';


class subsidized_project_fan extends Component {

  constructor(props) {
    super(props);
    this.state = {
      loadingLeave:false
    };
  }

  componentDidMount() {
    const {page} = this.props.match.params;
    const {subsidized_project_fan} = this.props;
    var PageString = page.replace('page=', '');
    if (!subsidized_project_fan) {
      this.props.ProjectFanClubGet(Number(PageString)-1);
    }
    document.title = "定期支援中のプロジェクト｜KAKUSEIDA";

  }


  handlePageClick = (data) => {
    let selected = data.selected;
    this.props.ProjectFanClubGet(selected);
  };
  _renderTime(time) {
    var d = new Date(time);
    var date = d.getDate();
    var month = d.getMonth() + 1;
    var year = d.getFullYear();


    var timeReturn =  year.toString()+'年'+month.toString()+'月'+date.toString()+'日';
    return timeReturn;
  }



  goLink(event,id) {
    event.preventDefault();
    History.push(`/project-detail/${id}`);
  }

  _renderStatus(status,stopProject) {

    if (stopProject === 'stop') {
      return(
        <span style={{color:'red'}}>解散された</span>
      )
    }else {
      if (status === '0') {
        return(
          <span style={{color:'#0e6eb8'}}>支援中</span>
        )
      }
      else {
        return (
          <span style={{color:'#ddd'}}>支援キャンセル済み</span>
        )
      }
    }

  }
  actionLeaveOrJoinProjectReturn(event,status,project_id,banking){
    event.preventDefault();
    event.stopPropagation();
    var r = window.confirm("このリターン品を解約しますか。!");
    if (r === true) {
      this.props.stopDonateFanClub(project_id,banking,()=>this.loadingLeave());
    }
  }
  loadingLeave(){
    this.setState({
      loadingLeave:!this.state.loadingLeave
    })
  }

  renderContentOrBlanK(){
    const {subsidized_project_fan,loading , pageLoad} = this.props;

    if (subsidized_project_fan) {

      if (subsidized_project_fan.data.length !== 0) {
        return(
          <div className="cover-static cover_subsidized_project cover_subsidized_project_fan">

            <table className="static suported">
              <caption>定期支援中のプロジェクト</caption>
              <thead>
                <tr>
                  <th scope="col">プロジェクト名</th>
                  <th scope="col">定期課金間隔</th>
                  <th scope="col">決済金額</th>
                  <th scope="col">選択したリターン／お届け予定</th>
                  <th scope="col">お届け先</th>

                  <th scope="col">ステータス</th>

                </tr>
              </thead>

              <tbody>
                {
                  subsidized_project_fan.data.map( (item,i) => {

                    return(
                      <tr key={i} onClick={(event)=>this.goLink(event,item.project_id)}>
                        <td title={item.project_name} data-label="プロジェクト名">
                          <div className="cover-mobi-m">
                            <div  className="project-name">
                              <pre>
                                <LinesEllipsis
                                  text={item.project_name}
                                  maxLine='2'
                                  ellipsis='...'
                                  trimRight
                                  basedOn='letters'
                                />
                              </pre>
                          </div>
                            <p className="image-project">
                              <img src={item.thumbnail} alt="thumbnail"/>
                            </p>
                          </div>
                        </td>



                        <td data-label="定期課金間隔">
                          <div className="cover-mobi-m">
                            {
                              item.number_month
                            }
                            カ月
                          </div>

                        </td>
                        <td data-label="決済金額">
                          <div className="cover-mobi-m">
                            <NumberFormat value={item.invest_amount} displayType={'text'} thousandSeparator={true} suffix={'円'} />
                          </div>

                        </td>
                        <td title={item.return_amount} data-label="選択したリターン／お届け予定">
                          <div className="cover-mobi-m">
                            <pre>
                              <LinesEllipsis
                                text={item.return_amount}
                                maxLine='3'
                                ellipsis='...'
                                trimRight
                                basedOn='letters'
                              />
                            </pre>
                          </div>


                        </td>
                        <td data-label="お届け先">
                          <div className="cover-mobi-m">
                            {
                              item.address_return
                            }

                          </div>

                        </td>

                        <td data-label="決済状況">
                          <div className="cover-mobi-m">
                            {
                              this._renderStatus(item.status_join_fanclub,item.status_fanclub)
                            }
                            {
                              item.status_join_fanclub === '0' ?
                              <button
                                className={item.status_join_fanclub === '0' ? 'link-to-project-detail goleave' : 'link-to-project-detail leave'}
                                onClick={ item.status_join_fanclub === '0' ? (event)=> this.actionLeaveOrJoinProjectReturn(event,item.status_join_fanclub ,item.project_id,item.backing_levels_id) : null }
                              >
                                {
                                  item.status_join_fanclub === '0' ? '解約する' : '支援キャンセル済み'
                                }
                              </button> : null
                            }

                          </div>


                        </td>


                      </tr>
                    )
                  })
                }



              </tbody>
            </table>



              <div className="cover-paginate custom-suport">
                {
                  loading ?
                    <div className="loading-io">
                      <LoadingScroll></LoadingScroll>
                    </div> : null
                }
                <ReactPaginate
                  previousLabel={"«"}
                  nextLabel={"»"}
                  breakLabel={<span>...</span>}
                  breakClassName={"break-me"}
                  pageCount={subsidized_project_fan.page_count}
                  marginPagesDisplayed={2}
                  pageRangeDisplayed={2}
                  forcePage={Number(pageLoad)}
                  onPageChange={this.handlePageClick}
                  containerClassName={"pagination"}
                  subContainerClassName={"pages pagination"}
                  activeClassName={"active"} />
              </div>




            <div className="clear-fix"></div>
          </div>
        )
      }else {
        return (
          <Blank data="定期支援中のプロジェクトがまだありません。"></Blank>
        )
      }
    }else {
      return (
        <Loading></Loading>
      )
    }
  }

  render() {
    const {profile} = this.props;
    const {loadingLeave} = this.state;

    if (loadingLeave) {
      return (<Loading></Loading>)
    }

    return (
      <div className="area my-page">
        <div className="wraper">
          <HeaderProfile></HeaderProfile>
          {/* Profile Detail */}
          <div className="nav-here">
            {
              profile ?
              <NavProfile
                patron={profile.patron} patronfanclub={profile.patronfanclub}
                numberProject={profile.numberProject}/>
              : null
            }
          </div>
          {/* NAV Detail */}
          <div className="clear-fix"></div>
        </div>
        <div className="">
          <div className="wraper">
            {this.renderContentOrBlanK()}
          </div>
        </div>

      </div>
    );
  }
}

const mapStateToProps = (state) => {
    return {
      subsidized_project_fan: state.common.subsidized_project_fan.data,
      pageLoad:state.common.subsidized_project_fan.page,
      loading:state.common.loading,
      profile:state.common.profile,
    }
}
export default connect(mapStateToProps, actions )(subsidized_project_fan);
