/* @flow */

import React, { Component } from 'react';
import Banner from './banner';
import * as actions from '../../../actions/TopPage';
import { connect } from 'react-redux';
import Loading from '../../common/loading';
import { Link } from 'react-router-dom';
import Social from './social';
import Breadcrumbs from '../../Breadcrumbs';
import FormatFunc from '../../common/FormatFunc';

var CONFIG = require('../../../config/common');
if (process.env.NODE_ENV === "development") {
  CONFIG = CONFIG.CONFIG.DEV;
}else {
  CONFIG = CONFIG.CONFIG.PRODUCT;
}

class NewDetail extends Component {
  componentDidMount() {
    const {id} = this.props.match.params;
    this.props.NewsDetail(id);
  }
  htmlReturn(data) {
    return{
      __html:data
    }
  }
  backTo() {
    this.props.history.goBack();
  }
  render() {
    const {new_detail} = this.props;
    const {id} = this.props.match.params;

    const shareLink = CONFIG.MAIN_URL+'new-detail/'+id;
    var linkBreacrm;

    if (new_detail) {

      var label;

    

      if (new_detail.title > 10) {
        label = new_detail.title.substring(0,10) + '...';
      }else {
        label = new_detail.title.substring(0,10);

      }
      linkBreacrm = [
        {last:false,label:"最新の活動・ニュース",link:'/news-list/page=1'},
        {last:true,label:label,link:null}
      ]
    }

    return (
      <div className="about_box bg">
      {
        new_detail ?
        <div>
          <Banner></Banner>
          {/* .Banner */}
          <div className="wraper"><div className="cover-Breadcrumbs"><Breadcrumbs linkBreacrm={linkBreacrm} ></Breadcrumbs></div></div>

          <div style={{display:'block'}} className="pd-area">



            <div style={{background:'#fff'}} className="wraper padding">
              <Social shareLink={shareLink}></Social>
              <div className="title-green">
                <p className="green-time"><FormatFunc date={new_detail.created} ></FormatFunc></p>
                <p className="title-page">{new_detail.title}</p>

              </div>
              <p className="des-thumnail">

              </p>

              {
                new_detail.content ?
                <div className="div-content" dangerouslySetInnerHTML={this.htmlReturn(new_detail.content)} ></div> : <p style={{textAlign:'center', border: "3px dotted #0071BA",padding:"10px"}}>コンテンツがまだありません。</p>
              }
              {/* .WYSIWYG */}
              <div className="button-bottom">

                <button onClick={ () => this.backTo() } className="button prev">前へ</button>
                <Link to="/news-list/page=0" className="back-button-list">一覧へ</Link>

              </div>
            </div>

          </div>
        </div> : <Loading></Loading>
      }

      </div>
    );
  }
}
const mapStateToProps = (state) => {
  return {
    new_detail: state.common.new_detail,
    loading:state.common.loading
  }
}

export default connect(mapStateToProps, actions)(NewDetail);
