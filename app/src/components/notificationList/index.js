/* @flow */

import React, { Component } from 'react';
import * as actions from '../../actions/notification';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import ReactPaginate from 'react-paginate';
import LoadingScroll from '../common/loadingScroll';
import FormatFunc from '../common/FormatFunc';

class Notification extends Component {
  componentDidMount() {
    document.title = "お知らせ一覧｜KAKUSEIDA";
    const {page} = this.props.match.params;
    var PageString = page.replace('page=', '');
    if (!this.props.NotificationData) {
      this.props.notificationList(Number(PageString)-1);
    }

  }

  handlePageClick = (data) => {
    let selected = data.selected;
    this.props.notificationList(selected);
  };



  render() {
    const {NotificationData,pageLoad} = this.props;

    return (
      <div className="area my-page">
        <div className="wraper">
          <h1 className="title-noti">お知らせ一覧</h1>
          <ul className="personal-panel-width personal-panel notification-list" style={{display:"block",overflow: "hidden" , height:'auto'}}>
            {
              NotificationData && NotificationData.data.length > 0 ?
              NotificationData.data.map((item,i)=>{


                return (
                  <li key={i} style={item.status === '0' ? {background: 'floralwhite'} : null}>
                    <Link  to={`/${item.link}`}  className="notification">
                      <div className="notification-image">
                        <img src={item.thumbnail} alt="thum" />
                      </div>
                      <div className="notification-text">
                        <span className="text-name"> </span> {item.message}
                        <p className="calendar-time">
                          <span className="time-text">

                            <FormatFunc date={item.created} ></FormatFunc>
                          </span>
                        </p>
                      </div>
                    </Link>
                  </li>
                )

              }):
              <li className="noti-list">
                <p>通知がまだありません。</p>
              </li>



            }
          </ul>
          <div className="cover-paginate noti">
            {
              this.props.loading?<div className="loading-io"><LoadingScroll></LoadingScroll></div>:null
            }
            {
              NotificationData && NotificationData.data.length > 0 ?
              <ReactPaginate
                previousLabel={"«"}
                nextLabel={"»"}
                breakLabel={<span>...</span>}
                breakClassName={"break-me"}
                pageCount={NotificationData.page_count}
                marginPagesDisplayed={2}
                forcePage={Number(pageLoad)}
                pageRangeDisplayed={2}
                onPageChange={this.handlePageClick}
                containerClassName={"pagination"}
                subContainerClassName={"pages pagination"}
                activeClassName={"active"} /> : null
            }
          </div>
          <div className="clear-fix"></div>

        </div>
      </div>
    );
  }
}
const mapStateToProps = (state) => {

  return {
    NotificationData: state.notificationsList.notification.data,
    pageLoad:state.notificationsList.notification.pageLoad,
    loading:state.common.loading,
  }
}

export default connect(mapStateToProps, actions)(Notification);
