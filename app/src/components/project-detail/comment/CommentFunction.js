/* @flow */

import React, { Component } from 'react';
import LoadingScroll from '../../common/loadingScroll';


export default class CommentFunction extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loadingFunc:false
    };
  }
  ToogleComment(){
    this.props.ToogleComment()
  }
  loading() {
    this.setState({
      loadingFunc:!this.state.loadingFunc
    })
  }
  deleteComment(id) {
    this.props.deleteComment(id,()=>this.loading())
  }
  SetvalueEdit(comment){
    this.props.toogleCommentEdit(comment);
  }
  render() {
    const {loadingFunc} = this.state;
    const {comment,profile} = this.props;
    return (
      <div className="func-comment">
        <div className="loading-func">
          {
            loadingFunc ? <LoadingScroll></LoadingScroll> : null
          }
        </div>

        <button onClick={ () => this.ToogleComment() } className="button-func">
          返信
        </button>
        {
          comment.user_id === profile.id ?
          <button className="button-func" onClick={ () => this.SetvalueEdit(comment) }>
            編集
          </button> : null
        }
        {
          comment.owner === true || comment.user_id === profile.id ?
          <button className="button-func" onClick={()=>this.deleteComment(comment.id)}>
            削除
          </button> : null

        }
        <p className="edited-text">
          {
            comment.updated ? '編集済み' : null
          }
        </p>


      </div>
    );
  }
}
