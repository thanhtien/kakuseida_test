/* @flow */

import React, { Component } from 'react';
import LoadingScroll from '../../common/loadingScroll';

class InputBox extends Component {

  constructor(props) {
    super(props);
    this.state = {
      value: '',
      loadingInput:false
    };

  }


  handleChange(event) {
    this.setState({value: event.target.value});
  }



  handleSubmit(event) {

    if (this.state.value !== '') {
      var data = {
        project_id:this.props.projectId,
        comment_id:this.props.commentId,
        comment:this.state.value
      }
      var firebaseData = {
        action:"sendProjectReply",
        project_id:this.props.projectId,
        comment_id:this.props.commentId,
        comment:this.state.value
      }

      this.props.ReplyBox(data,()=>this.LoadingInput(),this.props.socket,this.props.closeAdd);
      this.props.firebaseCURL(firebaseData);

      this.setState({
        value:''
      })
    }

    event.preventDefault();
  }

  LoadingInput() {
    this.setState({
      loadingInput:!this.state.loadingInput
    })

  }


  typing() {
    var data = {
      project_id:this.props.projectId,
      status:true
    }
    this.props.socket.emit('user-typing',data);
  }
  stopType() {
    var data = {
      project_id:this.props.projectId,
      status:false
    }
    this.props.socket.emit('user-typing',data);
  }


  render() {
    const {profile} = this.props;
    const {loadingInput} = this.state;

    return (
      <div className="group replyClass">
        {
          profile ?
          <p className="avatar-comment">
            <img src={profile.profileImageURL} alt="img"/>
          </p>
          : null
        }

        <textarea
          placeholder="コメント"
          onFocus ={()=>this.typing()}
          onBlur ={()=>this.stopType()}
          value={this.state.value}
          onChange={(event)=>this.handleChange(event)}
        />
          <div className="border"></div>
          <span className="highlight"></span>
          <span className="bar"></span>
          <button onClick={(event)=>this.handleSubmit(event)} type="submit" className="icon send" />
          {
            loadingInput ? <div className="loading-e"><LoadingScroll></LoadingScroll></div> : null
          }


      </div>
    );
  }
}


export default InputBox;
