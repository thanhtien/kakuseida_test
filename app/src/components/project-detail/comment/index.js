/* @flow */
import React, { Component } from 'react';
import CommentList from './CommentList';
import InputBox from './InputBox';
import LoadingScroll from '../../common/loadingScroll';

export default class CommentComponent extends Component {

  constructor(props) {
    super(props);
    this.state = {
      page:1,
      loading:false
    };
  }

  LoadMoreComment() {
    const {page_count,projectId} = this.props;
    if (page_count !== this.state.page) {
      this.setState({
        page:this.state.page + 1
      });
      this.props.LoadMoreComment(this.state.page,projectId,()=>this.loading())
    }
  }

  loading() {
    this.setState({
      loading:!this.state.loading
    })
  }

  render() {
    const {profile,CommentBox,ReplyBox,projectId,comment,page_count,loadMoreReply,socket,typing,firebaseCURL,deleteReply,deleteComment,CommentBoxEdit,ownerProject,ReplyBoxEdit} = this.props;


    return (
      <div className="wraper">
        <div className="pd-area active pd-are-comment">
          <h1 className="title-comment"><i className="icss-chat"></i><span >コメント</span></h1>
        </div>

        <div className="comments-container">


          {/* Comment Box */}

          {
            comment.length !== 0 && comment ?
            <div className="first_li_comment">
              {
                page_count !== this.state.page || page_count === 0 ?
                  <div>
                    {
                      this.state.loading ? <span>もっと読み込む</span> : <span onClick={()=>this.LoadMoreComment()}>もっと読み込む</span>
                    }
                    {
                      this.state.loading ?  <LoadingScroll></LoadingScroll> : null
                    }
                  </div>
                : null
              }
            </div> : null
          }
          {
            comment.length !== 0 && comment ?
            <ul id="comments-list" className="comments-list">


              {
                comment.map((item,i)=>{
                  return <CommentList
                    ReplyBoxEdit={ReplyBoxEdit}
                    ownerProject={ownerProject}
                    deleteReply={deleteReply}
                    CommentBoxEdit={CommentBoxEdit}
                    deleteComment={deleteComment}
                    firebaseCURL={firebaseCURL}
                    typing={typing}
                    socket={socket}
                    loadMoreReply={loadMoreReply}
                    key={i}
                    projectId={projectId}
                    profile={profile}
                    ReplyBox={ReplyBox}
                    comment={item}/>
                })
              }
            </ul>
            : <p className="comment-empty">コメントはまだありません</p>
          }
          {/* Comment Box */}




          {/* input Box */}

          {
            profile ?
            <InputBox
              firebaseCURL={firebaseCURL}
              typing={typing}
              CommentBox={CommentBox}
              projectId={projectId}
              profile={profile}
              socket={socket}></InputBox>
            : null
          }

          {/* input Box */}

      	</div>
      </div>

    );
  }
}
