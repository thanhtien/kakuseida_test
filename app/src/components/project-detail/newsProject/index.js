/* @flow */

import React, { Component } from 'react';

import { Link } from 'react-router-dom';
import { LazyLoadImage } from 'react-lazy-load-image-component';
import FormatFunc from '../../common/FormatFunc';

var CONFIG = require('../../../config/common');
if (process.env.NODE_ENV === "development") {
  CONFIG = CONFIG.CONFIG.DEV;
}else {
  CONFIG = CONFIG.CONFIG.PRODUCT;
}

export default class MyComponent extends Component {
  renderDes(content) {
    return content.replace(/<\/?[^>]+(>|$)/g, "").substring(0, 200);
  }
  render() {
    const {report,idProject} = this.props;

    return (
      <div className="about_box">

        <h3 className="title-box-project"><span className="icon"><img src={CONFIG.MAIN_URL+"/img/index/ico-anoucer.svg"} alt="alt"/></span><span className="title-area">最新の活動・ニュース</span></h3>

          <div style={{background:'#fff'}}>

            {
              <div className="cover-list-new padding-zero">
                {
                  report.data.map( (item , i) => {

                    return (
                      <div className="each-item" key={i} >
                        <Link className="new-detail" style={{color:'initial'}} to={`/project-detail/${idProject}/news-detail/${item.id}`}>
                          <div className="thumnail">
                            <LazyLoadImage src={item.thumbnail} effect="blur" alt="thum"></LazyLoadImage>
                          </div>
                          <div className="text-right-only">
                            <p className="time">
                              <FormatFunc date={item.created}></FormatFunc>

                            </p>
                            <p className="title">{item.title}</p>
                            <p className="des">
                              {
                                this.renderDes(item.content)
                              }

                            </p>

                          </div>
                        </Link>


                      </div>
                    )
                  } )
                }
              </div>
            }


            <div className="clear-fix"></div>
          </div>

      </div>
    );
  }
}
