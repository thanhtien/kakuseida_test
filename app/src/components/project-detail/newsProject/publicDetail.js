/* @flow */

import React, { Component } from 'react';
import Banner from './banner';
import * as actions from '../../../actions/projectDetail';
import { connect } from 'react-redux';
import Loading from '../../common/loading';
import { Link } from 'react-router-dom';
import Social from './social';
import FormatFunc from '../../common/FormatFunc';

var CONFIG = require('../../../config/common');
if (process.env.NODE_ENV === "development") {
  CONFIG = CONFIG.CONFIG.DEV;
}else {
  CONFIG = CONFIG.CONFIG.PRODUCT;
}

class PublicDedail extends Component {
  componentDidMount() {
    const {idNew} = this.props.match.params;
    this.props.ProjectDetailNewsGet(idNew);
  }
  htmlReturn(data) {
    return{
      __html:data
    }
  }
  backTo() {
    this.props.history.goBack();
  }
  render() {
    const {data} = this.props;
    const {id} = this.props.match.params;
    const shareLink = CONFIG.MAIN_URL+'new-detail/'+id;

    if (data) {
      document.title = `${data.title}｜KAKUSEIDA`;
    }

    return (
      <div className="about_box bg">
      {
        data ?
        <div>
          <Banner></Banner>
          {/* .Banner */}

          <div style={{display:'block'}} className="pd-area">



            <div style={{background:'#fff'}} className="wraper padding">
              <Social shareLink={shareLink}></Social>
              <div className="title-green">
                <p className="green-time">{<FormatFunc date={data.created}></FormatFunc>}</p>
                <p className="title-page">{data.title}</p>

              </div>
              <p className="des-thumnail">

              </p>

              {
                data.content ?
                <div dangerouslySetInnerHTML={this.htmlReturn(data.content)} ></div> : <p style={{textAlign:'center', border: "3px dotted #0071BA",padding:"10px"}}>コンテンツがまだありません。</p>
              }
              {/* .WYSIWYG */}
              <div className="button-bottom">

                <Link to={`/project-detail/${id}`} className="back-button-list button-center">プロジェクト詳細画面へ</Link>


              </div>
            </div>

          </div>
        </div> : <Loading></Loading>
      }

      </div>
    );
  }
}
const mapStateToProps = (state) => {
  return {
    data: state.ProjectDetail.projectReportDetail,
    loading:state.common.loading
  }
}

export default connect(mapStateToProps, actions)(PublicDedail);
