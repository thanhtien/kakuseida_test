import React, { Component } from 'react';

import CardProject from '../cardProject/indexProfile';

export default class ProjectListPostNow extends Component {
  render() {
    const {data} = this.props;
    return (
      <div className="area area-list">
        <div className="wraper">
            <div className="row">
              {
                data.map( (item , i) => {
                  return(
                    <CardProject key={i} item={item}/>
                  )
                })
              }
            </div>
        </div>
      </div>
    );
  }
}
