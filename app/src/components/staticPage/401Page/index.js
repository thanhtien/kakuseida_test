/* @flow */

import React, { Component } from 'react';

import { Link } from 'react-router-dom';
import history from '../../../history';
var CONFIG = require('../../../config/common');

if (process.env.NODE_ENV === "development") {
  CONFIG = CONFIG.CONFIG.DEV;
}else {
  CONFIG = CONFIG.CONFIG.PRODUCT;
}
export default class Page404 extends Component {
  onBackButtonEvent(e){
    history.push('/');
  }
  componentDidMount() {
    window.onpopstate = (e) => this.onBackButtonEvent(e);
    document.title = "401エラー｜KAKUSEIDA";
  }
  render() {
    return (
      <div id="notfound">
    		<div className="cover-notfound">
          <p className="img-notfound">
            <img src={CONFIG.MAIN_URL+"img/common/logo.svg"} alt="CLOUD FUNDING SYSTEM"/>
          </p>
          <p className="text-bold">
            401 エラー
          </p>
          <p className="mess">
            申し訳ございませんが、該当ページがございません。
          </p>
          <Link className="button-back" to="/">トップページ</Link>
        </div>
    	</div>
    );
  }
}
