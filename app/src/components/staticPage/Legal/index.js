import React, { Component } from 'react';
import Breadcrumbs from '../../Breadcrumbs';

export default class Legal extends Component {
  componentDidMount() {
    document.title = "特定商取引法に基づく表記｜KAKUSEIDA";

  }
  render() {
    const linkBreacrm = [
      {last:true,label:'特定商取引法に基づく表記',link:null}
    ]
    return (
      <div className="about_box">
        <div className="banner-css">
          <div className="wraper">
            <p className="title-white">
              特定商取引法に基づく表記
            </p>
          </div>
        </div>
        <div className="wraper">
          <Breadcrumbs linkBreacrm={linkBreacrm}></Breadcrumbs>
          <p className="big-title">
            事業者
          </p>
          <div className="cover-text">
            <p>
              株式会社カクセイ
            </p>
          </div>

          <p className="big-title">
            運営責任者
          </p>
          <div className="cover-text">
            <p>
              KAKUSEIDA運営室
            </p>
          </div>


          <p className="big-title">
            所在地
          </p>
          <div className="cover-text">
            <p>
              東京都渋谷区恵比寿1丁目23番9号 ヴェルミドール恵比寿 7階
            </p>
          </div>


          <p className="big-title">
            電話番号
          </p>
          <div className="cover-text">
            <p>
              03-6804-6204 <br/>
              受付時間：土日祝日年末年始を除く平日
            </p>
          </div>

          <p className="big-title">
            メールアドレス
          </p>
          <div className="cover-text">
            <p>
              info@kakuseida.com
            </p>
          </div>


          <p className="big-title">
            ホームページ
          </p>
          <div className="cover-text">
            <a href="https://kakusei.com" target="_blank" rel="noopener noreferrer">
              {"https://kakusei.com"}
            </a>
          </div>

          <p className="big-title">
            販売価格
          </p>
          <div className="cover-text">
            <p>
              各プロジェクトに表記された価格に準じます。各プロジェクトの募集期間の終了日に決済が完了します。また、All or Nothing型のプロジェクトの場合は、各プロジェクトで定まった募集期間内に定まった目標金額が成立しない場合募集期間の終了日になった場合でも決済はされません。
            </p>
          </div>


          <p className="big-title">
            引渡時期
          </p>
          <div className="cover-text">
            <p>
              各プロジェクトの募集期間終了日に代金支払が確認できた時点でお客様への引渡しが完了するものとします。
            </p>
          </div>


          <p className="big-title">
            購入可能な範囲の制限
          </p>
          <div className="cover-text">
            <p>
              購入は日本国内に限ります。
            </p>
          </div>


          <p className="big-title">
            販売価格以外の必要手数料
          </p>
          <div className="cover-text">
            <p>
              なし。（支払手数料やリターンに関する送料などは例外を除いて請求致しません。）
            </p>
          </div>

          <p className="big-title">
            注文方法
          </p>
          <div className="cover-text">
            <p>
              インターネット
            </p>
          </div>

          <p className="big-title">
            支払方法
          </p>
          <div className="cover-text">
            <p>
              クレジットカードまたは銀行による支払い。
            </p>
          </div>


          <p className="big-title">
            支払時期
          </p>
          <div className="cover-text">
            <p>
              クレジットカードによるお支払いは各プロジェクトで定まった募集期間内に定まった目標額が成立した場合に支払が完了します。
            </p>
          </div>

          <p className="big-title">
            返金・キャンセルについて
          </p>
          <div className="cover-text">
            <p>
              一定期間内に販売された金額が、各プロジェクトに設定された最低目標額を超えた場合のみ販売が決定するという販売ルール上、 キャンセルは承っておりません。キャンセルが出ることで最低目標数を下回る場合、起案者や他のお客様にご迷惑がかかりますので予めご了承ください。
            </p>
          </div>


        </div>
    </div>
    );
  }
}
