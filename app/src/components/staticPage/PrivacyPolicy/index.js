import React, { Component } from 'react';
import Breadcrumbs from '../../Breadcrumbs';

export default class PrivacyPolicy extends Component {
  componentDidMount() {
    document.title = "プライバシーポリシー｜KAKUSEIDA";
  }
  render() {
    const linkBreacrm = [
      {last:true,label:'プライバシーポリシー',link:null}
    ]
    return (
      <div className="about_box">
        <div className="banner-css">
          <div className="wraper">
            <p className="title-white">
              プライバシーポリシー
            </p>
          </div>
        </div>
        <div className="wraper">
          <Breadcrumbs linkBreacrm={linkBreacrm}></Breadcrumbs>

          <p className="big-title">
            個人情報収集について
          </p>
          <div className="cover-text">

              <p>個人情報とは、個人に関する情報であり、当該情報に含まれる氏名、生年月日その他の記述等により特定の個人を識別することができるもの（他の情報と容易に照合することができ、それにより特定の個人を識別することができることとなるものを含む。）を指します。</p>
              <p>
                <a rel="noopener noreferrer" href="https://www.kakusei.com/" target="_blank">株式会社カクセイ</a> （以下「当社」)の運営するカクセイだ！（以下「本サービス」）は、個人情報を収集することがあります。
              </p>
              <p>
                以下に、個人情報の利用目的を公表します。
              </p>

          </div>
          <p className="big-title">
            個人情報の利用について
          </p>
          <div className="cover-text">

              <p>当社は、収集した個人情報を以下の目的で利用することができるものとします。</p>
              <p style={{paddingLeft:'15px',paddingTop:'15px'}}>
                1. 本人確認、認証サービスのため <br/>
                2. アフターサービス、お問い合わせ、苦情対応のため  <br/>
                3. 当社及び本サービスへのお問い合わせ、本サービスの運営上必要な事項の通知（電子メールによるものを含むものとします。）<br/>
                4. ユーザーが投稿した情報の掲載<br/>
                5. システムの維持、不具合対応のため<br/>
                6. 当社サービスの改善や新サービスの開発等に役立てるため<br/>
                7. その他当社の各サービスにおいて個別に定める目的のため
              </p>

          </div>

          <p className="big-title">
            個人情報の第三者への提供
          </p>
          <div className="cover-text">

              <p>当社は、以下に定める場合には、個人情報を第三者に提供することができるものとします。</p>
              <p style={{paddingLeft:'15px',paddingTop:'15px'}}>
                1. 本人の同意がある場合 <br/>
                2. 裁判所、検察庁、警察、税務署、弁護士会またはこれらに準じた権限を持つ機関から、個人情報の開示を求められた場合 <br/>
                3. 保険金請求のために保険会社に開示する場合 <br/>
                4. 当社が行う業務の全部または一部を第三者に委託する場合 <br/>
                5. 当社に対して秘密保持義務を負う者に対して開示する場合 <br/>
                6. 当社の権利行使に必要な場合 <br/>
                7. 合併、営業譲渡その他の事由による事業の承継の際に、事業を承継する者に対して開示する場合 <br/>
                8. 個人情報保護法その他の法令により認められた場合
              </p>
          </div>

          <p className="big-title">
            個人情報の開示
          </p>
          <div className="cover-text">

              <p>
                当社 は、ご本人から、個人情報の開示を求められたときは、ご本人に対し、遅滞なく開示します。<br/>
                ただし、開示することにより次のいずれかに該当する場合は、その全部または一部を開示しないこともあり、開示しない決定をした場合には、その旨を遅滞なく通知します。
              </p>
              <p style={{paddingLeft:'15px',paddingTop:'15px'}}>
                1. ご本人または第三者の生命、身体、財産その他の権利利益を害するおそれがある場合 <br/>
                2. 当社の業務の適正な実施に著しい支障を及ぼすおそれがある場合 <br/>
                3. 他の法令に違反することとなる場合

              </p>
          </div>

          <p className="big-title">
            個人情報の訂正等
          </p>
          <div className="cover-text">

              <p>
                当社 は、ユーザーご本人より当該本人の個人情報の訂正、追加、削除、利用の停止または消去を求められた場合には、ユーザーご本人であることを確認させていただいた上で合理的な期間内に対応いたします。
              </p>

          </div>

          <p className="big-title">
            プライバシーポリシーの更新について
          </p>
          <div className="cover-text">
            <p>
              当社 は、個人情報保護を図るため、法令等の変更や必要に応じて、プライバシーポリシーを改訂することがあります。その際は、最新のプライバシーポリシーを本サービスのサイト内に掲載いたします。
            </p>
          </div>

          <p className="big-title">
            免責
          </p>
          <div className="cover-text">
            <p>
              当社 及び本サービスでは、お客様のID及びパスワードの管理については関与いたしません。<br/>お客様の不注意によりID及びパスワードが第三者に利用された場合は、ご登録いただいている個人情報を閲覧される可能性がございますので、ご利用にあたっては、使用および管理について責任を負うと共に、使用上の過誤または第三者による不正使用等について十分注意をして下さい。
            </p>
          </div>

          <p className="big-title">
            お問い合わせ先
          </p>
          <div className="cover-text">
            <p>
              個人情報に関するお問い合わせ先は、 <a href="mailto:info@kakuseida.com" target="_top">info@kakuseida.com</a>  です。<br/> 株式会社カクセイ 平成30年8月1日 制定
            </p>
          </div>

        </div>
    </div>
    );
  }
}
