import React, { Component } from 'react';
import Breadcrumbs from '../../Breadcrumbs';
import ModalTowa from './modal-towa';

var CONFIG = require('../../../config/common');
if (process.env.NODE_ENV === "development") {
  CONFIG = CONFIG.CONFIG.DEV;
}else {
  CONFIG = CONFIG.CONFIG.PRODUCT;
}

export default class IntroKuseida extends Component {
  constructor(props) {
    super(props);
    this.state = {
      visible: false,
      urlImage:''
    };
  }

  show() {

    this.setState({ visible: true });
  }

  hide() {
    this.setState({ visible: false });
  }

  setUrlImage(index) {
    switch (index)
      {
        case 1:
          if( window.innerWidth>767 ){
            console.log('pc');
            this.setState({
              urlImage:CONFIG.MAIN_URL+"img/static/modal1.png"
            },() => {
              this.show()
            })
          }else{
            console.log('sp');
            this.setState({
              urlImage:CONFIG.MAIN_URL+"img/static/modal1-sp.png"
            },() => {
              this.show()
            })
          }
        break;
        case 2:
          if( window.innerWidth>767 ){
            this.setState({
              urlImage:CONFIG.MAIN_URL+"img/static/modal2.png"
            },() => {
              this.show()
            })
          }else{
            this.setState({
              urlImage:CONFIG.MAIN_URL+"img/static/modal2-sp.png"
            },() => {
              this.show()
            })
          }
        break;
        case 3:
          if( window.innerWidth>767 ){
            this.setState({
              urlImage:CONFIG.MAIN_URL+"img/static/modal3.png"
            },() => {
              this.show()
            })
          }else{
            this.setState({
              urlImage:CONFIG.MAIN_URL+"img/static/modal3-sp.png"
            },() => {
              this.show()
            })
          }
        break;
        default: return false;
      }
  }

  componentDidMount() {
    document.title = "カクセイだとは｜KAKUSEIDA";
  }

  render() {
    const linkBreacrm = [
      {last:true,label:'KAKUSEIDAとは？',link:null}
    ]
    const {urlImage,visible} = this.state;


    return (
      <div className="towa about_box">
        {
          /*modal Towa */
          <ModalTowa
            imageLink={urlImage}
            visible={visible}
            onClose={ () => this.hide() }
          />
          /*modal Towa */
        }
        <div className="banner-css banner-towa">
          <div className="wraper">
            <p className="title-white">
              KAKUSEIDAとは？
            </p>
          </div>
        </div>
        <div className="wraper">
          <Breadcrumbs linkBreacrm={linkBreacrm}></Breadcrumbs>
          <div className="box-cta">
            <h3 className="box-cta-title">株式会社カクセイ（以下“カクセイ”）が運営するクラウドファンディングサイトです。<br/>機能面は通常のクラウドファンディングサイトです。</h3>
            <ul className="box-cta-list">
              <li id="modal1" onClick={ () => this.setUrlImage(1) }>
                <img src={CONFIG.MAIN_URL+"img/static/cta-1.png"} alt=""/>
              </li>
              <li id="modal2" onClick={ () => this.setUrlImage(2) }>
                <img src={CONFIG.MAIN_URL+"img/static/cta-2.png"} alt=""/>
              </li>
              <li id="modal3" onClick={ () => this.setUrlImage(3) }>
                <img src={CONFIG.MAIN_URL+"img/static/cta-3.png"} alt=""/>
              </li>
            </ul>
          </div>

          <div className="desc">
            <h2 className="desc-title">現状は集まった資金分がクリエーターに支援される<strong>ALL-IN</strong>型の募集に限定していますが、今後は以下の形式の募集形式に対応する機能が付加される予定です。</h2>
            <ul className="list-note">
              <li><span>定期課金型</span>：定期的に一定額の課金が行われる形式</li>
              <li><span>ALL or Nothing型</span>：一定額の金額が集まらなければ、支援者に資金を返金する形式</li>
              <li><span>投資型</span>：支援者のリターンの形式が投資運用スタイルとして募られるもの</li>
            </ul>
          </div>
        </div>
    </div>
    );
  }
}
