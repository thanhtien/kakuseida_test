/* @flow */

import React, { Component } from 'react';
import Rodal from 'rodal';

export default class ModalTowa extends Component {

  onClose() {
    this.props.onClose()
  }

  render() {
    const { visible , imageLink } = this.props;

    var width;
    var height;
    var rate;

    if( window.innerWidth>767 ){
      width =  50;
      height =  67;
      rate = '%';
    }else{
      width =  350;
      height =  218;
      rate = 'px';
    }

    return (
      <div id="cover-modal">
    		<Rodal measure={rate} width={width} height={height} visible={visible} onClose={ () => this.onClose() }>
          <img src={imageLink} alt=""/>
        </Rodal>
    	</div>
    );
  }
}
