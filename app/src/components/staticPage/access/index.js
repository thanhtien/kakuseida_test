import React, { Component } from 'react';
import Breadcrumbs from '../../Breadcrumbs';
import GOOGLE_URL from './GooGleURL';


class index extends Component {
    componentDidMount() {
        document.title = "事業所案内（MAP・アクセス）｜KAKUSEIDA";

    }
    render() {
        const linkBreacrm = [
            { last: true, label: '事業所案内（MAP・アクセス）', link: null }
        ]
        console.log(GOOGLE_URL.URL1);
        return (
	        <div className="access" >
        		<div className="banner-css banner-access">
        	        <div className="wraper">
        	            <p className="title-white">
        	              事業所案内
        	            </p>
        	        </div>
                </div>
                <div className="wraper">
        			<Breadcrumbs linkBreacrm={linkBreacrm}></Breadcrumbs>

        			<div className="map-wrapper map1">
						<iframe title="map1" src={GOOGLE_URL.URL1} allowfullscreen></iframe>
        				<div className="map-info">
							<h3 className="map-name">東京本社</h3>
							<p className="map-address">〒150-0013　東京都渋谷区恵比寿1丁目23</p>
							<p className="map-address">番9号　ヴェルミドール恵比寿 7階</p>
        				</div>
        			</div>

        			<div className="map-wrapper map2">
						<iframe title="map2" src={GOOGLE_URL.URL2} allowfullscreen></iframe>
        				<div className="map-info">
							<h3 className="map-name">大阪支社</h3>
							<p className="map-address">〒556-0011　大阪府浪速区難波中2丁目7</p>
							<p className="map-address">番7号　 ナンバFKビル4階</p>
        				</div>
        			</div>
    			</div>
	      	</div>
        );
    }
}




export default index;
