/* @flow */

import React, { Component } from 'react';
import { Field, reduxForm,reset } from 'redux-form';
import { connect } from 'react-redux';
import * as actions from '../../../actions/common';
import validate from './validate';
import renderFieldInput from '../../common/renderFieldInput';
import renderFieldTextArea from '../../common/renderFieldTextArea';
import Captcha from '../../common/ReCAPTCHA';
import scrollToFirstError from './scrollToFirstError';
import Loading from '../../common/loading';
import Thank from './thank';
import Breadcrumbs from '../../Breadcrumbs';
class Contact extends Component {
  /**
   * [handleFormSubmit Send Email]
   */
  handleFormSubmit(data) {
    this.props.contactForm(data);
  }
  resetStatusContact() {
    this.props.resetStatusContact(false);
  }
  componentDidMount() {
    this.props.resetStatusContact(false);
    document.title = "お問い合わせ｜KAKUSEIDA";
  }

  render() {
    const { handleSubmit , contact_form_status , loading  } = this.props;
    const linkBreacrm = [
      {last:true,label:'お問い合わせ',link:null}
    ]

    if (loading) {
      return(
        <Loading></Loading>
      )
    }

    return (
      <div className="about_box contact">
        <div className="banner-css contact-banner">
          <div className="wraper">
            <p className="title-white">
              お問い合わせ
            </p>
          </div>
        </div>
        <div className="wraper">
          <Breadcrumbs linkBreacrm={linkBreacrm}></Breadcrumbs>
          <p className="contact-information">
            受付時間：土日祝日年末年始を除く平日 <br/>
            ご質問やご意見はお気軽にお問い合わせください。<br/>
          </p>

          {
            !contact_form_status ?
            <div className="form-contact">
              <div className="form">
                <form onSubmit={handleSubmit(this.handleFormSubmit.bind(this))}>
                  <div className="form-field">
                    <p className="form-label">
                      お名前 <span>必須</span>
                    </p>
                    <Field
                      name="user_name"
                      placeholder="山田　太郎"
                      component={renderFieldInput}
                      type="text" />
                    <div className="clear-fix"></div>
                  </div>

                  <div className="form-field">
                    <p className="form-label">
                      返信先メールアドレス <span>必須</span>
                    </p>
                    <Field
                      name="email"
                      placeholder="xxx@gmail.com"
                      component={renderFieldInput}
                      type="text" />
                    <div className="clear-fix"></div>
                  </div>

                  <div className="form-field">
                    <p className="form-label">
                      お問い合わせ件名 <span>必須</span>
                    </p>
                    <Field
                      name="subject"
                      placeholder="例：支援のことについて"
                      component={renderFieldInput}
                      type="text" />
                    <div className="clear-fix"></div>
                  </div>


                  <div className="form-field textarea">
                    <p className="form-label">
                      お問い合わせ内容  <span>必須</span>
                    </p>

                    <Field
                      name="message"
                      placeholder="お問い合わせ内容を記入してください"
                      component={renderFieldTextArea}
                      type="text" />
                    <div className="clear-fix"></div>
                  </div>


                  <div className="form-field">
                    <p className="form-label">CAPTCHA<span>必須</span></p>
                    <Field name='captcharesponse' component={Captcha}/>
                    <div className="clear-fix"></div>
                  </div>

                  <div className="form-field">
                      <p className="form-label fix-sp-mb"></p>
                      <p className="form-submit"><input className="submit-contact" type="submit" value="送信"/></p>
                  </div>
                </form>
              </div>
            </div>:
            <Thank resetStatusContact={() => this.resetStatusContact()}></Thank>

          }





        </div>
    </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    loading: state.common.loading,
    contact_form_status:state.common.contact_form_status
  }
};
const afterSubmit = (result, dispatch) => {
  return (
    dispatch(reset('form-contact'))
  )
};

export default reduxForm({
    form: 'form-contact',
    onSubmitFail: (errors , dispatch) => scrollToFirstError(errors , dispatch),
    validate,
    onSubmitSuccess:afterSubmit

})(connect(mapStateToProps, actions)(Contact));
