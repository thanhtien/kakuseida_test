/* @flow */

import React, { Component } from 'react';

import { Link } from 'react-router-dom';
export default class MyComponent extends Component {
  resetStatusContact() {
    this.props.resetStatusContact();
  }
  render() {
    return (
      <div className="thank_contact">
        <p>お問い合わせが送信されました。 <br/>誠にありがとうございます。</p>
        <Link onClick={() => this.resetStatusContact()} to="/">トップページへ</Link>

      </div>
    );
  }
}
