import React, { Component } from 'react';
import Breadcrumbs from '../../Breadcrumbs';



class index extends Component {
    componentDidMount() {
        document.title = "代表者挨拶｜KAKUSEIDA";

    }
    render() {
        const linkBreacrm = [
          { last: true, label: '代表者挨拶', link: null }
        ]
        return (
        <div className="daihyou-aisatsu" >
			<div className="banner-css banner-daihyou-aisatsu">
		        <div className="wraper">
		            <p className="title-white">
		              代表者挨拶
		            </p>
		        </div>
	        </div>
	        <div className="wraper">
				<Breadcrumbs linkBreacrm={linkBreacrm}></Breadcrumbs>
				<h2 className="headline headline2">石橋泰寛（共同創業者）</h2>
				<h3 className="headline headline3">生命体としてもっているすべての可能性を発現させたい！</h3>
				<p className="text">
					この一念からこの株式会社カクセイは生まれました<br/>
					その可能性を信じて、パッションのあるすべての人々に実現の可能性を提供し、我々もともに成長する<br/>
					パートナーの仲間とともに人類がまだ到達しえなかった体験をしてみよう<br/>
					人生は一度しかない（たぶん？☺）、やれるならやってみよ～ぜ<br/>
					初めてみる自分、こんなはずじゃないほどの圧倒的な自分はきっと絶景だ<br/>
					遭ってみませんか、初めて逢う自分に<br/>
					半端な生き方はもうやめよう、そう、覚醒しよう
				</p>
				<p className="signature">石橋泰寛<span>共同創業者</span></p>

				<h2 className="headline headline2">平山智浩（共同創業者/代表取締役）</h2>
				<h3 className="headline headline3">「より多くの人々と喜びを分かち合う“共通の志”を実現するために」</h3>
				<p className="text">
					カクセイは、不動産にかかわるサービスを通じて、顧客の豊かな生活づくりを支援し、<br/>
					新たな社会や文化の構築に貢献することから始まりました<br/>
					社会全体を見渡すと、ブロックチェーンやAIの技術革新、またシェアリングエコノミーの成長など、<br/>
					多くの先進的な技術やサービスが世に出てきています<br/>
					これからも次々にそれらが生みだされ、私たちの暮らしを革新的に変えていくことでしょう<br/>
					カクセイは、あらゆるテクノロジーを駆使して人々の生活・ビジネスには必要な不動産の最大価値化を実現します<br/>
					あらゆる空間において、我々ははスペース・イノベーション・カンパニーとして世の中を牽引していきます
				</p>
				<p className="signature">平山智浩<span>共同創業者/代表取締役</span></p>
	        </div>
      	</div>
        );
    }
}




export default index;
