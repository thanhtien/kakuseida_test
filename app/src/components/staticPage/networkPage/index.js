/* @flow */

import React, { Component } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import {faWifi } from '@fortawesome/free-solid-svg-icons';

export default class MyComponent extends Component {
  render() {
    return (
      <div className="Offline">
        <div className="cover-offline">
          <div>
            <div className="wifi">
              <FontAwesomeIcon  icon={faWifi} />
            </div>
            <p className="wifi-text">ネットワークの接続に関する問題がありますので、ご確認ください。</p>
          </div>
        </div>
      </div>
    );
  }
}
