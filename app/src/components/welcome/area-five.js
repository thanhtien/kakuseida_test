/* @flow */

import React, { Component } from 'react';
import CardProject from '../cardProject';

import {  Link } from 'react-router-dom';
var CONFIG = require('../../config/common');

if (process.env.NODE_ENV === "development") {
  CONFIG = CONFIG.CONFIG.DEV;
}else {
  CONFIG = CONFIG.CONFIG.PRODUCT;
}
export default class AreaFive extends Component {


  render() {
    const {ProjectEnoughExpired} = this.props;
    return (
      <div className="area area5">
          <div className="wraper">
              <h3 className="titleArea">
                <span className="icon"><img src={CONFIG.MAIN_URL+"img/index/ico-next.png"} alt=""/></span>
                <span className="title-area">終了間際のプロジェクト</span>
              </h3>
              <div className="row">
                {
                  ProjectEnoughExpired.map( (item , i) => {
                    return(
                      <CardProject key={i} item={item}/>
                    )
                  })
                }
              </div>
              {/* ROW */}
              <p className="linkAll"><Link to={"/project-enough-expired-list/page=1"}>もっと見る</Link></p>
          </div>
          {/* WRAPER */}
      </div>
    );
  }
}
