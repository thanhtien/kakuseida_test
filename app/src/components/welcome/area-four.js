/* @flow */

import React, { Component } from 'react';


import CardProject from '../cardProject';
var CONFIG = require('../../config/common');

if (process.env.NODE_ENV === "development") {
  CONFIG = CONFIG.CONFIG.DEV;
}else {
  CONFIG = CONFIG.CONFIG.PRODUCT;
}
export default class AreaFour extends Component {



  render() {
    const { recommentProject } = this.props;
    return (
      <div className="area area4">
          <div className="wraper">
            <h3 className="titleArea">
              <span className="icon">
                <img src={CONFIG.MAIN_URL+"img/index/ico-star.png"} alt=""/>
              </span>
              <span className="title-area">オススメのプロジェクト</span>
            </h3>
              <div className="row">
                {
                  recommentProject.map( (item , i) => {
                    return(
                      <CardProject key={i} item={item}/>
                    )
                  })
                }
              </div>
              {/* ROW */}
          </div>
          {/* WRAPER */}
      </div>
    );
  }
}
