/* @flow */
import React, { Component } from 'react';
import CardProject from '../cardProject';

import {  Link } from 'react-router-dom';
var CONFIG = require('../../config/common');

if (process.env.NODE_ENV === "development") {
  CONFIG = CONFIG.CONFIG.DEV;
}else {
  CONFIG = CONFIG.CONFIG.PRODUCT;
}
export default class AreaOne extends Component {

  render() {
    const { ProjectNewest,page } = this.props;
    var PageLink = Number(page) + 1;
    return (
      <div className="area area1">
          <div className="wraper">
              <h3 className="titleArea">
                <span className="icon"><img src={CONFIG.MAIN_URL+"img/index/ico-bell.png"} alt=""/></span>
                <span className="title-area">新着のプロジェクト</span>
              </h3>
              <div className="row">
                  {
                    ProjectNewest.map( (item , i) => {
                      return(
                        <CardProject key={i} item={item}/>
                      )
                    })
                  }

              </div>
              {/* ROW */}
              <p className="linkAll">
                <Link to={"/new-project-list/page="+PageLink}>もっと見る</Link>
              </p>
          </div>
          {/* WRAPER */}
      </div>
    );
  }
}
