/* @flow */
import React, { Component } from 'react';

import CardProject from '../cardProject';
import {  Link } from 'react-router-dom';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import {faHistory} from '@fortawesome/free-solid-svg-icons';

var CONFIG = require('../../config/common');

if (process.env.NODE_ENV === "development") {
  CONFIG = CONFIG.CONFIG.DEV;
}else {
  CONFIG = CONFIG.CONFIG.PRODUCT;
}

export default class AreaSeven extends Component {


  render() {
    const {ProjectExpired,page} = this.props;
    var PageLink = Number(page) + 1;

    return (
      <div className="area area7">
        <div className="wraper">
            <h3 className="titleArea">
              <span className="icon">
                <span className="icon"><img src={CONFIG.MAIN_URL+"img/index/ico-clock.png"} alt=""/></span>
              </span>
              <span className="title-area">過去のプロジェクト</span>
            </h3>
            <div className="row">
              {
                ProjectExpired.map( (item , i) => {
                  return(
                    <CardProject key={i} item={item}/>
                  )
                })
              }
            </div>
            {/* ROW */}
            <p className="linkAll"><Link to={"/project-expired-list/page="+PageLink}>もっと見る</Link></p>
        </div>
        {/* WRAPER */}
      </div>
    );
  }
}
