import React, { Component } from 'react';

import CardProject from '../cardProject';
import {  Link } from 'react-router-dom';
var CONFIG = require('../../config/common');

if (process.env.NODE_ENV === "development") {
  CONFIG = CONFIG.CONFIG.DEV;
}else {
  CONFIG = CONFIG.CONFIG.PRODUCT;
}
export default class AreaSix extends Component {


  render() {
    const {ProjectEnoughtGoalAmount,page} = this.props;
    var pageLink = Number(page) + 1 ;
    return (
      <div className="area area6">
        <div className="wraper">
            <h3 className="titleArea">
              <span className="icon"><img src={CONFIG.MAIN_URL+"img/index/ico-clock.png"} alt=""/></span>
              <span className="title-area">あと少しで目標100％になるプロジェクト</span>
            </h3>
            <div className="row">
              {
                ProjectEnoughtGoalAmount.map( (item , i) => {
                  return(
                    <CardProject key={i} item={item}/>
                  )
                })
              }
           </div>
             {/* ROW */}
             <p className="linkAll"><Link to={"/project-enough-goal-amount-list/page="+pageLink}>もっと見る</Link></p>
        </div>
        {/* WRAPER */}
      </div>
    );
  }
}
