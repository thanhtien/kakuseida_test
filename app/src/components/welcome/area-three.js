/* @flow */

import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { LazyLoadImage } from 'react-lazy-load-image-component';
import {ParseUrl} from '../common/parseImage'

var CONFIG = require('../../config/common');

if (process.env.NODE_ENV === "development") {
  CONFIG = CONFIG.CONFIG.DEV;
}else {
  CONFIG = CONFIG.CONFIG.PRODUCT;
}
export default class AreaThree extends Component {

  _renderTemplate(item,i) {

    return(
      <div key={i} className="col">
          <p className="img">
            <Link to={'/news-list/new-detail/'+item.id} >
                <LazyLoadImage
                alt={"thumbnail"}
                effect="blur"
                /*src={ParseUrl('medium',item.thumnail)} />*/
                src={item.thumnail} />
            </Link>
          </p>

          <div className="boxCol">
              <p className="postTitle">
                <Link to={'/news-list/new-detail/'+item.id} >{item.title}</Link>
              </p>
          </div>
      </div>
    )
  }
  // <Link to={'new-detail/'+item.id} ></Link>
  render() {
    const {News} = this.props;
    return (
      <div className="area area3">
          <div className="wraper">
              <h3 className="titleArea">
                <span className="icon"><img src={CONFIG.MAIN_URL+"img/index/ico-anoucer.png"} alt=""/></span>
                <span className="title-area">最新の活動・ニュース</span>
              </h3>
              <div className="row">
                {
                  News.map( (item , i) => {
                    return(
                      this._renderTemplate(item , i)
                    )
                  })
                }
              </div>
              {/* ROW */}
              <p className="btn"><a href="/news-list/page=1">ニュース一覧へ</a></p>
          </div>
          {/* WRAPER */}
      </div>
    );
  }
}
