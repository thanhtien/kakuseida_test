/* @flow */

import React, { Component } from 'react';
import NumberFormat from 'react-number-format';


import Slider from "react-slick";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faTag , faUser } from '@fortawesome/free-solid-svg-icons';
import { Link } from 'react-router-dom';
import LinesEllipsis from 'react-lines-ellipsis'
import { LazyLoadImage } from 'react-lazy-load-image-component';

var CONFIG = require('../../config/common');

if (process.env.NODE_ENV === "development") {
  CONFIG = CONFIG.CONFIG.DEV;
}else {
  CONFIG = CONFIG.CONFIG.PRODUCT;
}
export default class AreaTwo extends Component {
  _renderProjectEndDateTitle(data) {


    if (data) {

      if (data.format_collection_end_date.status) {
        return '終了';
      }
      if (data.format_collection_end_date.date > 0) {
        return data.format_collection_end_date.date + "日";
      }else {
        return data.format_collection_end_date.hour+"時"+data.format_collection_end_date.minutes+"分";
      }
    }
  }

  _renderProjectEndDate(data) {


    if (data) {

      if (data.format_collection_end_date.status) {
        return '終了';
      }
      if (data.format_collection_end_date.date > 0) {
        return <span>{data.format_collection_end_date.date} 日</span>;
      }else {
        return <span>{data.format_collection_end_date.hour}時{data.format_collection_end_date.minutes}分</span>;
      }
    }
  }

  _capitalizeFirstLetter(string) {
    if (string) {
      return string.charAt(0).toUpperCase() + string.slice(1);
    }
  }

  componentDidMount() {
    window.addEventListener("resize", this.updateDimensions.bind(this));
    if (this.img) {
      this.setState({
        heightImage:((this.img.offsetWidth*2)/3).toString()
      })
    }
  }

  updateDimensions() {
    if (this.img) {
      this.setState({
        heightImage:((this.img.offsetWidth*2)/3).toString()
      })
    }
  }


  _renderTemplate(item,i) {
    const numberWidth = Math.round((Number(item.collected_amount)/Number(item.goal_amount))*100);
    return(
      <div key={i} className="col">
        <p style = {{height:this.img !== undefined ? this.state.heightImage+'px' : '0px' , overflow:'hidden'}} ref={ (img) => this.img = img} className="img">
          <Link to={'/project-detail/'+item.id}>
            <LazyLoadImage
              alt={"thumbnail"}
              effect="blur"
              src={item.thumbnail} />
          </Link>
        </p>
        <div className="boxCol">

            <div title={item.project_name} className="postTitle">
              <Link to={'/project-detail/'+item.id}>
                <LinesEllipsis
                  text={item.project_name}
                  maxLine='2'
                  ellipsis='...'
                  trimRight
                  basedOn='letters'
                />

              </Link>
            </div>


            <p  className="rPeople">
                <span title={item.category.name} className="note"><Link to={'/categories/'+item.category.slug+'/page=1'}>
                <FontAwesomeIcon icon={faTag} />{' '+item.category.name}</Link></span>

                {
                  item.user ?
                  <span title={item.user.name} className="people">
                    <span className="name-user" style={{cursor:'pointer'}}>
                      <FontAwesomeIcon icon={faUser} />
                    {' '+ this._capitalizeFirstLetter(item.user.name) }</span></span> :null
                }
            </p>



            <div className="meter">
                <div className="countNum" style={numberWidth >=100 ? {width:"100%"} : {width:numberWidth+"%"}}>
                  <span className="numTr">{numberWidth}%</span>
                </div>
            </div>
            <div className="rowBot">
                <div className="rowBot-l">
                    <p title={item.collected_amount} className="price tooltip">

                        <span className="small-title">現在</span>
                        <span className="number">
                          <span >
                            <NumberFormat value={item.collected_amount} displayType={'text'} thousandSeparator={true} suffix={'円'} />
                          </span>
                        </span>

                    </p>
                    <p title={item.now_count.now_count} className="pNumber">
                        <span className="small-title">支援者数</span>
                        <span className="number">{item.now_count.now_count}人<span>
                    </span></span></p>
                </div>
                <div className="rowBot-r">
                    <p title={this._renderProjectEndDateTitle(item)}  className="date">

                      <span className="small-title">残り</span>
                      <span className="number">{this._renderProjectEndDate(item)}<span>
                      </span></span>
                    </p>
                </div>
            </div>
        </div>
        {/* BOXCOL */}
      </div>
    );
  }
  render() {
    const {faProject} = this.props;
    var settings = {
      infinite: false,
      speed: 500,
      slidesToShow: 4,
      slidesToScroll: 4,
      variableWidth: false,
      responsive:[
        {
          breakpoint: 768,
          settings: {
            arrows: false,
            slidesToShow: 2,
            slidesToScroll: 2,
            dots: true,
            customPaging: function(i) {
            return (
              <span>{''}</span>
            );
          }
          }
        },
      ]
    };

    return (
      <div id="area2" className="area area2">

          <div className="wraper">
          <h3 className="titleArea">
            <span className="icon"><img src={CONFIG.MAIN_URL+"img/index/ico-crown.png"} alt=""/></span>
            <span className="title-area">人気のプロジェクト</span>
          </h3>
              <div className="row">
                <Slider {...settings}>
                  {
                    faProject?
                    faProject.map( (item , i) => {
                      return(
                        this._renderTemplate(item , i)
                      )
                    }):null

                  }
                </Slider>
              </div>
              {/* ROW */}
          </div>
          {/* WRAPER */}
      </div>
    );
  }
}
