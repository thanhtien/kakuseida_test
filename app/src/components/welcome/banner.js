// import React, { Component } from 'react';
// import NumberFormat from 'react-number-format';
// import { LazyLoadImage } from 'react-lazy-load-image-component';
// import Slider from "react-slick";
// var CONFIG = require('../../config/common');
//
// if (process.env.NODE_ENV === "development") {
//   CONFIG = CONFIG.CONFIG.DEV;
// }else {
//   CONFIG = CONFIG.CONFIG.PRODUCT;
// }
// export default class Banner extends Component {
//   //Constructor
//   constructor(props) {
//     super(props);
//     this.state = {
//       value: 0,
//     };
//   }
//
//   _renderProjectEndDate(data) {
//     if (data) {
//
//       if (data.format_collection_end_date.status) {
//         return '終了';
//       }
//       if (data.format_collection_end_date.date > 0) {
//         return <span>{data.format_collection_end_date.date} 日</span>;
//       }else {
//         return <span>{data.format_collection_end_date.hour}時{data.format_collection_end_date.minutes}分</span>;
//       }
//     }
//   }
//
//   SetStyle(numberWidth){
//     if (numberWidth === 0) {
//       return {width:"0%"};
//     }
//     if (numberWidth>0 && numberWidth < 100) {
//       return {width:numberWidth+"%" , minWidth:"2%"};
//     }else {
//       return {width:"100%"};
//     }
//   }
//
//   //Render Item
//   _myRenderItem(item,i) {
//     const numberWidth = Math.round((Number(item.collected_amount)/Number(item.goal_amount))*100);
//
//     return(
//       <div key={i}>
//         <div   className="wraper">
//           <div className="row" style={{position: 'relative'}}>
//             {
//               item.project_type === '1' ?
//               <div className="ribbon ribbon-top-right"><span>定期課金のプロジェクト</span></div>
//               : null
//             }
//              <p className="img">
//               <a href={'project-detail/'+item.id}>
//                <LazyLoadImage
//                  alt={"thumbnail"}
//                  effect="blur"
//                  src={item.thumbnail} />
//                </a>
//              </p>
//              <div className="colRight">
//                 <div className="centerRight">
//                    <p className="title"><a href={'project-detail/'+item.id}>{item.project_name}</a></p>
//                    <div className="rowCount">
//                      {
//                        item.project_type !== '1'?
//                        <p className="date inlBlock">
//                          <span className="fix-sp-padding">残り</span>
//                          <span className="spDate">
//                            {
//                              this._renderProjectEndDate(item)
//
//
//                            } </span></p>  :null
//                      }
//
//                       <div className="price inlBlock">
//                          <p className="textTop">
//                             集まっている金額
//                             <span className="spPrice">
//                                <NumberFormat value={item.collected_amount} displayType={'text'} thousandSeparator={true} suffix={'円'} />
//                             </span>
//                          </p>
//                          {
//                            item.project_type !== '1' ?
//                            <div className="pTram">
//                               <p  className="cover-yellow">
//                                  <span
//                                     style={this.SetStyle(numberWidth)}
//                                  className="yellow">
//                                  </span>
//                               </p>
//                               <p className="number">
//                                  {numberWidth}%
//                               </p>
//                            </div> :null
//                          }
//                       </div>
//                    </div>
//                    <p className="btnView"><a href={'project-detail/'+item.id}>{'VIEW'}</a></p>
//                 </div>
//              </div>
//              {/* .COLRIGHT */}
//           </div>
//          </div>
//       </div>
//     )
//   }
//   //Render
//   render() {
//     const {bannerData} = this.props;
//     //Setting Slick
//     const settings = {
//       customPaging: function(i) {
//         return (
//           <span className="thumnail-img">
//             <img
//               alt={"thumbnail"}
//               src={bannerData[i].thumbnail} />
//           </span>
//         );
//       },
//       dots: true,
//       dotsClass: "slick-dots slick-thumb listImages",
//       infinite: true,
//       speed: 500,
//       slidesToShow: 1,
//       slidesToScroll: 1,
//       autoplay: false,
//       autoplaySpeed: 3000,
//       arrows: false,
//       responsive: [
//       {
//         breakpoint: 768,
//         settings: {
//           dots: true,
//           customPaging: function(i) {
//             return (
//               <span>{''}</span>
//             );
//           }
//         }
//       },
//       ]
//     };
//
//     return (
//       <div className="baner-here"  >
//         <div className="banner" style={ bannerData.length === 0 ? {paddingBottom:"60px"} : null } >
//
//           {
//             bannerData.length > 0 ?
//             <Slider {...settings}>
//
//               {
//                 bannerData.map( (item , i) => {
//                   return(
//                     this._myRenderItem(item,i)
//                   )
//                 })
//               }
//             </Slider>: <div className="wraper"><p className="img-empty"><img src={CONFIG.MAIN_URL+`img/index/empty-banner.jpg`} alt="empty"/></p></div>
//           }
//         </div>
//       </div>
//     );
//   }
//
// }
//
//
//
/* @flow */

import React, { Component } from 'react';
import Loading from '../common/loadingScroll';
var CONFIG = require('../../config/common');

if (process.env.NODE_ENV === "development") {
  CONFIG = CONFIG.CONFIG.DEV;
}else {
  CONFIG = CONFIG.CONFIG.PRODUCT;
}

export default class Banner extends Component {


  constructor(props) {
    super(props);
    this.state = {
      loading:true
    };
  }

  componentDidMount() {
    var mobilevideo = document.getElementsByTagName("video")[0];
    mobilevideo.setAttribute("playsInline", "");
    mobilevideo.setAttribute("muted", "");
    mobilevideo.addEventListener("playing", () => {
      this.setState({
        loading:false
      })
    });
  }

  render() {
    const {loading} = this.state;
    return (
      <div className="baner-here">
        {
          loading ?
          <div className="loading-style">
            <Loading ></Loading>
          </div> : null
        }


        <div className="banner">
          <div className="video-here-banner" >
            <video poster={`${CONFIG.MAIN_URL}video/poster.png`}  id="video-background" autoPlay muted playsInline loop>
              <source src={`${CONFIG.MAIN_URL}video/banner.mp4`} type="video/mp4"/>
              Your browser does not support the video tag.
            </video>
          </div>
        </div>
      </div>
    );

  }
}
// <div className="banner" style={ bannerData.length === 0 ? {paddingBottom:"60px"} : null } >
