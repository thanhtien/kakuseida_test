/* @flow */
import React, { Component } from 'react';

import CardProject from '../cardProject';
import {  Link } from 'react-router-dom';
var CONFIG = require('../../config/common');

if (process.env.NODE_ENV === "development") {
  CONFIG = CONFIG.CONFIG.DEV;
}else {
  CONFIG = CONFIG.CONFIG.PRODUCT;
}



export default class FanClub extends Component {

  render() {
    const {fanclub} = this.props;

    return (
      <div className="area area7">
        <div className="wraper">
            <h3 className="titleArea">
              <span className="icon"><img src={CONFIG.MAIN_URL+"img/index/ico-cross.png"} alt=""/></span>
              <span className="title-area">定期課金のプロジェクト</span>
            </h3>
            <div className="row">
              {
                fanclub.map( (item , i) => {
                  return(
                    <CardProject key={i} item={item}/>
                  )
                })
              }
            </div>
            {/* ROW */}
            <p className="linkAll"><Link to={"/fanclub-list/page=1"}>もっと見る</Link></p>
        </div>
        {/* WRAPER */}
      </div>
    );
  }
}
