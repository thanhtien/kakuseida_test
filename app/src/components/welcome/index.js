
import React, { Component } from 'react';
import Banner from './banner';
import { connect } from 'react-redux';
import * as actions from '../../actions/TopPage';
import Loading from '../common/loading';
//Import Area
import AreaOne from './area-one';
import AreaTwo from './area-two';
import AreaThree from './area-three';
import AreaFour from './area-four';
import AreaFive from './area-five';
import AreaSix from './area-six';
import AreaSeven from './area-seven';
import FanClub from './fanclub';
import Project100 from './Project100';
import Fade from 'react-reveal/Fade';
import LazyLoad from 'react-lazyload';
const IE = localStorage.getItem('ie');

class welcome extends Component {

  componentDidMount() {
    document.title = "カクセイだクラウドファンディング｜KAKUSEIDA";
    //Call API
    const {data} = this.props;

    if (data.length === 0) {
      this.props.fetchProject();
    }

  }




  /**
   * [_renderFixAreaOneIE Fix IE react reaval not working]
   * @return {[type]}                 [disable IE]
   */
  _renderFixAreaOneIE(ProjectNewest,newsProjectPage,mobileStatus,mobileApear) {

    if (IE !== 'false') {
      return <Fade  spy={mobileStatus} bottom><AreaOne ProjectNewest={ProjectNewest} page={newsProjectPage}/></Fade>
    }else {
      return <Fade appear={mobileApear} spy={mobileStatus} bottom><AreaOne ProjectNewest={ProjectNewest} page={newsProjectPage}/></Fade>
    }
  }



  render() {

    const {data,newsProjectPage,ProjectGoalPage,ProjectExpriedPage,Project100dPage} = this.props;

    if (data.length === 0) {
      return( <Loading></Loading> );
    }

    const bannerData= data[0];
    const News= data[1];
    const faProject= data[2];
    const recommentProject= data[3];
    const ProjectNewest = data[4];
    const ProjectEnoughtGoalAmount = data[6];
    const ProjectEnoughExpired = data[7];
    const ProjectExpired = data[5];
    const fanclub = data[8];
    const project100 = data[9];
    var mobileStatus = false;
    var mobileApear = false;

    if (window.innerWidth <= 768) {
      mobileStatus=true;
      mobileApear=false;
    }else {
      mobileStatus=true;
      mobileApear=false;
    }
    return (
      <div>
        <Banner bannerData={bannerData} />
        <div>
          {/* AREA1 */}
          <LazyLoad height={200}>
            {
              ProjectNewest.length !== 0 ?
              this._renderFixAreaOneIE(ProjectNewest,newsProjectPage,mobileStatus,mobileApear)
              : null
            }

          </LazyLoad>
          {/* AREA1 */}
          {/* AREA2 */}
          <LazyLoad height={200}>
            {
              faProject.length !== 0 ?
              <Fade bottom><AreaTwo faProject={faProject}/></Fade>:
              null
            }
          </LazyLoad>
          {/* AREA2 */}

          {/* AREA3 */}
          <LazyLoad height={200}>
            {
              News.length !== 0 ?
              <Fade bottom><AreaThree News={News} /></Fade> :
              null
            }
          </LazyLoad>
          {/* AREA3 */}

          <LazyLoad height={200}>
            {
              recommentProject.length !== 0 ?
              <Fade bottom><AreaFour recommentProject={recommentProject}/></Fade> :
              null
            }
          </LazyLoad>



          {/* AREA4 */}

          <LazyLoad height={200}>
            {
              ProjectEnoughExpired.length !== 0 ?
              <Fade bottom><AreaFive  ProjectEnoughExpired={ProjectEnoughExpired} /></Fade> :
              null
            }
          </LazyLoad>

          {/* AREA5 */}

          <LazyLoad height={200}>
            {
              ProjectEnoughtGoalAmount.length !== 0 ?
              <Fade bottom><AreaSix page={ProjectGoalPage} ProjectEnoughtGoalAmount={ProjectEnoughtGoalAmount}/></Fade>:
              null
            }
          </LazyLoad>

          <LazyLoad height={200}>
            {
              fanclub.length !== 0 ?
              <Fade bottom><FanClub fanclub={fanclub}/></Fade>:
              null
            }
          </LazyLoad>
          {/* FanClub */}


          {/* AREA6 */}

          <LazyLoad height={200}>
            {
              ProjectExpired.length !== 0 ?
              <Fade bottom><AreaSeven page={ProjectExpriedPage} ProjectExpired={ProjectExpired} /></Fade> :
              null
            }
          </LazyLoad>

          {/* AREA7 */}
          {
            project100.length !== 0 ?
            <Project100 page={Project100dPage} data={project100} />
            : null
          }

          {
            /*Project 100*/
          }
        </div>
      </div>
    );
  }


}

const mapStateToProps = (state) => {
  return {
    data: state.topPage.data,
    newsProjectPage: state.topPage.newsProject.page,
    ProjectEnoughPage: state.topPage.newsProject.page,
    ProjectGoalPage: state.topPage.ProjectGoal.page,
    ProjectExpriedPage: state.topPage.ProjectExpried.page,
    Project100dPage: state.topPage.Project100.page
  }
}

export default connect(mapStateToProps, actions)(welcome);
