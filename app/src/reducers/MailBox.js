import {
    LIST_PROJECT_MAIL,
    OWNER_PROJECT_PROFILE,
    RECIVE_EMAIL,
    SENDED_EMAIL,
    SEND_EMAIL_BOX,
    STATUS_MAILBOX,
    CHANGE_STATUS_EMAIL
} from '../actions/types';
export const reducer = (state = { EmailList:{data:null,page:0} , reciveEmail:{data:null,page:0} , SendedEmail:{data:null,page:0},status_send_mail:false , message:'' }, action) => {
  switch (action.type) {
    case CHANGE_STATUS_EMAIL:
    console.log(state.reciveEmail.data.data);
    return state = {
      ...state,
      reciveEmail:{
        ...state.reciveEmail,
        data:{
          ...state.reciveEmail.data,
          data:state.reciveEmail.data.data.map( (item,i) => {
            if (item.id === action.payload) {
              item.status = '1';
            }
            return item;
          })
        }
      }

    }
    case LIST_PROJECT_MAIL:
      return state = { ...state, EmailList: {data:action.payload,page:action.page} }
    case RECIVE_EMAIL:
      return state = { ...state, reciveEmail: {data:action.payload,page:action.page} }
    case SENDED_EMAIL:
      return state = { ...state, SendedEmail: {data:action.payload,page:action.page}}
    case SEND_EMAIL_BOX:
      return state = {...state, SendedEmail:{data:action.payload,page:0}, status_send_mail:true,message:action.message }
    case OWNER_PROJECT_PROFILE:
      return state = { ...state, ownerData: action.payload }
    case STATUS_MAILBOX:
      return state = {...state,status_send_mail:false,message:''}

    default:
      return state
  }
}
