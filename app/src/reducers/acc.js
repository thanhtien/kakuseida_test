import {
    LOAD
} from '../actions/types';
export const reducer = (state = {}, action) => {
  switch (action.type) {
    case LOAD:
      return state = { ...state, data: action.payload }
    default:
      return state
  }
}
