import {
    LOAD_BACKER,
    DONATE_OK,
    SETTING_VISA_OK_FIRST,
    DISABLE_DONATE
} from '../actions/types';
export const reducer = (state = { backerData:null , disable:false , profileUpdate:null , typePay:'stripe'}, action) => {
  switch (action.type) {
    case LOAD_BACKER:
      return state = { ...state, backerData: action.payload }
    case DONATE_OK:
      return state = { ...state, disable: true }
    case DISABLE_DONATE:
      return state = { ...state, disable: false }
    case SETTING_VISA_OK_FIRST:
      return state = { ...state, profileUpdate:action.payload }
    case 'CHANGE_TYPE_DONATE':
      return state = { ...state, typePay:action.payload }
    default:
      return state
  }
}
