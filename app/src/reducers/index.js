import { combineReducers } from 'redux';
import { reducer as authReducer } from './auth';
import { reducer as topPage } from './topPage';
import { reducer as formReducer } from 'redux-form';
import { reducer as common } from './common';
import { reducer as uploadImage } from './upload';
import { reducer as ProjectDetail } from './projectDetail';
import { reducer as Donate } from './donate';
import { reducer as EditPublicProject } from './EditPublicProject';
import { reducer as notificationsList } from './notification';
import {reducer as notifications} from 'react-notification-system-redux';
import {reducer as wishlist} from './wishlist';
import {reducer as MailBox} from './MailBox';

const rootReducer = combineReducers({
    form: formReducer,
    auth: authReducer,
    topPage: topPage,
    common:common,
    uploadImage,
    ProjectDetail,
    Donate,
    notifications,
    notificationsList,
    EditPublicProject,
    wishlist,
    MailBox

});
export default rootReducer;
