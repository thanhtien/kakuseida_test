import {
  LIST_WISHLIST_PAGE,
  WISH_LIST,
  LEAVE_WISH_LIST_PAGE

} from '../actions/types';
export const reducer = (state = { wishlistPage:{data:null,pageLoad:0} }, action) => {
  switch (action.type) {
    case LIST_WISHLIST_PAGE:
      return state = { ...state, wishlistPage: {data:action.payload , pageLoad:action.page}  }
    case WISH_LIST:
      return {
        ...state,
        wishlistHeader:action.payload
      }
    case LEAVE_WISH_LIST_PAGE:

    return {
      ...state,
      wishlistPage:{
        ...state.wishlistPage,
        data:{
          ...state.wishlistPage.data,
          data:action.payload.data,
          pageLoad:Number(action.page) - 1,
          page_count:action.payload.page_count,

        }
      }
    }

    default:
      return state
  }
}
