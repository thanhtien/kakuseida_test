
module.exports = function (socket,io) {

    socket.on( 'comment_project', function( data ) {
      var dataBack = JSON.parse(data);
      io.sockets.emit( dataBack.room, {
        data: dataBack
      });
    });

    socket.on( 'annouce_project', function( data ) {
      var dataBack = JSON.parse(data);
      io.sockets.emit( dataBack.room, {
        data: dataBack
      });
    });



};
